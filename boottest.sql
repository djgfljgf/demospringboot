/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : localhost:3306
 Source Schema         : boottest

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : 65001

 Date: 01/08/2019 10:14:48
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for stdchk_category
-- ----------------------------
DROP TABLE IF EXISTS `stdchk_category`;
CREATE TABLE `stdchk_category`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` int(11) DEFAULT NULL COMMENT '序号，用于排序',
  `parent_id` int(11) DEFAULT NULL COMMENT '父分类',
  `serialno` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '缓存编号，例如 \'1.1.1\'，考虑可根据各级分类的index自动生成',
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '分类的名称',
  `remark` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '备注',
  `deleted` tinyint(1) DEFAULT 0 COMMENT '1:表示此记录处于已删除状态',
  `weight` float DEFAULT 0 COMMENT '权重，统计时使用，一个[0,1]之间的浮点数',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_stdchk_category_stdchk_category1_idx`(`parent_id`) USING BTREE,
  CONSTRAINT `stdchk_category_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `stdchk_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 337 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '标准化检查分类' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of stdchk_category
-- ----------------------------
INSERT INTO `stdchk_category` VALUES (1, 1, NULL, '1', '安全风险分级管控', '', 0, 0.1);
INSERT INTO `stdchk_category` VALUES (2, 1, 1, '1.1', '工作机制', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (3, 1, 2, '1.1.1', '职责分工', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (4, 2, 2, '1.1.2', '制度建设', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (5, 2, 1, '1.2', '安全风险辨识评估', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (6, 1, 5, '1.2.1', '年度辨识评估', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (7, 2, 5, '1.2.2', '专项辨识评估', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (8, 3, 1, '1.3', '安全风险管控', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (9, 1, 8, '1.3.1', '管控措施', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (10, 2, 8, '1.3.2', '定期检查', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (11, 3, 8, '1.3.3', '现场检查', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (12, 4, 8, '1.3.4', '公告警示', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (13, 4, 1, '1.4', '保障措施', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (14, 1, 13, '1.4.1', '信息管理', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (15, 2, 13, '1.4.2', '教育培训', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (16, 2, NULL, '2', '事故隐患排查治理', '', 0, 0.1);
INSERT INTO `stdchk_category` VALUES (17, 1, 16, '2.1', '工作机制', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (18, 1, 17, '2.1.1', '职责分工', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (19, 2, 17, '2.1.2', '分级管理', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (20, 2, 16, '2.2', '事故隐患排查', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (21, 1, 20, '2.2.1', '基础工作', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (22, 2, 20, '2.2.2', '周期范围', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (23, 3, 20, '2.2.3', '登记上报', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (24, 3, 16, '2.3', '事故隐患治理', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (25, 1, 24, '2.3.1', '分级治理', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (26, 2, 24, '2.3.2', '安全措施', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (27, 4, 16, '2.4', '监督管理', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (28, 1, 27, '2.4.1', '治理督办', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (29, 2, 27, '2.4.2', '验收销号', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (30, 3, 27, '2.4.3', '公示监督', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (31, 5, 16, '2.5', '保障措施', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (32, 1, 31, '2.5.1', '信息管理', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (33, 2, 31, '2.5.2', '改进完善', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (34, 3, 31, '2.5.3', '资金保障', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (35, 4, 31, '2.5.4', '专项培训', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (36, 5, 31, '2.5.5', '考核管理', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (37, 3, NULL, '3', '通风', '1', 0, 0.16);
INSERT INTO `stdchk_category` VALUES (38, 1, 37, '3.1', '通风系统', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (39, 1, 38, '3.1.1', '系统管理', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (40, 2, 38, '3.1.2', '风量配置', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (41, 2, 37, '3.2', '局部通风', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (42, 1, 41, '3.2.1', '装备措施', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (43, 2, 41, '3.2.2', '风筒敷设', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (44, 3, 37, '3.3', '通风设施', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (45, 1, 44, '3.3.1', '设施管理', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (46, 2, 44, '3.3.2', '密闭', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (47, 3, 44, '3.3.3', '风门风窗', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (48, 4, 44, '3.3.4', '风桥', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (49, 4, 37, '3.4', '瓦斯管理', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (50, 1, 49, '3.4.1', '鉴定及措施', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (51, 2, 49, '3.4.2', '瓦斯检查', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (52, 3, 49, '3.4.3', '现场管理\n\n', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (53, 5, 37, '3.5', '突出防治', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (54, 1, 53, '3.5.1', '突出管理', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (55, 2, 53, '3.5.2', '设备设施', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (56, 6, 37, '3.6', '瓦斯抽采', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (57, 1, 56, '3.6.1', '抽采系统', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (58, 2, 56, '3.6.2', '检查与管理', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (59, 7, 37, '3.7', '安全监控', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (60, 1, 59, '3.7.1', '装备设置', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (61, 2, 59, '3.7.2', '检测试验', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (62, 3, 59, '3.7.3', '监控设备', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (63, 4, 59, '3.7.4', '资料管理', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (64, 8, 37, '3.8', '防灭火', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (65, 1, 64, '3.8.1', '防治措施', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (66, 2, 64, '3.8.2', '设施设备', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (67, 3, 64, '3.8.3', '控制指标', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (68, 4, 64, '3.8.4', '封闭时限', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (69, 9, 37, '3.9', '粉尘防治', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (70, 1, 69, '3.9.1', '鉴定及措施', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (71, 2, 69, '3.9.2', '设备设施', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (72, 3, 69, '3.9.3', '防除尘措施', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (73, 10, 37, '3.10', '井下爆破', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (74, 1, 73, '3.10.1', '物品管理', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (75, 2, 73, '3.10.2', '爆破管理\n', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (76, 11, 37, '3.11', '基础管理', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (77, 1, 76, '3.11.1', '组织保障', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (78, 2, 76, '3.11.2', '工作制度', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (79, 3, 76, '3.11.3', '资料管理', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (80, 4, 76, '3.11.4', '仪器仪表', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (81, 5, 76, '3.11.5', '岗位规范', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (82, 4, NULL, '4', '地质灾害防治与测量', '2', 0, 0.11);
INSERT INTO `stdchk_category` VALUES (83, 1, 82, '4.1', '规章制度', '', 0, 0.15);
INSERT INTO `stdchk_category` VALUES (84, 1, 83, '4.1.1', '制度建设', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (85, 2, 83, '4.1.2', '资料管理', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (86, 3, 83, '4.1.3', '岗位规范', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (87, 2, 82, '4.2', '组织保障与装备', '', 0, 0.15);
INSERT INTO `stdchk_category` VALUES (88, 1, 87, '4.2.1', '组织保障', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (89, 2, 87, '4.2.2', '装备管理', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (90, 3, 82, '4.3', '基础工作', '', 0, 0.3);
INSERT INTO `stdchk_category` VALUES (91, 1, 90, '4.3.1', '地质观测与分析', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (92, 2, 90, '4.3.2', '致灾因素普查地质类型划分', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (93, 4, 82, '4.4', '基础资料', '', 0, 0.3);
INSERT INTO `stdchk_category` VALUES (94, 1, 93, '4.4.1', '地质报告', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (95, 2, 93, '4.4.2', '地质说明书', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (96, 3, 93, '4.4.3', '采后总结', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (97, 4, 93, '4.4.4', '台账图纸', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (98, 5, 93, '4.4.5', '原始记录', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (99, 5, 82, '4.5', '预测预报', '', 0, 0.3);
INSERT INTO `stdchk_category` VALUES (100, 1, 99, '4.5.1', '地质预报', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (101, 6, 82, '4.6', '瓦斯地质', '', 0, 0.3);
INSERT INTO `stdchk_category` VALUES (102, 1, 101, '4.6.1', '瓦斯地质', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (103, 7, 82, '4.7', '资源回收及储量管理', '', 0, 0.3);
INSERT INTO `stdchk_category` VALUES (104, 1, 103, '4.7.1', '储量估算图', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (105, 2, 103, '4.7.2', '储量估算成果台账', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (106, 3, 103, '4.7.3', '统计管理', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (107, 8, 82, '4.8', '基础工作', '', 0, 0.25);
INSERT INTO `stdchk_category` VALUES (108, 1, 107, '4.8.1', '控制系统', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (109, 2, 107, '4.8.2', '测量重点', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (110, 3, 107, '4.8.3', '贯通精度', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (111, 4, 107, '4.8.4', '中腰线标定', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (112, 5, 107, '4.8.5', '原始记录及成果台账', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (113, 9, 82, '4.9', '基本矿图', '', 0, 0.25);
INSERT INTO `stdchk_category` VALUES (114, 1, 113, '4.9.1', '测量矿图', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (115, 2, 113, '4.9.2', '矿图要求', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (116, 10, 82, '4.10', '沉陷观测控制', '', 0, 0.25);
INSERT INTO `stdchk_category` VALUES (117, 1, 116, '4.10.1', '地表移动', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (118, 2, 116, '4.10.2', '资料台账', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (119, 11, 82, '4.11', '水文地质基础工作', '', 0, 0.3);
INSERT INTO `stdchk_category` VALUES (120, 1, 119, '4.11.1', '基础工作', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (121, 2, 119, '4.11.2', '基础资料', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (122, 3, 119, '4.11.3', '水文图纸', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (123, 4, 119, '4.11.4', '水害预报', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (124, 12, 82, '4.12', '防治水工程', '', 0, 0.3);
INSERT INTO `stdchk_category` VALUES (125, 1, 124, '4.12.1', '系统建立', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (126, 2, 124, '4.12.2', '技术要求', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (127, 3, 124, '4.12.3', '工程质量', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (128, 4, 124, '4.12.4', '疏干带压开采', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (129, 5, 124, '4.12.5', '辅助工程', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (130, 13, 82, '4.13', '水害预警', '', 0, 0.3);
INSERT INTO `stdchk_category` VALUES (131, 1, 130, '4.13.1', '水害预警', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (132, 14, 82, '4.14', '基础管理', '', 0, 0);
INSERT INTO `stdchk_category` VALUES (133, 1, 132, '4.14.1', '组织保障', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (134, 15, 82, '4.15', '防冲技术', '', 0, 0);
INSERT INTO `stdchk_category` VALUES (135, 1, 134, '4.15.1', '技术支撑', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (136, 2, 134, '4.15.2', '监测预警', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (137, 16, 82, '4.16', '防冲措施', '', 0, 0);
INSERT INTO `stdchk_category` VALUES (138, 1, 137, '4.16.1', '区域防冲措施', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (139, 2, 137, '4.16.2', '局部防冲措施', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (140, 17, 82, '4.17', '防护措施', '', 0, 0);
INSERT INTO `stdchk_category` VALUES (141, 1, 140, '4.17.1', '安全防护', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (142, 18, 82, '4.18', '基础资料', '', 0, 0);
INSERT INTO `stdchk_category` VALUES (143, 1, 142, '4.18.1', '台帐资料', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (144, 5, NULL, '5', '采煤', '', 0, 0.09);
INSERT INTO `stdchk_category` VALUES (145, 1, 144, '5.1', '基础管理', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (146, 1, 145, '5.1.1', '监测', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (147, 2, 145, '5.1.2', '规程措施', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (148, 3, 145, '5.1.3', '管理制度', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (149, 4, 145, '5.1.4', '支护材料', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (150, 5, 145, '5.1.5', '采煤机械化', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (151, 2, 144, '5.2', '岗位规范', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (152, 1, 151, '5.2.1', '专业技能', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (153, 2, 151, '5.2.2', '规范作业', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (154, 3, 144, '5.3', '质量与安全', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (155, 1, 154, '5.3.1', '顶板管理', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (156, 2, 154, '5.3.2', '安全出口与端头支护', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (157, 3, 154, '5.3.3', '安全设施', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (158, 4, 144, '5.4', '机电设备', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (159, 1, 158, '5.4.1', '设备选型', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (160, 2, 158, '5.4.2', '设备管理', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (161, 5, 144, '5.5', '文明生产', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (162, 1, 161, '5.5.1', '面外环境', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (163, 2, 161, '5.5.2', '面内环境', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (164, 6, NULL, '6', '掘进', '', 0, 0.09);
INSERT INTO `stdchk_category` VALUES (165, 1, 164, '6.1', '生产组织', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (166, 1, 165, '6.1.1', '机械化程度', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (167, 2, 165, '6.1.2', '劳动组织', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (168, 2, 164, '6.2', '设备管理', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (169, 1, 168, '6.2.1', '掘进机械', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (170, 2, 168, '6.2.2', '运输系统', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (171, 3, 164, '6.3', '技术保障', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (172, 1, 171, '6.3.1', '监测控制', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (173, 2, 171, '6.3.2', '现场图牌板', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (174, 3, 171, '6.3.3', '规程措施', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (175, 4, 164, '6.4', '岗位规范', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (176, 1, 175, '6.4.1', '专业技能', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (177, 2, 175, '6.4.2', '规范作业', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (178, 5, 164, '6.5', '工程质量与安全', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (179, 1, 178, '6.5.1', '保障机制', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (180, 2, 178, '6.5.2', '安全管控', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (181, 3, 178, '6.5.3', '规格质量', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (182, 4, 178, '6.5.4', '内在质量', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (183, 5, 178, '6.5.5', '材料质量', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (184, 6, 164, '6.6', '文明生产', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (185, 1, 184, '6.6.1', '灯光照明', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (186, 2, 184, '6.6.2', '作业环境', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (187, 7, NULL, '7', '机电', '', 0, 0.09);
INSERT INTO `stdchk_category` VALUES (188, 1, 187, '7.1', '设备与指标', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (189, 1, 188, '7.1.1', '设备证标', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (190, 2, 188, '7.1.2', '设备完好', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (191, 3, 188, '7.1.3', '固定设备', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (192, 4, 188, '7.1.4', '小型电器', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (193, 5, 188, '7.1.5', '矿灯', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (194, 6, 188, '7.1.6', '机电事故率', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (195, 7, 188, '7.1.7', '设备待修率', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (196, 8, 188, '7.1.8', '设备大修改造', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (197, 2, 187, '7.2', '煤矿机械', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (198, 1, 197, '7.2.1', '主提升(立斜井绞车)系统', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (199, 2, 197, '7.2.2', '主提升（带式传输机）系统', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (200, 3, 197, '7.2.3', '主通风机系统', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (201, 4, 197, '7.2.4', '压风系统', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (202, 5, 197, '7.2.5', '排水系统', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (203, 6, 197, '7.2.6', '瓦斯发电系统', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (204, 7, 197, '7.2.7', '地面供热降温系统', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (205, 3, 187, '7.3', '煤矿电气', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (206, 1, 205, '7.3.1', '地面供电系统', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (207, 2, 205, '7.3.2', '井下供电系统', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (208, 4, 187, '7.4', '基础管理', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (209, 1, 208, '7.4.1', '组织保障', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (210, 2, 208, '7.4.2', '管理制度', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (211, 3, 208, '7.4.3', '技术管理', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (212, 4, 208, '7.4.4', '设备技术性能测试', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (213, 5, 187, '7.5', '岗位规范', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (214, 1, 213, '7.5.1', '专业技能', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (215, 2, 213, '7.5.2', '规范作业', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (216, 6, 187, '7.6', '文明生产', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (217, 1, 216, '7.6.1', '设备设置', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (218, 2, 216, '7.6.2', '管网', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (219, 3, 216, '7.6.3', '机房卫生', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (220, 4, 216, '7.6.4', '照明', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (221, 5, 216, '7.6.5', '器材工具', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (222, 8, NULL, '8', '运输', '', 0, 0.08);
INSERT INTO `stdchk_category` VALUES (223, 1, 222, '8.1', '巷道硐室', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (224, 1, 223, '8.1.1', '巷道车场/硐室车房/装卸载站', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (225, 2, 222, '8.2', '运输路线', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (226, 1, 225, '8.2.1', '轨道（道路）系统', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (227, 2, 225, '8.2.2', '道岔', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (228, 3, 225, '8.2.3', '窄轨架线电机车牵引网络', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (229, 4, 225, '8.2.4', '运输方式改善', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (230, 3, 222, '8.3', '运输设备', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (231, 1, 230, '8.3.1', '设备管理', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (232, 2, 230, '8.3.2', '普通轨斜巷（井）人车/架空乘人装置/机车/调度绞车/无极绳连续牵引车、绳牵引卡轨车、绳牵引单轨吊车/单轨吊车/无轨胶轮车', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (233, 4, 222, '8.4', '运输安全设施', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (234, 1, 233, '8.4.1', '挡车装置和跑车防护装置', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (235, 2, 233, '8.4.2', '安全警示', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (236, 3, 233, '8.4.3', '物料捆绑/连接装置', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (237, 5, 222, '8.5', '运输管理', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (238, 1, 237, '8.5.1', '组织保障/制度保障', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (239, 2, 237, '8.5.2', '技术资料/检测检验', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (240, 6, 222, '8.6', '岗位规范', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (241, 1, 240, '8.6.1', '专业技能/规范作业', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (242, 7, 222, '8.7', '文明生产', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (243, 1, 242, '8.7.1', '作业场所', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (244, 9, NULL, '9', '职业卫生', '', 0, 0.06);
INSERT INTO `stdchk_category` VALUES (245, 1, 244, '9.1', '职业卫生管理', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (246, 1, 245, '9.1.1', '组织保障', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (247, 2, 245, '9.1.2', '责任落实', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (248, 3, 245, '9.1.3', '制度完善', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (249, 4, 245, '9.1.4', '经费保障', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (250, 5, 245, '9.1.5', '工作计划', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (251, 6, 245, '9.1.6', '档案管理', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (252, 7, 245, '9.1.7', '危害告知', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (253, 8, 245, '9.1.8', '工伤保险', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (254, 9, 245, '9.1.9', '检测评价', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (255, 10, 245, '9.1.10', '个体防护', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (256, 11, 245, '9.1.11', '公告警示', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (257, 2, 244, '9.2', '职业病危害', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (258, 1, 257, '9.2.1', '监测人员', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (259, 2, 257, '9.2.2', '粉尘', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (260, 3, 257, '9.2.3', '噪声', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (261, 4, 257, '9.2.4', '高温', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (262, 5, 257, '9.2.5', '化学毒物', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (263, 3, 244, '9.3', '职业健康防护', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (264, 1, 263, '9.3.1', '上岗前检查', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (265, 2, 263, '9.3.2', '在岗期间检查', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (266, 3, 263, '9.3.3', '离岗检查', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (267, 4, 263, '9.3.4', '应急检查', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (268, 5, 263, '9.3.5', '结果告知', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (269, 6, 263, '9.3.6', '监护档案', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (270, 4, 244, '9.4', '职业病诊断鉴定', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (271, 1, 270, '9.4.1', '职业病诊断', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (272, 2, 270, '9.4.2', '职业病病人待遇', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (273, 3, 270, '9.4.3', '治疗、定期检查和康复', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (274, 4, 270, '9.4.4', '职业病病人安置', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (275, 5, 270, '9.4.5', '诊断、鉴定资料', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (276, 5, 244, '9.5', '工会监督', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (277, 1, 276, '9.5.1', '工会监督与维权', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (278, 10, NULL, '10', '安全培训和应急管理', '', 0, 0.06);
INSERT INTO `stdchk_category` VALUES (279, 1, 278, '10.1', '安全培训', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (280, 1, 279, '10.1.1', '基础保障', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (281, 2, 279, '10.1.2', '组织实施', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (282, 3, 279, '10.1.3', '持证上岗', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (283, 4, 279, '10.1.4', '培训档案', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (284, 2, 278, '10.2', '班组安全建设', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (285, 1, 284, '10.2.1', '制度建设', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (286, 2, 284, '10.2.2', '组织建设', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (287, 3, 284, '10.2.3', '现场管理', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (288, 3, 278, '10.3', '应急管理', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (289, 1, 288, '10.3.1', '机构和职责', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (290, 2, 288, '10.3.2', '制度建设', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (291, 3, 288, '10.3.3', '应急保障', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (292, 4, 288, '10.3.4', '安全避险系统', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (293, 5, 288, '10.3.5', '应急广播系统', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (294, 6, 288, '10.3.6', '个体防护装备', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (295, 7, 288, '10.3.7', '紧急处置权限', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (296, 8, 288, '10.3.8', '技术资料', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (297, 9, 288, '10.3.9', '队伍建设', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (298, 10, 288, '10.3.10', '应急预案', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (299, 11, 288, '10.3.11', '应急演练', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (300, 12, 288, '10.3.12', '资料档案', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (301, 11, NULL, '11', '调度和地面设施', '', 0, 0.06);
INSERT INTO `stdchk_category` VALUES (302, 1, 301, '11.1', '调度基础工作', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (303, 1, 302, '11.1.1', '组织机构', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (304, 2, 302, '11.1.2', '管理制度', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (305, 3, 302, '11.1.3', '技术支撑', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (306, 2, 301, '11.2', '调度管理', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (307, 1, 306, '11.2.1', '组织协调', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (308, 2, 306, '11.2.2', '应急处置', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (309, 3, 306, '11.2.3', '深入现场', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (310, 4, 306, '11.2.4', '调度记录', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (311, 5, 306, '11.2.5', '调度汇报', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (312, 6, 306, '11.2.6', '雨季“三防”', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (313, 3, 301, '11.3', '调度信息化', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (314, 1, 313, '11.3.1', '通信装备', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (315, 2, 313, '11.3.2', '监控系统', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (316, 3, 313, '11.3.3', '人员位置监测', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (317, 4, 313, '11.3.4', '图像监视', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (318, 5, 313, '11.3.5', '信息管理系统', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (319, 4, 301, '11.4', '岗位规范', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (320, 1, 319, '11.4.1', '专业技能', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (321, 2, 319, '11.4.2', '规范作业', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (322, 5, 301, '11.5', '文明生产', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (323, 1, 322, '11.5.1', '文明办公', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (324, 6, 301, '11.6', '地面办公场所', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (325, 1, 324, '11.6.1', '办公室', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (326, 2, 324, '11.6.2', '会议室', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (327, 7, 301, '11.7', '两堂一舍', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (328, 1, 327, '11.7.1', '职工食堂', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (329, 2, 327, '11.7.2', '职工澡堂', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (330, 3, 327, '11.7.3', '职工宿舍及洗衣房', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (331, 8, 301, '11.8', '工业广场', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (332, 1, 331, '11.8.1', '工业广场', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (333, 2, 331, '11.8.2', '工业道路', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (334, 3, 331, '11.8.3', '环境卫生', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (335, 9, 301, '11.9', '地面设备材料库', '', 0, NULL);
INSERT INTO `stdchk_category` VALUES (336, 1, 335, '11.9.1', '设备库房', '', 0, NULL);

-- ----------------------------
-- Table structure for stdchk_item
-- ----------------------------
DROP TABLE IF EXISTS `stdchk_item`;
CREATE TABLE `stdchk_item`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` int(11) DEFAULT NULL COMMENT '序号，用于排序',
  `category_id` int(11) DEFAULT NULL COMMENT '标准化检查分类ID',
  `categoryName` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '缓存分类的名称，根据三级分类名称自动生成，三级分类用\"/\"分割',
  `serialno` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '缓存编号，例如 \'1.1.1.1\'，考虑可根据各级分类的index和本条目的index自动生成',
  `demand` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '基本要求',
  `standard` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '标准项目内容',
  `method_content` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '检查方式及内容',
  `required_info` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '所需资料',
  `detailed_rules` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '扣分细则',
  `score` float DEFAULT 5 COMMENT '标准分值',
  `remark` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '备注',
  `enabled` tinyint(1) DEFAULT 1 COMMENT '是否使用此项标准，0：停用 1：使用',
  `defResponsible_id` int(11) DEFAULT NULL COMMENT '预设的责任人ID',
  `deleted` tinyint(1) DEFAULT 0 COMMENT '1:表示此记录处于已删除状态',
  `flags` int(11) DEFAULT NULL COMMENT '标志位',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_stdchk_item_stdchk_category1_idx`(`category_id`) USING BTREE,
  INDEX `fk_stdchk_item_sys_user1_idx`(`defResponsible_id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 406 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '标准化检查条目' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of stdchk_item
-- ----------------------------
INSERT INTO `stdchk_item` VALUES (1, 1, 3, '安全风险分级管控/工作机制/职责分工', '1.1.1(1)', '建立安全风险分级管控工作责任体系，矿长全面负责，分管负责人负责分管范围内的安全风险分级管控工作', '查资料和现场。未建立责任体系不得分，随机抽查，矿领导1人不清楚职责扣1分', '1.查有责任体系正式文件，看是否按本标准要求明确了矿领导班子成员的责任；2.对矿班子成员进行随机抽考。抽考前要确定抽考人数。', '明确责任体系的相关文件(也可以在安全生产责任制中或安全风险分级管控工作制度中体现)', '1.无责任体系文件不得分，指责内容不健全的建议补充（暂不扣分）；2.抽考矿班子成员，1人不清楚自身职责扣一分', 4, NULL, 1, 97, 0, 1);
INSERT INTO `stdchk_item` VALUES (2, 2, 3, '安全风险分级管控/工作机制/职责分工', '1.1.1(2)', '有负责安全风险分级管控工作的管理部门', '查资料。未明确管理部门不得分', '查责任体系文件或风险分级管控工作制度，是否明确了一个部门负责安全风险分级管控工作', '责任体系文件或风险分级管控工作制度', '未明确不得分。明确部门或成立风险管控领导小组，办公室设在某科室，均符合标准要求', 2, NULL, 1, 97, 0, 1);
INSERT INTO `stdchk_item` VALUES (3, 1, 4, '安全风险分级管控/工作机制/制度建设', '1.1.2(1)', '建立安全风险分级管控工作制度，明确安全风险的辨识范围、方法和安全风险的辨识、评估、管控工作流程', '查资料。未建立制度不得分，辨识范围、方法或工作流程1处不明确扣2分', '1.查风险管控工作制度，必须明确风险的范围（可以概括的说明），必须明确本矿辨识和评估分级分别采用了什么方法；2.查是否按照本标准要求明确了风险辨识和评估的工作流程，流程要求要包括本标准要求的所有内容', '风险管控工作制度或风险分级管控的相关制度', '1.范围和方法不明确扣2分（可以按灾害类型明确范围，也可按工作地点明确范围）；2.未明确风险管控的工作流程扣2分；3.工作流程内容不完善作为建议提出，赞不扣分', 4, NULL, 1, 97, 0, 1);
INSERT INTO `stdchk_item` VALUES (4, 1, 6, '安全风险分级管控/安全风险辨识评估/年度辨识评估', '1.2.1(1)', '每年底矿长组织各分管负责人和相关业务科室、区队进行年度安全风险辨识，重点对井工煤矿瓦斯、水、火、粉尘、顶板、冲击地压及提升运输系统，露天煤矿边坡、爆破、机电运输等容易导致群死群伤事故的危险因素开展安全风险辨识；及时编制年度安全风险辨识评估报告，建立可能引发重特大事故的重大安全风险清单，制定相应的管控措施；  将辨识评估结果用于确定下一年度安全生产工作重点，并指导和完善下一年度生产计划、灾害预防和处理计划、应急救援预案', '查资料。未开展辨识或辨识组织者不符合要求不得分，辨识内容（危险因素不存在的除外）缺1项扣2分，评估报告、风险清单、管控措施缺1项扣2分，辨识成果未体现缺1项扣1分', '1.年度风险辨识评估报告，看组织者是否为矿长；2.查风险辨识评估是否按安全风险分级管控工作制度要求进行的（是否同制度中明确的辨识范围、方法和安全风险的辨识、评估、管控工作流程向一致）3.查风险辨识评估的是否安全、准确；4.检查是否有重大安全风险清单，重大风险管控措施是否具有可操作性；5.查辨识评估成果是否应用于确定下一年度安全生产工作重点，并指导和完善下一年度生产计划、灾害预防和处理计划、应急救援预案', '1.年度风险识别评估报告（包括参加人员签字）；2.年度安全生产工作重点安排、下一年度生产计划、灾害预防和处理计划、应急预案', '1.矿长未组织开展年度辨识评估或辨识不得分（必须由矿长组织、本矿完成，可以有中介机构配合，但各分管负责人和相关业务科室必须参加；由中介机构出具报告的不得分）；参加人员不全提出整改建议（暂不扣分）；2.未按照风险分级管控制度开展风险辨识工作的扣2分；3.辨识评估不全面（5+2），缺1项扣2分，危险因素不存在的除外；4.对存在的水文条件极复杂、煤层自燃及容易自燃、高瓦斯及突出、有冲击地压等4类重大灾害矿井，以及采用斜井人车运输人员的，未将相应风险辨识评估为重大风险的，扣2分（多项辨识不准确的，累计上限扣2分）；5.重大风险管控措施不完善或操作性不强建议补充完善（暂不扣分）；6.没有保存风险辨识的原始记录（辨识资料中只体现重大风险没有体现其他级别的风险）没有参加人签字，建议补充（暂不扣分）；7.在下年度安全生产重点工作安全中未明确对辨识评估出的重大风险作为安全工作的重点惊醒管控扣1分，在完善下一年度生产计划、灾害预防和处理计划、应急救援预案中未体现应用扣1分', 10, NULL, 1, 97, 0, 1);
INSERT INTO `stdchk_item` VALUES (5, 1, 7, '安全风险分级管控/安全风险辨识评估/专项辨识评估', '1.2.2(1)', '新水平、新采（盘）区、新工作面设计前，开展1次专项辨识： 1.专项辨识由总工程师组织有关业务科室进行； 2.重点辨识地质条件和重大灾害因素等方面存在的安全风险； 3.补充完善重大安全风险清单并制定相应管控措施； 4.辨识评估结果用于完善设计方案，指导生产工艺选择、生产系统布置、设备选型、劳动组织确定等', '查资料和现场。未开展辨识不得分，辨识组织者不符合要求扣2分，辨识内容缺1项扣2分，风险清单、管控措施、辨识成果未在应用中体现缺1项扣1分', '查看联合布置图纸和采掘接续计划等资料，查找当年是否有新水平、新采（盘）区、新工作面、是否开展了新设计；2.查辨识评估会议纪要或评估报告，看组织者是不是总工程师；参加人员是否专业齐全；3.查风险辨识评估是否全面、准确地对地质条件和重大灾害因素等安全方面存在的安全风险进行了辨识评估；4.形成风险清单，若辨识评估出新的重大风险，查是否补充重大安全风险清单；5.查制定的重大风险管控措施是否具有可操作性；6.查是否体现应用', '1.多水平联合布置图纸和采掘接续计划；2.专项辨识会议纪要或专项辨识评估报告；3.相关设计方案等；4.年度生产计划', '1.新水平,新采(盘)区,新工作面设计前, 未进行专项辨识评估不得分；2.辨识组织者不是总工程师要求扣2分；辨识专业科室不齐全建议补充 (暂不扣分)；3.没有全面对地质条件和重大灾害因素等方面存在的安全风险进行辨识评估,缺1项扣2分,没有参加人签字建议补充 (此处暂不扣分)；4.对于重大灾害风险评估不准确,取值不合理的,1处扣1分,扣2分为止;其他风险辨识不准确,建议重新取值 (此处暂不扣分)；5.辨识评估出新增的重大风险,未补充重大安全风险清单扣2分；6.重大风险管控措施不完善或操作性不强,建议补充完善(此处暂不扣分)；7.未在相关设计中体现应用扣1分；8.对于2017年7月1日后完成的设计, 相关专项辨识在设计之后完成的,如果对照辨识评估结果完善原设计的扣1分,未完善的本项不得分', 8, NULL, 1, 97, 0, 1);
INSERT INTO `stdchk_item` VALUES (6, 2, 7, '安全风险分级管控/安全风险辨识评估/专项辨识评估', '1.2.2(2)', '生产系统、生产工艺、主要设施设备、重大灾害因素（露天煤矿爆破参数、边坡参数）等发生重大变化时，开展1次专项辨识：  1.专项辨识由分管负责人组织有关业务科室进行；  2.重点辨识作业环境、生产过程、重大灾害因素和设施设备运行等方面存在的安全风险； 3.补充完善重大安全风险清单并制定相应的管控措施；  4.辨识评估结果用于指导重新编制或修订完善作业规程、操作规程', '查资料和现场。未开展辨识不得分，辨识组织者不符合要求扣2分，辨识内容缺1项扣2分，风险清单、管控措施、辨识成果未在应用中体现缺1项扣1分', '1. 查年度和月度作业计划、 技术改造方案等资料, 并根据现场实际, 判断是否存在“生产系统、生产工艺、 主要设施设备、重大灾害因素(露天煤矿爆破参数、 边坡参数) 等发生重大变化” 的情况;2.查辨识评估会议纪要或评估报告, 看组织者是否正确;3. 查是否对作业环境、 生产过程、 重大灾害因素和设施设备运行等方面存在的安全风险进行全面、 准确辨识评估;4. 形成风险清单,若辨识评估出新增的重大风险, 查是否补充重大安全风险清单;5.查新制定重大风险管控措施是否具有可操作性;6. 查是否体现应用', '1.年度和月度作业计划、 技术改造方案等;2.专项辨识会议纪要或评估报告、 专项辨识评估相关原始记录;3. 相关作业规程、 操作规程', '1. 生产系统、生产工艺、主要设施设备发生重大变化时,未开展专项辨识评估不得分(任何1项未开展专项辨识, 此项均不得分);2.辨识组织者不符合要求扣2分;3. 对作业环境、生产过程、重大灾害因素和设施设备运行等方面存在的安全风险未全面辨识评估;缺1项扣2分(累计上限扣2分);没有参加人签字建议补充(此处暂不扣分);4.对于重大灾害风险评估不准确,取值不合理的,1处扣1分,扣2分为止; 其他风险辨识不准确, 建议重新取值 (此处暂不扣分);5.辨识评估出新增的重大风险未补充重大安全风险清单扣2分;6.重大风险管控措施不完善或操作性不强建议补充完善(此处暂不扣分);7.未在修订完善 作业规程、操作规程中体现应用扣1分', 8, NULL, 1, 97, 0, 1);
INSERT INTO `stdchk_item` VALUES (7, 3, 7, '安全风险分级管控/安全风险辨识评估/专项辨识评估', '1.2.2(3)', '启封火区、排放瓦斯、突出矿井过构造带及石门揭煤等高危作业实施前，新技术、新材料试验或推广应用前，连续停工停产1个月以上的煤矿复工复产前，开展1次专项辨识： 1.专项辨识由分管负责人组织有关业务科室、生产组织单位（区队）进行；  2.重点辨识作业环境、工程技术、设备设施、现场操作等方面存在的安全风险；  3.补充完善重大安全风险清单并制定相应的管控措施；  4.辨识评估结果作为编制安全技术措施依据', '查资料和现场。未开展辨识不得分，辨识组织者不符合要求扣2分，辨识内容缺1项扣2分，风险清单、管控措施、辨识成果未在应用中体现缺1项扣1分', '1. 查找当年新、 老采掘工程平面图, 看是否有密闭开启, 查看局部通风机运行记录、 停复产审批记录,并根据现场实际,判断是否有启封火区、排放瓦斯、 突出矿井过构造带及石门揭煤等高危作业; 查看设备设施、 材料台账,判断是否有新技术、新材料投入使用;2. 查辨识评估会议纪要, 看组织者是否正确;3.查是否对作业环境、工程技术、设备设施、现场操作等方面存在的安全风险,进行全面、 准确辨识评估;4.形成风险清单,若辨识评估出新增的重大风险, 查是否补充重大安全风险清单;5. 查新制定重大风险管控措施是否具有可操作性;6. 查是否体现应用', '1.当年新、老采掘工程平面图;2. 各台局部通风机运行记录;3.停复产审批记录;4. 设备设施、材料台账;5. 专项辨识会议纪要或评估报告;6. 专项辨识评估相关原始记录;7. 相关安全技术措施', '1.存在本标准中的高危作业和停产1个月的情况, 未开展专项辨识评估不得分(任何1项未开展专项辨识, 均不得分);2. 辨识组织者不符合要求扣2分;3. 对作业环境、工程技术、设备设施、现场操作等方面存在的安全风险未全面辨识评估,缺1项扣2分; 没有参加人签字建议补充 (此处暂不扣分);4.对于重大灾害风险评估不准确,取值不合理的,1处扣1分,扣2分为止; 其他风险辨识不准确, 建议重新取值 (此处暂不扣分);5.辨识评估出新增的重大风险未补充重大安全风险清单扣2分;6.重大风险管控措施不完善或操作性不强酌情扣0.5~1分;7. 未在相关安全技术措施中体现应用扣1分', 8, NULL, 1, 97, 0, 1);
INSERT INTO `stdchk_item` VALUES (8, 4, 7, '安全风险分级管控/安全风险辨识评估/专项辨识评估', '1.2.2(4)', '本矿发生死亡事故或涉险事故、出现重大事故隐患或所在省份发生重特大事故后，开展1次针对性的专项辨识；\n1.专项辨识由矿长组织分管负责人和业务科室进行；\n2.识别安全风险辨识结果及管控措施是否存在漏洞、盲区；\n3.补充完善重大安全风险清单并制定相应的管控措施；\n4.辨识评估结果用于指导修订完善设计方案、作业规程、操作规程、安全技术措施等技术文件\n', '查资料和现场。未开展辨识不得分，辨识组织者不符合要求扣2分，辨识内容缺1项扣2分，风险清单、管控措施、辨识成果未在应用中体现缺1项扣1分', '1.查看调度事故统计台账及网上信息,看本矿是否发生过死亡及涉险事故;2.查辨识评估会议纪要, 看组织者是否正确;3. 查是否识别安全风险辨识结果及管控措施存在漏洞、 盲区;4.形成风险清单,若辨识评估出新增的重大风险, 查是否补充重大安全风险清单;5.查新制定重大风险管控措施是否具有可操作性;6.查是否体现应用', '1.事故统计台账2.专项辨识会议纪要或评估报告3.专项辨识相关原始记录4.相关设计方案、作业规程、操作规程、安全技术措施等技术文件', '1.本矿发生死亡事故或涉险事故、出现重大事故隐患或所在省份发生重特大事故后，未开展专项辨识训估不得分;2.辨识组织者不符合要求扣2分:3.安全风险辨识结果及管控措施若存在漏洞、盲区未识别出，缺1项扣2分;没有参加人签字，建议补充(暂不扣分) ;4.对于重大灾害风险评估不准确，取值不合理的，1处扣1分，扣2分为止;其他风险辨识不准确，建议重新取值(暂不扣分) ;5.辨识评估出新增的重大风险未补充重大安全风险清单扣2分;6.重大风险管控措施不完善或操作性不强，建议补充完善(暂不扣分);7.未在相关设计方案、作业规程、操作规程、安全技术措施中体现应用扣1分', 6, NULL, 1, 97, 0, 1);
INSERT INTO `stdchk_item` VALUES (9, 1, 9, '安全风险分级管控/安全风险管控/管控措施', '1.3.1(1)', '1.重大安全风险管控措施由矿长组织实施，有具体工作方案，人员、技术、资金有保障', '查资料。组织者不符合要求、未制定方案不得分，人员、技术、资金不明确、不到位1项扣1分', '1.查重大安全风险管控措施实施工作方案是否体现矿长组织实施重大风险管控措施;2.查是否针对所有重大安全风险管控措施制定了实施方案;查是否有人员、技术、资金保障', '重大安全风险管控措施实施工作方案', '1.组织者不符合要求或未制定方案均不得分;2.重大风险管控措施实施方案未覆盖全部重大风险，缺1项扣1分;3.因人员、技术、资金不明确、不到位影响方案实施的，1项扣1分', 5, NULL, 1, 97, 0, 1);
INSERT INTO `stdchk_item` VALUES (10, 2, 9, '安全风险分级管控/安全风险管控/管控措施', '1.3.1(2)', '2.在划定的重大安全风险区域设定作业人数上限', '查资料和现场。未设定人数上限不得分，超1人扣0.5分', '1.查重大风险管控措施实施方案，查是否划定了重大安全风险区域、设定了作业人数上限(也可在会议纪要等资料中明确规定);2.查各重大风险区域人员是否超限;3.查是否在重大风险区域挂牌警示并设定作业人数上限', '1.重大隐患管控措施实施方案等划定重大安全风险区域设定作业人数上限相关文件）；2.作业规程、安全技术措施；3.本省、本企业劳动作业定员标准', '1.未设定作业人数上限不得分；2.超1人扣0.5分；3.未挂牌警示设定作业人数上限建议补充完善（此处暂不扣分）', 4, NULL, 1, 97, 0, 1);
INSERT INTO `stdchk_item` VALUES (11, 1, 10, '安全风险分级管控/安全风险管控/定期检查', '1.3.2(1)', '1.矿长每月组织对重大安全风险管控措施落实情况和管控效果进行一次检查分析，针对管控过程中出现的问题调整完善管控措施，并结合年度和专项安全风险辨识评估结果，布置月度安全风险管控重点，明确责任分工', '查资料。未组织分析评估不得分，分析评估周期不符合要求，每缺1次扣3分，管控措施不做相应调整或月度管控重点不明确1处扣2分，责任不明确1处扣1分', '1.查人员定位系统等，查看矿长是否按规定周期组织对重大安全风险管控措施落实情况和管控效果进行检查和分析，检查分析要有原始记录:2.查是否全面检查重大风险管控措施落实情况;3.查是否针对管控过程中出现的问题调整完善管控措施;4.查是否按要求布置月度安全风险管控重点，并明确责任分工', '1.相关会议纪要；2.现场检查记录', '1.未组织检查分析不得分;2.未对重大风险管控措施进行全面检查，缺1项扣1分;3.检查分析周期不符合要求,每缺1次扣3分（检查分析周期高于标准不扣分）;4.管控措施应调整而不做相应调整或月度管控重点不明确1处扣2分;5.责任不明确1处扣1分', 8, NULL, 1, 97, 0, 1);
INSERT INTO `stdchk_item` VALUES (12, 2, 10, '安全风险分级管控/安全风险管控/定期检查', '1.3.2(2)', '2.分管负责人每旬组织对分管范围内月度安全风险管控重点实施情况进行一次检查分析，检查管控措施落实情况，改进完善管控措施', '查资料。未组织分析评估不得分，分析评估周期不符合要求，每缺1次扣3分，管控措施不做相应调整1处扣2分', '1.查分管领导是否按规定周期组织对月度安全风险管控重点实施情况进行检查分析，要有检查分析原始记录;2.是否对矿长布置的分管范围内月度安全风险管控重点措施落实情况进行检查分析，是否针对管控过程中出现的问题调整完善管控措施', '1.相关会议纪要；2.现场检查记录', '1.未组织检查分析不得分;2.周期不符合要求，每缺1次扣3分(分析周期高于标准不扣分，可以和矿长每月的检查分析合并1次);3.没有覆盖分管范围内的全部重大安全风险或未按矿长布置的本专业分管范围的重点进行检查，每缺1项扣1分(扣到4分为止);4.管控措施应调整而不做相应调整1处扣2分(累计扣到2分);5.无参加人员签字，建议补充完善(此处暂不扣分)', 8, NULL, 1, 97, 0, 1);
INSERT INTO `stdchk_item` VALUES (13, 1, 11, '安全风险分级管控/安全风险管控/现场检查', '1.3.3(1)', '按照《煤矿领导带班下井及安全监督检查规定》，执行煤矿领导带班制度，跟踪重大安全风险管控措施落实情况，发现问题及时整改', '查资料和现场。未执行领导带班制度不得分，未跟踪管控措施落实情况或发现问题未及时整改1处扣2分', '1.查煤矿领导跟班记录，看是否有跟踪重大风险的管控措施落实情况和管控效果；3.是否跟踪发现问题整改情况', '煤矿领导带班记录、交接班记录、检查问题落实清单', '1.未执行领导带班制度不得分；2.记录中未能体现跟踪重大风险管控措施落实情况1处扣2分；3.发现管控措施失效等问题未及时跟踪整改情况1处扣2分。', 6, NULL, 1, 97, 0, 1);
INSERT INTO `stdchk_item` VALUES (14, 1, 12, '安全风险分级管控/安全风险管控/公告警示', '1.3.4(1)', '在井口（露天煤矿交接班室）或存在重大安全风险区域的显著位置，公告存在的重大安全风险、管控责任人和主要管控措施', '查现场。未公示不得分，公告内容和位置不符合要求1处扣1分', '查公告牌板，公告内容是否齐全，并符合要求', '公告排板（亦可采用电子屏）', '1.未公告不得分，位置不符合要求1出处扣分；2.公告重大安全风险内容与重大安全风险清单内容不一致，管控责任人和重大安全风险管控工作方案中责任人不一致，1处扣1分', 4, NULL, 1, 97, 0, 1);
INSERT INTO `stdchk_item` VALUES (15, 1, 14, '安全风险分级管控/保障措施/信息管理', '1.4.1(1)', '采用信息化管理手段，实现对安全风险记录、跟踪、统计、分析、上报等全过程的信息化管理', '查现场。未实现信息化管理不得分，功能每缺1项扣1分', '查看所使用的信息化手段，看是否额能实现对安全风险管控情况近行记录、跟踪、统计、分析、上报等全过程的信息化管理', NULL, '1.未采用信息化管理手段不得分；2.未实现对风险管控情况进行跟踪、统计、分析、上报的功能缺1项扣1分（使用电子邮件、社交软件等通信软件，能实现上述功能的不扣分）', 4, NULL, 1, 97, 0, 1);
INSERT INTO `stdchk_item` VALUES (16, 1, 15, '安全风险分级管控/保障措施/教育培训', '1.4.2(1)', '1．入井（坑）人员和地面关键岗位人员安全培训内容包括年度和专项安全风险辨识评估结果、与本岗位相关的重大安全风险管控措施', '查资料。培训内容不符合要求1处扣 1分', '通过检查入井（坑）人员和地面关键岗位人员的培训记录、现场询问工作人员，看培训内容是否符合规定', '1.培训、同通知、记录、签到表、教案、考试卷；2.煤矿员工花名册', '培训内容不符合要求1处扣1分', 6, NULL, 1, 97, 0, 1);
INSERT INTO `stdchk_item` VALUES (17, 2, 15, '安全风险分级管控/保障措施/教育培训', '1.4.2(2)', '2．每年至少组织参与安全风险辨识评估工作的人员学习1次安全风险辨识评估技术', '查资料和现场。未组织学习不得分，现场询问相关学习人员，1人未参加学习扣1分', '1.查培训记录、签到表、教案、考试卷，看培训人员和内容是否符合要求；2.现场询问工作人员上年度是否参加有关培训', '1.培训、同通知、记录、签到表、教案、考试卷；2.年度辨识和专项辨识报告中的人员名单', '1.未组织学习不得分；2.经查资料和询问，1名参与风险辨识评估人员未参加学习扣1分', 5, NULL, 1, 97, 0, 1);
INSERT INTO `stdchk_item` VALUES (18, 1, 18, '事故隐患排查治理/工作机制/职责分工', '2.1.1(1)', '1．有负责事故隐患排查治理管理工作的部门', '查资料。无管理部门不得分', '查煤矿“事故隐患排查治理责任体系”，也可在岗位责任制中明确', '相关设立或批准文件，或事故隐患责任体系和制度', '未明确管理部门不得分', 2, NULL, 1, 95, 0, 1);
INSERT INTO `stdchk_item` VALUES (19, 2, 18, '事故隐患排查治理/工作机制/职责分工', '2.1.1(2)', '2．建立事故隐患排查治理工作责任体系，明确矿长全面负责、分管负责人负责分管范围内的事故隐患排查治理工作，各业务科室、生产组织单位（区队）、班组、岗位人员职责明确', '查资料和现场。责任未分工或不明确不得分，矿领导不清楚职责 1人扣2分、部门负责人不清楚职责1人扣0.5分', '1.查煤矿“事故隐患排查治理贵任体系”分工(按标准检查矿班子成员，主要检查岗位人员责任制内容是否明确);2.询问矿领导、副总工程师、部门负责人岗位职责', '事故隐患排查责任体系和制度或岗位责任制', '1.未建立责任体系或矿领导职责未分工不得分;2.矿领导不清楚职责1人扣2分;副总工程师、部门负责人隐患排查治理职责不清楚每人扣0.5分', 4, NULL, 1, 95, 0, 1);
INSERT INTO `stdchk_item` VALUES (20, 1, 19, '事故隐患排查治理/工作机制/分级管理', '2.1.2(1)', '对排查出的事故隐患进行分级，并按照事故隐患等级明确相应层级的单位（部门）、人员负责治理、督办、验收', '查资料和现场。未对事故隐患进行分级扣2分，责任单位和人员不明确1项扣1分', '1.查制度，看是否有事故隐有分级管理相应条款;2.查管理台账，看台账中是否明确分级\n管理', '1.事故隐患排查治理制度；2.事故隐患管理台账', '1.未对事故隐患进行分级扣2分；2.各级隐患治理责任单位和人员不明确，1项扣2分', 4, NULL, 1, 95, 0, 1);
INSERT INTO `stdchk_item` VALUES (21, 1, 21, '事故隐患排查治理/事故隐患排查/基础工作', '2.2.1(1)', '编制事故隐患年度排查计划，并严格落实执行', '查资料和现场。未编制排查计划或未落实执行不得分', '1.查矿井的年度隐患排查计划;2.范围是否全覆盖性(井下和地面重要场所）', '1.矿年度事故隐患排查计划；2.采掘平面图', '1.无计划或未落实执行的不得分;2.计划缺项或内容不全，作建议提出暂不扣分', 4, NULL, 1, 95, 0, 1);
INSERT INTO `stdchk_item` VALUES (22, 1, 22, '事故隐患排查治理/事故隐患排查/周期范围', '2.2.2(1)', '1．矿长每月至少组织分管负责人及安全、生产、技术等业务科室、生产组织单位（区队）开展一次覆盖生产各系统和各岗位的事故隐患排查，排查前制定工作方案，明确排查时间、方式、范围、内容和参加人员', '查资料和现场。未组织排查不得分，组织人员、范围、频次不符合要求1项扣2分，未制定工作方案扣1分、方案内容缺1项扣0.5分', '1.查工作方案是否符合要求;2.查事故隐患排查台账所填内容与方案是否一致', '1.事故隐患排查制度;2.矿长月度事故隐患排查工作方案;3.事故隐患排查治理台账', '1. 排查频次不够，不符合要求，扣2分;2.排查范围末做到覆盖全部生产系统和全部岗位，扣2分;3.未制定工作方案扣1分，方案内容缺1项扣0.5分(矿长每月的隐患排查可以和风险管控措施检查分析一并进行)', 6, NULL, 1, 95, 0, 1);
INSERT INTO `stdchk_item` VALUES (23, 2, 22, '事故隐患排查治理/事故隐患排查/周期范围', '2.2.2(2)', '2.矿各分管负责人每旬组织相关人员对分管领域进行1次全面的事故隐患排查', '查资料和现场。分管负责人未组织排查不得分，组织人员、范围、频次不符合要求1项扣2分', '查事故隐患排查治理台账，看排查组织人、范围、频次、和是否符合要求', '隐患排查治理台账', '1.未组织排查不得分;2.频次不够，扣2分(可以和矿长每月的隐患排查合并1次;可以和每旬的风险管控重点检查分析合并开展);3.排查范围不符合要求，1项扣2', 6, NULL, 1, 95, 0, 1);
INSERT INTO `stdchk_item` VALUES (24, 3, 22, '事故隐患排查治理/事故隐患排查/周期范围', '2.2.2(3)', '3．生产期间，每天安排管理、技术和安检人员进行巡查，对作业区域开展事故隐患排查', '查资料和现场。未安排不得分，人员、范围、频次不符合要求1项扣1分', '1.现场查安检人员的排查记录与所查现场是否相符合;2.查领导人员的带班记录', '1.安检人员的下井记录;2.职能科室负责人、区队长下井带班记录;3.安监处下发的信息单;4.煤矿下发的隐患通知单', '未安排巡查和排查不得分，人员、范围、频次不符合要求1项扣1分', 5, NULL, 1, 95, 0, 1);
INSERT INTO `stdchk_item` VALUES (25, 4, 22, '事故隐患排查治理/事故隐患排查/周期范围', '2.2.2(4)', '4．岗位作业人员作业过程中随时排查事故隐患', '查资料和现场。未进行排查不得分', '1.查1~2个岗位有无隐患排查记录；2.查1~2个岗位现场是否有隐患', '采掘工作面的现场岗位人员隐患排查记录', '1.现场无记录，且岗位人员均不知道需开展作业过程事故隐患排查治理，不得分;2.现场无记录，提问岗位工人，不知道如何排查，1人扣1分;3.现场有隐患的1处扣1分;4.现场有记录，但不完善，建议整改，暂不扣分', 4, NULL, 1, 95, 0, 1);
INSERT INTO `stdchk_item` VALUES (26, 1, 23, '事故隐患排查治理/事故隐患排查/登记上报', '2.2.3(1)', '1．建立事故隐患排查台账，逐项登记排查出的事故隐患', '查资料。未建立台账不得分，登记不全缺1项扣0.5分', '1.检查是否建立事故隐患排查台账(查矿事故隐患台账里名称、地点、时间、治理单位及负责人)；2.查台账记录与矿长或分管副矿长组织的月度或旬排查内容是否一致(查台账登记内容来源)', '1.矿月度事故隐患排查记录;2.分管领导旬事故隐患记录', '1.事故隐患排查台账为建立不得分；2.事故隐患排查台账漏项，扣0.5分', 3, NULL, 1, 95, 0, 1);
INSERT INTO `stdchk_item` VALUES (27, 2, 23, '事故隐患排查治理/事故隐患排查/登记上报', '2.2.3(2)', '2．排查发现重大事故隐患后，及时向当地煤矿安全监管监察部门书面报告，并建立重大事故隐患信息档案', '查资料。不符合要求不得分', '1.排查有重大事故隐患的矿井是否落实上报，是否有上报文件等;2.查制度中是否有上报条款;3.查月度、旬度台账是否有重大隐患;4.查看是否有重大事故隐患信息档案', '1.事故隐患排查制度;2.重大事故隐患排查档案及上报上级告知书;3.月度、旬度台账', '1.制度中未明确相应内容、发现台账中有重大事故隐患且不能提供上报文件等材料，不得分;2.发现有重大事故隐患，未建立信息档案的，不得分', 2, NULL, 1, 95, 0, 1);
INSERT INTO `stdchk_item` VALUES (28, 1, 25, '事故隐患排查治理/事故隐患治理/分级治理', '2.3.1(1)', '1．事故隐患治理符合责任、措施、资金、时限、预案“五落实”要求', '查资料和现场。不符合要求不得分', '1.检查事故隐患排查治理台账中“五落实”要素项是否完备(具体内容如确不需要，可填无。例如某项隐患治理不需要资金，则在“资金”要素项填无);2.查现场正在整改的隐患是否存在“五落实”不到位而影响隐患治理的情况', '1.事故隐患排查治理制度;2.1年内的事故隐患排查治理台账和专项事故隐患排查治理措施', '1.台账中“五落实”要素项不完备，缺1项扣1分;2.因“五落实”不到位，影响隐患治理的，不得分', 2, NULL, 1, 95, 0, 1);
INSERT INTO `stdchk_item` VALUES (29, 2, 25, '事故隐患排查治理/事故隐患治理/分级治理', '2.3.1(2)', '2．重大事故隐患由矿长组织制定专项治理方案，并组织实施；治理方案按规定及时上报', '查资料和现场。组织者不符合要求或未按方案组织实施不得分，治理方案未及时上报扣2分', '1.查1年内隐患台账，看是否有重大事故隐患;2.查是否按治理方案进行落实、治理;3.查组织者是否是矿长;4.查治理方案是否符合《煤矿生产安全事故隐患排查治理制度建设指南》要求', '1年内的隐患治理台账；重大事故隐患排查治理方案', '1.组织者不是矿长，不得分;2.未按方案实施不得分;3.未上报扣2分;4.发现方案明显不符合《煤矿生产安全事故隐患排查治理制度建设指南》要求的，1处扣1分', 6, NULL, 1, 95, 0, 1);
INSERT INTO `stdchk_item` VALUES (30, 3, 25, '事故隐患排查治理/事故隐患治理/分级治理', '2.3.1(3)', '3．不能立即治理完成的事故隐患，由治理责任单位（部门）主要责任人按照治理方案组织实施', '查资料和现场。组织者不符合要求或未按方案组织实施不得分', '检查月度事故隐患排查治理台账，对不能立即治理完成的隐患是否均有治理方案并组织', '事故隐患排查治理台账', '发现隐患组织实施人不符合要求或未按照方案组织实施不得分', 4, NULL, 1, 95, 0, 1);
INSERT INTO `stdchk_item` VALUES (31, 4, 25, '事故隐患排查治理/事故隐患治理/分级治理', '2.3.1(4)', '4．能够立即治理完成的事故隐患，当班采取措施，及时治理消除，并做好记录', '查资料和现场。当班未采取措施或未及时治理不得分，不做记录扣3分，记录不全1处扣0.5分', '现场检查当班区队班组、安监人员现场排查记录', '现场事故隐患排查治理记录', '1.当班未采取措施或未及时治理不得分，不做记录扣3分;2.记录不全1处扣0.5分', 5, NULL, 1, 95, 0, 1);
INSERT INTO `stdchk_item` VALUES (32, 1, 26, '事故隐患排查治理/事故隐患治理/安全措施', '2.3.2(1)', '1．事故隐患治理有安全技术措施，并落实到位', '查资料和现场。没有措施、措施不落实不得分', '1.查隐患治理台账等资料是否有安全技术措施并落实:2对照措施查正在整改的隐患，看措施是否落实', '1.事故隐患排查治理台账;2.相关安全技术措施', '1.没有措施、措施不落实不得分:2.措施存在重大缺陷或明显违反《煤矿安全规程》规定，1处扣0.5分，扣到2分为止', 4, NULL, 1, 95, 0, 1);
INSERT INTO `stdchk_item` VALUES (33, 2, 26, '事故隐患排查治理/事故隐患治理/安全措施', '2.3.2(2)', '2．对治理过程危险性较大的事故隐患，治理过程中现场有专人指挥，并设置警示标识；安检员现场监督', '查资料和现场。现场没有专人指挥不得分、未设置警示标识扣1分、没有安检员监督扣1分', '1.从事故隐患排查治理台账抽选危险性较大的事故隐患，查看安全技术措施，是否有专人指挥、安检员现场监督，可通过人员定位系统查看指挥专人和安检员是否真实在现场;2.对治理中的危险性较大隐患，现场查看是否按要求治理', '1.事故隐患排查治理台账; 2.相关安全技术措施', '1.对于可能危及治理人员及接近治理区人员安全(如爆炸、人员坠落、坠物、冒顶、电击、机械伤人等)的隐患未设置警戒的区域，现场无技术措施，扣1分；2.现场没有专人指挥不得分，未设置警示标识扣1分，没有安检员监督扣1分', 4, NULL, 1, 95, 0, 1);
INSERT INTO `stdchk_item` VALUES (34, 1, 28, '事故隐患排查治理/监督管理/治理督办', '2.4.1(1)', '1．事故隐患治理督办的责任单位（部门）和责任人员明确；2．对未按规定完成治理的事故隐患，由上一层级单位（部门）和人员实施督办；3．挂牌督办的重大事故隐患，治理责任单位（部门）及时记录治理情况和工作进展，并按规定上报', '查资料。督办责任不明确、不落实1次扣2分，未实行提级督办1次扣2分，未及时记录或上报1次扣2分', '1.查隐患排查治理制度是否有相应规定;2.查事故隐患治理台账，逾期未完成的隐患是否提级督办;3.查是否有挂牌督办的重大事故隐患', '1.隐患排查治理制度;2.事故隐患治理台账及制度等相关资料', '督办责任不明确、不落实1次扣2分，未实行提级督办1次扣2分，未及时记录或上报1次扣2分', 7, NULL, 1, 95, 0, 1);
INSERT INTO `stdchk_item` VALUES (35, 1, 29, '事故隐患排查治理/监督管理/验收销号', '2.4.2(1)', '1．煤矿自行排查发现的事故隐患完成治理后，由验收责任单位（部门）负责验收，验收合格后予以销号', '查资料和现场。未进行验收不得分，验收单位不符合要求扣2分，验收不合格即销号的不得分', '查矿事故隐患排查治理台账是否按要求执行', '事故隐患排查治理台账', '未进行验收不得分，验收单位不符合要求扣2分，验收不合格就销号的不得分', 4, NULL, 1, 95, 0, 1);
INSERT INTO `stdchk_item` VALUES (36, 2, 29, '事故隐患排查治理/监督管理/验收销号', '2.4.2(2)', '2．负有煤矿安全监管职责的部门和煤矿安全监察机构检查发现的事故隐患，完成治理后，书面报告发现部门或其委托部门（单位）', '查资料和现场。未按规定报告不得分', '检查上级检查的事故隐患告知书及上报完成治理材料', '事故隐患排查治理书面报告和上级检查记录', '1.未按规定报告不得分;2.内部未闭环管理，无闭环程序的，可提建议(暂不扣分)', 4, NULL, 1, 95, 0, 1);
INSERT INTO `stdchk_item` VALUES (37, 1, 30, '事故隐患排查治理/监督管理/公示监督', '2.4.3(1)', '1．每月向从业人员通报事故隐患分布、治理进展情况；2．及时在井口（露天煤矿交接班室）或其他显著位置公示重大事故隐患的地点、主要内容、治理时限、责任人、停产停工范围；3．建立事故隐患举报奖励制度，公布事故隐患举报电话，接受从业人员和社会监督', '查资料和现场。未定期通报、未及时公告扣2分，通报和公告内容每缺1项扣1分，未设立举报电话扣2分，接到举报未核查或核实后未进行奖励扣2分', '1.查看有关通报资料:2.查看事故隐惠举报制度;3.查看举报电话公示情况:4.随机抽问煤矿职工是否知道事故隐惠举报电话，是否曾有举报事故隐患但未核查和奖励的情况', '公示、公告、通报、会议纪要等', '1.未定期通报(通报不限形式)、未及时公告扣2分:2.通报和公告内容每缺1项扣1分:3.未建立制度、公布举报电话扣2分，接到举报未核查或核实后未进行奖励扣2分:4.没有重大隐患的煤矿，需要在公示牌注上无重大事故隐患(建议项，暂不扣分)', 5, NULL, 1, 95, 0, 1);
INSERT INTO `stdchk_item` VALUES (38, 1, 32, '事故隐患排查治理/保障措施/信息管理', '2.5.1(1)', '采用信息化管理手段，实现对事故隐患排查治理记录统计、过程跟踪、逾期报警、信息上报的信息化管理', '查资料和现场。未采取信息化手段不得分，管理内容缺1项扣1分', '查看所使用的信息化手段，是否能实现对事故隐患 排查治理记录统计、过程跟踪、逾期报警、信息上报的信息化管理', NULL, '1.未采用信息化手段管理不得分:2.未实现对事故隐患排查治理记录统计、过程跟踪、逾期报警、信息上报功能缺1项扣1分(使用电子邮件、社交软件等通信功能，能实现上述功 能的不扣分)', 3, NULL, 1, 95, 0, 1);
INSERT INTO `stdchk_item` VALUES (39, 1, 33, '事故隐患排查治理/保障措施/改进完善', '2.5.2(1)', '矿长每月组织召开事故隐患治理会议，对一般事故隐患、重大事故隐患的治理情况进行通报，分析事故隐患产生的原因，提出加强事故隐患排查治理的措施，并编制月度事故隐患统计分析报告', '查资料。未召开会议定期通报、未编制报告不得分，报告内容不符合要求扣2分', '1.查月度事故隐患排查治理分析会议纪要;查统计分析报告;2.查是否深入分析隐患出现的原因，提出改进措施;是否有相同的隐患重复出现', '1.矿月度事故隐患排查治理分析会议纪要;2.月度统计分析报告', '1.矿长未按要求召开会议定期通报或未编制报告不得分;2.报告内容不符合要求(未统计分析当月隐患出现的原因、未提出改进措施)扣2分', 3, NULL, 1, 95, 0, 1);
INSERT INTO `stdchk_item` VALUES (40, 1, 34, '事故隐患排查治理/保障措施/资金保障', '2.5.3(1)', '建立安全生产费用提取、使用制度。事故隐患排查治理工作资金有保障', '查资料和现场。未建立并执行制度不得分，资金无保障扣2分', '1.查是否建立安全费用提取制度;2.查隐患排查治理工作有无经费预算，是否能够保障隐患排查治理工作运行', '安全生产费用提取制度', '1.未建立并执行制度不得分;2.没有经费预算或预算资金不能够保障工作运行的，扣2分', 3, NULL, 1, 95, 0, 1);
INSERT INTO `stdchk_item` VALUES (41, 1, 35, '事故隐患排查治理/保障措施/专项培训', '2.5.4(1)', '每年至少组织安全管理技术人员进行1次事故隐患排查治理方面的专项培训', '查资料。未按要求开展培训不得分', '查培训责任部门是否有专项培训相关内容，如培训计划、教案、签到表等', '矿年度培训计划和培训资料', '未按要求开展培训不得分', 3, NULL, 1, 95, 0, 1);
INSERT INTO `stdchk_item` VALUES (42, 1, 36, '事故隐患排查治理/保障措施/考核管理', '2.5.5(1)', '1．建立日常检查制度，对事故隐患排查治理工作实施情况开展经常性检查；2．检查结果纳入工作绩效考核', '查资料。未建立制度、未执行制度不得分，检查结果未运用扣1分', '1.查看是否建立日常检查制度;2.查罚款单，对照查相应的工资表，看是不是罚款转到相应工资单里', '1.单位日常检查制度;2.月度绩效考核台账;3.罚款统计台账', '1.制度未建立不得分;2.未执行不得分;3. 检查结果未与绩效考核挂钩，发现1人次扣1分', 3, NULL, 1, 95, 0, 1);
INSERT INTO `stdchk_item` VALUES (43, 1, 39, '通风/通风系统/系统管理', '3.1.1(1)', '1.全矿井、一翼或者一个水平通风系统改变时，编制通风设计及安全技术措施，经企业技术负责人审批；巷道贯通前应当制定贯通专项措施，经矿总工程师审批；井下爆炸物品库、充电硐室、采区变电所、实现采区变电所功能的中央变电所有独立的通风系统', '查资料和现场。改变通风系统（巷道贯通）无审批措施的扣10分，其他1处不符合要求扣5分', '1.查通风系统图等图纸资料，查看通风系统、通风方法、通风方式、生产水平、生产布局、采捆工作面个数，检查需要独立通风硐室个数;2.查全矿井、一翼或者一个水平通风系统改变时通风技术及安全技术措施是否经矿上级企业的技术负责人审批;3.查巷道贯通措施是否包括巷道贯通通知单、施工单位贯通措施(捆进方式、瓦斯检查等)、通风部门制定的贯通通风系统风流调整安全技术措施;所有措施审批是否符合规定要求;4.现场检查进回风巷道、采区、一翼、矿井总回风巷、回风井筒等;查矿井通风系统图、网络图、采掘工程平面图与现场实际是否一致', '1.与标准对应材料、图纸;2.贯通措施计划配风量、实际风量数据3.贯通后受影响区域测风记录;4.冬季、夏季井下、硐室、工作场所温度记录', '1.改变通风系统(巷道贯通)无审批措施的1次扣10分;措施内容不全，缺了影响到矿上安全的措施内容的扣5分;2.由非上级企业技术负责人审批的扣5分;3.措施针对性不强，影响区域不清楚的，扣5分;4.贯通措施(必须有贯通通知单、施工单位措施、通风部门措施);缺1项扣1分，扣到5分为止;5.应实现未实现独立通风系统的1处扣5分', 20, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (44, 2, 39, '通风/通风系统/系统管理', '3.1.1(2)', '2.井下没有违反《煤矿安全规程》规定的扩散通风、采空区通风和利用局部通风机通风的采煤工作面；对于允许布置的串联通风，制定安全技术措施，其中开拓新水平和准备新采区的开掘巷道的回风引入生产水平的进风中的安全技术措施，经企业技术负责人审批，其他串联通风的安全技术措施，经矿总工程师审批', '查现场和资料。不符合要求1处扣10分', '1.井下所有工作面是否违反《煤矿安全规程》规定的扩散通风(超过6m)、采空区通风和利用局部通风机通风的采煤工作面;2.对于允许串联通风的，一是查措施有无，二是查措施审批及内容是否齐全;3.矿井是否有新水平和准备新采区的开掘巷道的回风引入生产水平的进风中的安全技术措施，是否经企业技术负责人审批;其他串联通风的安全技术措施，是否经矿总工程师审批;4.查安全技术措施是否完善', '与标准对应材料、图纸', '1.有处超过6m的扩散通风扣10分;2.无串联通风措施，扣10分;3.安全技术措施没有审批或措施审批不符合规定，扣10分4.安全技术措施存在重大缺陷或明显违反《煤矿安全规程》1处扣1分，扣到5分为止', 20, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (45, 3, 39, '通风/通风系统/系统管理', '3.1.1(3)', '3.采区专用回风巷不用于运输、安设电气设备，突出区不行人；专用回风巷道维修时制定专项措施，经矿总工程师审批', '查现场和资料。不符合要求1处扣2分', '1.查现场专用回风巷道有无行人、运输机电设备，巷道断面是否符合规定;2.查专用回风巷道维修时是否制定专项措施，是否经矿总工程师审批;3.查矿井瓦斯等级、自然发火等级及是否煤层群开采，是否联合布置', '与标准对应材料、图纸', '不符合要求1处扣2分', 5, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (46, 4, 39, '通风/通风系统/系统管理', '3.1.1(4)', '4.装有主要通风机的回风井口的防爆门符合规定，每6个月检查维修1次；每季度至少检查1次反风设施；制定年度全矿性反风技术方案，按规定审批，实施有总结报告，并达到反风效果', '查资料和现场。未进行反风演习扣5分，其他1处不符合要求扣2分', '1.查现场主要通风机房:一是主要通风机运转情况及记录;二是反风演习操作步骤图;三是防爆门检修情况;2.查主要通风机、反风设施检查、维修记录;反风设施包括地面(包括风机与反风相关闸门)、井下(含安全出口反向风门)、防爆门稳固设施等;3.查反风演习计划及总结报告:①风机是否实施反风;②风机反风时间是否大于2h;③反风后的矿井风量是否符合规定要求', '1.反风系统图;2.主要通风机和反风设施检查维修记录;3.年度全矿性反风技术方案;4.反风演习计划;5.反风演习总结报告', '1.未进行反风演习扣5分;2.没有技术方案扣2分;3.没有总结报告扣2分;4.没有防爆门检修记录或反风设施检查记录扣2分;5.技术方案或总结报告内容不符合要求扣0.5分，扣到2分为止;6.反风后的矿井风量不符合要求扣2分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (47, 1, 40, '通风/通风系统/风量配置', '3.1.2(1)', '1.新安装的主要通风机投入使用前，进行1次通风机性能测定和试运转工作，投入使用后每5年至少进行1次性能测定；矿井通风阻力测定符合《煤矿安全规程》规定', '查资料。通风机性能或者通风阻力未测定的不得分，其他1处不符合要求扣1分', '1.查新安装的主要通风机性能测定报告中性能测定曲线是否至少有3条(有困难时期、容易时期、现状了个时期特性曲线);2.查在用通风机5年1次的性能测定报告中性能测定曲线是否至少有3条(有困难时期、容易时期、现状3个时期特性曲线);3.查性能测定报告是否按照《煤矿在用主通风机系统安全检测检验规范》(AQ1011-2005)加盖安全检验专用章;4.矿井通风阻力测定报告是否符合《煤矿安全规程》规定(新井投产前必须进行1次矿井通风阻力测定，以后每3年至少测定1次。生产矿井转入新水平生产、改变一翼或者全矿井通风系统后，必须重新进行矿井通风阻力测定)', '1.主要通风机性能测定报告;2.通风阻力测定报告', '1.通风机性能未测定扣10分;性能测定报告没有按照《煤矿在用主通风机系统安全检测检验规范》(AQ1011- 2005) 加盖安全检验专用章不得分;2.没有按规定开展通风阻力测定扣10分;3.性能测定报告曲线低于3条扣1分;4.其他1处不符合要求扣1分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (48, 2, 40, '通风/通风系统/风量配置', '3.1.2(2)', '2.矿井每年进行1次通风能力核定；每10天至少进行1次井下全面测风，井下各硐室和巷道的供风量满足计算所需风量', '查资料和现场。未进行通风能力核定不得分，其他1处不符合要求扣5分', '1.查矿井测风记录，是否满足周期要求;2.查矿井通风能力核定报告;3.查井下测风站、点；测风记录、通风旬报表、月报表;4.现场选点测风并计算是否符合计算风量要求', '1.矿井通风系统图;2.矿井通风能力核定报告;3.矿井测风记录', '1.未进行矿井通风能力核定扣10分;2.矿井通风能力核定报告中有明显错误影响核定结果的，1处扣1分，扣到5分为止3.井下各硐室和巷道的供风量不满足计算所需风量，1处扣5分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (49, 3, 40, '通风/通风系统/风量配置', '3.1.2(3)', '3.矿井有效风量率不低于85％；矿井外部漏风率每年至少测定1次，外部漏风率在无提升设备时不得超过5％，有提升设备时不得超过15％', '查资料。未测定扣5分，有效风量率每低、外部漏风率每高1个百分点扣1分', '1.查通风甸报表、月报表、风量配备计划;2.查各工作面及粥室有效风量;3.查矿井有效风量率，外部漏风率是否符合规定', '1.通风报表2.风量配备计划;3.外部漏风率测定报', '1.未测定扣5分;2.有效风量率每低1%、外部漏风率每高1%扣1分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (50, 4, 40, '通风/通风系统/风量配置', '3.1.2(4)', '4.采煤工作面进、回风巷实际断面不小于设计断面的2/3；其他通风巷道实际断面不小于设计断面的4/5；矿井通风系统的阻力符合AQ1028规定；矿井内各地点风速符合《煤矿安全规程》规定', '查现场和资料。巷道断面1处（长度按5m计）不符或者阻力超规定扣2分；风速不符合要求1处扣5分', '1.查现场和图纸、采据作业规程;2.查采掘工作面进、回风巷道实际断面是否小于设计断面的2/3;其他通风巷道实际断面是否小于设计断面的4/5;3.查用风地点风速是否符合《煤矿安全规程》规定', '1.采掘工程平面图，通风系统图、通风网络图、通风立体示意图;2.采据工作面作业规程;3.矿井通风阻力报告;4.测风记录', '1.断面不符合规定，每处扣2分;2.矿井通风系统阻力超过规定扣2分;3.风速不符合要求1处扣5分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (51, 5, 40, '通风/通风系统/风量配置', '3.1.2(5)', '5.矿井主要通风机安设监测系统，能够实时准确监测风机运行状态、风量、风压等参数', '查现场。未安监测系统的不得分，其他1处不符合要求扣1分', '1.查监控终端;2.查主要通风机房监测系统是否能够实时准确监测风机负压、风量、运行状态，是否有现场显示，是否上传安全监控系统主机，参数显示是否准3.查主要通风机房安装的静压计、速压计、全压计显示是否准确', NULL, '1.未安监测系统扣5分;2.不能准确测定扣1分，其他1处不符合要求扣1分', 5, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (52, 1, 42, '通风/局部通风/装备措施', '3.2.1(1)', '1.掘进通风方式符合《煤矿安全规程》规定，采用局部通风机供风的掘进巷道应安设同等能力的备用局部通风机，实现自动切换。局部通风机的安装、使用符合《煤矿安全规程》规定，实行挂牌管理，不发生循环风；不出现无计划停风，有计划停风前制定专项通风安全技术措施', '查现场和资料。1处发生循环风不得分；无计划停风1次扣10分；其他1处不符合要求扣2分', '1.查现场和掘进工作面作业规程等资料;2.查是否安装2台同等能力的通风机并能自动切换，查自动切换记录，现场实操1次3.查局都通风机是否有循环风:4.抽查现场监控系统看是否存在无计划停风;有计划停风是否有专项通风安全技术措施;5.查是否挂牌管理，管理牌内容是否按测风周期及时更新', '1.采掘工程平面图、通风系统围2.掘进工作面作业规程;3.停风专项通风安全技术措施', '1.1处局部通风机循环风扣35分；2.无计划停风1次扣10分；3.其他1处不符合要求扣2分', 35, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (53, 2, 42, '通风/局部通风/装备措施', '3.2.1(2)', '2.局部通风机设备齐全，装有消音器（低噪声局部通风机和除尘风机除外），吸风口有风罩和整流器，高压部位有衬垫；局部通风机及其启动装置安设在进风巷道中，地点距回风口大于10m，且10m范围内巷道支护完好，无淋水、积水、淤泥和杂物；局部通风机离巷道底板高度不小于0.3m', '查现场。不符合要求1处扣2分', '1.现场查安装位置;2.查通风机完好情况;3.查安装高度;4.查安装地点巷道及环境情况', NULL, '不符合要求，1处扣2分', 15, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (54, 1, 43, '通风/局部通风/风筒敷设', '3.2.2(1)', '1.风筒末端到工作面的距离和自动切换的交叉风筒接头的规格、安设标准符合作业规程规定', '查现场和资料。不符合要求1处扣5分', '1.查风筒末端到工作面的距离是否符合作业规程规定;2.查自动切换的交叉风简接头的规格、安设情况是否符合作业规程规定;3.查风筒出口风量是否符合作业规程规定', '掘进工作面作业规程', '不符合要求，1处扣5分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (55, 2, 43, '通风/局部通风/风筒敷设', '3.2.2(2)', '2.使用抗静电、阻燃风筒，实行编号管理。风筒接头严密，无破口（末端20m除外），无反接头；软质风筒接头反压边，硬质风筒接头加垫、螺钉紧固', '查现场。使用非抗静电、非阻燃风筒不得分；其他1处不符合要求扣0.5分', '1.查现场风筒编号管理、接头、破口、反压边等情况;2.查硬质、软制风筒接头情况;3.查风简抗静电、抗阻燃情况', '风筒有关合格证件', '1.风筒材质不符合要求扣15分;2.其他1处不符合要求扣0.5分', 15, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (56, 3, 43, '通风/局部通风/风筒敷设', '3.2.2(3)', '3.风筒吊挂平、直、稳，软质风筒逢环必挂，硬质风筒每节至少吊挂2处；风筒不被摩擦、挤压', '查现场。不符合要求1处扣0.5分', '查现场风简吊挂情况是否满足标准要求', NULL, '1处不符合要求扣0.5分', 15, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (57, 4, 43, '通风/局部通风/风筒敷设', '3.2.2(4)', '4.风筒拐弯处用弯头或者骨架风筒缓慢拐弯，不拐死弯；异径风筒接头采用过渡节，无花接', '查现场。不符合要求1处扣1分', '1.查现场风筒拐弯处是否拐死弯;2.查异径风筒接头是否有过渡节，是否花接', NULL, '1处不符合要求扣1分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (58, 1, 45, '通风/通风设施/设施管理', '3.3.1(1)', '1.及时构筑通风设施（指永久密闭、风门、风窗和风桥），设施墙（桥）体采用不燃性材料构筑，其厚度不小于0.5m（防突风门、风窗墙体不小于0.8m），严密不漏风', '查现场。应建未建或者构筑不及时不得分；其他1处不符合要求扣10分', '1.查构筑通风设施的材料;2.查构筑厚度(0.5 m、0.8m);3.查通风设施检查及维修记录;4.离构筑物10cm左右用耳听、手摸，感觉有无漏风情况', '1.通风系统图；2.通风设施检查及维修记录', '1.通风设施构筑不及时、应构筑通风设施未构筑扣15分;2.其他1处不符合要求扣10分;3.发现漏风扣10分', 15, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (59, 2, 45, '通风/通风设施/设施管理', '3.3.1(2)', '2.密闭、风门、风窗墙体周边按规定掏槽，墙体与煤岩接实，四周有不少于0.1m的裙边，周边及围岩不漏风；墙面平整、无裂缝、重缝和空缝，并进行勾缝或者抹面或者喷浆，抹面的墙面1m2内凸凹深度不大于10mm', '查现场。不符合要求1处扣5分', '1.查密闭管理台账，并查看现场掏槽是否符合规定;2.查四周不少于0.1 m的裙边:3.查周边围岩漏风情况;4.查抹面的平整度', '密闭、风门、风窗管理台账', '1处不符合要求扣5分', 7, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (60, 3, 45, '通风/通风设施/设施管理', '3.3.1(3)', '3.设施5m范围内支护完好，无片帮、漏顶、杂物、积水和淤泥', '查现场。1处不符合要求不得分', '查现场5m范围内支护是否宪好,是否有片帮、漏顶、杂物、积水(深度大于100mm)和淤泥', '密闭、风门、风窗、风桥管理台账', '1处不符合要求扣4分', 4, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (61, 4, 45, '通风/通风设施/设施管理', '3.3.1(4)', '4.设施统一编号，每道设施有规格统一的施工说明及检查维护记录牌', '查现场。1处不符合要求不得分', '查现场设施是否统一编号，每道设施有无规格统一的(矿方自己统一就行)施工说明及检查维护记录', NULL, '1处不符合要求扣4分', 4, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (62, 1, 46, '通风/通风设施/密闭', '3.3.2(1)', '1.密闭位置距全风压巷道口不大于5m，设有规格统一的瓦斯检查牌板和警标，距巷道口大于2m的设置栅栏；密闭前无瓦斯积聚。所有导电体在密闭处断开（在用的管路采取绝缘措施处理除外）', '查现场。不符合要求1处扣5分', '1.查现场密闭位置距全风压巷道口是否大于5m;2.查距全风压巷道口大于2m的是否设置栅栏;3.查所有导电体在密闭处是否断开:4.查是否设有统一规格的瓦斯检查牌板和警标', '密闭管理台账', '1.不设置栅栏扣5分，栅栏设置不合格1处扣1分，扣到2分为止；2.其他1处不符合要求扣5分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (63, 2, 46, '通风/通风设施/密闭', '3.3.2(2)', '2.密闭内有水时设有反水池或者反水管，采空区密闭设有观测孔、措施孔，且孔口设置阀门或者带有水封结构', '查现场。不符合要求1处扣5分', '现场查密闭内有水时是否设有反水池或者反水管，采空区密闭是否设有观测孔、措施孔，且孔口是否设置阀门或者带有水封结构', NULL, '1处不符合要求扣5分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (64, 1, 47, '通风/通风设施/风门风窗', '3.3.3(1)', '1.每组风门不少于2道，其间距不小于5m（通车风门间距不小于1列车长度），主要进、回风巷之间的联络巷设具有反向功能的风门，其数量不少于2道；通车风门按规定设置和管理，并有保护风门及人员的安全措施', '查现场。不符合要求1处扣5分', '1.查矿井通风系统图和风门、风窗管理台账;2.查主要风门、风窗分布情况:3.查每组风门是否不少于2道，问距不小于5m (通车风门间距不小于1列车长度);主要进，回风巷之间的联络巷是否设有反向功能的风门，数量不少于2道;4.查通车风门是否按规定设置和管理，并有保护风门及人员的安全措施', '1.矿井通风系统图;2.风门、风窗管理台账3.安全措施', '1处不符合要求扣5分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (65, 2, 47, '通风/通风设施/风门风窗', '3.3.3(2)', '2.风门能自动关闭，并连锁，使2道风门不能同时打开；门框包边沿口，有衬垫，四周接触严密，门扇平整不漏风；风窗有可调控装置，调节可靠', '查现场。不符合要求1处扣5分', '1.查风门能否自动关闭:2.查风门能否连锁,两道风门是香能同时打开，以1人能否通过为标准;3.查风门质量', '1.矿井通风系统图；2.风门、风窗管理台账；3.安全措施', '1处不符合要求扣5分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (66, 3, 47, '通风/通风设施/风门风窗', '3.3.3(3)', '3.风门、风窗水沟处设有反水池或者挡风帘，轨道巷通车风门设有底槛，电缆、管路孔堵严，风筒穿过风门（风窗）墙体时，在墙上安装与胶质风筒直径匹配的硬质风筒', '查现场。不符合要求1处扣5分', '现场查风门、风窗水沟处是否设有反水地或者挡风帘，轨道巷通车风门是否设有底槛，电缆、管路孔是否堵严，风筒穿过风门(风窗)墙体时，是否在墙上安装与胶质风简直径匹配的硬质风简', NULL, '1处不符合要求扣5分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (67, 1, 48, '通风/通风设施/风桥', '3.3.4(1)', '1.风桥两端接口严密，四周为实帮、实底，用混凝土浇灌填实；桥面规整不漏风', '查现场。不符合要求1处扣5分', '查现场风桥两端接口是否严密，四周是否为实帮、实庭，是否用混凝土浇灌填实，桥面是否规整不漏风', '风桥管理台账', '1处不符合要求扣5分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (68, 2, 48, '通风/通风设施/风桥', '3.3.4(2)', '2.风桥通风断面不小于原巷道断面的4/5，呈流线型，坡度小于30°；风桥上、下不安设风门、调节风窗等', '查现场。不符合要求1处扣5分', '查现场风桥通风断面是否大于原巷道断面的4/5， 是否符合呈流线型、坡度小于30°的要求，风桥上、下是否安设风门、调节风窗等', '风桥管理台账', '1处不符合要求扣5分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (69, 1, 50, '通风/瓦斯管理/鉴定及措施', '3.4.1(1)', '1.按《煤矿安全规程》规定进行煤层瓦斯含量、瓦斯压\n力等参数测定和矿井瓦斯等级鉴定及瓦斯涌出量测定', '查资料。未鉴定、测定不得分', '查资料，是否按《煤矿安全规程》第一百七十条规定进行煤层瓦斯含量、瓦斯压力等参数测定和矿井瓦斯等级鉴定及瓦斯涌出量测定', '煤屋瓦斯含量、瓦斯压力等参数测定和矿井瓦斯等级鉴定及瓦斯涌出量测定相关报告', '未鉴定、为测定不得分；瓦斯含量测定单位无资质不得分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (70, 2, 50, '通风/瓦斯管理/鉴定及措施', '3.4.1(2)', '2.编制年度瓦斯治理技术方案及安全技术措施，并严格落实 ', '查资料。未编制的，扣\n5分；其他不符合要求1处扣1分', '1.查是否编制年度瓦斯治理技术方案及安全技术措施，并严格落实;2.查高瓦斯和突出煤层的掘进工作面是否编制瓦斯治理专项措施，是否在瓦斯治理技术方案及安全技术措施中体现', '年度瓦斯治理技术方案及安全技术措施', '1.为编制1项扣5分；2.其他一处不符合要求扣1分', 15, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (71, 1, 51, '通风/瓦斯管理/瓦斯检查', '3.4.2(1)', '1.矿长、总工程师、爆破工、采掘区队长、通风区队长、工程技术人员、班长、流动电钳工、安全监测工等下井时，携带便携式甲烷检测报警仪。瓦斯检查工下井时携带便携式甲烷检测报警仪和光学瓦斯检测仪', '查现场或者资料。不符合要求1处扣2分', '1.查近期发放记录和资料，查现场;2.查矿长、总工程师、爆破工、采掘区队长、通风区队长、工程技术人员、班长、流动电钳工、安全监测工等下井时，是否携带便携式甲烷检测报警仪:3.查瓦斯检查工下井时是否携带便携式甲烷检测报警仪和光学瓦斯检测仪;4.查班长是否携带便携式甲烷检测报警仪', '便携式甲烷检测报警仪发放台账', '1.有关人员未携带1人扣2分，携带甲烷检测仪但未开机1人扣2分;2.光学瓦斯检测仪漏气、瓦斯检查工不能正确操作光学瓦斯检测仪1人次扣2分;3.其他不符合要求1处扣2分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (72, 2, 51, '通风/瓦斯管理/瓦斯检查', '3.4.2(2)', '2.瓦斯检查符合《煤矿安全规程》规定；瓦斯检查工在井下指定地点交接班，有记录', '查资料和现场。不符合要求1处扣5分', '1.查资料和现场:①到通风区(科)值班室检查、通风值班记录:②查瓦斯检查工每次瓦斯检查汇报记录:2.查矿是否每月制定井下瓦斯测点设置计划;瓦斯检查地区个数;3.查瓦斯检查次数范围是否符合《煤矿安全规程》第一百七十五条、第一百八十条规定', '1.通风值班记录;2.瓦斯检查工每次瓦斯检查汇报记录;3.通风瓦斯日报', '1.无月度测点设置计划、测点设置不符合《煤矿安全规程》规定、测点检查周期不符合规定，1处扣5；2.其他1处不符合要求扣5分', 15, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (73, 3, 51, '通风/瓦斯管理/瓦斯检查', '3.4.2(3)', '3.瓦斯检查做到井下记录牌、瓦斯检查手册、瓦斯检查班报（台账）“三对口”；瓦斯检查日报及时上报矿长、总工程师签字，并有记录', '查资料和现场。不符合要求1处扣1分', '1.查资料和现场，瓦斯检查是否做到井下记录牌、瓦斯检查手册、瓦斯检查班报(台账)“三对口”;2.查瓦斯检查日报是否及时上报矿长、总工程师签字，并有记录', '1.瓦斯检查手册;2.瓦斯检查班报;3.瓦斯检查日报', '1处不符合要求扣1分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (74, 1, 52, '通风/瓦斯管理/现场管理\n\n', '3.4.3(1)', '1.采掘工作面及其他地点的瓦斯浓度符合《煤矿安全规程》规定；瓦斯超限立即切断电源，并撤出人员，查明瓦斯超限原因，落实防治措施', '查资料和现场。瓦斯超限1次扣5分；其他1处不符合要求扣1分', '1.查资料和现场，采掘工作面及其他地点瓦斯浓度是否符合《煤矿安全规程》第百七十一条，第一百七十二条，第一百七十三条、第一百七十四条、第一百七十五条、第一百七十六条的规定;2.查瓦斯超限后是否按规定采取措施', '1.查监控报表；2.调度跟踪记录', '1.瓦斯超限1次扣5分；2.其他1处不符合要求扣1分', 15, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (75, 2, 52, '通风/瓦斯管理/现场管理\n\n', '3.4.3(2)', '2.临时停风地点停止作业、切断电源、撤出人员、设置栅栏和警示标志；长期停风区在24h内封闭完毕。停风区内甲烷或者二氧化碳浓度达到3.0%或者其他有害气体浓度超过《煤矿安全规程》规定不立即处理时，在24h内予以封闭，并切断通往封闭区的管路、轨道和电缆等导电物体', '查资料和现场。未按规定执行1项扣10分', '1.查资料和现场，临时停风地点是否停止作业、切新电源、撤出人员、设置栅栏和警示标志;2.查长期停风区是否在24h内封闭完毕;3.查停风区内甲烷或者二氧化碳浓度是否达到3.0%或者其他有害气体浓度超过《煤矿安全规程》规定不立即处理时，是否在24h内予以封闭，并切断通往封闭区的管路、轨道和电缆等导电物体;4.现场抽查有无临时停风地点，是否按要求采取措施', '临时停风', '未按规定执行，1处扣10分', 15, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (76, 3, 52, '通风/瓦斯管理/现场管理\n\n', '3.4.3(3)', '3.瓦斯排放按规定编制专项措施，经矿总工程师批准，并严格执行，且有记录；采煤工作面不使用局部通风机稀释瓦斯', '查资料。无措施或者未执行不得分；其他1处不符合要求扣5分', '1.查瓦斯排放是否按规定编制专项措施，是否经矿总工程师批准，并严格执行，且有记录;2.查采煤工作面是否使用局部通风机稀释瓦斯', '瓦斯排放按规定编制转向措施', '1.无专项措施不得分；2.限量、断电、撤人（警戒）这三个条件缺1处扣5分；3.其他1处不符合要求扣5分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (77, 1, 54, '通风/突出防治/突出管理', '3.5.1(1)', '1.编制矿井、水平、采区及井巷揭穿突出煤层的防突专项设计，经企业技术负责人审批，并严格执行', '查资料和现场。未编审设计不得分；执行不严格1处扣15分', '1.查资料和现场，瓦斯地质图是否在突出区域;2.查是否编制矿井、水平、采区及井巷揭穿突出煤层的防突专项设计，是否经企业技术负责人审批，并严格执行', '1.瓦斯地质图；2.防突专项设计', '1.未编审设计不得分;2.执行不严格1处扣15分', 25, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (78, 2, 54, '通风/突出防治/突出管理', '3.5.1(2)', '2.区域预测结果、区域防突措施、保护效果检验、保护范围考察结果经企业技术负责人审批；预抽煤层瓦斯区域防突措施效果检验及区域验证结果经矿总工程师审批，按预测、检验结果，采取相应防突措施', '查现场和资料。未审批不得分；执行不严格1处扣15分', '1.查现场和资料，区域预测结果、区域防突措施、保护效果检验、保护范围考察结果是否经企业技术负责人审批;2.查预抽煤层瓦斯区域防突措施效果检验及区域验证结果是否经矿总工程师审批，是否按预测、检验结果，采取相应防突措施', '区域预测结果、区域防突措施、保护效果检验、保护范围考察结果资料', '1.未审批不得分;2.审批明显有错误扣15分;3.执行不严格1处扣15分', 25, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (79, 3, 54, '通风/突出防治/突出管理', '3.5.1(3)', '3.突出煤层采掘工作面编制防突专项设计及安全技术措施，经矿总工程师审批，实施中及时按现场实际作出补充修改，并严格执行', '查资料和现场。未编审设计及措施或者未执行不得分；执行不严格1处扣5分', '查资料和现场，突出煤层采掘工作面是否编制防突专项设计及安全技术措施，是否经矿总工程师审批，实施中是否及时按现场实际作出补充修改，并严格执行', '1.防突专项设计；2.安全技术措施', '1.未编审设计及措施或者未执行扣25分;2.执行不严格1处扣5分', 25, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (80, 1, 55, '通风/突出防治/设备设施', '3.5.2(1)', '压风自救装置、自救器、防突风门、避难硐室等安全防护设备设施符合《防治煤与瓦斯突出规定》要求', '查现场。不符合要求1处扣2分', '查现场压风自救装置、自助器、防突风门、避难硐室等安全防护设备设施是否符合《防治没与瓦斯突出规定》要求', NULL, '不符合要求1处扣2分', 25, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (81, 1, 57, '通风/瓦斯抽采/抽采系统', '3.6.1(1)', '1.瓦斯抽采设施、抽采泵站符合《煤矿安全规程》要求', '查现场和资料。不符合要求1处扣5分', '查现场是否符合《煤矿安全规程》第一百八十二条规定，主要查地面瓦斯永久抽采泵站和井下移动泵站', NULL, '不符合要求，1处扣5分', 15, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (82, 2, 57, '通风/瓦斯抽采/抽采系统', '3.6.1(2)', '2.编制瓦斯抽采工程（包括钻场、钻孔、管路、抽采巷等）设计，并按设计施工', '查现场和资料。不符合要求1处扣2分', '查现场和施工原始记录，是否编制瓦斯抽采工程(包括钻场、钻孔、管路、抽采巷等)设计，并按设计施工', '1.施工设计2.施工原始记录', '不符合要求，1处扣2分', 15, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (83, 1, 58, '通风/瓦斯抽采/检查与管理', '3.6.2(1)', '1.对瓦斯抽采系统的瓦斯浓度、压力、流量等参数实时监测，定期人工检测比对，泵站每2h至少1次，主干、支管及抽采钻场每周至少1次，根据实际测定情况对抽采系统进行及时调节', '查资料和现场。未按规定检测核实的1次扣5分，其他1处不符合要求扣2分', '1.查资料和现场对瓦斯抽采系统的瓦斯浓度、压力、流量等参数是否实时监测，是否定期进行人工检测比对;2. 查泵站是否每2h至少进行1次人工检测对比，主干、支管及抽采钻场是否每周至少进行1次人工检测对比，根据实际测定情况对抽采系统是否进行及时调节', '1.检测报告；2.人工检测记录；3.调节记录', '1.未按规定检测比对1次扣5分；2.其他1处不符合要求扣2分', 15, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (84, 2, 58, '通风/瓦斯抽采/检查与管理', '3.6.2(2)', '2.井上下敷设的瓦斯管路，不得与带电物体接触并应当有防止砸坏管路的措施。每10天至少检查1次抽采管路系统，并有记录。抽采管路无破损、无漏气、无积水；抽采管路离地面高度不小于0.3m（采空区留管除外）', '查资料和现场。管路损坏或者与带电物体接触不得分；其他1处不符合要求扣1分', '1.查井上下敷设的瓦斯管路，是否与带电物体接触并有防止砸坏管路的措施;2.查是否每10天至少检查1次抽采管路系统，并有记录;3.查抽采管路有无破损、有无漏气，有无积水;4.查抽采管路离地面高度是否小于0.3 m(采空区留管除外)', '1.管路保护措施；2.检查记录', '1.管路损坏或者与带电物体接触不得分;2.其他1处不符合要求扣1分', 15, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (85, 3, 58, '通风/瓦斯抽采/检查与管理', '3.6.2(3)', '3.抽采钻场及钻孔设置管理牌板，数据填写及时、准确，有记录和台账', '查资料和现场。不符合要求1处扣0.5分', '查资料和现场，抽采钻场及钻孔是否设置管理牌板，数据填写是否及时、准确，是否有记录和台账', '抽采记录和台账', '不符合要求1处扣0.5分', 15, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (86, 4, 58, '通风/瓦斯抽采/检查与管理', '3.6.2(4)', '4.高瓦斯、突出矿井计划开采的煤量不超出瓦斯抽采的达标煤量，生产准备及回采煤量和抽采达标煤量保持平衡', '查资料。不符合要求不得分', '1.查高瓦斯、突出矿井计划开采的煤量是否超出瓦斯抽采的求达标煤量;2.查生产准备及回采煤量和抽采达标煤是否保持平衡', '抽采达标煤矿有关资料', '不符合要求不得分', 15, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (87, 5, 58, '通风/瓦斯抽采/检查与管理', '3.6.2(5)', '5.矿井瓦斯抽采率符合《煤矿瓦斯抽采达标暂行规定》要求', '查资料。不符合要求不得分', '查资料和现场，矿井瓦斯抽采率是否符合《煤矿瓦斯抽采达标暂行规定》要求', '瓦斯抽采台账', '不符合要求不得分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (88, 1, 60, '通风/安全监控/装备设置', '3.7.1(1)', '1.矿井安全监控系统具备“风电、甲烷电、故障”闭锁及手动控制断电闭锁功能和实时上传监控数据的功能；传感器、分站备用量不少于应配备数量的20%', '查资料和现场。系统功能不全扣5分，其他1处不符合要求扣2分', '1.查安全监控系统图、监控台账、闭锁记录;2.查矿井安全监控系统是否具备“风电、甲烷电、故障”闭锁及手动控制断电闭锁功能和实时上传监控数据的功能;3.查传感器、分站备用量是否不少于应配备数量的20%;4.故障闭锁现场操作，升井后到监控中心查时间是否对应上', '1.监控台账；2.闭锁记录；3.安全监控系统图', '1.系统功能不全扣5分；2.其他1处不符合要求扣2分', 15, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (89, 2, 60, '通风/安全监控/装备设置', '3.7.1(2)', '2.安全监控设备的种类、数量、位置、报警浓度、断电浓度、复电浓度、电缆敷设等符合《煤矿安全规程》规定，设备性能、仪器精度符合要求，系统装备实行挂牌管理', '查资料和现场。报警、断电、复电1处不符合要求扣5分；其他1处不符合要求扣2分', '查资料和井下现场各种设置，安全监控设备的种类、数量、位置、报警浓度、断电浓度、复电浓度、电缆敷设等是否符合《煤矿安全规程》规定，设备性能、仪器精度是否符合要求，系统装备是否实行挂牌管理，测点删除、增加有无批准', '安全监控设备布置图', '1.报警、断电、复电1处不符合要求扣5分；2.其他1处不符合扣2分', 15, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (90, 3, 60, '通风/安全监控/装备设置', '3.7.1(3)', '3.安全监控系统的主机双机热备，连续运行。当工作主机发生故障时，备用主机应在5min内自动投入工作。中心站设双回路供电，并配备不小于2h在线式不间断电源。中心站设备设有可靠的接地装置和防雷装置。站内设有录音电话', '查现场或资料。不符合要求1处扣2分', '1.查现场或监控系统历史记录、防雷实验报告;2.查安全监控系统的主机是否为双机热备、连续运行当工作主机发生故障时，备用主机是否在5min内自动投入工作;3.查中心站是否设双回路供电，并配备不小于2h在线式不间断电源;4.查中心站设备是否设有可靠 的接地装置和防雷装置，站内是否设有录音电话', '1.监控系统历史纪录；2.防雷实验报告', '不符合要求1处扣2分', 15, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (91, 4, 60, '通风/安全监控/装备设置', '3.7.1(4)', '4.分站、传感器等在井下连续使用6～12个月升井全面检修，井下监控设备的完好率为100%，监控设备的待修率不超过20%，并有检修记录', '查资料或现场。未按规定升井检修1次（台）扣3分，其他1处不符合要求扣1分', '1.查挂牌管理安装日期、现场调校日期、台 账或现场;2.查分站、传感器等在井 下连续使用6-12个月是否升井全面检修;3.查井下监控设备的完好率是否为100%，监控设备的待修率是否不超过20%，并有检修记录', '1.设备管理台账；2.检修记录', '1.未按规定升井检修1次（台）扣3分；2.其他1处不符合要求扣1分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (92, 1, 61, '通风/安全监控/检测试验', '3.7.2(1)', '安全监控设备每月至少调校、测试1次；采用载体催化元件的甲烷传感器每15天使用标准气样和空气样在设备设置地点至少调校1次，并有调校记录；甲烷电闭锁和风电闭锁功能每15天测试1次，其中，对可能造成局部通风机停电的，每半年测试1次，并有测试签字记录', '查资料和现场。不符合要求1处扣2分', '1.查现场设备安装日期、调教日期、调校记录、台账、计算机历史记录;2.查安全监控设备是否每月至少调校、测试1次;3.查采用载体催化元件的甲烷传感器是否每15天使用标准气样和空气样在设备 设置地点至少调校1次，并有调校记录;4.查甲烷电闭锁和风电闭锁功能是否每15天测试1次其中，对可能造成局部通风机停电的，是否每半年测试1次，并有测试签字记录', '1.调校记录及台账；2.测试记录', '不符合要求1处扣2分', 15, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (93, 1, 62, '通风/安全监控/监控设备', '3.7.3(1)', '1.安全监控设备中断运行或者出现异常情况，查明原因，采取措施及时处理，其间采用人工检测，并有记录', '查资料和现场。不符合要求1处扣5分', '查监控运行中心原始记录、台账、瓦检员瓦斯检查记录，是否按照规定、时间规范处理', '监控运行中心原始记录、台账、瓦检员瓦斯检查记录', '不符合要求1处扣5分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (94, 2, 62, '通风/安全监控/监控设备', '3.7.3(2)', '2.安全监控系统显示和控制终端设置在矿调度室，24h有监控人员值班', '查现场和资料。1处不符合要求不得分', '查资料和现场，安全监控系统显示和控制终端是否设置在矿调度室，24h是否有监控人员值班', NULL, '不符合要求不得分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (95, 1, 63, '通风/安全监控/资料管理', '3.7.4(1)', '有监控系统运行状态记录、运行日志，安全监控日报表经矿长、总工程师签字；建立监控系统数据库，系统数据有备份并保存2年以上', '查资料和现场。数据无备份或者数据库缺少数据扣5分，其他1处不符合要求扣2分', '1.查资料和现场，有无监控系统运行状态记录、运行日志，安全监控日报表是否经矿长、总工程师签字;2.查是否建立监控系统数据库，系统数据有无备份并保存2年以上', '监控系统运行状态记录、运行日志，安全监控日报，备份系统数据', '1.数据无备份或者数据库缺少数据扣5分；2.其他1处不符合要求扣2分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (96, 1, 65, '通风/防灭火/防治措施', '3.8.1(1)', '1.按《煤矿安全规程》规定进行煤层的自燃倾向性鉴定，制定矿井防灭火措施，建立防灭火系统，并严格执行', '查资料和现场。未鉴定不得分，其他1处不符合要求扣5分', '1.查资料和现场，是否按《煤矿安全规程》规定进行煤层的自燃倾向性鉴定；2.查是否制定矿井防灭火措施，是否建立防灭火系统，并严格执行', '1.矿井灭火设计、防灭火系统图;2.煤层的自燃倾向鉴定报告', '1.未鉴定扣10分；2.其他1处不符合要求扣5分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (97, 2, 65, '通风/防灭火/防治措施', '3.8.1(2)', '2.开采自燃、容易自燃煤层的采掘工作面作业规程有防止自然发火的技术措施，并严格执行', '查资料和现场。不符合要求1处扣2分', '1.查防灭火检查记录;2.查开采自燃、容易自燃煤层的采搁工作面作业规程是否有防止自然发火的技术措施，并严格执行;3.查井下回采工作面进、回风巷预筑防火门、位置、备用材料是否符合规定;4.查现场采握工作面回风流中一氧化碳传感器、温度传感器是否运行正常;5.查监控中心一氧化碳异常情况有无分析记录', '1.矿井防灭火专项设计；2.防止然发火的术措施;3. 防灭检查记录', '不符合要求1处扣2分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (98, 3, 65, '通风/防灭火/防治措施', '3.8.1(3)', '3.井下易燃物存放符合规定，进行电焊、气焊和喷灯焊接等作业符合《煤矿安全规程》规定，每次焊接制定安全措施，经矿长批准，并严格执行', '查资料和现场。不符合要求1处扣2分', '1.查井下易燃物存放是否符合《煤矿安全规程》第二百五十五条规定;2.查电气焊和喷灯焊接作业是否符合《煤矿安全规程》第二百五十四条规定(同调度台账对照)', '1.安全措施；2.调度台账', '不符合要求1处扣2分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (99, 4, 65, '通风/防灭火/防治措施', '3.8.1(4)', '4.每处火区建有火区管理卡片，绘制火区位置关系图；启封火区有计划和安全措施，并经企业技术负责人批准', '查资料。不符合要求1处扣5分', '1.查每处火区是否建有火区管理卡片，是否绘制火区位置关系图2.查启封火区是否有计划和安全措施，并经企业技术负责人批准', '1.火区位置关系图；2.启封火区是否有计划和安全措施', '不符合要求1处扣5分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (100, 1, 66, '通风/防灭火/设施设备', '3.8.2(1)', '1.按《煤矿安全规程》规定设置井上、下消防材料库，配足消防器材，且每季度至少检查1次', '查资料和现场。缺消防材料库不得分，其他1处不符合要求扣1分', '1.查灾害预防和处理计划和现场，是否按《煤矿安全规程》第二百五十六条规定设置并上、下消防材料库;2.查是否配足消防器材，且按规定周期检查(每季度至少检查1次);3.查器材实际配备与灾防计划是否一致', '灾害预防和处理计划', '1.缺消防材料库不得分；2.其他1处不符合要求扣1分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (101, 2, 66, '通风/防灭火/设施设备', '3.8.2(2)', '2.按《煤矿安全规程》规定井下爆炸物品库、机电设备硐室、检修硐室、材料库等地点的支护和风门、风窗采用不燃性材料，并配备有灭火器材，其种类、数量、规格及存放地点，均在灾害预防和处理计划中明确规定', '查资料和现场。不符合要求1处扣2分', '1.查资料和现场，是否按《煤矿安全规程》规定对井下爆炸物品库、机电设备码室、检修码室、材料库等地点的支护和风门、风窗采用不燃性材料，并配备有灭火器材;2.查种类、数量、规格及存放地点，是否在灾害预防和处理计划中明确规定', NULL, '不符合要求1处扣2分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (102, 3, 66, '通风/防灭火/设施设备', '3.8.2(3)', '3.矿井设有地面消防水池和井下消防管路系统，每隔100m(在带式输送机的巷道中每隔50m)设置支管和阀门，并正常使用。地面消防水池保持不少于200m3的水量，每季度至少检查1次', '查现场。无消防水池或者水量不足不得分；缺支管、阀门，1处扣2分；其他1处不符合要求扣0.5分', '1.查现场是否设有地面消防水池和井下消防管路系统;2.查是否每隔100m(在带式输送机的巷道中每隔50m)设置支管和阀门，并正常使用;3.查消防水池是否保持不少于200 m³的水量，是否每季度至少检查1次', '消防系统图', '1.无消防水池或者水量不足不得分:2.缺支管、阀门，1处扣2分;3.其他1处不符合要求扣0.5分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (103, 4, 66, '通风/防灭火/设施设备', '3.8.2(4)', '4.开采容易自燃和自燃煤层，确定煤层自然发火标志气体及临界值，开展自然发火预测预报工作，建立监测系统；在开采设计中明确选定自然发火观测站或者观测点，每周进行1次观测分析。发现异常，立即采取措施处理', '查资料和现场。无监测系统不得分，1处预测预报不符合要求扣5分，其他1处不符合要求，扣2分', '1.查是否有标志性气体分析化验报告现场做气体预测预报;2.查发火观测站点位置是否明确;3.查观测站设置是否合理;4.查观测记录每周是否开展1次观测分析;5.查记录有异常时是否采取措施', '1.放灭火设计；2.标志性气体分析化验报告；3.预测预报痕迹', '1.无监测系统不得分;2.1处预测预报不符合要求扣5分;观测点设置位置不合适扣2分;3.其他1处不符合要求，扣2分', 15, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (104, 1, 67, '通风/防灭火/控制指标', '3.8.3(1)', '无一氧化碳超限作业，采空区密闭内及其他地点无超过35℃的高温点（因地温和水温影响的除外）', '查资料和现场。有超限作业不得分；其他1处不符合要求扣2分', '查计算机运行记录和现场，有无一氧化碳超限作业，采空区密闭内及其他地点有无超过35℃的高温点(因地温和水温影响的除外)', '1.CO检查记录；2.温度检查记录', '1.有超限作业不得分:2.其他1处不符合要求扣2分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (105, 1, 68, '通风/防灭火/封闭时限', '3.8.4(1)', '及时封闭与采空区连通的巷道及各类废弃钻孔；采煤工作面回采结束后45天内进行永久性封闭', '查资料和现场。1处不符合要求，扣2分', '查生产记录、调度台账、密闭台账，是否及时封闭与采空区连通的巷道及各类废弃钻孔，采煤工作面回采结束后45天内是否进行永久性封闭', '1.生产记录资料；2.调度台账；3.密闭台账', '1处不符合要求，扣2分', 5, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (106, 1, 70, '通风/粉尘防治/鉴定及措施', '3.9.1(1)', '按《煤矿安全规程》规定鉴定煤尘爆炸性；制定年度综合防尘、预防和隔绝煤尘爆炸措施，并组织实施', '查资料和现场。未鉴定或者无措施不得分；其他1处不符合要求，扣2分', '1.查资料和现场，是否按《煤矿安全规程》规定鉴定煤尘爆炸性2.查是否制定年度综合防尘、预防和隔绝煤尘爆炸措施，并组织实施', '1.煤尘爆炸性鉴定报告；2.防尘措施', '1.未鉴定或或者无措施不得分:2.其他1处不符合要求扣2分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (107, 1, 71, '通风/粉尘防治/设备设施', '3.9.2(1)', '1.按照AQ1020规定建立防尘供水系统；防尘管路吊挂平直，不漏水；管路三通阀门便于操作', '查现场。未建立系统不得分，缺管路1处扣5分，其他1处不符合要求扣2分', '1.查现场是否按照AQ1020规定建立防尘供水系统;2.查防尘管路吊挂是否平直，有无漏水现象;3.查管路三通阀门是否便于操作', '方程系统图', '1.未建立系统不得分；2.缺管路1处扣5分；3.其他1处不符含要求扣2分', 15, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (108, 2, 71, '通风/粉尘防治/设备设施', '3.9.2(2)', '2.运煤（矸）转载点设有喷雾装置，采掘工作面回风巷至少设置2道风流净化水幕，净化水幕和其他地点的喷雾装置符合AQ1020规定', '查现场。缺装置1处扣5分；其他1处不符合要求扣1分', '1.查现场运煤(矸)转载点是否设有喷雾装置;2.查采据工作面回风巷是否设置风流净化水幕(至少2道);3.查净化水幕和其他地点的喷雾装置是否符合AQ 1020规定', '防尘系统图', '1. 缺装置1处扣5分;2.其他1处不符合要求扣1分', 15, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (109, 3, 71, '通风/粉尘防治/设备设施', '3.9.2(3)', '3.按《煤矿安全规程》要求安设隔爆设施，且每周至少检查1次，隔爆设施安装的地点、数量、水量或者岩粉量及安装质量符合AQ1020规定', '查资料和现场。未设隔爆设施，1处扣5分；其他1处不符合要求扣2分', '1.查资料和现场记录牌板;2.查是否按《煤矿安全规程》要求安设隔爆设施，且每周至少检查1次;3.查隔爆设施安装的地点、数量、水量或者岩粉量及安装质量是否符合AQ 1020规定', '防尘系统图', '1.未设隔爆设施，1处扣5分;2.其他1处不符合要求扣2分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (110, 4, 71, '通风/粉尘防治/设备设施', '3.9.2(4)', '4.采煤机、掘进机内外喷雾装置使用正常；液压支架和放顶煤工作面的放煤口安设喷雾装置，降柱、移架或者放煤时同步喷雾，喷雾压力符合《煤矿安全规程》要求；破碎机安装有防尘罩和喷雾装置或者除尘器', '查现场。缺外喷雾装置或者喷雾效果不好1处扣5分；其他1处不符合要求扣2分', '1.查现场采煤机、掘进机内外喷雾装置是否使用正常;2.查液压支架和放顶煤工作面的放煤口是否安设喷雾装置;3.查降柱、移架或者放煤时是否同步喷雾;4.查喷雾压力是否符合《煤矿安全规程》要求;5.查破碎机有无安装防尘罩和喷雾装置或者除尘器', NULL, '1.缺外喷雾装置或者喷雾效果不好1处扣5分;2.缺内喷雾装置扣2分;3.其他1处不符合要求扣2分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (111, 1, 72, '通风/粉尘防治/防除尘措施', '3.9.3(1)', '1.采用湿式钻孔或者孔口除尘措施，爆破使用水炮泥，爆破前后冲洗煤壁巷帮；炮掘工作面安设有移动喷雾装置，爆破时开启使用', '查现场。未湿式钻孔或者无措施扣5分；其他1处不符合要求扣2分', '1.查爆破说明书，查现场;2.查锚喷、锚索打眼是否采用湿式钻孔或者孔口除尘措施;3.查爆破是否使用水炮泥，爆破前后是否冲洗煤壁巷帮;4.查炮掘工作面是否安设有移动喷雾装置，爆破时是否开启使用', '爆破说明书及爆破防尘措施', '1.未湿式钻孔或者无措施扣5分；2.其他1处不符合要求扣2分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (112, 2, 72, '通风/粉尘防治/防除尘措施', '3.9.3(2)', '2.喷射混凝土时，采用潮喷或者湿喷工艺，并装设除尘装置。在回风侧100m范围内至少安设2道净化水幕', '查现场。不符合要求1处扣5分', '1.查资料和现场，喷射混凝土时是否采用潮喷或者湿喷工艺，并装设除尘装置;2.查在回风侧100m范围内是否安设净化水幕(至少2道)', NULL, '1.不符合要求1处扣5分;2.喷雾不能覆盖全断面1处扣1分，扣到2分为止', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (113, 3, 72, '通风/粉尘防治/防除尘措施', '3.9.3(3)', '3.采煤工作面按《煤矿安全规程》规定采取煤层注水措施，注水设计符合AQ1020规定', '查资料和现场。采煤工作面未注水1处扣5分；其他1处不符合要求扣2分', '1.查水分化验单、煤层注水台账;2.查采煤工作面是否按《煤矿安全规程》规定采取煤层注水措施;3.查注水设计是否符合AQ 1020规定', '煤层注水设计及注水台账', '1.采煤工作面未注水1处扣5分(符合规程规定的除外);2.其他1处不符合要求扣2分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (114, 4, 72, '通风/粉尘防治/防除尘措施', '3.9.3(4)', '4.定期冲洗巷道积尘或者撒布岩粉。主要大巷、主要进回风巷每月至少冲洗1次，其他巷道冲洗周期或者撒布岩粉由矿总工程师确定。巷道中无连续长5m、厚度超过2mm的煤尘堆积', '查资料和现场。煤尘堆积超限1处扣5分；其他1处不符合要求扣2分', '1.查工作记录和现场，是否定期冲洗巷道积尘或者撒布岩粉;2.查主要大巷、主要进回风巷是否每月至少冲洗1次;3.查其他巷道冲洗周期或者撒布岩粉是否由矿总工程师确定;4.查巷道中有无连续长5m、厚度超过2 mm的煤尘堆积', '工作记录', '1.煤尘堆积超限1处扣5分;2.其他1处不符合要求扣2分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (115, 1, 74, '通风/井下爆破/物品管理', '3.10.1(1)', '1.井下爆炸物品库、爆炸物品贮存及运输符合《煤矿安全规程》规定', '查现场。不符合要求1处扣5分', '1.查井下爆炸物品材料库管理规章制度是否健全;2.查炸药、电雷管存储数量是否超过规定3.查炸药、电雷管等级是否符合规定;4.查火药库的设施、设备、消防等是否符合规定要求', '爆炸物品管理规章制度', '不符合要求1处扣5分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (116, 2, 74, '通风/井下爆破/物品管理', '3.10.1(2)', '2.爆炸物品领退、电雷管编号制度健全，发放前电雷管进行导通试验', '查资料和现场。未进行导通试验扣10分，缺1项制度扣5分', '1.查原始编码台账是否有健全的爆炸物品领退、电雷管编号制度;2.查电雷管发放前，是否进行导通试验，是否留有记录', '1.爆炸物品管理规章制度;2.记录台账', '1.未进行导弹试验扣10分；缺1项制度扣5分', 20, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (117, 1, 75, '通风/井下爆破/爆破管理\n', '3.10.2(1)', '1.爆破作业执行“一炮三检”、“三人连锁”制度，采取停送电（突出煤层）、撤人、设岗警戒措施。特殊情况下的爆破作业，制定安全技术措施，经矿总工程师批准后执行', '查资料和现场。1处不符合要求不得分', '1.查有无作业规程、爆破作业有无专项措施;2.查“一烟三检”记录(爆破员手册)，爆破作业是否执行“一炮三检”“三人连锁”制度;3.查爆破作业是否采取停送电(突出煤层1、撤人、设岗警戒措施;4.查特殊情况下的爆破作业是否制定安全技术施，并经矿总工程师批准后执行', '1.爆破作业规程;2.特殊情况下作业的安全技术措施;3.爆破员手册', '1处不符合要求不得分', 20, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (118, 2, 75, '通风/井下爆破/爆破管理\n', '3.10.2(2)', '2.编制爆破作业说明书，并严格执行。现场设置爆破图牌板', '查资料和现场。无爆破说明书或者不执行不得分，其他1处不符合要求扣2分', '1.查是否有爆破作业说明书；2查是否严格执行爆破作业说明书；3.查现场是否设置爆破图牌板', '爆破作业说明书', '1.无爆破作业说明书或者不执行不得分；2.其他1处不符合要求扣2分', 15, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (119, 3, 75, '通风/井下爆破/爆破管理\n', '3.10.2(3)', '3.爆炸物品现场存放、引药制作符合《煤矿安全规程》规定', '查现场。不符合要求1处扣2分', '查爆炸物品现场存放、引药制作是否符合《煤矿安全规程》规定', NULL, '不符合要求1处扣2分', 15, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (120, 4, 75, '通风/井下爆破/爆破管理\n', '3.10.2(4)', '4.残爆、拒爆处理符合《煤矿安全规程》规定', '查现场和资料。不符合要求不得分', '查作业规程和现场，残爆、柜爆处理是否符合《煤矿安全规程》第三百七十二条规定', '作业规程', '不符合要求不得分', 20, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (121, 1, 77, '通风/基础管理/组织保障', '3.11.1(1)', '按规定设有负责通风管理、瓦斯管理、安全监控、防尘、防灭火、瓦斯抽采、防突和爆破管理等工作的管理机构', '查资料。未设置机构不得分，机构不完善扣5分', '查矿机构设置文件是否设有负责通风管理、瓦斯管理、安全监控、防尘、防灭火、瓦斯抽采、防突和爆破管理等工作的管理机构', '机构设置文件', '1.未设置机构不得分:2.机构不完善扣5分:3.机构文件有1处不规范扣1分，扣完5分为止', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (122, 1, 78, '通风/基础管理/工作制度', '3.11.2(1)', '1.有完善矿井通风、瓦斯防治、综合防尘、防灭火和安全监控等专业管理制度，各工种有岗位安全生产责任制和操作规程，并严格执行', '查资料和现场。缺制度或者操作规程不得分；其他1处不符合要求扣5分', '1.查是否有完善矿井通风、瓦斯防治、综合防尘、防灭火和安全监控等专业管理制度;2.查是否有各工种岗位安全生产责任制和操作规程;3.查现场是否严格执行以上制度和操作规程', '1.各有关专业管理制度2.各有关工种岗位责任制:3.各有关工种操作规程', '1.缺制度或者操作规程不得分;2.其他1处不符含要求扣5分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (123, 2, 78, '通风/基础管理/工作制度', '3.11.2(2)', '2.制定瓦斯防治中长期规划和年度计划。矿每月至少召开1次通风工作例会，总结安排年、季、月通风工作，并有记录', '查资料。缺1项计划或者总结扣5分，其他1处不符合要求扣2分', '1.查是否制定瓦斯防治中长期规划和年度计划;2.查是否存有通风工作例会记录(每月至少一次);3.查是否存有年、季、月通风工作总结记录', '1.瓦斯防治中长期规划;2.瓦斯防治年度计划:3.通风例会会议纪要;4.通风工作总结记录', '1.缺1项计划或者总结扣5分;2.其他1处不符合要求扣2分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (124, 1, 79, '通风/基础管理/资料管理', '3.11.3(1)', '有通风系统图、分层通风系统图、通风网络图、通风系统立体示意图、瓦斯抽采系统图、安全监控系统图、防尘系统图、防灭火系统图等；有测风记录、通风值班记录、通风（反风）设施检查及维修记录、粉尘冲洗记录、防灭火检查记录；有密闭管理台账、煤层注水台账、瓦斯抽采台账等；安全监控及防突方面的记录、报表、账卡、测试检验报告等资料符合AQ1029及《防治煤与瓦斯突出规定》要求，并与现场实际相符', '查资料和现场。图纸、记录、台账等资料缺1种扣2分，与现场实际不符1处扣5分；其他1处不符合要求扣0.5分', '1.查是否有通风系统图、分层通风系统图、通风网络图、通风系统立体示意图、瓦斯抽采系统图、安全监控系统图、防尘系统图、防灭火系统图等;2.查是否有测风记录、通风值班记录、通风(反风)设施检查及维修记录、粉尘冲洗记录、防灭火检查记录;3.查是否有密闭管理台账、煤层注水台账、瓦斯抽采台账等;4.查安全监控及实方面的记录、报表账卡、测试检验报售等资料是否符合AQ1029及《防治煤与瓦斯突出规定》要求，并与现场实际相符', '标准列出的相关资料', '1.图纸、记录、台账等资料缺1种扣2分;2.图纸与现场实际不符，1处扣5分;3.其他1处不符合要求扣0.5分', 20, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (125, 1, 80, '通风/基础管理/仪器仪表', '3.11.4(1)', '按检测需要配备检测仪器，每类仪器的备用量不小于应配备使用数量的20%，仪器的调校、维护及收发和送检工作有专门人员负责，按期进行调校、检验，确保仪器完好', '查资料和现场。仪器数量不足或者无专门人员负责扣5分，其他不符要求1台次扣2分', '1.查库房、台账、仪器强检合格证、出厂合格证;2.查是否配备相关的检测仪器，且每类仪器的备用量不小于应配使用数量的20% ；3.查仪器的调校、维护及收发和送检工作是否有专门人员负责；4.查仪器是否按期进行调校、检验', '仪器仪表管理台账', '1.仪器数量不足或者无专门人员负责扣5分2.其他不符要求1台次扣2分', 20, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (126, 1, 81, '通风/基础管理/岗位规范', '3.11.5(1)', '1.管理和技术人员掌握相关的岗位职责、管理制度、技术措施', '查资料和现场。不符合要求1处扣5分', '查管理和技术人员是否掌握相关的岗位职责、管理制度、技术措施。随机抽考管理和技术人员，提问3个问题，如果都回答不正确，视为不符合要求;随机抽取1名关键岗位人员，提问1个最关键应知应会问题，如果回答不正确，视为不符合要求', '各项岗位职责、管理制度、技术措施', '不符合要求1处扣5分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (127, 2, 81, '通风/基础管理/岗位规范', '3.11.5(2)', '2.现场作业人员严格执行本岗位安全生产责任制；掌握本岗位相应的操作规程和安全措施，操作规范；.无“三违”行为', '查现场。发现“三违”不得分，不执行岗位责任制、不规范操作1人次扣3分', '1.查现场作业人员是否严格执行本岗位安全生产责任制;2.查现场作业人员是否掌握本岗位相应的操作规程和安全措施，操作规范;3.查现场作业人员有无“三违”行为', '1.岗位安全生产责任制；2.岗位操作规程和安全措施', '1.发现“三违”行为不得分:2.不执行岗位责任制、不规范操作，1人次扣3分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (128, 3, 81, '通风/基础管理/岗位规范', '3.11.5(3)', '3.作业前对作业范围内空气环境、设备运行状态及巷道支护和顶底板完好状况等实时观测，进行安全确认', '查现场。1人次不确认扣3分，其他1处不符合要求扣1分', '查现场作业前是否对作业范围内空气环境、设备运行状态及巷道支护和顶底板完好状况等实时观测，进行安全确认', NULL, '1.1人次现场不确认扣3分；2.其他1处不符合要求扣1分', 10, NULL, 1, 81, 0, 1);
INSERT INTO `stdchk_item` VALUES (129, 1, 84, '地质灾害防治与测量/规章制度/制度建设', '4.1.1(1)', '有以下制度：1.地质灾害防治技术管理、预测预报、地测安全办公会议制度；2.地测资料、技术报告审批制度；3.图纸的审批、发放、回收和销毁制度；4.资料收集、整理、定期分析、保管、提供制度；5.隐蔽致灾地质因素普查制度；6.岗位安全生产责任制度', '查资料。每缺1项制度扣5分；制度有缺陷1处扣1分', '查制度建设资料，看制度是否缺项，内容是否符合要求', '1.地质灾害防治技术管理制度;2.预测预报制度;3.地测安全办公会议制度;4.地测资料、技术报告审批制度;5. 图纸的审批、发放、回收和销毁制度;6、资料收集、整理、定期分析、保管、提供制度;7.隐蔽致灾地质因素普查制度;8.岗位安全生产责任制度', '1.制度内容不全面，每缺1项制度扣5分;2.制度有缺陷(如未覆盖全部地质专业人员) 1处扣1分', 15, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (130, 1, 85, '地质灾害防治与测量/规章制度/资料管理', '4.1.2(1)', '图纸、资料、文件等分类保管，存档管理，电子文档定期备份', '查资料。未分类保管扣5分，存档不齐，每缺1种扣3分，电子文档备份不全，每缺1种扣2分', '1.查图纸、资料、文件等是否按档案管理要求分类保管;2.抽查1-2份资料是否存档，以及电子文档备份情况;3.查看索引、目录汇总', '1.分类目录索引台账;2.保管图纸、资料、文件等各类型的资料;3.电子文档存档时间、内容、文档位置等纸质目录', '1.未分类保管扣5分;2.存档不全，每缺1种扣3分；3.没有索引目录扣5分', 15, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (131, 1, 86, '地质灾害防治与测量/规章制度/岗位规范', '4.1.3(1)', '1.管理和技术人员掌握相关的岗位职责、管理制度、技术措施；2.现场作业人员严格执行本岗位安全生产责任制，掌握本岗位相应的操作规程和安全措施，操作规范，无“三违”行为；3.作业前进行安全确认', '查资料和现场。发现“三违”不得分，不执行岗位责任制、不规范操作1人次扣3分', '1.抽查提问所设岗位人员是否掌握其岗位责任制、管理制度和技术措施;随机抽考管理和技术人员，提问3个问题，如果都回答不正确，视为不符合要求;随机抽取一名关键岗位人员，提问1 个最关键应知应会问题，如果回答不正确，视为不符合要求;2.查并下现场有探放水作业的检查是否按规程、设计、安全措施执行(现场检查);3.查现场各专业作业前是否进行安全确认;4. 对照岗位责任抽查，看现场是否有“三违”行为', '1.岗位职责;2.管理制度;3.安全技术措施等', '1.现场发现“三违”行为不得分;2.不执行岗位责任制、不规范操作1人次扣3分，如不按探放水设计施工扣3分', 20, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (132, 1, 88, '地质灾害防治与测量/组织保障与装备/组织保障', '4.2.1(1)', '矿井按规定设立负责地质灾害防治与测量部门，配备相关人员', '查资料。未按要求设置部门不得分，设置不健全扣10分，人员配备不能满足要求扣5分', '查组织机构文件及地质灾害防治与测量部门人员配备情况是否符合规定', '1.建立地质防水机构文件；2.地测人员分工汇总表（查学历、所学专业等）资料；3.文件和人员台账', '1.未按要求设置部门不得分，设置不健全扣10分；2.人员配备不能满足要求扣到5分为止', 25, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (133, 1, 89, '地质灾害防治与测量/组织保障与装备/装备管理', '4.2.2(1)', '1.工器具、装备完好，满足规定和工作需要；2.地质工作至少采用一种有效的物探装备；3.采用计算机制图；4.地测信息系统与上级公司联网并能正常使用', '查资料和现场。因装备不足或装备落后而影响安全生产的不得分；装备不能正常使用1台扣2分；无物探装备扣5分；未采用计算机制图扣10分；地测信息系统未与上级公司扣10分，不能正常使用扣5分', '1.查仪器台账是否有物探仪、钻探、测量设备、绘图仪；2.现场操作，检查地测信息系统能否及时上传和正常使用；3.查地测信息系统与上级公司联网情况', '1.钻探设备台账；2.测量仪器定期校验证书；3.物探装备及物探资料', '1.因装备不足或装备落后而影响安全生产的不得分:2. 使用未按规定校验的装备1台扣2分:3.无物探装备扣5分:4.未采用计算机制图扣10分;5.地测信息系统未与上级公司联网扣10分，不能正常使用扣5分', 25, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (134, 1, 91, '地质灾害防治与测量/基础工作/地质观测与分析', '4.3.1(1)', '1.按《煤矿地质工作规定》要求进行地质观测与资料编录、综合分析；2.综合分析资料能满足生产工作需要', '查资料。未开展地质观测、无观测资料或综合分析资料不能满足生产需要不得分，资料无针对性扣5分，地质观测与资料编录不及时、内容不完整、原始记录不规范1处扣2分', '1.查目前正在采掘的工作面观测资料及地质编录是否按《煤矿地质工作规定》要求进行:是否有专门记录本，内容和图纸是否符合地质工作规定要求，是否做到及时、准确，完整、统一；2.查收集资料整理情况，检查收集资料是否于2天内反映到相关图纸成果台账、素描圈等地质文档中，起到综合分析作用，以满足生产工作需要;3.查综合分析内容是否符合《煤矿地质工作规定》第五十四条的规定:①含煤地层层序、沉积特征及其演化规律;②煤层结构、煤体结构、煤层厚度、煤质变化的原因和规律;③构造及其组合特征、形成机制、展布规律和预测方法④含煤地层中岩浆侵入体的特征、分布规律及其对煤层和煤质的影响;⑤瓦斯(或二氧化碳)赋存规律;⑥水文地质特征;⑦煤层顶底板、冲击地压、陷落柱、老空区、地热和边坡稳定性等地质问题;⑧隐蔽致灾地质因素分析;⑨采探对比分析;⑩煤矿勘探、建设和生产中 新出现的地质问题;4.抽查原始地质记录本， 看描述内容是否齐全、及时', '1.采掘工作面的原始记录本;2.实测资料整理图纸、台账和文档3.地质预报、采掘工作面地质分析资料;4. 综合分析资料', '1.未开展地质观测、无观测资料或综合分析资料不能满足生产需要不得分；2.资料无针对性扣5分;3.地质观测与资料编录不及时、内容不完整、原始记录不规范1处扣2分(不重复扣分)', 10, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (135, 1, 92, '地质灾害防治与测量/基础工作/致灾因素普查地质类型划分', '4.3.2(1)', '1.按规定查明影响煤矿安全生产的各种隐蔽致灾地质因素；2.按“就高不就低”原则划分煤矿地质类型，出现影响煤矿地质类型划分的突水和煤与瓦斯突出等地质条件变化时，在1年内重新进行地质类型划分', '查资料。矿井隐蔽致灾地质因素普查不全面，每缺1类扣5分；普查方法不当扣2分；未按原则划分煤矿地质类型扣5分；未及时划分煤矿地质类型不得分', '1.查是否有《矿井隐蔽致灾地质因素普查报告》;2.对照《煤矿地质工作规定》第四章节规定，主要查影响煤矿安全生产的各种隐蔽致灾地质因素普查内容是否全面，普查是否及时，普查内容是否及时修改、完善并可指导矿井安全生产:3.查《矿井地质类型划分报告)是否按规定时间修改，划分是否正确，出现影响煤矿地质类型划分的突水和煤与瓦斯突出等地质条件变化时，是否在1年内重新进行地质类型划分', '矿井隐蔽致灾地质因素普查报告', '1矿井隐蔽致灾地质因素普查不全面，每缺1类扣5分；2.普查方法不当、未及时普查完善，扣2分；3.未按原则划分煤矿地质类型扣5分;4.未及时划分煤矿地质类型不得分', 10, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (136, 1, 94, '地质灾害防治与测量/基础资料/地质报告', '4.4.1(1)', '有满足不同生产阶段要求的地质报告，按期修编，并按要求审批', '查资料。地质类型划分报告、生产地质报告、隐蔽致灾地质因素普查报告不全，每缺1项扣3分；地质报告未按期修编1次扣3分；未按要求审批1次扣2分', '1.查有无地质类型划分报告，是否5年修编1次;2.查有无生产地质报告，是否5年修编1次;3.查有无矿井隐蔽致灾地质因素报告4.查地质类型划分报告和生产地质报告是否由上级企业总工程师审批；隐蔽致灾地质因素报告是否由矿总工程师审批', '1.矿井地质类型划分报告;2.矿井生产地质报告;3.矿井隐蔽致灾地质因素普查报告4.这3个报告的审批文件', '1.地质类型划分报告、生产地质报告、隐蔽致灾地质因素普查报告不全，每缺1项扣3分;2.报告未按期修编1次扣3分;3.这3个报告未按要求审批1次扣2分', 10, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (137, 1, 95, '地质灾害防治与测量/基础资料/地质说明书', '4.4.2(1)', '采掘工程设计施工前，按时提交由总工程师批准的采区地质说明书、回采工作面地质说明书、掘进工作面地质说明书；井巷揭煤前，探明煤层厚度、地质、构造、瓦斯地质、水文地质及顶底板等地质条件，编制揭煤地质说明书', '查资料。资料不全，每缺1项扣2分；地质说明书未经批准扣2分；文字、原始资料、图纸数字不符，内容不全，1处扣1分', '1.查是否有果区地质说明书、采煤工作面地质说明书、提进工作面地质说明书、揭煤地质说明书;2.查文字内容、原始资料、围纸是否符合《煤矿地质工作规定》;对照矿上审批程序查审批是否符合《煤矿地质工作规定》', '近期编制的采区地质说明书、采煤工作面地质说明书、据进工作面地质说明书、揭煤地质说明书', '1.地质说明书每缺1项扣2分;2.地质说明书未经批准或者未按规定审批扣2分:3.文字、原始资料、图纸数字不符，内容不全，1处扣1分', 5, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (138, 1, 96, '地质灾害防治与测量/基础资料/采后总结', '4.4.3(1)', '采煤工作面和采区结束后，按规定进行采后总结', '查资料。采后总结不全，每缺1份扣3分，内容不符合规定1次扣3分', '1.查最近结束的采区和采煤工作面采后总结报告，有无总结；2.查采区、采煤工作面总结报告是否符合《煤矿地质工作规定》(重点是上报期限、具体审批人、主要内容等)', '工作面和采区总结报告', '1.采后总结有缺项(总结重点内容要有构造、异常区和水文、瓦斯、煤质和回采率）或内容不符合规定扣3分;2.采后总结每缺1份扣3分', 5, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (139, 1, 97, '地质灾害防治与测量/基础资料/台账图纸', '4.4.4(1)', '1.有《煤矿地质工作规定》要求必备的台账、图件等地质基础资料；2.图件内容符合《煤矿地质测量图技术管理规定》要求，图种齐全有电子文档', '查资料。台账不全，每缺1种扣3分；台账内容不全不清，1处扣1分；检查全部地质图纸，图种不全的，每缺1种扣5分；图幅不全扣2分，无电子文档扣2分，未及时更新1处扣1分，图例、注记不规范1处扣1分；素描图不全，每缺1处扣3分，要素内容不全1处扣1分；日常用图中采掘工程及地质内容未及时填绘的1处扣1分', '1.抽查是否有《煤矿地质工作规定》要求必备的台账、图件等地质基础资料，内容是否齐全，与原始记录是否一致，素描图纸是否齐全:2.查图件内容是否符合《煤矿地质测量图技术管理规定》要求，图种是否齐全并有电子文档;3.查是否及时更新', '1.必备的10种台账:钻孔成果台账;地质构造台账:矿井瓦斯资料台账;煤质资料台账井筒、石门风煤点台账;工程地质资料台账;资源/储量台账井田及周边采空区、老窑地质资料台账;井下火区地质资料台账;封闭不良地质钻孔台账;2.必备的10种图件:煤矿地层综合柱状图;煤矿地形地质图或基岩地质图;煤矿煤岩层对比图;煤矿可采煤层底板等高线及资源/储量估算图(急倾斜煤层加绘立面投影图和立面投影资源/储量估算图);煤矿地质剖面图;煤矿水平地质切面图(煤层倾角大于25°的多煤层煤矿);勘探钻孔柱状图;矿井瓦斯地质图;井上下对照圈;采掘(剥)工程平面图(急倾斜煤层要绘采掘工程立面图);3.井巷、石门地屋编录;4.工程地质相关图', '1.台账不全，每缺1种扣3分;2.台账内容不全不清，1处扣1分;3.全部地质图纸图种不全的，每缺1种扣5分;4.图幅不全扣2分，无电子文档扣2分，未及时更新1处扣1分，图例、注记不规范1处扣1分5.素描图不全，每缺1处扣3分，要素内容不全1处扣1分;6.日常用图中采掘工程及地质内容未及时填绘1处扣1分', 10, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (140, 1, 98, '地质灾害防治与测量/基础资料/原始记录', '4.4.5(1)', '1.有专用原始记录本，分档按时间顺序保存；2.记录内容齐全，字迹、草图清楚', '查资料。记录本不全，每缺1种扣3分；其他1处不符合要求扣1分', '1.查专用原始记录本是否齐全、是否混记，是否分档按时间顺序保存;2.随机抽查记录内容是否齐全，字迹、草图是否清楚', '全部专用原始记录本', '1.记录本不全，每缺1种扣3分:2.记录内容不全或图、字不清楚扣1分;3.记录本内容混记扣1分', 5, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (141, 1, 100, '地质灾害防治与测量/预测预报/地质预报', '4.5.1(1)', '地质预报内容符合《煤矿地质工作规定》要求，内容齐全，有年报、月报和临时性预报，并以年为单位装订成册，归档保存', '查资料。采掘地点预报不全，每缺1个采掘工作面扣5分，预报内容不符合规定、预报有疏漏、失误1处扣1分，未经批准1次扣2分；未预报造成工程事故本项不得分', '1.查地质预报内容是否符合《煤矿地质工作规定》要求，文字、附图是否齐全，能否指导生产，是否经过审批;2.查预报是否有疏漏，是否有年报、月报和临时性预报，是否有因未预报造成工程事故情况:3.查预报是否以年为单位装订成册，归档保存', '当年或上一年报、月报和临时性预报', '1.采拥地点预报不全，每缺1个采掘工作面扣5分;2.预报内容不符合规定、预报有疏漏、失误1处扣1分，未经批准1次扣2分:3.未预报造成工程事故不得分', 10, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (142, 1, 102, '地质灾害防治与测量/瓦斯地质/瓦斯地质', '4.6.1(1)', '1.突出矿井及高瓦斯矿井每年编制并至少更新1次各主采煤层瓦斯地质图，规范填绘瓦斯赋存采掘进度、煤层赋存条件、地质构造、被保护范围等内容，图例符号绘制统一，字体规范；2.采掘工作面距保护边缘不足50m前，编制发放临近未保护区通知单，按规定揭露煤层及断层，探测设计及探测报告及时无误；3.根据瓦斯地质图及时进行瓦斯地质预报', '查资料。瓦斯预报错误造成工程事故或误揭煤层及断层的不得分；未编制下发临近未保护区通知单的，1次扣2分；未编制揭煤探测设计及探测报告扣5分；其他1项不符合要求扣1分；', '1.查突出矿井及高瓦斯矿井主采煤层瓦斯地质图，检查是否每年至少更新1次各主采煤层瓦斯地质图，是否规范填绘瓦斯赋存采掘进度、煤层赋存条件、地质构造、被保护范围等内容，图例符号绘制是否统一，字体是否规范;2.查采掘工作面距保护边缘不足50m前是否编制发放临近未保护区通知单，是否按规定揭露煤层及断层，编制探测设计及探测报告是否及时无误;3.查瓦斯地质预报及编制是否及时，瓦斯地质图和瓦斯地质预报是否及时更新和发放', '1.突出矿井及高瓦斯矿井各主采煤层瓦斯地质图瓦斯地质台账;2.采掘工作面距保护边缘不足50m前，编制发放的临近未保护区通知单，揭露煤层及断层探测设计及探测报告3.突出矿井及高瓦斯矿井的瓦斯地质预报', '1.瓦斯预报错误或不到位造成工程事故或误揭煤层及断层的不得分;2.未编制下发临近未保护区通知单，1次扣2分；3.未编制揭煤探测设计及探测报告扣5分；4.因构造预报不准造成瓦斯超限扣5分;5.其他1项不符合要求扣1分', 15, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (143, 1, 104, '地质灾害防治与测量/资源回收及储量管理/储量估算图', '4.7.1(1)', '有符合《矿山储量动态管理要求》规定的各种图纸，内容符合储量、损失量计算图要求', '查资料。图种不全，每缺1种扣2分，其他1项不符合要求扣1分', '1.查是否有《矿山储量动态管理要求》(国土资发[2008]163号)规定的各种图纸;2.查图纸内容是否符合储量、损失量计算图要求', '规定的6种必备图纸：采区储量计算图(比例尺1:1000或1: 2000);矿井储量计算图(比例尺1:1000或1 : 5000);工作面损失计算图(比例尺1:500或1:1000、1:2000）分煤层“损失量”计算图(比例尺1：1000或1 : 2000或1 : 5000;煤层资源变动对比储量估算图(比例尺1: 1000或1 : 2000或1 ; 5000);工作面探煤厚计算图(比例尺1:500或1 :1000)', '1.图种不全，每缺1种扣2分2.储量计算不符合要求扣1分3.其他1项不符含要求扣1分', 6, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (144, 1, 105, '地质灾害防治与测量/资源回收及储量管理/储量估算成果台账', '4.7.2(1)', '有符合《矿山储量动态管理要求》规定的储量计算台账和损失量计算台账，种类齐全、填写及时、准确，有电子文档', '查资料。每种台账至少抽查1本，台账不全或未按规定及时填写的，每缺1种扣2分；台账内容不全、数据前后矛盾的，1处扣1分', '1.查是否有《矿山储量动态管理要求》规定的储量计算台账和损失量计算台账;2.查计算台账种类是否齐全、是否填写及时、准确，是否有电子文档', '1.必备的2种损失量台账:分工作面各月损失量分析及回采率计算基础台账;分月分采区煤层损失量分析及回采率计算基础台账;2.必备的7种储量台账:矿并期末保有储量计算基础台账;矿井“三下”压煤台账;矿井永久煤柱台账;矿井损失量摊销台账;矿井储量增减、变动审批情况台账(存在储量重大增减变动的，须有地质矿产政府部门随时验证确认的《矿山资源储量核实报告》或《生产矿井地质报告》;矿井储量动态数字台账，“三量”计算成果台账', '1.台账不全或未按规定及时填写的，每缺1种扣2分;2.台账内容不全、数据前后矛盾的，1处扣1分;3.台账无电子文档，扣1分', 6, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (145, 1, 106, '地质灾害防治与测量/资源回收及储量管理/统计管理', '4.7.3(1)', '1.储量动态清楚，损失量及构成原因等准确；2.储量变动批文、报告完整，按时间顺序编号、合订；3.定期分析回采率，能如实反映储量损失情况；4.采区、工作面结束有损失率分析报告；5.每半年进行1次全矿回采率总结；6.三年内丢煤通知单完整无缺，按时间顺序编号、合订；7.采区、工作面回采率符合要求', '查资料。回采率达不到要求不得分，其他1项不符合要求扣2分', '1.查煤矿年度分析报告中的储量动态是否清楚，损失量及构成原因等是否准确;2查储量变动批文、报告是否完整，是否按时间顺序编号、合订；3.查是否定期分析回采率，能否如实反映储量损失情况；4.查是否有采区、工作面结束损失率分析报告；5.查半年采后总结，是否每半年进行1次全矿回采率总结; 6.查三年内丢煤通知单是否完整无缺、按时间顺序编号、合订；7.抽查采区、工作面回采率是否符合《生产煤矿回采率管理暂行规定》(国家发展和改革委员会令第17号)要求(采区煤层厚度≤1.3 m，≥85%;1.3-3.5m,≥80%;≥3.5m,≥75%;工作面煤层厚度≤1.3m,≥97%; 1.3~3.5 m,≥95%;≥3.5m,93%', '1.储量动态、损失台账2.储量变动批交、报告;3.回采率定期分析资料；4.采区、工作面结束的损失率分析报告;5.每半年1次的全矿回采率总结;6.3年内丢煤通知单；7.采区、工作面回采率资料；8.矿年度分析报告', '1.回采率(指采区、工作面回采率，矿井回采率暂不考核)达不到要求不得分;2.其他1项不符合要求扣2分', 8, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (146, 1, 108, '地质灾害防治与测量/基础工作/控制系统', '4.8.1(1)', '1.测量控制系统健全，精度符合《煤矿测量规程》要求；2.及时延长井下基本控制导线和采区控制导线', '查资料和现场。控制点精度不符合要求1处扣1分；井下控制导线延长不及时1处扣2分；未按规定敷设相应等级导线或导线精度达不到要求的，1处扣2分', '1.查图纸、台账:查测量控制系统是否健全，精度是否符合《煤矿测量规程》要求;2.查现场及图纸:检查井下基本控制导线和采区控制导线延长是否及时（井下基本控制导线300~500m、采区控制导线应随巷道掘进30~100m延长1次）;近期测量点精度是否符合《煤矿测量规程》要求', '1.测量控制系统（地面、井上下联系测量、井下）台账及附图；2.相关定向、测量报告', '1.控制点精度不符合要求1处扣1分；2.井下控制导线延长不及时1处扣2分；3.未按规定敷设相应等级导线或导线精度达不到要求的，1处扣2分', 10, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (147, 1, 109, '地质灾害防治与测量/基础工作/测量重点', '4.8.2(1)', '1.贯通、开掘、放线变更、停掘停采线、过断层、冲击地压带、突出区域、过空间距离小于巷高或巷宽4倍的相邻巷道等重点测量工作，执行通知单制度；2.通知单按规定提前发送到施工单位、有关人员和相关部门', '查资料。贯通及过巷通知单未按要求发送、开掘及停头通知单发放不及时的，1次扣5分；巷道掘进到特殊地段时漏发通知单的，1次扣3分；其他通知单，1处错误扣2分，漏发扣3分', '1.查贯通、开掘、放线变更、停拥停采线、过断层、冲击地压带、突出区域、过空间距离小于巷高或巷宽4倍的相邻巷道等通知单是否符合要求(单向贯通时,岩巷20~30m，煤巷30~40m,快速掘进50 ~100m,过巷等安全通知单提前20m;两头相向贯通时，岩巷相距大于40m,煤巷两炮据之间相距大于50m，煤巷单向综掘之间相距大于70 m,开采冲击地压煤层矿井炮掘相距30m,开采冲击地压煤层矿井综掘相距50m,突出矿井煤巷掘进60~80m);2.查通知单是否按规定提前发送到施工单位、有关人员和相关部门;3.查贯通、过巷通知单的编写和发放是否符合要求;4.查通知单发放是否符合审批要求', '1.所有贯通及过巷通知单、开掘及停头通知单；2.签收单', '1.贯通及过巷通知单未按要求发送、开掘及停头通知单发放不及时的，1次扣5分；2、巷道掘进到特殊地段时漏发通知单的，1次扣3分;其他通知单，1处错误扣2分，漏发扣3分', 10, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (148, 1, 110, '地质灾害防治与测量/基础工作/贯通精度', '4.8.3(1)', '贯通精度满足设计要求,两井贯通和一井内3000m以上贯通测量工程应有设计，并按规定审批和总结', '查资料和现场。两井间贯通或3000m以上贯通测量工程未编制贯通测量设计书或未经审批、没有总结的，每缺1项扣3分；贯通后重要方向误差超过允许偏差值的，1处扣5分', '1.查两井贯通或一井内3000m以上贯通测量工程是否编制贯通测量设计书及是否有审批，有无总结;2.查贯通后重要方向误差是否在设计规定范围内', '两井贯通和一井内3000m以上贯通测量工程设计、审批和总结等资料', '1.两井间贯通或3000 m以上贯通测量工程未编制贯通测量设计书或未经上级公司总工程师审批、没有总结的，每缺1项扣3分；2.贯通后重要方向误差超过允许偏差值的，1处扣5分', 8, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (149, 1, 111, '地质灾害防治与测量/基础工作/中腰线标定', '4.8.4(1)', '中腰线标定符合《煤矿测量规程》要求', '查资料和现场。掘进方向偏差超过限差1处扣3分', '1.查巷道是否每掘进100m对中腰线点进行1次检查，掘进方向偏差是否超限2.查中腰线标定是否符合《煤矿测量规程》第二百零五条规定，查看现场是否符合要求', NULL, '掘进方向偏差超过限差1处扣3分', 6, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (150, 1, 112, '地质灾害防治与测量/基础工作/原始记录及成果台账', '4.8.5(1)', '1.导线测量、水准测量、联系测量、井巷施工标定、陀螺定向测量等外业记录本齐全,并分档按时间顺序保存,记录内容齐全，书写工整无涂改；2.测量成果计算资料和台账齐全', '查资料。无专用记录本扣2分；无目录、索引、编号，导致查找困难扣1分；记录本不全，每缺1种扣3分，无编号1处扣1分；误差超限1处扣2分；原始记录内容不全1处扣1分；无测量成果计算资料和标定解算台账扣5分，测量成果计算资料和标定解算台账中数据不全或错误的，1处扣2分', '1.查是否有导线测量、水准测量、联系测量、井巷施工标定、陀螺定向测量等外业专用记录本;2.查原始记录内容是否齐全、无误：有无目录、索引编号，是否有测量成果计算资料和标定解算台账，测量成果计算资料和标定解算台账中数据是否齐全或无误;3.查是否有测量成果台账;4.对照成果台账现场查是否有标定解算台账，并核查计算数据和计算过程是否是先计算再标定的', '1.导线测量、水准测量、联系测量、井巷施工标定、陀螺定向测量等外业记录本；2.测量成果计算资料和标定解算台账', '1.无专用记录本扣2分;2.无目录、索引、编号，导致查找困难扣1分;3.记录本不全，每缺1种扣3分，无编号1处扣1分;误差超限1处扣2分;4.原始记录内容不全或更改方法不正确，1处扣1分;5.无测量成果计算资料和标定解算台账扣5分;6.测量成果计算资料和标定解算台账中数据不全或错误的，1处扣2分', 6, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (151, 1, 114, '地质灾害防治与测量/基本矿图/测量矿图', '4.9.1(1)', '有采掘工程平面图、工业广场平面图、井上下对照图、井底车场图、井田区域地形图、保安煤柱图、井筒断面图、主要巷道平面图等《煤矿测量规程》规定的基本矿图', '查资料。图种不全，每缺1种扣4分', '1.查阅是否有采掘工程平面图、工业广场平面图、并上下对照图、井底车场图、井田区域地形图、保安煤柱图、井筒断面图、主要巷道平面图等基本矿图;2.抽查矿图，主要是井田区域地形图、保安煤柱图', '采担工程平面图、工业广场平面图、井上下对照图、井底车场图、井田区域地形图、保安煤柱图、井筒新面图、主要巷道平面图等基本矿图', '图种不全，每缺1种扣4分(本项仅检查图种是否齐全，内容正误在下项中检查）', 20, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (152, 1, 115, '地质灾害防治与测量/基本矿图/矿图要求', '4.9.2(1)', '1.基本矿图采用计算机绘制，内容、精度符合《煤矿测量规程》要求；2.图符、线条、注记等符合《煤矿地质测量图例》要求；3.图面清洁、层次分明，色泽准确适度，文字清晰，并按图例要求的字体进行注记；4.采掘工程平面图每月填绘1次，井上下对照图每季度填绘1次，图面表达和注记无矛盾；5.数字化底图至少每季度备份1次', '查资料。图符不符合要求1种扣2分；图例、注记不规范1处扣0.5分；填绘不及时1处扣2分；无数字化底图或未按时备份数据扣2分', '1.查基本矿图是否采用计算机绘制，是否按《煤矿测量规程》规定的要求填绘相关内容，用时更新，精度是否符合要求; 2.查图符、线条、注记等是否符合《煤矿地质测量图例》要求;3.查图面是否清洁、层次分明，色泽准确造度，文字清晰，是否按图例要求的字体进行注记；4.查采掘工程平面图是否按每月填绘1次，并上下对照图是否按每季度填绘1次，图面表达和注记有无矛盾;5.查数字化底图是否至少每季度备份1次', '1.采掘工程平面图、工业广场平面图、井上下对照图、井底车场图、井田区域地形图、保安煤柱图、并筒断面图、主要巷道平面图等基本矿圈;2.数字化底图备份', '1.图符不符合要求1扣2分；2.图例、注记不规范1处扣0.5分;填绘不及时1处扣2分;3.无数字化底图或未按时备份数据扣2分', 20, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (153, 1, 117, '地质灾害防治与测量/沉陷观测控制/地表移动', '4.10.1(1)', '1.进行地面沉陷观测；2.提供符合矿井情况的有关岩移参数', '查资料和现场。未进行地面沉陷观测扣10分，岩移参数提供不符合要求1处扣3分', '查资料和现场，是否进行地面沉陷观测，岩移参数提供是否符合要求', '1.地面沉陷观测资料;2.矿井有关岩移参数计算成果资料', '1.未进行地面沉陷观测扣10分;2.岩移参数提供不符合要求1处扣3分', 15, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (154, 1, 118, '地质灾害防治与测量/沉陷观测控制/资料台账', '4.10.2(1)', '1.及时填绘采煤沉陷综合治理图；2.建立地表塌陷裂缝治理台账、村庄搬迁台账；3.绘制矿井范围内受采动影响土地塌陷图表', '查资料。不符合要求1处扣1分', '1. 查采煤沉陷综合治理图填绘是否及时:2.查是否有地表塌陷裂缝治理台账、村庄搬迁台账;3.查是否有矿井范围内受采动影响土地塌陷图表', '1.采煤沉陪综合治理图;2.地表塌陷裂缝治理台账、村庄搬迁台账;3.矿井范围内受采动影响土地塌陷图表', '不符合要求1处扣1分', 5, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (155, 1, 120, '地质灾害防治与测量/水文地质基础工作/基础工作', '4.11.1(1)', '1.按《煤矿防治水规定》要求进行水文地质观测；2.开展水文地质类型划分工作，发生重大及以上突（透）水事故后，恢复生产前应重新确定；3.对井田范围内及周边矿井采空区位置和积水情况进行调查分析并做好记录，制定相应的安全技术措施', '查资料和现场。水文地质观测不符合《煤矿防治水规定》1处扣2分；未及时划分水文地质类型扣5分；采空区有1处积水情况不清楚扣2分；未制定相应的安全技术措施扣5分', '1.查水文地质观测资料是否矿井涌水量1旬查1次，地下水位每月查1次;其他方面是否按《煤矿防治水规定》要求进行水文地质观测;2.查水文地质类型划分报告是否按要求(要求每3年修编1次，内容符合《煤矿防治水规定》要求)开展水文地质类型划分工作，发生重大及以上突(透)水事故后是否重新确定;3.查是否有井田范围内及周边矿井采空区位置和积水情况进行的调查分析记录及制定的安全技术措施', NULL, NULL, 15, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (156, 1, 121, '地质灾害防治与测量/水文地质基础工作/基础资料', '4.11.2(1)', '1.有井上、井下和不同观测内容的专用原始记录本，记录规范，保存完好；2.按《煤矿防治水规定》要求编制水文地质报告、矿井水文地质类型划分报告、水文地质补充勘探报告，按规定修编、审批水文地质报告；3.建立防治水基础台账（含水文钻孔管理台账）和计算机数据库，并每季度修正1次', '查资料。每缺1种报告扣4分，每缺1种台账扣2分；无水文钻孔管理记录或台账记录不全，1处扣2分；其他1处不符合要求扣1分', '1.查全部井上、井下和不同观测内容的专用原始记录本(如水文观测原始记录台账)，记录是否规范，保存是否完好;2.查是否有专门的水文地质报告或在地质报告中含有水文地质报告;3.查是否按《煤矿防治水规定》要求编制水文地质报告、矿井水文地质类型划分报告、水文地质补充勘探报告，是否按规定修编、审批水文地质报告;4.现场抽查煤矿是否按《煤矿防治水规定》第十六条规定，建立15种防治水基础台账(含水文钻孔管理台账)和计算机数据库，是否每季度修正1次', '1.全部井上、井下和不同观测内容的专用原始记录本;2.水文地质报告、矿井水文地质类型划分报告、水文地质补充勘探报告，审批文件;15种防治水台账(矿井涌水量观测成果台账;气象资料台账;地表水文观测成果台账;钻孔水位、井泉动态观测成果及河流渗漏台账;抽(放)水试验成果台账:矿井突水点台账;井田地质钻孔综合成果台账;井下水文地质钻孔成果台账;水质分析成果台账;水源水质受污染观测资料台账;水源井(孔)资料台账;封孔不良钻孔资料台账;矿井和周边煤矿采空区相关资料台账;水闸门(墙)观测资料台账;其他专门项目的资料台账;4.台账的计算机数据库', '1.每缺1种报告扣4分，每缺1种台账扣2分;\n2.无水文钻孔管理记录或台账记录不全，1处扣2分;\n3.其他1处不符合要求扣1分', 10, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (157, 1, 122, '地质灾害防治与测量/水文地质基础工作/水文图纸', '4.11.3(1)', '1.绘制有矿井充水性图、矿井涌水量与各种相关因素动态曲线图、矿井综合水文地质图、矿井综合水文地质柱状图、矿井水文地质剖面图，图种齐全有电子文档，图纸内容全面、准确；2.在采掘工程平面图和充水性图上准确标明井田范围内及周边采空区的积水范围、积水量、积水标高、积水线、探水线、警戒线', '查资料。每缺1种图纸扣3分，图纸电子文档缺1种扣2分；图种内容有矛盾的1处扣1分；积水区及其参数未在采掘工程平面图和充水性图上标明的1处扣5分，参数标注有误的1处扣2分', '1.查矿井充水性图、矿井涌水量与各种相关因素动态曲线图、矿井综合水文地质图、矿井综合水文地质柱状图、矿井水文地质剖面图，图种是否齐全，是否有电子文档，图纸内容是否全面、准确;2.查采掘工程平面图和矿井充水性图是否准确标明井田范围内及周边采空区的积水范围、积水量、积水标高、积水线、探水线、警戒线', '1.矿井充水性图、矿井涌水量与各种相关因素动态曲线图、矿井综合水文地质图、矿井综合水文地质柱状图、矿井水文地质剖面图，各图种电子文档;2.标明井田范围内及周边采空区的积水范围、积水量、积水标高、积水线、探水线、警戒线的采掘工程平面图和充水性图', '1.每缺1种图纸扣3分，图纸电子文档缺1种扣2 分;\n2.图种内容有矛盾的1处扣1分;\n3.积水区及其参数未在采掘工程平面图和充水性. 图上标明的1处扣5分，参数标注有误的1处扣2分', 10, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (158, 1, 123, '地质灾害防治与测量/水文地质基础工作/水害预报', '4.11.4(1)', '1.年报、月报、临时预报应包含突水危险性评价和水害处理意见等内容，预报内容齐全、下达及时；2.在水害威胁区域进行采掘前，应查清水文地质条件，编制水文地质情况分析报告，报告编制、审批程序符合规定；3.水文地质类型中等及以上的矿井，年初编制年度水害分析预测表及水害预测图；4.编制矿井中长期防治水规划及年度防治水计划，并组织实施', '查资料。因预报失误造成事故不得分，预报缺1次扣2分，预报不能指导生产的1次扣2分；图表不符、描述不准确1处扣1分；预报下发不及时1次扣2分；审批、接收手续不齐全1次扣1分；突水危险性评价缺1次扣2分；无年度水害分析图表扣2分；无中长期防治水规划或年度防治水计划扣3分，未组织实施扣5分', '1.抽查是否有年报、月报、临时预报，是否包含突水危险性评价和水害处理意见等内容，预报内容是否齐全、下达是否及时；2.查在水害威胁区域进行采掘前是否查清水文地质条件，是否编制水文地质情况分析报告，编制、审批程序是否符合规定;3.查地质类型中等及以上的矿井，年初是否编制年度水害分析预测表及水害预测图;4.查是否编制矿井中长期防治水规划及年度防治水计划，是否组织实施', '1.所有年报、月报、临时预报;2.在水客威胁区域采掘前编制的水文地质情况分析报告，审批文件;3.水文地质类型中等及以上矿井年度水害分析预测表及水害预测图;4.矿井中长期防治水规划及年度防治水计划、组织实施验收的总结资料', '1.因预报失误造成事故不得分，预报缺1次扣2分，预报不能指导生产的1次扣2分;2.图表不符、描述不准确1处扣1分:预报下发不及时1次扣2分;审批、接收手续不齐全1次扣1分；突水危险性评价缺1次扣2分;无年度水害分析预测图表扣2分;3.无中长期防治水规划或年度防治水计划扣3分;未组织实施扣5分;4.年度防治水计划无针对性(与开采计划、水文地质类型不一致等) 扣1分', 10, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (159, 1, 125, '地质灾害防治与测量/防治水工程/系统建立', '4.12.1(1)', '1.矿井防排水系统健全，能力满足《煤矿防治水规定》要求；2.水文地质类型复杂、极复杂的矿井建立水文动态观测系统', '查资料和现场。防排水系统达不到规定要求不得分；未按规定建观测系统不得分，系统运行不正常扣5分', '1.查矿井防排水系统是否健全(水泵、排水管路、配电设备、水仓)，能力是否满足《煤矿防治水规定》要求，是否达到初设及矿井涌水量要求;2.查水文地质类型复杂、极复杂的矿井是否建立水文动态观测系统，运行是否正3.抽查水文台账、资料', '矿井防排水系统资料', '1.防排水系统达不到规定要求不得分:2.未按规定建水文动态观测系统或人工观测系统不得分，系统运行不正常扣5分', 10, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (160, 1, 126, '地质灾害防治与测量/防治水工程/技术要求', '4.12.2(1)', '1.井上、井下各项防治水工程有设计方案和施工安全技术措施，并按程序审批，工程结束提交总结报告及验收报告；2.制定采掘工作面超前探放水专项安全技术措施，探测资料和记录齐全；3.探放水工程设计有单孔设计；井下探放水采用专用钻机，由专业人员和专职探放水队伍施工；4.对井田内井下和地面的所有水文钻孔每半年进行1次全面排查，记录详细；5.防水煤柱留设按规定程序审批；6.制定并严格执行雨季“三防”措施', '查资料和现场。各类防治水工程设计及措施不完善扣5分，未经审批扣3分；验收、总结报告内容不全1处扣1分；对充水因素不清地段未坚持“有掘必探”扣10分，单孔设计未达到要求扣3分；无定期排查分析记录，每缺1次扣2分；防水煤柱未按规定程序审批扣3分；未执行雨季“三防”措施扣2分', '1.查井上、井下是否有各项防治水工程设计方案和施工安全技术措施、工程总结报告及验收报告，是否经矿总工程师审批;2.查是否有采掘工作面超前探放水专项安全技术措施、探测资料和记录；3.查探放水工程设计是否有单孔设计；并下探放水是否采用专用钻机，是否由专业人员和专职探放水队伍施工；4.查是否有对井田内井下和地面的所有水文钻孔每半年进行1次全面排查及记录；5.查防水煤柱留设是否按规定程序审批；6.查是否制定并严格执行雨季“三防”措施；7.查所有防治水工程及超前探放水措施落实情况', '1.井上、井下各项防治水工程设计方案和施工安全技术措施、审批文件，工程结束总结报告及验收报告;2.采掘工作面超前探放水专项安全技术措施，探测资料和记录;3.探放水工程设计单孔设计;井下探放水专用钻机，专业人员和专职探放水队伍资质;4.查井田内井下和地面的所有水文钻孔每半年1次全面排查记录;5.防水煤柱留设及审批文件;6.雨季“三防”措施及实施验收总结资料', '1.各类防治水工程设计及措施不完善扣5分，未经审批扣3分:2.验收、总结报告内容不全1处扣1分;3.对充水因素不清地段未坚持“有掘必探”扣10分，单孔设计未达到要求扣3分;有设计未按照设计施工的，1处扣1分，扣到3分为止;4.无定期排查分析记录，每缺1次扣2分;5.防水煤柱未按规定程序审批扣3分;未执行雨季“三防”措施扣2分', 15, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (161, 1, 127, '地质灾害防治与测量/防治水工程/工程质量', '4.12.3(1)', '防治水工程质量均符合设计要求', '查资料和现场。工程质量未达到设计标准1次扣5分；探放水施工不符合规定1处扣5分；超前探查钻孔不符合设计1处扣2分', '1.查防治水工程质量是否达到设计要求，是否对工程质量进行验证，并查施工验收总结资料；2.抽查探放水工程记录，工程质量是否符合设计要求', '1.防治水工程施工验收总结资料;2.探放水工程记录', '1.工程质量未达到设计标准1次扣5分;2.探放水施工不符合规定1处扣5分;3.超前探查钻孔不符合设计1处扣2分', 15, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (162, 1, 128, '地质灾害防治与测量/防治水工程/疏干带压开采', '4.12.4(1)', '用物探和钻探等手段查明疏干、带压开采工作面隐伏构造、构造破碎带及其含（导）水情况，制定防治水措施', '查资料和现场。疏干、带压开采存在地质构造没有查明不得分，其他1项不符合要求扣1分', '查物探、钻探情况资料、水情况分析报告，疏干、带压开采工作面是否用物探和钻探等手段查明隐伏构造、构造破碎带及其含(导)水情况，分析是否正确，是否制定防治水措施，措施是否有效', '1.疏干、带压开采工作面隐伏构造、构造破碎带及其含(导)水情况分析报告;2.物探、钻探资料', '1.疏干、带压开采存在地质构造没有查明不得分:2.其他  项不符合要求扣1分', 5, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (163, 1, 129, '地质灾害防治与测量/防治水工程/辅助工程', '4.12.5(1)', '1.积水能够及时排出；2.按规定及时清理水仓、水沟，保证排水畅通', '查现场。排积水不及时，影响生产扣4分；未及时清理水仓、水沟扣1分', '1.查现场是否存在积水未及时排出，影响正常生产；2.查现场是否每年在雨季前及时清理水仓、水沟，是否保证排水畅通', '清理水仓、水沟计划及验收总结资料', '1.排积水不及时，影响生产扣4分；2.未及时清理水仓、水沟扣1分', 5, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (164, 1, 131, '地质灾害防治与测量/水害预警/水害预警', '4.13.1(1)', '对断层水、煤层顶底板水、陷落柱水、地表水等威胁矿井生产的各种水害进行检测、诊断，发现异常及时预警预控', '查资料和现场。未进行水害检测、诊断或异常情况未及时预警不得分', '1.现场查是否有各种水害进行分析、检测、诊断及预警预控的技术措施记录;2.现场查是否有各种水害检测、诊断的处置方案', '1.各种水客进行分析、检测、诊断、预警预控的技术措施记录；2.各种水害检测、诊断的处置方案', '未进行水害检测、诊断或异常情况未及时预警不得分', 5, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (165, 1, 133, '地质灾害防治与测量/基础管理/组织保障', '4.14.1(1)', '1.按规定设立专门的防冲机构并配备专门防冲技术人员；健全防冲岗位责任制及冲击地压分析、监测预警、定期检查、验收等制度；2.冲击地压矿井每周召开1次防冲分析会，防冲技术人员每天对防冲工作分析1次', '查资料和现场。无管理机构不得分；岗位责任制及冲击危险性分析、监测预警、检查验收制度不全，每缺1项扣2分；人员不足，每缺1人扣1分；其他1处不符合要求扣1分', '1.查文件是否设立专门的防冲机构;是否按规定配备专门防冲技术人员(《煤矿防治冲击地压细则》正在制定中，暂无定员标准，灵活掌握);2.查是否有防冲岗位责任制及冲击地压分析、监测预警、定期检查、验收等制度;3.查冲击地压矿井是否有每周召开1次防冲分析会的会议记录', '1.设立专门防冲机构文件;2.防冲岗位责任制及冲击地压分析、监测预警、定期检查、验收等制度;3.防冲分析会会议记录', '1.无管理机构不得分;2.防冲岗位责任制及冲击危险性分析、监测预警、检查验收制度不全，每缺1项扣2分;3.人员不足，每缺人扣1分;4.其他1处不符合要求扣1分', 10, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (166, 1, 135, '地质灾害防治与测量/防冲技术/技术支撑', '4.15.1(1)', '1.冲击地压矿井应进行煤岩层冲击倾向性鉴定，开采具有冲击倾向性的煤层，应进行冲击危险性评价；2.冲击地压矿井应编制中长期防冲规划与年度防冲计划；3.按规定编制防冲专项设计,按程序进行审批；4.冲击危险性预警指标按规定审批；5.有冲击地压危险的采掘工作面有防冲安全技术措施并按规定及时审批', '查资料。未进行冲击倾向性鉴定、冲击危险性评价，或未编制中长期防冲规划与年度防冲计划、无防冲专项设计或未确定冲击危险性预警指标不得分；工作面设计不符合防冲规定1项扣5分，作业规程中无防冲专项安全技术措施扣5分；采掘工作面防冲安全技术措施审批不及时1次扣5分', '1.查冲击地压矿井是否对每一煤层都进行煤岩层冲击倾向性鉴定，开采具有冲击倾向性的煤层是否进行冲击危险性评价;2.查冲击地压矿井是否编制中长期防冲规划与年度防冲计划;3.查是否编制防冲专项设计，设计是否按程序进行审批(上一级公司审批);4.查冲击危险性预警指标是否按规定审批;5.查有冲击地压危险的采据工作面是否有防冲安全技术措施并按规定及时审批(上一级公司审批)', '1.冲击倾向性鉴定报告、冲击危险性评价报告；2.冲击地压矿井编制的中长期防冲规划与年度防冲计划；3.防冲专项设计；4.冲击危险性预警指标按规定审批文件；5.有冲击地压危险的采掘工作面的防冲安全技术措施及审批文件', '1.未进行冲击倾向性鉴定、冲击危险性评价或未编制中长期防冲规划与年度防冲计划、无防冲专项设计或未确定冲击危险性预警指标不得分；2.工作面设计不符合防冲规定1项扣5分，作业规程中无防冲专项安全技术措施扣5分；3.采据工作面防冲安全技术措施审批不及时1次扣5分', 20, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (167, 1, 136, '地质灾害防治与测量/防冲技术/监测预警', '4.15.2(1)', '1.建立冲击地压区域监测和局部监测预警系统，实时监测冲击危险性；2.区域监测系统应覆盖所有冲击地压危险区域，经评价冲击危险程度高的采掘工作面应安装应力在线监测系统；3.监测系统运行正常，出现故障时及时处理；4.监测指标发现异常时，应采用钻屑法及时进行现场验证', '查资料和现场。未建立区域及局部监测预警系统不得分；监测系统故障处理不及时1次扣2分；区域监测系统布置不合理1处扣1分；发现异常未及时验证1次扣3分', '1.现场检查是否建立冲击地压区域监游和局部监测预警系统，实时监测冲击危险性;2.现场检查区域监测系统是否覆盖所有冲击地压危险区诚，冲击危险程度高的采据工作面是否安装应力在线监测系统;3.查监测系统运行是否正常，出现故障时是否及时处理;4.查监测指标发现异常时是否采用钻屑法及时进行现场验证', '1.建立冲击地压区城监测和局部监测预警系统的记录；2.应力在线监测系统；3.监测系统的运行资料；4.监测指标发现异常时应采用钻属法及时进行现场验证的资科', '1.未建立区域及局部监测预警系统不得分:2.监测系统故障处理不及时1次扣2分:3.区域监测系统布置不合理1处扣1分:4.发现异常未及时验证1次扣3分', 20, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (168, 1, 138, '地质灾害防治与测量/防冲措施/区域防冲措施', '4.16.1(1)', '冲击地压矿井开拓方式、开采顺序、巷道布置、采煤工艺等符合规定；保护层采空区原则不留煤柱，留设煤柱时，按规定审批', '查资料和现场。不符合要求1处扣5分', '1.现场检查冲击地压矿并开拓方式、开采顺序、巷道布置、采煤工艺等是否符合规定;2查保护层采空区留设煤柱时是否彼规定审批', '1. 冲击地压矿井开拓方式、开采顺序、巷道布置、采煤工艺等现场及资料;2. 保护层采空区留设煤柱实际及审批文件', '不符合要求1处扣5分', 10, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (169, 1, 139, '地质灾害防治与测量/防冲措施/局部防冲措施', '4.16.2(1)', '1.钻机等各类装备满足矿井防冲工作需要；2.实施钻孔卸压时，钻孔直径、深度、间距等参数应在设计中明确规定，钻孔直径不小于100mm，并制定安全防护措施；3.实施爆破卸压时，装药方式、装药长度、装药量、封孔长度以及连线方式、起爆方式等参数应在设计中明确规定，并制定安全防护措施；4.实施煤层预注水时，注水方式、注水压力、注水时间等应在设计中明确规定；5.有冲击地压危险的采煤工作面推进速度应在作业规程中明确规定并执行；6.冲击地压危险工作面实施解危措施后，应进行效果检验', '查资料和现场。不落实防冲措施不得分；1项落实不到位扣5分', '1.查台账及钻机等各类装备是否满足矿井防冲工作需要;2.查实施钻孔卸压时，钻孔直径、深度间距等参数是否在设计中明确规定，钻孔直径不小于100mm并制定安全防护措施;3.查实施爆破卸压时，装药方式、装药长度、装药量、封孔长度以及连线方式起爆方式等参数是否在设计中明确规定，并制定安全防护措施;4.查实施煤层预注水时，注水方式、注水压力、注水时间等是否在设计中明确规定5.查有冲击地压危险的采煤工作面推进速度是否在作业规程中明确规定并执行;6.查冲击地压危险工作面实施解危措施后，是否进行效果检验', '1.钻机等各类装备；\n2.钻孔卸压施工设计及安全防护措施;\n3.爆破卸压设计及安全防护措施;\n4.煤层预注水时，注水方式、注水压力、注水时间等设计；\n5.冲击地压危险的采煤工作面作业规程；\n6.效果检验结果', '1.不落实防冲措施不得分2.1项落实不到位扣5分', 20, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (170, 1, 141, '地质灾害防治与测量/防护措施/安全防护', '4.17.1(1)', '1.煤层爆破作业的躲炮距离不小于300m；2.冲击危险区采取限员、限时措施，设置压风自救系统，设立醒目的防冲警示牌、防冲避灾路线图；3.冲击地压危险区存放的设备、材料应采取固定措施，码放高度不应超过0.8m；大型设备、备用材料应存放在采掘应力集中区以外；4.冲击危险区各类管路吊挂高度不应高于0.6m,电缆吊挂应留有垂度；5.U型钢支架卡缆、螺栓等采取防崩措施；6.加强冲击地压危险区巷道支护，采煤工作面两巷超前支护范围和支护强度符合作业规程规定；7.严重冲击地压危险区域采掘工作面作业人员佩戴个人防护装备', '查现场和资料。爆破作业躲炮时间和距离不符合要求1次扣2分；未采取限员限时措施扣5分；未设置压风自救系统扣5分，压风自救系统不完善1处扣2分；图牌板不全，每缺1块扣2分；悬挂不醒目、不规范1处扣2分；通信线路未防护扣4分；巷道不畅通1处扣2分，设备材料码放、管线吊挂不符合要求1处扣2分；锚索、U型钢支架卡缆、螺栓等未采取防崩措施1处扣2分；有冲击地压危险的采掘工作面作业人员未佩戴个人防护装备，发现1人扣1分', '1.查现场煤层爆破作业的躲炮距离是否不小于300m;现场看挂牌管理在什么位置\n2.查现场，看冲击危险区是否采取限员限时措施，是否设置压风自救系统，是否设立醒目的防冲警示牌、防冲避灾路线图;\n3.查冲击地压危险区存放的设备、材料是否采取固定措施，码放高度是否超过0.8m;大型设备、备用材料是否存放在采掘应力集中区以外;', '1.冲击地压危险区掘进、采煤工作面作业规程;\n2.防冲警示牌、防冲避灾路线图;\n3.冲击地压危险区存放的设备、材料存放的大型设备、备用材料;\n4.冲击危险区各类管路吊挂、电缆吊挂\n5.冲击地压危险区巷道支护，采煤工作面两巷超前支护范围和支护强度涉及的作业规程', '1. 爆破作业躲炮时间和距离不符合要求1次扣2分;\n2. 未采取限员限时措施扣5分; 未设置压风自救系统扣5 分, 压风自救系统不完善1处扣2分;图牌板不全,每缺1块扣2分; 悬挂不醒目、 不规范1处扣2分;\n3.通信线路未防护扣4分;巷道不畅通1处扣2分, 设备材料码放、 管线吊挂不符合要求1处扣2分;\n4.锚索、U形钢支架卡缆、 螺栓等未采取防崩措施1处扣2分;', 10, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (171, 1, 143, '地质灾害防治与测量/基础资料/台帐资料', '4.18.1(1)', '1.作业规程中防冲措施编制内容齐全、规范，图文清楚、保存完好，执行、考核记录齐全；2.建立钻孔、爆破、注水等施工参数台账，上图管理；3.现场作业记录齐全、真实、有据可查，报表、阶段性工作总结齐全、规范；4.建立冲击地压记录卡和统计表', '查资料。防冲措施内容不齐全1处扣2分，内容不规范1处扣1分；未建立台账或未上图管理扣5分，台账和图纸不全，每缺1次扣1分；现场作业记录、报表、阶段性工作总结等不齐全1项扣2分；发生冲击地压不及时上报、无记录或瞒报不得分', '1.查作业规程中防冲措施编制内容是否齐全、规范,图文清楚、 保存完好, 执 行、考核记录是否齐全;\n2. 查是否建立钻孔、爆破、 注水等施工参数台账, 是否上图管理,台账是否有签字;\n3.查现场作业记录是否齐全、 真实、 有据可查, 查报表、 阶段性工作总结是否齐全、 规范;\n4. 查是否建立冲击地压记录卡和统计表,查煤矿调度部门冲击地压原始记录', '1. 作业规程, 执行、 考核记录;\n2. 钻孔、 爆破、注水等施工参数台账,上图管理;\n3. 现场作业记最,报表、 阶段性工作总结;\n4.冲击地压记录卡和统计表, 调度处冲击地压原始记录台账', '1. 防冲措施内容不齐全1处扣2分,内容不规范1处扣1分;\n2.未建立台账或未上图管理扣5分,台账和图纸不全,每缺1次扣1分;3. 现场作业记录,报表、阶段性工作总结等不齐全1项扣2分:\n4.发生冲击地压不及时上报,无记录或瞒报不得分', 10, NULL, 1, 87, 0, 1);
INSERT INTO `stdchk_item` VALUES (172, 1, 146, '采煤/基础管理/监测', '5.1.1(1)', '采煤工作面实行顶板动态和支护质量监测，进、回风巷实行顶板离层观测；有相关监测、观测记录，资料齐全', '查现场和资料。未开展动态监测、观测和无记录资料不得分，记录资料缺1项扣0.5分', '1. 查采煤队矿压资料, 是否有完善的工作面、 进风巷、 回风巷矿压观测结果;2.查工作面、进风巷、 回风巷有无动态监测或台账记录; 工作面、 进风巷、 回风巷矿压监测结果;矿压监测结果记录资料是否存在错误;\n3. 查锚板支护是否有顶板离层仪, 并现场观测两帮移动量情况', '1. 月度工作面、 进风巷、 回风巷矿压观测结果;\n2. 动态监测记录、台账记录', '1. 现场没有矿压观测站点,扣3分;\n2. 现场缺1个矿压观测站点, 扣0.5分;\n3. 工作面、进风巷、回风巷矿压观测结果的记录资料不全,扣0.5分', 3, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (173, 1, 147, '采煤/基础管理/规程措施', '5.1.2(1)', '1.作业规程符合《煤矿安全规程》等要求。采煤工作面地质条件发生变化时，及时修改作业规程或补充安全技术措施；2.矿总工程师组织人员定期对作业规程贯彻实施情况进行复审，且有复审意见；3.工作面安装、初次放顶、强制放顶、收尾、回撤、过地质构造带、过老巷、过煤柱、过冒顶区，以及托伪顶开采时，制定安全技术措施并组织实施；4.作业规程中支护方式的选择、支护强度的计算有依据；5.作业规程中各种附图完整规范；6.放顶煤开采工作面开采设计制定有防瓦斯、防灭火、防水等灾害治理专项安全技术措施，并按规定进行审批和验收', '查资料和现场。内容不全，每缺1项扣1分，1项不符合要求扣0.5分', '1. 查采煤队技术作业规程、 补充技术安全措施、 “零星工程\"施工专项措施是否齐全;\n2. 查规程措施是否按采煤作业规程编制、 审批、 复审、 贯彻、 实施制度的规定程序化地执行;\n3.查年度作业计划是否有基本要求第3项中的工作, 是否制定了安全技术措施井组织实施;\n4. 查业务管理部门下达的编制技术作业规程的任务书或通知单、 工作面地质说明书, 工作面开采设计说明书、 技术作业规程的全部“复审意见”, 是否有签字并存在具体意见;\n5. 查作业规程中支护方式的迭择、 支护强度的计算是否有依据', '1.采煤工作面作业规程;\n2.矿井年度作业计划;\n3.作业规程的全部复审意见和补充技术安全措施;\n4.采煤工作面全部\"零星工程\" 施工专项措施;\n5.业务管理部门下达编制的技术作业规程的任务书或通知单;\n6.工作面地质说明书;\n7. 工作面开采设计说明书;\n8.采煤作业规程编制、 审批、 复审、 贯彻、 实施制度;9.放顶煤开采设计批准文件', '1,内容缺1项，扣1分;\n2.有1项不符合要求扣0.5分', 5, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (174, 1, 148, '采煤/基础管理/管理制度', '5.1.3(1)', '1.有岗位安全生产责任制度；2.有工作面顶板管理制度，有支护质量检查、顶板动态监测和分析制度，有变化管理制度；3.有采煤作业规程编制、审批、复审、贯彻、实施制度；4.有工作面机械设备检修保养制度、乳化液泵站管理制度、文明生产管理制度、有工作面支护材料设备配件备用制度等', '查资料。制度不全，每缺1项扣1分，1项不符合要求扣0.5分', '1.查煤矿是否建立“基本要求”中的各项管理制度;\n2.随机抽查涉及“水、火、瓦斯、煤尘、顶板、煤与瓦斯突出”等生产系统和安全防护系统管理制度，所编制度是否齐全有针对性、便于实际管理', '1.岗位安全生产责任制度;\n2.工作面顶板管理制度\n3.支护质量检查、顶板动态监测和分析制度;\n4.变化管理制度;\n5.采煤作业规程编制、审批、复审、贯彻、实施制度;\n6.工作面机械设备检修保养制度;\n7.乳化液泵站管理制度;\n8.文明生产管理制度\n9.工作面支护材料设备配件备用制度;\n10.“水、火、瓦斯、煤尘、顶板、煤与瓦斯突出”等生产系统和安全防护系统管理制度', '1.每缺1项制度，扣1分；\n2.任一制度中缺关键内容或不符合要求扣0.5分', 3, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (175, 1, 149, '采煤/基础管理/支护材料', '5.1.4(1)', '支护材料有管理台账，单体液压支柱完好，使用期限超过8个月后，应进行检修和压力试验，记录齐全；现场备用支护材料和备件符合作业规程要求', '查现场和资料。不符合要求1处扣0.5分', '1,查采煤工作面支护材料管理台账和单体液压支柱打压试验报告，是否存在超8个月期限使用的单体液压支柱;\n2.查使用超过8个月的单体液压支柱是否有检修和并下试验记录；新单体液压支柱是香有合格证;3章现场备用支护材料和备是否符合要求', '1.工作面支护材料管理台账\n2.单体液压支柱检修和打压试验报告\n3.作业规程\n4.新单体液压支柱合格证', '1.工作面支护材料管理台账填写不全，扣0.5分\n2.单体液压支柱压力验报告不规范，扣0.5分\n3.新单体液压支柱没有合格证，扣0.5分', 2, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (176, 1, 150, '采煤/基础管理/采煤机械化', '5.1.5(1)', '采煤工作面采用机械化开采', '查现场。未使用机械化开采不得分', '现场检查井下采煤工作面是否是采用机械化开采', NULL, '未采用机械化开采不得分', 2, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (177, 1, 152, '采煤/岗位规范/专业技能', '5.2.1(1)', '管理和技术人员掌握相关的岗位职责、管理制度、技术措施，作业人员掌握本岗位操作规程、作业规程相关内容和安全技术措施', '查资料和现场。不符合要求1处扣0.5分', '抽查提问在岗人员是否掌握岗位责任制、管理制度、技术措施；随机抽考管理和技术人员，提3个问题，如果都回答不正确，视为不符合要求；随机抽取1名关键岗位人员，提问1个最关键应知应会问题，如果回答不正确，视为不符合要求', '1.岗位操作规程;\n2.作业规程、安全技术措施、操作规程等', '1.管理和技术人员，提同一个问题,3个人都回答不正确，扣0.5分;\n2.操作人员，向同一个人提3个问题，都回答不正确，扣0.5分;\n3.区队管理人员，提1个与岗位相关的关键问题回答不正确，扣0.5分;\n4.直至本小项分值扣完为止', 2, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (178, 1, 153, '采煤/岗位规范/规范作业', '5.2.2(1)', '1.现场作业人员严格执行本岗位安全生产责任制，掌握本岗位相应的操作规程和安全措施，操作规范，无“三违”行为2.作业前进行安全确认；3.零星工程施工有针对性措施、有管理人员跟班', '查现场和资料。发现“三违”不得分，其他不符合要求1处扣1分', '1.查现场各专业作业前是否掌握本岗位安全确认的内容;\n2.对照岗位操作规程抽查现场实操，看岗位作业人员能否规范作业;\n3.查看现场是否有“三违”行为;\n4.查零星工程台账有无零星工程验收单，是否有措施，定位系统有无管理人员(班组长以上)在现场', '1.技术作业规程安全技术措施;\n2.零星工程施工针对性措施;\n3.零星工程台账', '1.发现1处“三违”行为，扣3分;2.对其他不符合要求1处扣1分', 3, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (179, 1, 155, '采煤/质量与安全/顶板管理', '5.3.1(1)', '1.工作面液压支架初撑力不低于额定值的80%，有现场检测手段；单体液压支柱初撑力符合《煤矿安全规程》要求', '查现场。沿工作面均匀选10个点现场测定， 1点不符合要求扣1分', ' 1.在工作面沿工作面均匀选10个点现场测定，查初撑力是否低于额定值的80%;\n2.查单体液压支柱压力表初撑力是否符合《煤矿安全规程》第一百零一条规定', '1.液压支架初撑力检测设备;\n2.单体液压支柱压力表', '1.1点不符合要求扣1分2.压力表缺失或损坏的,1处扣0.5分，扣满1分为止', 4, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (180, 2, 155, '采煤/质量与安全/顶板管理', '5.3.1(2)', '2.工作面支架中心距（支柱间排距）误差不超过100mm，侧护板正常使用，架间间隙不超过100mm（单体支柱间距误差不超过100mm）；支架（支柱）不超高使用，支架（支柱）高度与采高相匹配，控制在作业规程规定的范围内，支架的活柱行程不小于200mm（企业特殊定制支架、支柱以其技术指标为准）', '查现场。沿工作面均匀选10个点现场测定，1点不符合要求扣1分', '查沿工作面均匀选10个点现场测定值是否符合规定要求', '作业规程', '1点不符合要求扣1分', 4, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (181, 3, 155, '采煤/质量与安全/顶板管理', '5.3.1(3)', '3.液压支架接顶严实，相邻支架（支柱）顶梁平整，无明显错茬（不超过顶梁侧护板高的2/3），支架不挤不咬；采高大于3.0m或片帮严重时，应有防片帮措施；支架前梁（伸缩梁）梁端至煤壁顶板垮落高度不大于300mm。高档普采（炮采）工作面机道梁端至煤壁顶板垮落高度不大于200mm，超过200mm时采取有效措施', '查现场和资料。不符合要求1处扣1分', '查全工作面，逐条对照“基本要求”是否符合规定要求', '作业规程', '不符合要求1处扣1分', 2, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (182, 4, 155, '采煤/质量与安全/顶板管理', '5.3.1(4)', '4.支架顶梁与顶板平行,最大仰俯角不大于7°；支架垂直顶底板，歪斜角不大于5°；支柱垂直顶底板，仰俯角符合作业规程规定', '查现场和资料。不符合要求1处扣0.5分', '查全工作面，看液压支架(单体液压支柱)是否垂直顶底板仰俯角是否符合作业规程规定', '作业规程', '不符合要求1处扣1分', 2, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (183, 5, 155, '采煤/质量与安全/顶板管理', '5.3.1(5)', '5.工作面液压支架（支柱顶梁）端面距符合作业规程规定。工作面“三直一平”，液压支架（支柱）排成一条直线，其偏差不超过50mm。工作面伞檐长度大于1m时，其最大突出部分，薄煤层不超过150mm，中厚以上煤层不超过200mm；伞檐长度在1m及以下时，最突出部分薄煤层不超过200mm，中厚煤层不超过250mm', '查现场和资料。不符合要求1处扣1分', '1.要求被检煤矿在工作面液压支架大柱前，提前拉上一条线或分二至三段拉线，以线为基准进行检查;\n2.查工作面液压支架(单体液压支柱顶梁)端面距是否符合作业规程规定;\n3.查工作面是否“三直一平”，液压支架(单体液压支柱)是否排成一条直线，偏差是否超过50mm;液压支架是否有吊斜;\n4.有伞檐时，与陪检人员共同目测确定：伞檐长度大于1m时，薄煤层最大突出部分是否超过150mm,中厚以上煤层是否超过200mm;伞檐长度在1m及以下时，薄煤层最大突出部分是否超过200mm,中厚以上煤层是否超过250mm', '作业规程', '1.不符合要求1处扣1分；2.液压支架吊斜不成直线扣1分', 4, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (184, 6, 155, '采煤/质量与安全/顶板管理', '5.3.1(6)', '6.工作面内液压支架（支柱）编号管理，牌号清晰', '查现场。不符合要求1处扣0.5分', '1.查工作面液压支架(单体液压支柱)有无编号管理;\n2.查液压支架(单体液压支柱)所编牌号是否清晰，是否存在缺编号码', NULL, '液压支架(单体液压支柱)缺失牌号或牌号不清晰,1处扣0.5分', 2, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (185, 7, 155, '采煤/质量与安全/顶板管理', '5.3.1(7)', '7.工作面内特殊支护齐全；局部悬顶和冒落不充分的，悬顶面积小于10m2时应采取措施，悬顶面积大于10m2时应进行强制放顶。特殊情况下不能强制放顶时，应有加强支护的可靠措施和矿压观测监测手段', '查现场和资料。1处不符合要求不得分', '1.查工作面是否有局部悬顶和冒落不充分情况，与陪检人员共同目测确定：悬顶面积小于10m2时是否采取措施；悬顶面积大于10m2时是否进行强制放顶；特殊情况下不能强制放顶时，是否有加强支护的可靠措施和矿压观测监测手段;\n2.查上、下隅角以里部分是否有局部悬顶和冒落不充分情况观测监测手段', '1.安全技术措施(悬顶面积小于10m2时)\n2.强制放顶补充安全技术措施(悬顶面积大于10m2时);\n3.加强支护补充安全技术措施和矿压观测监测补充安全技术措施(特殊情况下不能强制放顶时)', '1处不符合要求不得分', 2, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (186, 8, 155, '采煤/质量与安全/顶板管理', '5.3.1(8)', '8.不随意留顶煤、底煤开采，留顶煤、托夹矸开采时，制定专项措施', '查现场和资料。不符合要求1处扣0.5分，留顶煤、托夹矸回采时无专项措施不得分', '1.查工作面是否随意留顶煤、底煤开采\n2.查工作面留顶煤、托夹矸开采时，是否制定有专项措施', '1.作业规程;\n2.留顶煤、托夹矸开采的专项补充安全技术措施', '1.工作面留顶煤、托矸开采时，没有制定专项补充安全技术措施，扣2分；\n2.工作面存在1处留顶煤、底煤开采，且留煤厚度大于0.3m,扣0.5分扣到1分为止;\n3.专项补充安全技术措施制定不规范、针对性不强或没有严格落实1处扣0.5分', 2, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (187, 9, 155, '采煤/质量与安全/顶板管理', '5.3.1(9)', '9.工作面因顶板破碎或分层开采，需要铺设假顶时，按照作业规程的规定执行', '查现场和资料。不符合要求1处扣0.5分', '查全工作面，铺设假顶质量是否符合作业规程或铺设假顶补充安全技术措施的相关规定', '1.作业规程;\n2.铺设假顶补充安全技术措施', '不符合要求1处扣0.5分', 2, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (188, 10, 155, '采煤/质量与安全/顶板管理', '5.3.1(10)', '10.工作面控顶范围内顶底板移近量按采高不大于100mm/m；底板松软时，支柱应穿柱鞋，钻底小于100mm；工作面顶板不应出现台阶式下沉', '查现场。不符合要求1处扣0.5分', '1.查全工作面，工作面控顶范围内顶底板移近量是否按采高不大于100mm/m;\n2.查单体液压支柱底板松软时，是否穿柱鞋，是否使钻底小于100mm;\n3.查工作面顶板是否出现台阶式下沉', '1.测量工具;2.单体液压支柱', '不符合要求1处扣0.5分', 2, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (189, 11, 155, '采煤/质量与安全/顶板管理', '5.3.1(11)', '11.坚持开展工作面工程质量、顶板管理、规程落实情况的班评估工作，记录齐全，并放置在井下指定地点', '查现场。1处不符合要求不得分', '1.查生产现场有无\n\"班评估”记录资料；2.查“班评估”记录是否齐全;\n3.查“班评估”内容是否突出“质量与安全”中的重点“顶板管理”项目', '“班评估”记录手册', '1.没有开展“班评估“工作，扣2分;\n2.记录不符合要求的1处扣0.5分', 2, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (190, 1, 156, '采煤/质量与安全/安全出口与端头支护', '5.3.2(1)', '1.工作面安全出口畅通，人行道宽度不小于0.8m，综采（放）工作面安全出口高度不低于1.8m，其他工作面不低于1.6m。工作面两端第一组支架与巷道支护间距不大于0.5m，单体支柱初撑力符合《煤矿安全规程》规定', '查现场。1处不符合要求不得分', '1.查工作面两个安全出口是否畅通;\n2.查现场人行道宽度是否不小于0.8综采(放)工作面安全出口高度是否不低于1.8m,其他工作面是否不低于1.6m;\n3.查工作面两端第\n组支架与巷道支护间距是否不大于0.5m:\n4.查单体液压支柱初撑力是否低于18MPa(《煤矿安全规程》第一百零一条规定)', '1.测量工具;\n2.单体液压支柱压力表', '1处不符合要求不得分', 4, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (191, 2, 156, '采煤/质量与安全/安全出口与端头支护', '5.3.2(2)', '2. 条件适宜时，使用工作面端头支架和两巷超前支护液压支架', '查现场。1处不符合要求不得分', '查条件适宜时是否使用工作面端头支架和两巷超前液压支架', NULL, '1处不符合要求不得分', 1, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (192, 3, 156, '采煤/质量与安全/安全出口与端头支护', '5.3.2(3)', '3.进、回风巷超前支护距离不小于20m，支柱柱距、排距允许偏差不大于100mm，支护形式符合作业规程规定；进、回风巷与工作面放顶线放齐（沿空留巷除外），控顶距应在作业规程中规定；挡矸有效', '查现场和资料。超前支护距离不符合要求不得分，其他1处不符合要求扣0.5分', '1.查进、回风巷超前支护距离是否不小于20m;\n2.查进、回风巷超前支护使用的单体液压支柱的柱距、排距允许偏差是否不大于100mm,支护形式是否符合作业规程规定;\n3.查现场进、回风巷与工作面放顶线放齐(沿空留巷除外),控顶距是否符合作业规程的规定\n4.查看现场矸石是否冒落往人行道蹿矸，查进、回风巷切顶线支护挡矸是否有效', NULL, '1.进、回风巷超前支护距离有1处小于20m时扣4分;\n2.进、回风巷超前支护使用的单体液压支柱，每有1处柱距、排距偏差大于100mm时，扣0.5分;\n3.进、回风巷超前支护形式有1处不符合作业规程规时，扣0.5分;\n4.进、回风巷与工作面放顶线放齐(沿空留巷除外),控顶距有1处不符合作业规程的规定时，扣0.5分\n5.进、回风巷切顶线支护挡矸有1处效果差，扣0.5分;\n6.其他1处不符合要求', 4, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (193, 4, 156, '采煤/质量与安全/安全出口与端头支护', '5.3.2(4)', '4.架棚巷道超前替棚距离、锚杆、锚索支护巷道退锚距离符合作业规程规定。', '查现场和资料。不符合要求1处扣0.5分', '1.查现场架棚巷道超前替棚距离是否符合作业规程规定;\n2.查现场锚杆、锚索支护巷道退锚距离是否符合作业规程规定', '作业规程', '不符合要求1处扣0.5分', 2, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (194, 1, 157, '采煤/质量与安全/安全设施', '5.3.3(1)', '1.各转载点有喷雾灭尘装置，带式输送机机头、乳化液泵站、配电点等场所消防设施齐全', '查现场。1处不符合要求扣0.5分', '1.查备转载点是否有喷雾灭尘装置;\n2.查带式输送机机头、乳化液泵站、配电点、可燃性物料存放处配置的消防设施(如灭火器、砂箱、消防软管等)是否齐全是否有消防设施安全管理牌板', NULL, '1处不符合要求扣0.5分', 3, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (195, 2, 157, '采煤/质量与安全/安全设施', '5.3.3(2)', '2.设备转动外露部位、溜煤眼及煤仓上口等人员通过的地点有可靠的安全防护设施', '查现场。1处不符合要求不得分', '1.查现场设备转动外露部位是否有可靠的安全防护设施;\n2.查现场溜煤眼及煤仓上口等人员通过的地点，是否有可靠的安全防护设施', NULL, '1处不符合要求不得分', 2, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (196, 3, 157, '采煤/质量与安全/安全设施', '5.3.3(3)', '3.单体液压支柱有防倒措施；工作面倾角大于15º时，液压支架有防倒、防滑措施，其他设备有防滑措施；倾角大于25º时，有防止煤（矸）窜出伤人的措施', '查现场。不符合要求1处扣0.5分', '1.查现场单体液压支柱是否有防倒措施;\n2.查液压支架工作面倾角大于15°时，是否有防倒、防滑措施;\n3.查其他设备工作面倾角大于15°时，是否有防滑措施;\n4.查倾角大于25°时，是否有防止煤(矸)窜出伤人的措施', NULL, '不符合要求1处扣0.5分', 3, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (197, 4, 157, '采煤/质量与安全/安全设施', '5.3.3(4)', '4.行人通过的输送机机尾设盖板；输送机行人跨越处有过桥；工作面刮板输送机信号闭锁符合要求', '查现场。不符合要求1处扣0.5分', '1.查行人通过的刮板输送机机尾是否设盖板;\n2.查带式输送机行人跨越处是否有过桥;\n3.查工作面刮板输送机信号闭锁是否符合要求', NULL, '不符合要求1处扣0.5分', 2, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (198, 5, 157, '采煤/质量与安全/安全设施', '5.3.3(5)', '5.破碎机安全防护装置齐全有效', '查现场。不符合要求不得分', '查现场破碎机观察板等防护措施是否被卸掉', NULL, '不符合要求不得分', 1, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (199, 1, 159, '采煤/机电设备/设备选型', '5.4.1(1)', '1.支护装备（泵站、支架及支柱）满足设计要求', '查现场和资料。不符合要求不得分', '现场对照资料，查支护装备(乳化液泵站、液压支架、单体液压支柱)是否满足工作面设计和作业规程要求', '1.工作面设计;\n2.作业规程', '不符合要求不得分', 2, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (200, 2, 159, '采煤/机电设备/设备选型', '5.4.1(2)', '2.生产装备选型、配套合理，满足设计生产能力需要', '查现场和资料。不符合要求不得分', '对照规程、设计，现场检查生产装备(采煤机、刮板输送机、转载机、液压支架、带式输送机)选型、配套是否合理，是否满足工作面开采设计需要', '1.工作面设计;\n2.作业规程', '不符合要求不得分', 2, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (201, 3, 159, '采煤/机电设备/设备选型', '5.4.1(3)', '3.电气设备满足生产、支护装备安全运行的需要', '查现场和资料。不符合要求不得分', '对照设计、规程现场检查电气设备是否满足生产、支护装备安全运行的需要', '1.工作面设计;\n2.作业规程', '不符合要求不得分', 2, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (202, 1, 160, '采煤/机电设备/设备管理', '5.4.2(1)', '1.泵站：（1）乳化液泵站完好，综采工作面乳化液泵压力不小于30MPa，炮采、高档普采工作面乳化液泵压力不小于18MPa，乳化液（浓缩液）浓度符合产品技术标准要求，并在作业规程中明确规定；（2）液压系统无漏、窜液，部件无缺损，管路无挤压；注液枪完好，控制阀有效；（3）采用电液阀控制时，净化水装置运行正常，水质、水量满足要求；（4）各种液压设备及辅件合格、齐全、完好，控制阀有效，耐压等级符合要求，操纵阀手把有限位装置', '查现场和资料。不符合要求1处扣1分', '1.对照资料现场检查乳化液泵站是否完好\n2.对照资料查综采工作面乳化液泵压力是否不小于30MPa;\n3.对照资料查炮采、高档普采工作面乳化液泵压力是否不小于18 MPa\n4.对照资料查乳化液(浓缩液)浓度是否符合产品技术标准要求，是否在作业规程中明确规定;\n5.查现场液压系统是否有漏、窜液，部件是否缺损，管路是否挤压;\n6.检查现场注液枪是否完好，控制阀是否有效;\n7.采用电液阀控制时，现场查净化水装置运行是否正常，并对照资料查水质、水量是否满足要求;', '1.乳化油(浓缩液产品技术说明书;\n2.配制乳化液所用水源水质的化验分析单\n3.乳化液泵和乳化液箱的使用保养技术说明书;\n4.作业规程', '不符合要求1处扣1分', 4, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (203, 2, 160, '采煤/机电设备/设备管理', '5.4.2(2)', '2.采（刨）煤机：（1）采（刨）煤机完好；（2）采煤机有停止工作面刮板输送机的闭锁装置；（3）采（刨）煤机设置甲烷断电仪或者便携式甲烷检测报警仪，且灵敏可靠；（4）采（刨）煤机截齿、喷雾装置、冷却系统符合规定，内外喷雾有效；（5）采（刨）煤机电气保护齐全可靠；（6）刨煤机工作面至少每隔30m装设能随时停止刨头和刮板输送机的装置或向刨煤机司机发送信号的装置；有刨头位置指示器；（7）大中型采煤机使用软启动控制装置；（8）采煤机具备遥控控制功能', '查现场和资料。第（1）～（6）项不符合要求1处扣0.5分，第（7）~（8）项不符合要求1处扣0.1分', '1.现场查采(刨)煤机是否完好;\n2.现场查采煤机是否有停止工作面刮板输送机的闭锁装置;\n3.现场查采(刨)煤机是否设置甲烷断电仪或者便携式甲烷检测报警仪，是否灵敏可靠;\n4.对照说明书现场查采(刨)煤机截齿喷雾装置、冷却系统是否符合规定，内外喷雾是否有效;\n5.现场查采(刨)煤机电气保护是否齐全可靠;\n6.现场查刨煤机工作面是否至少每隔30m装设能随时停止刨头和刮板输送机的装置或向刨煤机司机发送信号的装置；是否有刨头位置指示器;\n7.现场查大中型采煤机是否使用软启动控制装置(现场启动试验1次);8.现场检查果煤机是否具备遥控控制功能', '采(刨)煤机的使用保养技术说明书', '1.第(1)~(6)项不合要求,1处扣0.5分;\n2.第(7)~(81项不合要求,1处扣0.1分', 3, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (204, 3, 160, '采煤/机电设备/设备管理', '5.4.2(3)', '3.刮板输送机、转载机、破碎机：（1）刮板输送机、转载机、破碎机完好；（2）使用刨煤机采煤、工作面倾角大于12°时，配套的刮板输送机装设防滑、锚固装置；（3）刮板输送机机头、机尾固定可靠；（4）刮板输送机、转载机、破碎机的减速器与电动机软连接或采用软启动控制，液力偶合器不使用可燃性传动介质（调速型液力偶合器不受此限），使用合格的易熔塞和防爆片；（5）刮板输送机安设有能发出停止和启动信号的装置；（6）刮板输送机、转载机、破碎机电气保护齐全可靠，电机采用水冷方式时，水量、水压符合要求。', '查现场和资料。不符合要求1处扣0.5分', '1.现场查刮板输送机、转载机、破碎机是否有破损和漏油现象2.使用刨煤机采煤、工作面倾角大于12°时，现场查配套的刮板输送机装是否设防滑、锚固装置;3.现场查刮板输送机机头、机尾是否固定可靠;4.查变频器和耦合器或者直接启动电动机，刮板输送机、转载机、破碎机的减速器与电动机是否采用软连接或采用软启动控制(现场启动试验1次)\n5.现场查液力偶合器是否使用可燃性传动介质(调速型液力偶合器不受此限);老旧机器是否使用合格的易熔塞和防爆片(是否有工作油流出来)6.现场查刮板输送机是否安设有能发出停止和启动信号的装置7.对照说明书现场查刮板输送机、转载机、破碎机电气保护是否齐全可靠；电机采用水冷方式时，水量、水压是否符合说明书要求', '1.刮板输送机使用说明书;\n2.转载机使用说明书\n3.破碎机使用术说明书', '不符合要求1处扣0.5分', 2, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (205, 4, 160, '采煤/机电设备/设备管理', '5.4.2(4)', '4.带式输送机：（1）带式输送机完好，机架、托辊齐全完好，胶带不跑偏；（2）带式输送机电气保护齐全可靠；（3）带式输送机的减速器与电动机采用软连接或软启动控制，液力偶合器不使用可燃性传动介质（调速型液力偶合器不受此限），并使用合格的易熔塞和防爆片；（4）使用阻燃、抗静电胶带，有防打滑、防堆煤、防跑偏、防撕裂保护装置，有温度、烟雾监测装置，有自动洒水装置；（5）带式输送机机头、机尾固定牢固，机头有防护栏，有防灭火器材，机尾使用挡煤板、有防护罩。在大于16°的斜巷中带式输送机设置防护网，并采取防止物料下滑、滚落等安全措施；（6）连续运输系统有连锁、闭锁控制装置，全线安设有通信和信号装置；（7）上运式带式输送机装设防逆转装置和制动装置，下运式带式输送机装设软制动装置和防超速保护装置；（8）带式输送机安设沿线急停装置；（9）带式输送机系统宜采用无人值守集中综合智能控制方式', '查现场和资料。第（1）～（8）项不符合要求1处扣0.5分，第（9）项不符合要求扣0.1分', '1.对照说明书，现场检查带式输送机是否完好，机架、托辊是否齐全完好，胶带是否跑偏;2.对照说明书，现场检查带式输送机电气保护是否齐全可靠', '带式输送机使用说明书', '1.第1~8项不符合要求,1项扣0.5分;\n2.第9项不符合要求，扣0.1分', 2, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (206, 5, 160, '采煤/机电设备/设备管理', '5.4.2(5)', '5.辅助运输设备完好，制动可靠，安设符合要求，声光信号齐全；轨道铺设符合要求；钢丝绳及其使用符合《煤矿安全规程》要求，检验合格', '查现场。不符合要求1处扣0.5分', '1.对照设备使用保养技术说明书，现场检查辅助运输设备(绞车等)是否完好，制动是否可靠，安设是否符合要求，声光信号是否齐全;\n2.现场检查轨道铺设是否符合要求;\n3.通过现场观察钢丝绳磨损是否超限,\n般上限为原绳的三分之一，来检查钢丝绳及其使用是否符合《煤矿安全规程》要求；现场查钢丝绳质量合格证是否检验合格', NULL, '不符合要求1处扣0.5分', 1, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (207, 6, 160, '采煤/机电设备/设备管理', '5.4.2(6)', '6.通信系统畅通可靠，工作面每隔15m及变电站、乳化液泵站、各转载点有语音通信装置；监测、监控设备运行正常，安设位置符合规定', '查现场。不符合要求1处扣0.5分', '1.现场查通信系统是否畅通可靠，工作面每隔15m、变电站、乳化液泵站、各转载点是否设有语音通信装置;\n2.现场检查监测、监控设备运行是否正常，安设位置是否符合作业规程规定', NULL, '1.语音通信不清楚扣0.5分;\n2.其他不符合要求1处扣0.5分', 1, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (208, 7, 160, '采煤/机电设备/设备管理', '5.4.2(7)', '7.小型电器排列整齐，干净整洁，性能完好；机电设备表面干净，无浮煤积尘；移动变电站完好；接地线安设规范；开关上架，电气设备不被淋水；移动电缆有吊挂、拖曳装置', '查现场。1处不符合要求不得分', '1.查看现场小型电器是否排列整齐，干净整洁，性能完好;\n2,现场查看机电设备表面是否干净，无浮煤积尘;\n3.现场检查移动变电站是否完好;\n4.现场检查接地线是否安设规范;5.现场查看开关上架是否牢稳、防湿、防蚀;\n6.现场查看电气设备是否被淋水;\n7.现场查看移动电缆是否有吊挂、拖曳装置(单轨吊、电缆夹)', NULL, '1处不符合要求不得分', 1, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (209, 1, 162, '采煤/文明生产/面外环境', '5.5.1(1)', '1.电缆、管线吊挂整齐，泵站、休息地点、油脂库、带式输送机机头和机尾等场所有照明；图牌板（工作面布置图、设备布置图、通风系统图、监测通信系统图、供电系统图、工作面支护示意图、正规作业循环图表、避灾路线图；炮采工作面增设炮眼布置图、爆破说明书等）齐全、清晰整洁；巷道每隔100m设置醒目的里程标志', '查现场。不符合要求1项扣1分', '1.现场查电缆、管线是否吊挂整齐;\n2,现场查泵站、休息地点、油脂库、带式输送机机头和机尾、各类材料与配件码放地点等场所是否有照明\n3.现场查图牌板(工作面布置图、设备布置图、通风系统图、监测通信系统图、供电系统图、工作面支护示意图、正规作业循环图表、避灾路线图；炮采工作面增设炮眼布置图、爆破说明书等)是否齐全、清晰整洁;\n4.现场查巷道每隔100m是否设置醒目的里程标志', NULL, '1.图牌板图有错误，扣0.5分;\n2.其他不符合要求1项扣1分', 2, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (210, 2, 162, '采煤/文明生产/面外环境', '5.5.1(2)', '2.进、回风巷支护完整，无失修巷道；设备、物料与胶带、轨道等的安全距离符合规定，设备上方与顶板距离不小于0.3m', '查现场。不符合要求1项扣1分', '1.现场查工作面进、回风巷支护是否完整，无失修巷道;\n2.现场查设备、物料最突出部分与胶带、轨道等的安全距离是否符合《煤矿安全规程》规定;\n3.现场查设备上方与顶板距离是否不小于0.3m', NULL, '不符合要求1项扣1分', 3, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (211, 3, 162, '采煤/文明生产/面外环境', '5.5.1(3)', '3.巷道及硐室底板平整，无浮碴及杂物、无淤泥、无积水；管路、设备无积尘；物料分类码放整齐，有标志牌，设备、物料放置地点与通风设施距离大于5m', '查现场。不符合要求1项扣1分', '1.现场查工作面进回风巷道及硐室底板是否平整，是否有浮碴及杂物、淤泥、积水\n2.现场查管路、设备有无积尘;\n3.现场检查物料是否分类码放整齐，有无安全管理标志牌;\n4.现场查设备、物料放置地点与通风设施距离是否大于5m', NULL, '不符合要求1项扣1分', 2, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (212, 1, 163, '采煤/文明生产/面内环境', '5.5.2(1)', '工作面内管路敷设整齐，支架内无浮煤、积矸，照明符合规定。', '查现场。不符合要求1项扣1分', '1.现场查工作面内管路敷设是否整齐;\n2.现场查液压支架内有无浮煤、积矸;\n3.现场查工作面照明是否符合作业规程的规定', NULL, '不符合要求1项扣1分', 3, NULL, 1, 71, 0, 1);
INSERT INTO `stdchk_item` VALUES (213, 1, 166, '掘进/生产组织/机械化程度', '6.1.1(1)', '1.煤巷、半煤岩巷综合机械化程度不低于50%；2.条件适宜的岩巷宜采用综合机械化掘进；3.采用机械装、运煤（矸）；4.材料、设备采用机械运输，人工运料距离不超过300m', '查资料和现场。煤巷、半煤岩巷综合机械化程度不符合要求、没有采用机械化装运煤（矸）不得分，条件适宜的岩巷没有采用综掘的扣0.1分，人工运料距离超过规定每增加20m，扣0.1分', '1.查全矿掘进工作面现场，按照掘进机械化和运输机械化进尺占掘进总进尺比例,折合计算矿并综合机械化程度;\n2.查掘进作业规程中提供的岩石硬度系数、巷道倾角等条件,判断是否为适用机械化掘进，条件适宜的岩巷，有无采用机械化\n3.查现场的机械设备情况，判定是否采用机械装、运煤(矸);\n4.查现场，看材料、设备是否采用机械运输，人工运料距离是否超过300m', '作业规程', '1.综合机械化程度不50%,扣2分;\n2,没有采用机械化装运煤(矸)的，扣2分;\n3.条件适宜的岩巷没有采用综掘的，扣0.1分\n4.人工运料距离,300m以上每增加20m,扣0.1分', 2, NULL, 1, 69, 0, 1);
INSERT INTO `stdchk_item` VALUES (214, 1, 167, '掘进/生产组织/劳动组织', '6.1.2(1)', '1.掘进作业应按循环作业图表施工；2.完成考核周期内进尺计划；3.掘进队伍工种配备满足作业要求', '查现场和资料。不符合要求1项扣1分', '1.对照循环作业图表，查现场掘进作业是否按照图表进行;\n2.对照月度考核进尺计划，查现场作业情况：循环作业图表中每个圆班循环个数乘以循环进尺乘以每月25.5个工作日，与每个月实际进尺比对;或查矿调度台账实际完成进尺与计划比对;看是否完成考核周期进尺计划;\n3.通过查入井培训资格证，现场查特殊工种证检查工种配备是否满足要求', '1.矿月度计划表和完成情况调度统计台账\n2.作业规程循环作业图表;\n3.各工种入井培资格证、特殊工种证', '1.循环作业图表与煤矿现场不符合，扣1分;\n2.其他不符合要求1项扣1分', 3, NULL, 1, 69, 0, 1);
INSERT INTO `stdchk_item` VALUES (215, 1, 169, '掘进/设备管理/掘进机械', '6.2.1(1)', '1.掘进施工机（工）具完好；2.掘进机械设备完好，截割部运行时人员不在截割臂下停留和穿越，机身与煤（岩）壁之间不站人；综掘机铲板前方和截割臂附近无人时方可启动,停止工作和交接班时按要求停放综掘机，将切割头落地，并切断电源；移动电缆有吊挂、拖曳、收放、防拔脱装置，并且完好；掘进机、掘锚一体机、连续采煤机、梭车、锚杆钻车装设甲烷断电仪或者便携式甲烷检测报警仪；3.使用掘进机、掘锚一体机、连续采煤机掘进时，开机、退机、调机时发出报警信号，设备非操作侧设有急停按钮（连续采煤机除外），有前照明和尾灯；内外喷雾使用正常；4.安装机载照明的掘进机后配套设备（如锚杆钻车等）启动前开启照明；5.耙装机装设有封闭式金属挡绳栏和防耙斗出槽的护栏，固定钢丝绳滑轮的锚桩及其孔深和牢固程度符合作业规程规定，机身和尾轮应固定牢靠；上山施工倾角大于20°时，在司机前方设有护身柱或挡板，并在耙装机前增设固定装置；在斜巷中使用耙装机时有防止机身下滑的措施。耙装机距工作面的距离符合作业规程规定。耙装机作业时有照明。高瓦斯、煤与瓦斯突出和有煤尘爆炸危险性的矿井煤巷、半煤岩巷掘进工作面和石门揭煤工作面，不使用钢丝绳牵引的耙装机', '查现场和资料。掘进机械设备不完好或违反规定使用钢丝绳牵引的耙装机不得分；综掘机运行时有人员在截割臂下停留和穿越、机身与煤（岩）壁之间站人扣5分；其他1处不符合要求扣1分', '1.按照《煤矿安全规程》第一百一十九条、第一百二十条、六十一条(七)的规定，对照“基本要求”，在现场逐一进行检查;\n2.在检查过程中，要求启动综掘机查看规范操作(启动前，提示现场按规范操作),现场检查司机是否按流程启动，启动时看是否先撤离周围人', NULL, '1.掘进机械设备不完好或违反规定使用钢丝绳牵引的耙装机不得分;\n2.实操过程中，综掘机运行时有人员在截割臂下停留和穿越、机身与煤(岩)壁之间站人扣5分;\n3.其他1处不符合要求扣1分', 8, NULL, 1, 69, 0, 1);
INSERT INTO `stdchk_item` VALUES (216, 1, 170, '掘进/设备管理/运输系统', '6.2.2(1)', '1.后运配套系统设备设施能力匹配； 2.运输设备完好，电气保护齐全可靠；3.刮板输送机、带式输送机减速器与电动机实现软启动或软连接，液力偶合器不使用可燃性传动介质（调速型液力偶合器不受此限），使用合格的易熔塞和防爆片；开关上架，电气设备不被淋水；机头、机尾固定牢固；行人跨越处设过桥； 4.带式输送机胶带阻燃和抗静电性能符合规定，有防打滑、防跑偏、防堆煤、防撕裂等保护装置，装设温度、烟雾监测装置和自动洒水装置；机头、机尾应有安全防护设施;机头处有防灭火器材；连续运输系统安设有连锁、闭锁控制装置，沿线安设有通信和信号装置；采用集中综合智能控制方式；上运时装设防逆转装置和制动装置，下运时装设软制动装置且装设有防超速保护装置；大于16º的斜巷中使用带式输送机设置防护网，并采取防止物料下滑、滚落等安全措施；机头尾处设置有扫煤器；支架编号管理；托辊齐全、运转正常；5.轨道运输设备安设符合要求，制动可靠，声光信号齐全；轨道铺设符合要求；钢丝绳及其使用符合《煤矿安全规程》要求；其他辅助运输设备符合规定', '查现场和资料。不符合要求1处扣1分', '1.查作业规程中运煤设备匹配能力，检验后运配套系统设备设施能力是否匹配;2.现场看胶带磨损情况，接口是否完好；查看电气设备是否齐全可靠;3.通过观察电机直接启动后是否直接给负荷来检查井下现场减速器与电动机是否实现软启动或软连接；现场检查液力偶合器是否使用可燃性传动介质(调速型液力偶合器不受此限);并通过观察老旧机器是否有工作油流出来，以查验是否使用合格的易熔塞和防爆片；查看开关是否上架，电气设备是否水淋，机头、尾是否固定牢固；行人跨越处是否设过桥\n4.查阅胶带使用说明书，看阻燃性、抗静电性是否符合相关标准规定；现场查“基本要求”规定的带式输送机的各项装置、设施、器材是否齐全;是否采用集中综合控制方式;\n5.查上运胶带是否有防逆转装置和制动装置，下运时是否装设软制动装置且装设有防超速保护装置；查看大于16°的斜巷中是否设置防护网，是否采取防止物料下滑、滚落等安全措施;\n6.现场查机头、尾是否有扫煤器；支架是否编号，托辊是否齐全、正常\n7.现场查轨道声光信号、一坡三挡等安全设施，轨道高低差、水平差、扣件等是否完好；轨道运输设备制动是否可靠；钢丝绳使用是否符合要求;\n8.现场检查调度绞车、无极绳绞车的完好情况', '1.设备管理台账;\n2.操作规程;\n3.作业规程', '1.带式输送机胶带磨损严重、接头宽度不足，视为设备不完好，扣1分;\n2.其他不符合要求1处扣1分', 7, NULL, 1, 69, 0, 1);
INSERT INTO `stdchk_item` VALUES (217, 1, 172, '掘进/技术保障/监测控制', '6.3.1(1)', '1.煤巷、半煤岩巷锚杆、锚索支护巷道进行顶板离层观测，并填写记录牌板；进行围岩观测并分析、预报；2.根据地质及水文地质预报制定安全技术措施，落实到位；3. 做到有疑必探，先探后掘', '查现场和资料。1项不符合要求不得分', '1.查是否有现场牌板，是否有观测分析，是否有预报;\n2.查是否有顶板离层仪观测记录;\n3.查地质条件变化时是否有编制的安全技术措施', '1.地质预测预报表\n2.顶板离层仪观测记录;\n3.安全技术措施文件', '1.顶板发生离层，未采取措施或采取措施失效扣1分;\n2.其他1项不符合要求不得分', 2, NULL, 1, 69, 0, 1);
INSERT INTO `stdchk_item` VALUES (218, 1, 173, '掘进/技术保障/现场图牌板', '6.3.2(1)', '作业场所安设巷道平面布置图、施工断面图、炮眼布置图、爆破说明书(断面截割轨迹图)、正规循环作业图表等，图牌板内容齐全、图文清晰、正确、保护完好，安设位置便于观看', '查现场。不符合要求1处扣1分', '1.查现场是否有巷道平面布置图、施工断面图、炮眼布置图、爆破说明书等技术图牌板，对照作业规程查内容是否与循环作业图表相符合;\n2.现场检查安设的图牌板是否齐全，图文是否清晰，是否有照明，安设位置是否方便作业人员观看', NULL, '不符合要求1处扣1分', 3, NULL, 1, 69, 0, 1);
INSERT INTO `stdchk_item` VALUES (219, 1, 174, '掘进/技术保障/规程措施', '6.3.3(1)', '1.作业规程编制、审批符合要求，矿总工程师定期组织对作业规程的贯彻、执行情况进行检查，地质及水文地质条件发生较大变化时，及时修改完善作业规程或补充安全措施并组织实施；2.作业规程中明确巷道施工工艺、临时支护及永久支护的形式和支护参数、永久支护距掘进工作面的距离等，并制定防止冒顶、片帮的安全措施；3.巷道开掘、贯通前组织现场会审并制定专门措施；4.过采空区、老巷、断层、破碎带和岩性突变地带等应有针对性措施', '查资料和现场。无作业规程、审批手续不合格或无措施施工的扣5分，其他1处不符合要求扣1分', '1.检查作业规程编制是否符合要求，审批是否有矿总工程师签字；是否有审批手续，矿总工程师是否定期组织检查，是否及时修改和完善，贯彻、执行是否真实有效；查修改意见是否在规程中体现，或是否有补充安全技术措施\n2.对照作业规程，现场检查巷道施工工艺、支护形式、参数、布置等是否符合要求;\n3.查作业规程中是否有“基本要求”中要求的专项、针对性安全技术措施；并现场检查是否有落实', '1.作业规程及其审批手续\n2.总工程师定期组织对作业规程的贯彻、执行情况的记录\n3.补充安全技术措施的资料', '1.无作业规程、审批手续不合格或无措施施工的扣5分;\n2.作业规程内容及审批手续、传达贯彻执行情况后来补充，如先施工后有规程，扣2分;\n3.其他1处不符合要求扣1分', 5, NULL, 1, 69, 0, 1);
INSERT INTO `stdchk_item` VALUES (220, 1, 176, '掘进/岗位规范/专业技能', '6.4.1(1)', '1.建立并执行本岗位安全生产责任制；2.管理和技术人员掌握作业规程，作业人员熟知本岗位操作规程和作业规程相关内容', '查资料和现场。岗位安全生产责任制不全，每缺1个岗位扣2分,其他1处不符合要求扣1分', '1.查资料，看是否建立岗位责任制、岗位安全生产责任制;\n2.现场提问，随机抽考1名管理或技术人员3个问题，如果都回答不正确，视为不符合要求；随机抽考1名关键岗位人员1个最关键应知应会问题，如果回答不正确，视为不符合要求', '岗位责任制度、安全生产责任制度', '1.缺1个岗位的安全生产责任制，扣2分;2.其他1处不符合要求扣1分', 5, NULL, 1, 69, 0, 1);
INSERT INTO `stdchk_item` VALUES (221, 1, 177, '掘进/岗位规范/规范作业', '6.4.2(1)', '1.现场作业人员按操作规程及作业规程、措施施工；2.无“三违”行为；3.零星工程有针对性措施，有管理人员跟班；4.作业前进行安全确认', '查现场和资料。发现“三违”行为不得分，其他1处不符合要求扣1分', '1.现场查作业人员操作，实操1次;\n2.查现场有无“三违”行为;\n3.查零星工程台账有无零星工程验收单,是否有措施，定位系统有无管理人员(班组长以上)在现场(零星工程是界定在掘进工作面以内，作业规程没有做要求的，一个班之内的临时性工作);\n4.查这天管理人员的考勤和台账，查定位来检查管理人员是否跟班;\n5.通过现场检查人员手指口述和查确认单来确定是否进行安全确认', '1.作业规程;\n2.相关安全技术措施、零星工程台账', '1.现场发现“三违”，不得分;\n2.作业前没有进行安全确认，扣1分;\n3.作业人员现场操作错误，扣1分;\n4.其他1处不符合要求,扣1分', 5, NULL, 1, 69, 0, 1);
INSERT INTO `stdchk_item` VALUES (222, 1, 179, '掘进/工程质量与安全/保障机制', '6.5.1(1)', '1.建立工程质量考核制度，各种检查有现场记录；2.有班组检查验收记录', '查现场和资料。班组无工程质量检查验收记录不得分，其他1处不符合要求扣0.5分', '1.查是否有工程质量考核制度;\n2.查是否有班组检查验收记录', '1.工程质量考核制度及现场检查记录;\n2.工程质量验收考核表', '1.无工程质量检查验收记录，不得分;2.每缺少1种记录，或其内容不正确，或记录缺少主要内容，扣0.5分', 5, NULL, 1, 69, 0, 1);
INSERT INTO `stdchk_item` VALUES (223, 1, 180, '掘进/工程质量与安全/安全管控', '6.5.2(1)', '1.永久支护距掘进工作面距离符合作业规程规定；2.执行敲帮问顶制度，无空顶作业，空帮距离符合规程规定；3.临时支护形式、数量、安装质量符合作业规程要求；4.架棚支护棚间装设有牢固的撑杆或拉杆，可缩性金属支架应用金属拉杆，距掘进工作面10m内架棚支护爆破前进行加固；5.无失修巷道，运输设备完好、各种安全设施齐全可靠；6.压风、供水系统压力等符合施工要求', '查现场。出现空顶作业不得分，不按规程、措施施工1处扣3分，其他1处不符合要求扣1分', '1.查现场和作业规程，查现场岩壁到永久支护的最小空顶距是多少长度，并与作业规程规定的比较来看是否空顶作业;\n2.现场检查，敲帮问顶，看顶帮活矸是否清理干净;\n3.现场查看临时支护形式、数量、安装是否符合作业规程要求;\n4.现场查看架棚支护棚间是否装设有牢固的撑杆或拉杆，可缩性金属支架是否用金属拉杆，距掘进工作面10m内架棚支护爆破前是否进行加固;\n5.查看是否井下行人的巷道(除采煤工作面外)全都无失修；运输设备、各种安全设施(瓦斯传感器、六大避险设施等)是辈长是\n6.查现场压风、供水系统压力是否能保证各种施工工作正常进行', '1.作业规程;\n2.交接班记录;\n3.施工日志;\n4.各种监控监测记录', '1.超过作业规程规定的最大空顶距而且没有临时支护就不得分;\n2.不按照规程、措施施工，扣3分;\n3.其他1处不符合要求，扣1分', 10, NULL, 1, 69, 0, 1);
INSERT INTO `stdchk_item` VALUES (224, 1, 181, '掘进/工程质量与安全/规格质量', '6.5.3(1)', '1.巷道净宽误差符合以下要求：锚网（索）、锚喷、钢架喷射混凝土巷道有中线的0mm～100mm，无中线的-50mm～200mm；刚性支架、预制混凝土块、钢筋混凝土弧板、钢筋混凝土巷道有中线的0mm～50mm，无中线的-30mm～80mm；可缩性支架巷道有中线的0mm～100mm，无中线的-50mm～100mm\n2.巷道净高误差符合以下要求：锚网背（索）、锚喷巷道有腰线的0mm～100mm，无腰线的-50mm～200mm；刚性支架巷道有腰线的-30mm～50mm，无腰线的-30 mm～50mm；钢架喷射混凝土、可缩性支架巷道-30mm ～100mm；裸体巷道有腰线的0mm～150mm，无腰线的-30mm～200mm；预制混凝土、钢筋混凝土弧板、钢筋混凝土有腰线的0mm～50mm，无腰线的-30mm～80mm\n3.巷道坡度偏差不得超过±1‰\n4.巷道水沟误差应符合以下要求：中线至内沿距离-50mm～50mm，腰线至上沿距离-20mm～20mm，深度、宽度-30mm～30mm，壁厚-10mm', '1.查现场。按表7-2取不少于3个检查点现场检查，测点1处不符合要求但不影响安全使用的扣0.5分，影响安全使用的扣3分\n2.查现场。按表7-2取不少于3个检查点检查，测点不符合要求但不影响安全使用的1处扣0.5分，影响安全使用的1处扣3分\n3.查现场。按表7-2取不少于3个检查点检查，不符合要求1处扣1分\n4.查现场。按表7-2取不少于3个检查点现场检查，不符合要求1处扣0.5分', '对照“基本要求”现场实测检查', NULL, '1处不符合要求但不影响安全使用的扣0.5分，影晌安全使用的扣3分', 12, NULL, 1, 69, 0, 1);
INSERT INTO `stdchk_item` VALUES (225, 1, 182, '掘进/工程质量与安全/内在质量', '6.5.4(1)', '1.锚喷巷道喷层厚度不低于设计值90%（现场每25m打一组观测孔，一组观测孔至少3个且均匀布置），喷射混凝土的强度符合设计要求，基础深度不小于设计值的90%\n2.光面爆破眼痕率符合以下要求：硬岩不小于80％、中硬岩不小于50％、软岩周边成型符合设计轮廓；煤巷、半煤岩巷道超（欠）挖不超过3处（直径大于500mm，深度：顶大于250mm、帮大于200mm）\n3.锚网索巷道锚杆（索）安装、螺母扭矩、抗拔力、网的铺设连接符合设计要求，锚杆（索）的间、排距偏差-100 mm～100mm，锚杆露出螺母长度10mm～50mm（全螺纹锚杆10mm～100mm），锚索露出锁具长度150mm～250mm，锚杆与井巷轮廓线切线或与层理面、节理面裂隙面垂直，最小不小于75°，抗拔力、预应力不小于设计值的90%\n4.刚性支架、钢架喷射混凝土、可缩性支架巷道偏差符合以下要求：支架间距不大于50mm、梁水平度不大于40mm、支架梁扭距不大于50mm、立柱斜度不大于1°，水平巷道支架前倾后仰不大于1°，柱窝深度不小于设计值；撑（或拉）杆、垫板、背板的位置、数量、安设形式符合要求；倾斜巷道每增加5°支架迎山角增加1', '1.查现场和资料。未检查喷射混凝土强度扣6分，无观测孔扣2分，喷层厚度不符合要求1处扣1分，其他1处不符合要求扣0.5分\n2.查现场和资料。没有进行眼痕检查扣3分，其他1处不符合要求扣0.5分\n3.查现场。锚杆螺母扭矩连续3个不符合要求扣5分，抗拔力、预应力不符合要求1处扣1分，其他1处不符合要求扣0.5分\n4.查现场。按表7-2取不少于3个检查点现场检查，不符合要求1处扣0.5分', '对照“基本要求”现场实测检查', '1.混凝土强度试验报告;\n2.设计资料', '1.未检查喷射混凝土强度扣6分;\n2.无观测孔扣2分;\n3.喷层厚度不符合要求1处扣1分;\n4.其他1处不符合要求扣0.5分', 13, NULL, 1, 69, 0, 1);
INSERT INTO `stdchk_item` VALUES (226, 1, 183, '掘进/工程质量与安全/材料质量', '6.5.5(1)', '1.各种支架及其构件、配件的材质、规格，及背板和充填材质、规格符合设计要求；2.锚杆（索)的杆体及配件、网、锚固剂、喷浆材料等材质、品种、规格、强度等符合设计要求', '查资料和现场。现场使用不合格材料不得分，其他1处不符合要求扣1分', '1.查作业规程，看各种材料、规格及背板、充填材料是否符合设计;\n2.查材料抽检报告,看锚杆、锚固剂等材质、品种等是否符合设计', '1.锚杆、锚固剂合格证和检测检验证书\n2.作业规程;\n3.材料抽检报告', '1.现场使用不合格材料，不得分;2.其他1处不符合要求扣1分', 10, NULL, 1, 69, 0, 1);
INSERT INTO `stdchk_item` VALUES (227, 1, 185, '掘进/文明生产/灯光照明', '6.6.1(1)', '转载点、休息地点、车场、图牌板及硐室等场所照明符合要求', '查现场。不符合要求1处扣0.5分', '井下现场查这些场所是否都有足够照明', NULL, '不符合要求1处扣0.5分', 3, NULL, 1, 69, 0, 1);
INSERT INTO `stdchk_item` VALUES (228, 1, 186, '掘进/文明生产/作业环境', '6.6.2(1)', '1.现场整洁，无浮渣、淤泥、积水、杂物等，设备清洁，物料分类、集中码放整齐，管线吊挂规范；2.材料、设备标志牌齐全、清晰、准确，设备摆放、物料码放与胶带、轨道等留有足够的安全间隙；3.巷道至少每100m设置醒目的里程标志', '查现场。不符合要求1处扣0.5分', '1.检查现场是否整洁，有无浮渣、淤泥、积水、杂物等，设备是否清洁，物料分类、集中码放是否整齐管线吊挂是否规范;\n2.现场检查材料、\n设备标志牌是否齐全、清晰、准确，设备摆放、物料码放与胶带、轨道等是否留有足够的安全间隙;\n3.现场检查巷道是否至少每100m设置醒目的里程标志', NULL, '不符合要求1处扣0.5分', 7, NULL, 1, 69, 0, 1);
INSERT INTO `stdchk_item` VALUES (229, 1, 189, '机电/设备与指标/设备证标', '7.1.1(1)', '1.机电设备有产品合格证；2.纳入安标管理的产品有煤矿矿用产品安全标志，使用地点符合规定；3.防爆设备有防爆合格证', '查现场和资料。1台不符合要求不得分', '1.对照(查)机电设备台账、机电产品台账档案，查(看)是否有产品合格证是否有煤安标志证书是否有防爆合格证;\n2.现场查看设备选型及使用地点是否合规', '1.机电设备、小型电器及电缆等机电产品台账;\n2.矿井机电设备及产品安标、防爆合格证、产品合格证台账', '1.有安全标志但标志牌丢失扣0.2分;2.其他内容,1台不符合要求不得分', 4, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (230, 1, 190, '机电/设备与指标/设备完好', '7.1.2(1)', '机电设备综合完好率不低于90%', '查现场和资料。每降低1个百分点扣0.5分', '1.查矿井月检查考核基础表;\n2.综合各专业现场检查的不完好设备总数，计算机电设备综合完好率', '井提供的月机电设备完好检查考核基础表', '以现场检查完好设备为准，严格按照完好率不低于90%的指标，每降低1个百分点，扣0.5分', 3, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (231, 1, 191, '机电/设备与指标/固定设备', '7.1.3(1)', '大型在用固定设备完好', '查现场和资料。1台不完好不得分', '1.查矿井月检查考核基础表;\n2.现场检查大型固定设备的完好状况(大型固定设备指：主提升绞车、主提升胶带运输机、主运输胶带运输机、主变压器以及主排水泵、主通风机、固定压风机等)', '矿井提供的月大型固定设备完好检查考核基础表', '大型固定设备1台不完好，不得分', 2, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (232, 1, 192, '机电/设备与指标/小型电器', '7.1.4(1)', '小型电器设备完好率不低于95%', '查现场和资料。每降低1个百分点扣0.5分', '1.查矿井月检查考核基础表;\n2.综合各专业现场检查的不完好设备总数，计算小型电器完好率(小型电器指“五小电器”，如小型开关、电铃、接线盒按钮、电话、灯具等）', '矿井提供的月小型电器完好检查基础表', '每降低1个百分点，扣0.5分', 1.5, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (233, 1, 193, '机电/设备与指标/矿灯', '7.1.5(1)', '在用矿灯完好率100%，使用合格的双光源矿灯。完好矿灯总数应多出常用矿灯人数的10%以上', '查现场和资料。井下发现1盏红灯扣0.3分，1盏灭灯、不合格灯不得分，完好矿灯总数未满足要求不得分', '1.并下现场抽查人员携带的矿灯不低于20盏，看完好率是否为100%,是否为双光源灯，是否有红灯、灭灯等;\n2.查矿灯统计台账，完好矿灯总数是否满足要求', '矿井提供的全矿矿灯台账及月份检查基础表', '1.井下发现1盏红灯扣0.3分;\n2.1盏灭灯、不合格灯，不得分;\n3.完好矿灯总数未达指；\n标要求不得分', 1.5, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (234, 1, 194, '机电/设备与指标/机电事故率', '7.1.6(1)', '机电事故率不高于1%', '查资料。机电事故率达不到要求不得分', '1.查当月计划产量，生产机电事故统计以及月份机电事故考核基础表;\n2.按照公式计算出机电事故率：事故影响产量/当月计划产量=机电事故率', '1.原始真实的调度值班记录;\n2.当月产量计划', '机电事故率高于1%(不含),不得分', 1, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (235, 1, 195, '机电/设备与指标/设备待修率', '7.1.7(1)', '设备待修率不高于5%', '查现场和资料。设备待修率每增加1个百分点扣0.5分', '1.查机电设备台账及设备大修记录;\n2.查月份设备待修率考核基础表;\n3.现场查看待修设备数量', '1.设备待修率统计资料;\n2.机电设备台账;\n3.月份设备待修率考核基础表', '设备待修率每增加1个百分点，扣0.5分', 1, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (236, 1, 196, '机电/设备与指标/设备大修改造', '7.1.8(1)', '设备更新改造按计划执行，设备大修计划应完成90%以上', '查资料。无更新改造年度计划或未完成不得分；无大修计划或计划完成率全年低于90%，上半年低于30%不得分', '1.查是否有经过批准的矿井年度设备更新改造和设备大修计划，查计划是否符合要求;\n2.查项目完成结算单和验收单，看计划是否真正完成，计算完成率', '1.矿井年度更新改造计划;\n2.结算单和验收单', '1.无更新改造年度计划或未完成，不得分;\n2.无大修计划或计划完成率全年低于90%,上半年低于30%,不得分', 1, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (237, 1, 198, '机电/煤矿机械/主提升(立斜井绞车)系统', '7.2.1(1)', '1.提升系统能力满足矿井安全生产需要；2.各种安全保护装置符合《煤矿安全规程》规定；3.立井提升装置的过卷过放、提升容器和载荷等符合《煤矿安全规程》规定；4.提升装置、连接装置及提升钢丝绳符合《煤矿安全规程》规定；5.制动装置可靠，副井及负力提升的系统使用可靠的电气制动；6.立井井口及各水平阻车器、安全门、摇台等与提升信号闭锁；7.提升速度大于3m/s的立井提升系统内，安设有防撞梁和缓冲托罐装置；单绳缠绕式双滚筒绞车安设有地锁和离合器闭锁； 8.斜井提升制动减速度达不到要求时应设二级制动装置；9.提升系统通信、信号装置完善，主副井绞车房有能与矿调度室直通电话；10.上、下井口及各水平安设有摄像头，机房有视频监视器；11.机房安设有应急照明装置；12.使用低耗、先进、可靠的电控装置；13.主井提升宜采用集中远程监控，可不配司机值守，但应设图像监视，并定时巡检', '查现场和资料。第1～9项提人绞车1处不符合要求“煤矿机械”大项不得分，其他绞车不符合要求1处扣1分，第10、11项不符合要求1处扣0.5分，其他项不符合要求1处扣0.1分', '1.查煤矿机电专业对各系统能力核算有关资料，看提升系统能力核算是否正确，是否满足矿并安全生产需要;\n2.通过检查保护试验制度、试验记录和对现场人员的询问，结合现场对具备试验条件的保护进行试验，判断各种保护的有效性和可靠性;3.查立井提升装置过卷过放、容器、载荷是否符合《煤矿安全规程》要求;4.查制动压力是否经过严格的计算确定，制动力矩是否满足要求；若负力提升絞车制动闸参与调速说明电气制动不可靠;\n5.查立井井口各项闭锁是否符合规程要求；查大于3m/s的立井是否安设防撞及缓冲托罐装置，是否可靠；缠绕提升机地锁及离合器闭锁是否合格\n6.查二级制动是否符合要求;\n7.查通信、信号及直通电话是否符合要求；查视频监视装置、应急照明设置是否符合要求;\n8.查电控装置；查是否实现无人值守', '1.现场的提升设备各项保护试验制度记录;\n2.提升系统能力核算资料;\n3.制动压力计算资料', '1.第1~9项提人绞车1处不符合要求，扣20分;\n2.其他绞车不符合要求1处扣1分;\n3.直通电话存在自动或一键直拨等可导致占线情况的，扣0.5分;\n4.第10项、第11项不符合要求,1处扣0.5分;\n5.主井提升宜采用集中远程监控为鼓励、推广措施，没有采用的，扣0.1分\n6.其他项不符合要求,1处扣0.1分', 5, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (238, 1, 199, '机电/煤矿机械/主提升（带式传输机）系统', '7.2.2(1)', '1.钢丝绳牵引带式输送机：⑴运输能力满足矿井、采区安全生产需要，人货不混乘，不超速运人；⑵各种保护装置符合《煤矿安全规程》规定；⑶在输送机全长任何地点装设便于搭乘人员或其他人员操作的紧急停车装置；⑷上、下人地点设声光信号、语音提示和自动停车装置，卸煤口及终点下人处设有防止人员坠入及进入机尾的安全设施和保护；⑸上、下人和装、卸载处装设有摄像头，机房有视频监视器；⑹输送带、滚筒、托辊等材质符合规定，滚筒、托辊转动灵活，带面无损坏、漏钢丝等现象；⑺机房安设有与矿调度室直通电话；⑻使用低耗、先进、可靠的电控装置；⑼采用集中远程监控，实现无人值守 \n 2.滚筒驱动带式输送机：⑴运输能力满足矿井、采区安全生产需要；⑵电动机保护齐全可靠；⑶装设有防滑、防跑偏、防堆煤、防撕裂和输送带张紧力下降保护装置，以及温度、烟雾监测和自动洒水装置；⑷上运运输机装设防逆转和制动装置，下运运输机装设有软制动装置且装设防超速装置； ⑸减速器与电动机采用软连接或采用软启动控制，液力偶合器不使用可燃性传动介质（调速型液力偶合器不受此限）；⑹输送带、滚筒、托辊等材质符合规定，滚筒、托辊转动灵活，带面无损坏、漏钢丝等现象；⑺倾斜井巷使用的钢丝绳芯输送机有钢丝绳芯及接头状态检测装备；⑻钢丝绳芯输送机设有沿线紧急停车、闭锁装置，装、卸载处设有摄像头；⑼机头、机尾及搭接处设有照明，转动部位设有防护栏和警示牌，行人跨越处设有过桥；⑽连续运输系统安设有连锁、闭锁控制装置，沿线安设有通信和信号装置；⑾集中控制硐室安设有与矿调度室直通电话；⑿使用低耗、先进、可靠的电控装置；⒀采用集中远程监控，实现无人值守', '1.查现场和资料。 \n第⑴～⑸项提人带式输送机1处不符合要求扣4分，其他带式输送机1处\n不符合要求扣1分，第⑹、⑺项1处不符合要求扣0.5分，其他项1处不符合要求扣0.1分\n\n2.查现场和资料。第⑴～⑻项不符合要求1处扣1分，第⑼～⑾项不符合要求1处扣0.5分，其他项不符合要求1处扣0.1分 ', '1.能力核算同上；现场检查是否有人货混乘和超速运人;\n2.保护可靠性检查同上;\n3.查任一点紧急停车装置是否有效\n4.查上下人地点声光信号、语音提示、自动停车、防越位的装置设置是否齐全、可靠;\n5.查上下人及装、卸载处是否安设有摄像头，机房是否有监视器;6.查输送带、滚筒、托辊等材质，滚筒、托辊及带面状态;\n7.查电话接通方式;\n8.查现场是否使用低耗、先进、可靠的电控装置\n9.查调度室是否有主井提升集中远程监控图像监视，查巡检记录是否有人定时巡检', '1.机房或硐室保护试验制度及记录;\n2.运输机系统能力核算资料', '1.第(1)~(5)项提人带式输送机1处不符合要求，扣4分;\n2.其他带式输送机1处不符合要求，扣1分;\n3.第(6)、(7)项1处不符合要求扣0.5分;\n4.其他项1处不符合要求扣0.1分', 4, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (239, 1, 200, '机电/煤矿机械/主通风机系统', '7.2.3(1)', '1.主要通风机性能满足矿井通风安全需要；2.电动机保护齐全、可靠； 3.使用在线监测装置，并且具备通风机轴承、电动机轴承、电动机定子绕组温度检测和超温报警功能，具备振动监测及报警功能； 4.每月倒机、检查1次；5.安设有与矿调度室直通的电话；6.机房设有水柱计、电流表、电压表等仪表，并定期校准；7.机房安设应急照明装置；8.使用低耗、先进、可靠的电控装置', '查现场和资料。第1～6项不符合要求1处扣1分，第7项不符合要求扣0.5分，其他项不符合要求1处扣0.1分', '1.查产品说明书和检测报告的特性曲线，通过现场运行数据，验证通风机主要通风性能是否满足需要，是否在最佳工况点运行\n2.保护可靠性检查同上;\n3.查是否有在线监测装置，是否具有“基本要求”规定的各专项监测功能，监测数据(主要是温度和震动数据)是否准确;\n4.查是否有每月倒机记录台账;\n5.通过查直通电话是否;\n6.现场查机房是否设有各类仪表，并查机房运行日志看是否定期校准仪表;\n7.现场查机房是否有应急照明装置;\n8.查现场是否有低耗、先进、可靠的电控装置', '1,保护试验制度和记录;\n2.通风机特性曲线\n3.能力核算资料;\n4.倒机记录', '1.第1~6项不符合要求,1处扣1分;\n2.第7项不符合要求，扣0.5分\n3.其他项不符合要求,1处扣0.1分;\n4.对旋式风机闸板门电机非防爆，提建议改造(建议)', 2, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (240, 1, 201, '机电/煤矿机械/压风系统', '7.2.4(1)', '1. 供风能力满足矿井安全生产需要；2.压缩机、储气罐及管路设置符合《煤矿安全规程》和《特种设备安全法》等规定；3.电动机保护齐全可靠；4.压力表、安全阀、释压阀设置齐全有效，定期校准；5.油质符合规定，有可靠的断油保护；6.水冷压缩机水质符合要求，有可靠断水保护； 7.风冷压缩机冷却系统及环境符合规定；8.温度保护齐全、可靠，定值准确；9.井下压缩机运转时有人监护；10.机房安设有应急照明装置；11.使用低耗、先进、可靠的电控装置；12.地面压缩机采用集中远程监控，实现无人值守', '查现场和资料。第1～9项不符合要求1处扣1分，第10项不符合要求1处扣0.5分，其他项不符合要求1处扣0.1分', '1.能力核算同上;2.保护可靠性检查同上;\n3.现场查压缩机、储气罐及管路设置是否符合规定;4.查安全阀、释压阀定期试验调试记录，看是否定期校准；抽样试验看是否有效;\n5.查看释压阀安装位置是否合理;\n6.现场检查“基本要求”描述的第5~11项是否符合要求;\n7.现场检查机房，看地面压缩机是否有远程监控', '1.保护试验制度和记录;\n2.检测报告;\n3.能力核算资料;\n4.油质、水质检验报告;\n5.机房运行日志', '1.第1~9项不符合要求,1处扣1分;\n2.第10项不符合要求1处扣0.5分;\n3.释压阀安装位置不当，在释压时可能造成人员伤害的，建议整改，暂不扣分\n4.其他项不符合要求,1处扣0.1分', 2, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (241, 1, 202, '机电/煤矿机械/排水系统', '7.2.5(1)', '1. 矿井及采区主排水系统：⑴排水能力满足矿井、采区安全生产需要； ⑵泵房及出口，水泵、管路及配电、控制设备，水仓蓄水能力等符合《煤矿安全规程》规定；⑶有可靠的引水装置；⑷设有高、低水位声光报警装置；⑸电动机保护装置齐全、可靠；⑹排水设施、水泵联合试运转、水仓清理等符合《煤矿安全规程》规定；⑺ 水泵房安设有与矿调度室直通电话；⑻ 各种仪表齐全，及时校准；⑼ 使用低耗、先进、可靠的电控装置；⑽ 采用集中远程监控，实现无人值守。2.其他排水地点：⑴ 排水设备及管路符合规定要求；⑵ 设备完好，保护齐全、可靠；⑶ 排水能力满足安全生产需要；⑷ 使用小型自动排水装置', '查现场和资料。第1项中的第⑴～⑺小项不符合要求1处扣1分，其他项不符合要求1处扣0.1分；第2项中的第⑴～⑶项不符合要求1处扣0.5分，第⑷项不符合要求扣0.1分', '1.能力核算同上;\n2.现场检查泵房及出口，水泵、管路及配电、控制设备、水仓蓄水能力等是否符合《煤矿安全规程》规定;\n3.现场检查是否有引水装置，是否可靠;\n4.现场检查是否有高、低水位声光报警装置;\n5.保护可靠性检查同上;\n6.现场检查排水设施、水泵联合试运转、水仓清理等是否符合《煤矿安全规程》规定\n7.查直通电话记录，看水泵房与调度室是否有直通电话;\n8.现场检查各种仪表是否齐全，查仪表调校记录看是否及时校准;9.查现场是否有低耗、先进、可靠的电控装置;\n10.现场查看泵房是否实现远程监控(《煤矿安全规程》第三百十二条规定);\n11.现场检查其他排水地点是否符合要求，如每个机房现场抽查启动1台排水设备看运行和完好情况', '1.保护试验制度和记录;\n2.有效期内的主排水系统性能测试报告\n3.能力核算资料;\n4.仪表调校记录', '1.第1项中的第(1)(7)小项不符合要求,1处扣1分；其他项不符合要求1处扣0.1分;\n2.泵房和水仓间直通没隔水阀门,1处扣1分;\n3.第2项中的第(1)(3)项不符合要求,1处扣0.5分，第(4)项不符合要求扣0.1分', 4, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (242, 1, 203, '机电/煤矿机械/瓦斯发电系统', '7.2.6(1)', '1.抽采泵出气侧管路系统装设防回火、防回气、防爆炸的安全装置； 2.根据输送方式的不同，设置甲烷、流量、压力、温度、一氧化碳等各种监测传感器； 3.超温、断水等保护齐全、可靠；4.压力表、水位计、温度表等仪器仪表齐全、有效；5.机房安设有应急照明；6.电气设备防爆性能符合要求，保护齐全可靠；7.阀门装置灵活；8.机房有防烟火、防静电、防雷电措施', '查现场，不符合要求1处扣0.5分', '1.现场检查出气侧管路是否装设安全装置，并安排人员试动各项保护装置的齐全灵敏可靠性;\n2.现场检查是否设置有各种监测传感器;\n3.查阅当日或前一日专职人员试验超温、断水保护动作情况，检查保护设施是否有效;\n4.现场检查各种仪表是否齐全，查仪表调校记录是否及时校准有效;\n5.现场查机房是否有应急照明;\n6.通过查电气设备防爆性能检查原始记录，检查其是否符合防爆要求；查阅当日或前一日专职人员试验电气设备保护动作情况，检查保护设施是否可靠;\n7.现场抽查阀门转动是否灵活\n8.详细检查机房运行日志，并现场检查是否有“三防”措施', '1.机电部门出具的抽采设备各项保护整定试验调试检修等记录\n2.有效期内的抽采泵系统性能测试报告\n3.电气设备防爆性能检查记录;\n4.各种仪表校准报告\n5.防爆性能检查原始记录\n6.机房运行日志', '1.不符合要求,1处扣0.5分;\n2.瓦斯抽采泵,1处不符合要求，扣0.5分', 1.5, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (243, 1, 204, '机电/煤矿机械/地面供热降温系统', '7.2.7(1)', '1.热水锅炉：\n⑴安设有温度计、安全阀、压力表、排污阀；⑵按规定安设可靠的超温报警和自动补水装置；⑶系统中有减压阀，热水循环系统定压措施和循环水膨胀装置可靠，有高低压报警和连锁保护；⑷停电保护、电动机及其他各种保护灵敏可靠；⑸有特种设备使用登记证和年检报告；⑹安全阀、仪器仪表按规定检验，有检验报告；⑺水质合格，有检验报告\n2.蒸汽锅炉：\n⑴安设有双色水位计或两个独立的水位表；⑵按规定安设可靠的高低水位报警和自动补水装置；⑶按规定安设压力表、安全阀、排污阀；⑷按规定安设可靠的超压报警器和连锁保护装置；⑸温度保护、熄火保护、停电自锁保护以及电动机和其他各种保护灵敏、可靠；⑹有特种设备使用登记证和年检报告；⑺安全阀、仪器仪表按规定检验，有检验报告。⑻水质合格，有检验报告\n3.热风炉：\n⑴安设有防火门和栅栏，有防烟、防火、超温安全连锁保护装置，有CO检测和洒水装置；⑵电动机及其他各种保护灵敏、可靠；⑶出风口处电缆有防护措施；⑷锅炉距离入风井口不少于20米；⑸有国家或者当地煤炭安全监察部门颁发的安全性能合格证\n4.地面降温系统：\n⑴设备完好；⑵各类保护齐全可靠；⑶各种阀门、安全阀灵活可靠；⑷仪表正常，有检验报告；\n⑸水质合格，有化验记录\n', '查现场和资料。不符合要求1处扣0.5分', '1.查阅专职人员试验各项保护动作情况、连锁保护的完好性能检查的原始记录、机房运行日志，看3大类锅炉及地面降温系统各项保护、连锁是否完好有效;\n2.热水锅炉\n(1)现场查是否有温度计、安全阀、压力表、排污阀;\n(2)现场查是否有超温报警和自动补水装置，并试动是否可靠\n(3)现场查是否有减压阀，现场试验热水循环系统定压措施和循环水膨胀装置是否可靠;\n(4)现场查是否有特种设备使用登记证和热水锅炉年检报告；安全阀及仪器仪表的检验报告；并检查检验报告是否规范;\n(5)查现场并比对水质报告，看热水锅炉水质是否合格;\n3.蒸汽锅炉:\n(1)现场查是否有双色水位计或两个独立的水位表;\n(2)现场查是否有高低水位报警和自动补水装置，并试动是否可靠;\n(3)现场查是否有压力表、安全阀、排污阀;\n(4)现场查是否有超压报警器和连锁保护装置，并试动是否可靠;(5)现场查是否有特种设备使用登记证和蒸汽锅炉年检报告安全阀及仪器仪表的检验报告；并检查掘告是否规范;\n(6)查现场并比对水质检验报告，看蒸汽锅炉水质是否合格;\n4.热风炉\n(1)现场查是否有防火门和栅栏，防烟防火、超温安全连锁保护装置,CO检测和洒水装置;\n(2)现场查出风口处电缆是否有防护措施；锅炉距离入风井口是否不少于20m;热风炉有无国家或者当地煤矿安全监察部门颁发的安全性能合格证;\n5.地面降温系统:\n(1)现场查设备是否完好;\n(2)现场试动各种阀门、安全阀是否灵活可靠;\n(3)现场查仪表，并比对仪表检验报告，看是否正常;\n(4)现场检查水质并比对水质化验记录，看是否合格;\n6.详细检查机房运行日志', '1.保护性能检查原始记录\n2.机房运行日志;\n3.安全阀及仪器仪表的检验报告;\n4.锅炉年检报告;\n5.特种设备使用登记证、地方安监部门颁发的安全性能合格证\n6.水质检验报告', '不符合要求1处扣0.5分', 1.5, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (244, 1, 206, '机电/煤矿电气/地面供电系统', '7.3.1(1)', '1.有供电设计及供电系统图，供电能力满足矿井安全生产需要；2.矿井供电主变压器运行方式符合规定；3.主要通风机、提升人员的绞车、抽采瓦斯泵、压风机以及地面安全监控中心等主要设备供电符合《煤矿安全规程》规定；4.各种保护设置齐全、定值准确、动作灵敏可靠，高压配出侧装设有选择性的接地保护；5.变电所有可靠的操作电源；6.直供电机开关或带有电容器的开关有欠压保护；7.高压开关柜具有防止带电合闸、防止带接地合闸、防止误入带电间隔、防止带电合接地线、防止带负荷拉刀闸和通信功能；8.反送电开关柜加锁且有明显标志；9.矿井6000V及以上电网单相接地电容电流符合《煤矿安全规程》规定； 10.电气工作票、操作票符合《电力安全工作规程》的要求；11.防雷设施齐全、可靠；12.供电电压、功率因数、谐波参数符合规定；13.矿井主要变电所实现综合自动化保护和控制，实现无人值守；14.变电所有应急照明装置；15.矿井变电所安设有与电力调度及矿调度室直通电话，并有录音功能', '查现场和资料。第1项1处不符合要求不得分，第2、3项不符合要求1项扣3分，第4～10项不符合要求1处扣1分，第11～15项不符合要求1处扣0.5分', '1.现场查有无供电设计及供电系统图，检查井上供电系统图规范性及与实际是否相符；并查阅供电设计及矿井供电能力核定报告书，与地方供电部门签订的供电合同和调度协议，看供电能力能否满足需要2.对照供电系统图查现场，看主变压器运行方式是否符合规定\n3.现场重点查看主要通风机、提升人员的绞车、抽采瓦斯泵、压风机以及地面安全监控中心等主要设备供电是否符合《煤矿安全规程》规定;\n4.查继电保护整定计算书，结合现场检查各种保护设置是否齐全、定值是否准确、动作是否灵敏可靠，查看高压配出侧是否装设有选择性的接地保护;\n5.查检查变电所操作电源是否可靠;\n6.现场查直供电机开关或带有电容器的开关是否有欠压保护;\n7.现场查高压开关柜“五防”功能是否齐全可靠，是否具备通信功能;\n8.现场查反送电开关柜是否加锁且有明显标志;\n9.查矿井高压电网单相接地电容电流测试报告，并现场抽查调阅数字保护的各项整定值是否与计算值相符，来确定高压电网单相接地电容电流是否符合《煤矿安全规程》第四百五十三条规定;\n10.对照工作票和停送电记录，抽查“两票”制度执行情况;\n11.对照防雷设施检验记录，现场检查防雷设施是否齐全、可靠12.检查检测记录，看是否及时检测并掌握供电电压、功率因数、谐波参数;\n13.查现场，看矿井主要变电所是否实现综合自动化保护和控制，是否实现无人值守；变电所是否有应急照明装置；矿井变电所是否安设有与电力调度及矿调度室的直通电话，并试电话能否录音', '1.供电设计及中介机构最近编制的矿井供电能力核定报告书\n2.与地方供电部门签订的供电合同和调度协议;\n3.地面供电系统图;4.变电站电气设备预防性试验报告;5.继电保护整定计算书;\n6.矿井高压电网单相接地电容电流测试报告等;\n7.停送电记录;\n8.防雷设施检验记录\n9.供电电压、功率因数、谐波参数、电容电流检测记录', '1.第1项1处不符合要求，不得分;\n2.第2、3项不符合要求,1项扣3分;\n3.第4~10项不符合要求,1处扣1分;\n4.第11~15项不符合要求,1处扣0.5分', 5, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (245, 1, 207, '机电/煤矿电气/井下供电系统', '7.3.2(1)', '1.井下供配电网络：⑴各水平中央变电所、采区变电所、主排水泵房和下山开采的采区泵房供电线路符合《煤矿安全规程》规定，运行方式合理；⑵各级变电所运行管理符合规定；⑶矿井、采区及采掘工作面等供电地点均有合格的供电系统设计，符合现场实际；⑷按规定进行继电保护核算、检查和整定；⑸中央变电所安装有选择性接地保护装置；⑹配电网路开关分断能力、可靠动作系数和动、热稳定性以及电缆的热稳定性符合规定；⑺实行停送电审批和工作票制度；⑻井下变电所、配电点悬挂与实际相符的供电系统图；⑼调度室、变电所有停送电记录；⑽变电所及高压配电点设有与矿调度室直通电话；⑾变电所设置符合《煤矿安全规程》规定；⑿采区变电所专人值班或关门加锁并定期巡检；⒀采用集中远程监控，实现无人值守', '查现场和资料。 第⑴项1处不符合要求不得分，第⑵～⑿不符合要求1处扣0.5分，其他项不符合要求1处扣0.1分', '1.现场检查或对照井下供电图纸，重点查供电系统是否符合《煤矿安全规程》第四百三十六条~第四百三十八条规定，是否双回路供电，有无系统性缺陷;\n2.对照《煤矿安全规程》第四百三十九条要求，现场检查各级变电所运行管理是否符合规定;3.对照井下供电设计，抽查1个供电现场对比，与实际是否相符；特别需注意设计变化而实际并未及时修改或施工的情况;\n4.查井下供电系统继电保护整定计算书，是否按规定核算、检查、整定；特别注意个别中小矿井不按规定核算、检查、整定的情况：调阅开关保护整定值，与批准值是否一致;5.现场检查变电所的高压馈出线路上是否装有选择性接地保护6.查阅配电网络开关分断能力、可靠动作系数和动热稳定性以及电缆的热稳定性校验报告，或一个工作面的供电系统设计等资料，并现场抽查试动开关的各种保护齐全完好动作情况，检查是否符合《煤矿安全规程》第四百四十九条、第四百五十二条的规定;\n7.对照工作票和井下停送电记录检查停送电审批和工作票制度执行情况，重点是变电所是否存在无计划掉闸事故记录;\n8.检查井下变电所、配电点是否悬挂有图，且查图与实际是否相符\n9.现场检查调度室变电所是否有停送电记录；变电所及高压配电点是否设有与矿调度室直通电话;\n10.现场检查变电所相关布置是否符合《煤矿安全规程》;\n11.现场检查是否采用集中远程监控，实现无人值守', '1.完整的井下供电系统图;\n2.井下供电设计;\n3.井下供电系统继电保护整定计算书;\n4.电气设备防爆检查记录;\n5.配电网络开关分断能力、可靠动作系数和动热稳定性以及电缆的热稳定性校验报告;\n6.井下停送电记录', '1.第(1)项1处不符合要求，不得分;\n2.第(2)~(12)不符合要求,1处扣0.5分;\n3.其他项不符合要求,1处扣0.1分', 4, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (246, 2, 207, '机电/煤矿电气/井下供电系统', '7.3.2(2)', '2.防爆电气设备及小型电器防爆合格率100%', '查现场和资料。高瓦斯、突出矿井中，以及低瓦斯矿井主要进风巷以外区域出现1处失爆“煤矿电气”大项不得分，其他区域发现1处失爆扣4分', '1.依照煤矿防爆电气检查标准(GB383613836.20)和完好标准，结合检查记录，重点抽查关键区域电气设备、小型电器的防爆性能;\n2.查容易发生失爆的管理死角、盲点、盲区的设备', '矿井机电部门防爆检查组人员分片包干的月检查记录', '1.高瓦斯、突出矿井中，以及低瓦斯矿井主要进风巷以外区域出现1处失爆“煤矿电气”大项不得分;\n2.其他区域发现1处失爆，扣4分', 4, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (247, 3, 207, '机电/煤矿电气/井下供电系统', '7.3.2(3)', '3.采掘工作面供电：⑴配电点设置符合《煤矿安全规程》规定；⑵掘进工作面“三专两闭锁” 设置齐全、灵敏可靠；⑶采煤工作面瓦斯电闭锁设置齐全、灵敏可靠；⑷按要求试验，有试验记录', '查现场和资料。第(1)-(3)项1处不符合要求不得分；第(4)项不符合要求1处扣0.5分', '1.检查采掘配电点设置的地点是否符合《煤矿安全规程》第四百五十七条、第四百七十八条规定;\n2.抽查采区变电所是否有“三专”，现场试验风电、瓦斯电闭锁灵敏可靠性;\n3.查采掘工作面供电系统图及设备布置图，对照现场查看是否齐全，并试验是否灵敏可靠;\n4.检查日常运行维护人员的巡检试验记录，看是否按要求试验', '1.采掘工作面供电系统图及设备布置图\n2.煤矿安全规程', '1.第(1)~(3)项1处不符合要求，不得分；\n2.第(4)项不符合要求,1处扣0.5分', 2, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (248, 4, 207, '机电/煤矿电气/井下供电系统', '7.3.2(4)', '4.高压供电装备：⑴高压控制设备装有短路、过负荷、接地和欠压释放保护；⑵向移动变电站和高压电动机供电的馈电线上装有有选择性的动作于跳闸的单相接地保护；⑶真空高压隔爆开关装设有过电压保护；⑷推广设有通信功能的装备', '查现场或资料。不符合要求1处扣0.5分', '1.现场检查供电设备的各项保护装设是否齐全，是否投入使用;\n2.抽查开关漏电保护动作的可靠性;\n3.抽查开关保护整定值与批准值是否一致;\n4.抽查开关设备内部需要打开设备时必须有专职电工操作;5.检查设备巡检记录', '设备巡检记录', '不符合要求1处扣0.5分', 2, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (249, 5, 207, '机电/煤矿电气/井下供电系统', '7.3.2(5)', '5.低压供电装备：⑴采区变电所、移动变电站或者配电点引出的馈电线上有短路、过负荷和漏电保护；⑵有检漏或选择性的漏电保护；⑶按要求试验，有试验记录；⑷推广设有通信功能的装备', '查现场和资料。不符合要求1处扣0.5分', '1.现场检查供电设备的各项保护装设是否齐全，是否投入使用;\n2.抽查开关漏电保护动作的可靠性;\n3.抽查开关保护整定值与批准值是否一致;\n4.抽查开关设备内部需要打开设备时，是否有专职电工操作;5.检查设备巡检记录;6.查远方漏电试验记录', '1.设备巡检记录;2.远方漏电试验记', '不符合要求1处扣0.5分', 3, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (250, 6, 207, '机电/煤矿电气/井下供电系统', '7.3.2(6)', '6.变压器及电动机控制设备：⑴40KW及以上电动机使用真空磁力启动器控制；⑵干式变压器、移动变电站过负荷、短路等保护齐全可靠；⑶低压电动机控制设备有短路、过负荷、单相断线、漏电闭锁保护及远程控制功能', '查现场和资料。甩保护、铜铁保险、开关前盘带电1处扣1分，其他1处不符合要求扣0.5分', '1.现场检查供电设备的各项保护装设是否齐全，是否投入使用\n2.抽查开关漏电保护动作的可靠性;\n3.抽查开关保护整定值与批准值是否一致;\n4.抽查开关设备内部需要打开设备时，是否有专职电工操作;\n5.检查设备巡检记录', '设备巡检记录', '1.甩保护、铜铁保险开关前盘带电,1处扣1分;\n2.其他1处不符合要求，扣0.5分', 3, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (251, 7, 207, '机电/煤矿电气/井下供电系统', '7.3.2(7)', '7.保护接地符合《煤矿井下保护接地装置的安装、检查、测定工作细则》的要求', '查现场或资料。不符合要求1处扣0.5分', '1.重点检查中央变电所、采区变电所、主排水泵房等处电气设备保护接地规范程度和完好状况是否符合要求\n2.查接地细则并结合现场，检查易发生接地极、接地线规格小于规定等不规范、不合格问题', '1.矿井接地保护系统图和接地电阻定期则试值;\n2.接地细则', '1.外壳外接地没有在外壳上，而是接在底座上，扣0.5分;\n2.其他不符合要求,1处扣0.5分', 2, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (252, 8, 207, '机电/煤矿电气/井下供电系统', '7.3.2(8)', '8.信号照明系统：⑴井下信号、照明等其他220V单相供电系统使用综合保护装置；⑵保护齐全、可靠', '查现场或资料。不符合要求1处扣0.5分', '1.结合系统图，现场核查井下信号、照明是否使用综合保护装置;\n2.抽查综合保护装置是否动作可靠', NULL, '不符合要求1处扣0.5分', 1, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (253, 9, 207, '机电/煤矿电气/井下供电系统', '7.3.2(9)', '9. 电缆及接线工艺：⑴动力电缆和各种信号、监控监测电缆使用煤矿用电缆；⑵电缆接头及接线方式和工艺符合要求，无“羊尾巴”、“鸡爪子”、明接头；⑶各种电缆按规定敷设（吊挂），合格率不低于95%；⑷各种电气设备接线工艺符合要求', '查现场和资料。高瓦斯、突出矿井井下全范围以及低瓦斯矿井采区石门以里出现1处动力电缆不符合要求“煤矿电气”大项不得分，其他区域发现1处不符合要求扣3分；36V以上信号电缆不符合要求1处扣0.5分；本安电缆及电气设备接线工艺不符合要求1处扣0.2分；电缆合格率每降低1个百分点扣0.5分', '1.现场检查是否使用煤矿用电缆;\n2.重点检查各种电缆连接的质量，非本安系统电缆接头是否存在“鸡爪子”“羊尾巴”、明接头等\n3.对照电缆台账，检查巷道及沿线电缆敷设和吊挂是否合规，计算其合格率;\n4.由专职电工检查开关设备接线工艺是否符合完好标准;5.动力电缆不符合要求主要查动力电缆是否存在；一是使用淘汰型电缆、电缆规格与设计不符、电缆绝缘损坏等电缆状态差的问题；二是不等径、不同规格电缆直\n连、电缆连接装置或耐压不合格以及连接质量不合格等不合格连接；三是随时会发生顶板冒落、片帮以及轨道、管路设备堆积挤压电缆造成电缆短路等危险性吊挂等', '矿井电缆台账', '1.高瓦斯、突出矿井井下全范围以及低瓦斯矿井采区石门以里出现1处动力电缆不符合要求，“煤矿电气”大项不得分;\n2.其他区域发现1处不符合要求，扣3分;\n3.36V以上信号电缆不符合要求,1处扣0.5分;\n4.本安电缆及电气设备接线工艺不符合要求,1处扣0.2分;\n5.电缆合格率每降低1个百分点，扣0.5分;\n6.发现“羊尾巴”“鸡爪子”、明接头按照电气失爆处置', 3, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (254, 10, 207, '机电/煤矿电气/井下供电系统', '7.3.2(10)', '10. 井上下防雷电装置符合《煤矿安全规程》规定', '查现场和资料。不符合要求1处扣0.5分', '1.现场检查入井防雷、变电所防雷、瓦斯抽放站防雷设施是否完整;2.检查矿井上下防雷设施的测试报告', '矿井上下防雷设施及接地电阻的测试报告', '不符合要求1处扣0.5分', 1, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (255, 1, 209, '机电/基础管理/组织保障', '7.4.1(1)', '1.有负责机电管理工作的职能机构，有负责供电、电缆、小型电器、防爆、设备、配件、油脂、输送带、钢丝绳等日常管理工作职能部门', '查资料。无机构不得分，其他1处不符合要求扣0.5分', '查阅负责机电管理工作的职能机构成立文件和专业化小组工作职能图表及分工，机电科人员分工、职责是否明确', '1.机构成立文件;2.制度文件', '1.无机构不得分;\n2.其他1处不符合要求，扣0.5分', 1.5, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (256, 2, 209, '机电/基础管理/组织保障', '7.4.1(2)', '2.矿及生产区队配有机电管理和技术人员，责任、分工明确', '查资料。未配备人员不得分，其他1处不符合要求扣0.5分', '查基层区队机电管理是否有人员、有职责、有分工', '1.人员聘书;\n2.制度文件', '1.未配备人员，不得分;\n2.其他1处不符合要求扣0.5分', 1.5, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (257, 1, 210, '机电/基础管理/管理制度', '7.4.2(1)', '1.矿、专业管理部门建有以下制度（规程）：岗位安全生产责任制，操作规程，停送电管理、设备定期检修、电气试验测试、干部上岗检查、设备管理、机电事故统计分析、防爆设备入井安装验收、电缆管理、小型电器管理、油脂管理、配件管理、阻燃胶带管理、杂散电流管理以及钢丝绳管理等制度', '查资料。内容不全，每缺1种制度（规程）扣0.5分，制度（规程）执行不到位1处扣0.5分', '1.检查装订成册的机电管理制度汇编，是否有编制、审核批准人签字，并下发到全矿区队科室;\n2.查各项制度内容是否符合煤矿管理实际13.查记录看各项制度是否落实;\n4.查是否有各项管理台账', '1.机电管理制度汇编2.制度执行痕迹管理资料;3.出厂试验报告;4.第三方检验报告', '1.内容不全，每缺1种制度(规程),扣0.5分;\n2.制度(规程)执行不到位,1处扣0.5分;\n3.制度(规程)有明显缺陷1处扣0.1分，扣到0.5分为止', 1.5, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (258, 2, 210, '机电/基础管理/管理制度', '7.4.2(2)', '2.机房、硐室有以下制度、图纸和记录：⑴有操作规程，岗位责任、设备包机、交接班、巡回检查、保护试验、设备检修以及要害场所管理等制度；⑵有设备技术特征、设备电气系统图、液压（制动）系统图、润滑系统图；⑶有设备运转、检修、保护试验、干部上岗、交接班、事故、外来人员、钢丝绳检查（或其他专项检查）等记录', '查现场和资料。内容不全，每缺1种扣0.5分，执行不到位1处扣0.5分', '1.现场检查是否有基本要求制度、图纸记录;\n2.检查各项记录本，看记录内容是否与规定的一致\n3.查供电系统图', '1.记录本;2.供电系统图', '1.内容不全，每缺1种制度(规程),扣0.5分;2.制度(规程)执行不到位,1处扣0.5分;3.制度、图纸、记录有明显缺陷和错误,1处扣0.1分，扣到0.5分为止', 1.5, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (259, 1, 211, '机电/基础管理/技术管理', '7.4.3(1)', '1.机电设备选型论证、购置、安装、使用、维护、检修、更新改造、报废等综合管理及程序符合相关规定，档案资料齐全', '查现场和资料。不符合要求1处扣0.5分', '1.检查有无机电设备台账和档案资料，是否齐全；查看设备管理及程序是否符合要求;\n2.重点检查提人绞车、主要通风机等设备机械电控等关键部位技术改造后资料是否有留存，管理及程序是否符合要求', '机电设备台账和档案', '不符合要求1处扣0.5分', 1, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (260, 2, 211, '机电/基础管理/技术管理', '7.4.3(2)', '2.设备技术信息档案齐全，管理人员明确；主变压器、主要通风机、提升机、压风机、主排水泵、锅炉等大型主要设备做到一台一档', '查资料。无电子档案或无具体人员管理档案不得分，其他1处不符合要求扣0.5分', '1.查矿并大型固定设备一台一档资料是否完整，是否有电子档案;\n2.查与设备安全运行有关的说明书、原理图、配件图等原始资料是否齐全、完整；设备的更新、改造及大型检修和事故记载是否全面、翔实', '档案', '1.无电子档案或无具体人员管理档案不得分;\n2.其他1处不符合要求扣0.5分', 3, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (261, 3, 211, '机电/基础管理/技术管理', '7.4.3(3)', '3.矿井主提升、排水、压风、供热、供水、通讯、井上下供电等系统和井下电气设备布置等图纸齐全，并及时更新', '查资料。缺1种图不得分，图纸与实际不相符1处扣0.5分', '1.检查矿井主提升、排水、压风、供热、供水、通信、并上下供电等系统和井下电气设备布置等图纸是否齐全，是否及时更新，是否与实际相符;\n2.矿井动力电缆布置图、保护接地系统图等实用性基础性图纸是否齐全、与实际是否相符', '“基本要求”中列出的各种图纸', '1.缺1种图，不得分;\n2.图纸与实际不相符,1处扣0.5分', 2, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (262, 4, 211, '机电/基础管理/技术管理', '7.4.3(4)', '4.各岗位操作规程、措施及保护试验要求等与实际运行的设备相符', '查现场和资料。不符合要求1处扣0.5分', '1.检查汇编成册的覆盖全矿所有机电岗位的操作规程是否与实际相符;\n2.到机房硐室现场核查：查台账、查操作规程、与井下设备具体对照检查是否相符', '台账、操作规程', '不符合要求1处扣0.5分', 1, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (263, 5, 211, '机电/基础管理/技术管理', '7.4.3(5)', '5.持续有效地开展全矿机电专业技术专项检查与分析工作', '查资料。未开展工作不得分，工作开展效果不好1次扣1分', '1.检查是否有机电专项检查实施方案;\n2.检查是否有机电隐患排查问题整改落实表;\n3.检查是否有定期召开的机电例会、总结、分析报告、简报、通报等;\n4.查是否有专项记录、查是否有分析整改措施、是否无闭合', '1.专项记录;\n2.实施方案;\n3.整改落实表', '1.未开展工作不得分;\n2.工作开展效果不好,1次扣1分', 3, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (264, 1, 212, '机电/基础管理/设备技术性能测试', '7.4.4(1)', '1.大型固定设备更新改造有设计，有验收测试结果和联合验收报告', '查资料。没有或不符合要求不得分', '查阅矿井是否有批准的更新改造计划、竣工后的测试报告和联合验收报告', '矿井更新改造计划、测试报告、联合验收报告', '没有或不符合要求不得分', 1, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (265, 2, 212, '机电/基础管理/设备技术性能测试', '7.4.4(2)', '2.主提升设备、主排水泵、主要通风机、压风机及锅炉、瓦斯抽采泵等按《煤矿安全规程》检测；检测周期符合《煤矿在用安全设备检测检验目录（第一批）》或其他规定要求', '查资料。不符合要求1处扣0.5分', '检查有无有效期内的检测检验报告书，检测周期是否符合要求', '检测检验报告书', '不符合要求1处扣0.5分', 2, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (266, 3, 212, '机电/基础管理/设备技术性能测试', '7.4.4(3)', '3.主绞车的主轴、制动杆件、天轮轴、连接装置以及主要通风机的主轴、叶片等主要设备的关键零部件探伤符合规定', '查资料。不符合要求1处扣0.5分', '检查有无有效期内的检测检验报告书，检测周期是否符合要求', '检测检验报告书', '不符合要求1处扣0.5分', 2, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (267, 4, 212, '机电/基础管理/设备技术性能测试', '7.4.4(4)', '4.按规定进行防坠器试验、电气试验、防雷设施及接地电阻等测试', '查资料。1处不符合要求不得分', '检查有无有效期内的检测检验报告书，检测周期是否符合要求', '煤矿安全规程、检测检验报告书', '不符合要求1处扣0.5分', 2, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (268, 1, 214, '机电/岗位规范/专业技能', '7.5.1(1)', '1.管理和技术人员掌握相关的岗位职责、规程、设计、措施；2.作业人员掌握本岗位相应的操作规程和安全措施', '查资料和现场。不符合要求1处扣0.5分', '1.查资料，是否建立岗位责任制、岗位安全生产责任制;\n2.现场提问，随机抽考1名管理或技术人员回答3个问题，如果都回答不正确，视为不符合要求；随机抽取1名关键岗位人员回答1个最关键应知应会问题，如果回答不正确，视为不符合要求', NULL, '1.管理技术人员和普通作业人员3个问题都回答不正确，扣0.5分;\n2.机电专业关键岗位人员(如操作工)1个最关键问题回答不正确,1处扣0.5分', 2, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (269, 1, 215, '机电/岗位规范/规范作业', '7.5.2(1)', '1.严格执行岗位安全生产责任制；2.无“三违”行为；3.作业前进行安全确认', '查现场。发现 “三违”不得分；不执行岗位责任制、未进行安全确认1人次扣1分', '1.查现场实操，看是否执行岗位责任制，是否进行安全确认;\n2.现场检查有无“三违”行为', NULL, '1.发现“三违”行为不得分;\n2.不执行岗位责任制未进行安全确认,1人次扣1分', 3, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (270, 1, 217, '机电/文明生产/设备设置', '7.6.1(1)', '1.井下移动电气设备上架，小型电器设置规范、可靠；2.标志牌内容齐全；3.防爆电气设备和小型防爆电器有防爆入井检查合格证；4.各种设备表面清洁，无锈蚀', '查现场。不符合要求1处扣0.2分', '1.现场检查采掘配电点的电气设备和小型电器的设置是否规范、可靠;\n2.查标志牌、合格证是否齐全;\n3.查各种设备表面是否清洁，是否锈烛\n4.查是否有防爆设备入井台账', NULL, '不符合要求1处扣0.2分', 1.5, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (271, 1, 218, '机电/文明生产/管网', '7.6.2(1)', '1.各种管路应每100m设置标识，标明管路规格、用途、长度、管路编号等；2.管路敷设（吊挂）符合要求，稳固；3.无锈蚀，无跑、冒、滴、漏', '查现场。不符合要求1处扣0.5分', '现场沿途检查管路，标识是否齐全，悬挂是否规范、管路有无锈蚀，有无跑冒滴漏现象', NULL, '不符合要求1处扣0.5分', 2, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (272, 1, 219, '机电/文明生产/机房卫生', '7.6.3(1)', '1.机房硐室、机道和电缆沟内外卫生清洁；2.无积水，无油垢，无杂物；3.电缆、管路排列整齐', '查现场。卫生不好或电缆排列不整齐1处扣0.2分，其他1处不符合要求扣0.5分', '现场检查，含地面、井下机房硐室和配电点，电缆悬挂的沿线巷道等，是否卫生、整齐', NULL, '1.卫生不好或电缆排列不整齐,1处扣0.2分;\n2.其他1处不符合要求扣0.5分', 1.5, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (273, 1, 220, '机电/文明生产/照明', '7.6.4(1)', '机房、硐室以及巷道等照明符合《煤矿安全规程》要求', '查现场。不符合要求1处扣0.5分', '现场核查，以规程要求设置照明的地点和场所为主要检查对象，照明是否符合要求', NULL, '不符合要求1处扣0.5分', 1, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (274, 1, 221, '机电/文明生产/器材工具', '7.6.5(1)', '消防器材、电工操作绝缘用具齐全合格', '查现场。消防器材、绝缘用具欠缺、失效或无合格证1处扣0.5分', '检查地面机房、变电所、瓦斯泵房，井下变电所、水泵房及采掘配电点等要害场所，检查消防器材是否有消防部门检验并发的合格证；绝缘用具是否有相关部门颁发的检验合格证', NULL, '凡发现消防器材、绝缘用具欠缺、失效或无合格证,1处扣0.5分', 1, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (275, 1, 224, '运输/巷道硐室/巷道车场/硐室车房/装卸载站', '8.1.1(1)', '1.巷道车场：\n（1）.巷道支护完整，巷道（包括管、线、电缆）与运输设备最突出部分之间的最小间距符合《煤矿安全规程》规定；（2）.车场、车房、巷道曲线半径、巷道连接方式、运输方式设计合理，符合《煤矿安全规程》及有关规定要求；\n2.硐室车房：\n斜巷信号硐室、躲避硐、运输绞车车房、候车室、调度站、人车库、充电硐室、错车硐室、车辆检修硐室等符合《煤矿安全规程》及有关规定要求；\n3.装卸载站\n车辆装载站、卸载站和转载站符合《煤矿安全规程》及有关规定要求', '查现场。不符合要求1处扣2分', '巷道车场                                                                                                                                                                                                                                                                                                                                                                   1.抽查主要斜井、井底车场、主要运输巷道等1~3条运输巷道；用米尺测量巷道(包括管、线、电缆与运输设备最突出部分之间的最小间距\n(1)人行道0.8m(综采矿井、人车停车地点的巷道上下人侧1m)以上，巷道另侧0.3m(综采矿井0.5m);\n(2)测量双轨运输巷中两列列车最突出部分之间的距离，对开时是否不小于0.2m采用单轨吊车运输的巷道，对开时是否不小于0.8m;\n(3)采用无轨胶轮车运输的巷道:\n①双车道行驶，会车时不得小于0.5m;②单车道应当根据运距、运量、运速及运输车辆特性，在巷道的合适位置设置机车绕行道或者错车硐室，并设置方向标识;\n2.查巷道设计和现场对比，采区采掘巷道上下山条数是否足够                                                                                                                                                                                                                                                                                 硐室车房                                                                                                                                                                                                                                                                                                                                                                   1.查斜巷串车提升各车场是否设置信号硐室和躲避硐;\n2.查信号硐室：宽不小于1.5m,深不小于1.5m,高不小于2.0m;躲避硐宽度不得小于1.2m,深度不得小于0.7m,高度不得小于1.8m,躲避硐内严禁堆积物料;\n3.其他硐室是否符合(《煤矿安全规程》《煤矿斜井井筒及硐室设计规范》(GB504142007)和《煤矿井底车场硐室设计规范》(GB50416-2007)有关规定                                                                                                                                 装载卸站                                                                                                                                                                                                                                                                                                                                                                  现场检查装载点两列列车最突出部分之间的距离是否不小于0.7m,矿车摘挂钩地点是否不小于1m', NULL, '巷道车场                                                                                                                                                                                                         1.巷道(包括管、线、电缆)与运输设备最突出部分之间的最小间距不足0.3m,1处扣2分;2.人行道不符合要求,1处扣2分;3.其他1处不符合要求，扣2分;4.1条巷道按1处计算，同一巷道扣分不累计                                                                                                                                 硐室车房                                                                                                                                                                                                          1.未设置信号硐室、躲避硐室的，每缺1处扣2分\n2.信号硐室、躲避硐室宽、高、深1处不符合要求，扣2分;\n3.其他1处不符合要求扣2分                                                                                                                                                                               装载卸站                                                                                                                                                                                                         1处不符合要求扣2分', 8, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (276, 1, 226, '运输/运输路线/轨道（道路）系统', '8.2.1(1)', '1.运行7t及以上机车、3t及以上矿车，或者运送15t及以上载荷的矿井主要水平运输大巷、车场、主要运输石门、采区主要上下山、地面运输系统轨道线路使用不小于30kg/m的钢轨；其他线路使用不小于18kg/m的钢轨\n2.主要运输线路（主要运输大巷和主要运输石门、井底车场、主要绞车道，地面运煤、运矸干线和集中运载站车场的轨道）及行驶人车的轨道线路质量达到以下要求：⑴接头平整度：轨面高低和内侧错差不大于2mm；⑵轨距：直线段和加宽后的曲线段允许偏差为-2mm～5mm；⑶水平：直线段及曲线段加高后两股钢轨偏差不大于5mm；⑷轨缝不大于5mm；⑸扣件齐全、牢固,与轨型相符；⑹轨枕规格及数量应符合标准要求，间距偏差不超过50mm；⑺道碴粒度及铺设厚度符合标准要求，轨枕下应捣实；⑻曲线段设置轨距拉杆\n3.其他轨道线路不得有杂拌道（异型轨道长度小于50m为杂拌道），质量应达到以下要求：⑴接头平整度：轨面高低和内侧错差不大于2mm；⑵轨距：直线段和加宽后的曲线段允许偏差为-2mm～6mm；⑶水平：直线段及曲线段加高后两股钢轨偏差不大于8mm；⑷轨缝不大于5mm；⑸扣件齐全、牢固,与轨型相符；⑹轨枕规格及数量符合标准要求，间距偏差不超过50mm；⑺道碴粒度及铺设厚度符合标准要求，轨枕下应捣实\n4.异型轨道线路、齿轨线路质量符合设计及说明书要求\n5.单轨吊车线路达到以下要求：⑴下轨面接头间隙直线段不大于3mm；⑵接头高低和左右允许偏差分别为2mm和1mm；⑶接头摆角垂直不大于7°，水平不大于3°；⑷水平弯轨曲率半径不小于4m，垂直弯轨曲率半径不小于l0m；⑸起始端、终止端设置轨端阻车器\n6.无轨胶轮车主要道路采用混凝土、铺钢板等方式硬化', '1.查现场。1处不符合要求扣3分，单项扣至10分为止\n2.查现场。抽查1～3条巷道，接头平整度、轨距、水平不符合要求1处扣0.5分，其他1处不合格扣0.2分，单项扣至10分为止\n3.查现场。抽查1～3条巷道，接头平整度、轨距、水平不符合要求1处扣0.3分，其他1处不合格扣0.1分，单项扣至7分为止\n4.查现场。不符合要求1处扣1分，单项扣至5分为止\n5.查现场。轨端阻车器不符合要求扣3分，其他1处不符合要求扣0.5分，单项扣至5分为止\n6.查现场。不符合要求1处扣1分，单项扣至5分为止', '现场检查抽查1~3条运输巷道的轨型、运输设备型号等情况                                                                                                                                                                                                                                                                                     1.采用选点检查方法，检查3条主要运输巷道，每条巷道检查5个点，每个点为1处，每个点的主要观察项目(接头平整度轨距、水平),按照《煤矿窄轨铁道维修质量标准及检查评级办法》规定的检查方式上尺上线检查;\n2.主要检查项目①接头平整度是否满足要求，轨面高低和内侧错差不大于2mm;②轨距是否满足要求，直线段和加宽后的曲线段允许偏差为-2~5mm;③3水平是否满足要求,直线段及曲线段加高后两股钢轨偏差不大于5mm\n3.一般检查项目：轨缝是否不大于5mm;扣件是否齐全、牢固,与轨型相符；轨枕规格及数量是否符合标准要求，间距偏差不超过50mm;道碴粒度及铺设厚度是否符合标准要求，轨枕下应捣实；曲线段是否设置轨距拉杆                           1.采用选点检查方法，每个矿井检查3条主要运输巷道，每条巷道检查5个点，按照《煤矿窄轨铁道维修质量标准及检查评级办法》规定的检查方式，上尺上线检\n2.主要检查项目:①接头平整度是否满足要求，轨面高低和内侧错差不大于2mm;②轨距是否满足要求，直线段和加宽后的曲线段允许偏差为-2~6mm;③水平是否满足要求，直线段及曲线段加高后两股钢轨偏差不大于8mm;\n3.一般检查项目：轨缝是否不大于5mm;扣件是否齐全、牢固，与轨型相符；轨枕规格及数量是否符合标准要求，间距偏差是否不超过50mm;道碴粒度及铺设厚度是否符合标准要求，轨枕下是否捣实求                                                    对照设计和说明书,1现场检查质量是否符扣合要求上                                                                                                                                                                                                                                                                                                1.现场采用选点检查方法，按照《煤矿井下辅助运输设计规范》(GB50533-2009)第13.3.3条规定抽查不少于10点;\n2.检查下轨面接头间隙直线段是否不大于3mm;\n3.检查接头高低和左右允许偏差是否不超过2mm和1mm:/\n4.检查接头摆角垂直是否不大于7°,水平不大于3°;5.检查水平弯轨曲率半径是否不小于4m,垂直弯轨曲率半径不小于10m;\n6.检查起始端是否设置轨端阻车器                                                                                                                                                                                                                                                                                                                            现场采用选点检查方法，每个矿井抽查3条主要运输巷道(主要运输大巷、主要运输石门、井底车场),每条巷道检查5个点，按照《煤矿井下辅助运输设计规范》(GB50533-2009)第13.4条检查巷道硬化是否合格(运输巷道不应有底鼓现象，路面应平整、硬化', '矿井设计、矿并轨道线路台账、矿井运输系统图等资料                                                                                                                                                                                                                                                                                       矿井异型轨道线路、齿轨线路设计及说明书                                                                                                                                                                                                                                                                                                            单轨吊线路设计及说明书等', '1.（1）条巷道内轨型不符合要求扣3分；（2）扣到10分为止；2.检查3条主要运输巷道每条巷道检查5个点,每个检查点主要检查项目不符合要求扣0.5分,一般检查项目不符合要求每处扣0.2分(1个点最多扣0.7分,5个点累计扣分为1条巷道的总扣分,3条卷道累计扣分为本小项总扣分),扣到10分为止；3.（1）检查3条主要运输巷道，每条巷道检查5个点，每个检查点主要检查项目不符合要求扣0.4分、一般检查项目不符合要求每处扣0.1分；（2）发现1处杂拌道，扣2分；（3）扣到7分为止；4.1处不符合设计要求扣1分，扣到5分为止；5.（1）未设置轨端阻车器，扣3分；（2）其他1处不符合要求，扣0.5分；（3）扣到5分为止；6.巷道内1处不符合要求扣1分，扣到5分为止', 18, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (277, 1, 227, '运输/运输路线/道岔', '8.2.2(1)', '1.道岔轨型不低于线路轨型，无非标准道岔，道岔质量达到以下要求：⑴轨距按标准加宽后及辙岔前后轨距偏差不大于+3mm；⑵水平偏差不大于5mm；⑶接头平整度：轨面高低及内侧错差不大于2mm；⑷尖轨尖端与基本轨密贴，间隙不大于2mm，无跳动，尖轨损伤长度不超过100mm，在尖轨顶面宽20mm处与基本轨高低差不大于2mm；⑸心轨和护轨工作边间距按标准轨距减小28mm后，偏差+2mm；⑹扣件齐全、牢固，与轨型相符；⑺轨枕规格及数量符合标准要求，间距偏差不超过50mm，轨枕下应捣实\n2.单轨吊道岔达到以下要求：⑴道岔框架4个悬挂点的受力应均匀，固定点数均匀分布不少于7处；⑵下轨面接头轨缝不大于3mm；⑶轨道无变形，活动轨动作灵敏，准确到位；⑷机械闭锁可靠；⑸连接轨断开处设有轨端阻车器', '1.查现场。轨距、水平、接头平整度、尖轨、心轨和护轨工作边间距不符合要求1处扣1分，其他1处不符合要求扣0.5分\n2.查现场。机械闭锁、轨端阻车器不符合要求1处扣3分，其他1处不符合要求扣1分', '1.现场抽查3条巷道,每条巷道查3处道岔检查按照《煤矿窄轨铁道维修质量标准及检查评级办法》规定的检查方式,上尺上线检查；2.主要检查项目①轨距按标准加宽后及辙岔前后轨距偏差是否不大于+3mm②水平偏差是否不大于5mm;③接头平整度:轨面高低及内侧错差是否不大于2mm;④尖轨尖端与基本轨密贴,间隙是否不大于2mm,无跳动,尖轨损伤长度是否不超过100mm,在尖轨顶面宽20mm处与基本轨高低差是否不大于2mm;⑤心轨和护轨工作边间距按标准轨距减小28mm后,偏差是否不超过+2mm;3.一般检查项目查扣件是否齐全、牢固,与轨型相符;轨枕规格及数量是否符合标准要求,间距偏差是否不超过50mm,轨枕下应捣实;4.现场抽查,数量般不少于2组;5.检查道岔框架4个悬挂点的受力是否均匀,固定点数均匀分布是否不少于7处;6.查看下轨面接头轨缝是否不大于3mm;7.检查轨道有无变形,活动轨是否动作灵敏，准确定位;8.通过现场销子来检查机械闭锁是否可靠；9.查道岔连接轨断开处是否设置阻车机构', '煤矿窄轨铁道维修质量标准及检查评级办法', '1.发现非标准道岔或道岔轨型低于线路轨型,扣2分；2.1处道岔有主要检查项目不符合要求(不论几处)扣1分、一般检查项目不符合要求(不论几处)扣0.5分,3处道岔累计计分、3条巷道累计计分；3.扣到5分为止；4.机械闭锁、轨端阻车器不符合要求,1处扣3分；5.其他1处不符合要求扣1分', 5, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (278, 1, 228, '运输/运输路线/窄轨架线电机车牵引网络', '8.2.3(1)', '1.敷设质量达到以下要求：⑴架空线悬挂高度：自轨面算起，架空线悬挂高度在行人的巷道内、车场内以及人行道与运输巷道交叉的地方不小于2m；在不行人的巷道内不小于1.9m；在井底车场内，从井底到乘车场不小于2.2m；在地面或工业场地内，不与其他道路交叉的地方不小于2.2m；⑵架空线与巷道顶或棚梁之间的距离不小于0.2m；悬吊绝缘子距架空线的距离，每侧不超过0.25m；⑶架空线悬挂点的间距，直线段内不超过5m，曲线段内符合规定；⑷架空线直流电压不超过600V；⑸两平行钢轨之间每隔50m连接1根断面不小于50mm2的铜线或者其他具有等效电阻的导线。线路上所有钢轨接缝处，用导线或者采用轨缝焊接工艺加以连接。连接后每个接缝处的电阻符合要求；⑹不回电的轨道与架线电机车回电轨道之间，应加以绝缘。第一绝缘点设在2种轨道的连接处；第二绝缘点设在不回电的轨道上，其与第一绝缘点之间的距离应大于1列车的长度。在与架线电机车线路相连通的轨道上有钢丝绳跨越时，钢丝绳不得与轨道相接触；⑺绝缘点应经常检查维护，保持可靠绝缘；2.电机车架空线巷道乘人车场装备有架空线自动停送电开关', '查现场。架空线悬挂高度、架空线与巷道顶或棚梁之间的距离、悬吊绝缘子距架空线的距离、架空线悬挂点间距、架空线直流电压绝缘点不符合要求1处扣2分。其他1处不符合要求扣0.5分。架空线巷道乘人车场未装备有架空线自动停送电开关的不得分', '1.现场查架空线悬挂高度、架空线与巷道顶或棚梁之间的距离、架空线悬挂点的间距、架空线直流电压是否符合要求;2.查两平行钢轨之间是否每隔50m连接1根断面不小于50mm²的铜线或者其他具有等效电阻的导线;线路上所有钢轨接缝处是否用导线或者采用轨缝焊接工艺加以连接;连接后每个接缝处的电阻是否符合要求(I8kg/m钢轨,不大于0.00024Ω;22kg/m钢轨,不大于0.00021Ω;24kg/m钢轨,不大于0.00020Ω;30kg/m钢轨,不大于0.00019Ω；33kg/m钢轨,不大于0.00018Ω;38kg/m钢轨,不大于0.00017Ω;\n43kg/m钢轨,不大于0.00016Ω);查轨道焊缝是否有开焊现象3.查轨道绝缘点,第一绝缘点是否设在2种轨道的连接处;第二处是否设在距离第1处超过1列车长度的不回电的轨道上；4.在与架线电机车线路相连通的轨道上有钢丝绳跨越时,查钢丝绳是否与轨道相接触；5.结合《煤矿安全规程》要求,检查乘人车场是否装备有架空线自动停送电开关', NULL, '1.架空线悬挂高度、架空线与巷道顶或棚梁之间的距离、悬吊绝缘子距架空线的距离、架空线悬挂点间距、架空线直流电压绝缘点不符合要求等同类问题1处扣2分，不累计扣分；2.其他同类问题1处不符合要求扣0.5分,不累计扣分;3.架空线巷道乘人车场未装备有架空线自动停送电开关的扣4分', 4, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (279, 1, 229, '运输/运输路线/运输方式改善', '8.2.4(1)', '1.长度超过1.5km的主要运输平巷或者高差超过50m的人员上下的主要倾斜井巷，应采用机械方式运送人员\n2.逐步淘汰斜巷（井）人车提升，采用其他方式运送人员； 3.水平单翼距离超过4000m时，有缩短运输距离的有效措施；4.采用其他运输方式替代多级、多段运输', '1.查现场。1处不符合要求扣5分\n2.查现场。不符合要求1处扣0.1分', '1.现场查看运输线路图或矿井设计，确定运输人员方式是否为机械方式；2.①现场查是否使用斜巷（井）人车提升；②结合图纸、现场查水平单翼距离超过4000m时，是否有缩短运输距离的措施；③现场查是否采用其他运输方式替代3级（段）及以上运输', '1.①运输线路图；②矿井设计；2.运输线路图', '1.1处不符合要求扣5分；2.①使用斜巷（井）人车，1处扣0.1分；②水平单翼距离超过4000m时，没有缩短运输距离的有效措施，扣0.1分；③井下采用多级、多段运输方式，1处扣0.1分', 5, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (280, 1, 231, '运输/运输设备/设备管理', '8.3.1(1)', '1.在用运输设备综合完好率不低于90%；矿车、专用车完好率不低于85%；运送人员设备完好率100%\n2.人行车、架空乘人装置、机车、调度绞车、无极绳连续牵引车、绳牵引卡轨车、绳牵引单轨吊车、单轨吊车、齿轨车、无轨胶轮车、矿车、专用车等运输设备编号管理', '1.(普通轨斜巷（井）人车)查现场。完好率每降低1个百分点扣0.2分。综合完好率低于70%扣5分，矿车、专用车完好率低于60%扣5分。运送人员设备1台不完好扣2.5分\n2.查现场。不符合要求1处扣0.5分', '1.①现场查矿车、专用车共30辆,运人设备全部检查,检查标准执行《煤矿矿井机电设备完好标准》;②查人车是否存在危及乘车人员的安全问题(如没有车门或车链、轮对损坏或不转、连接装置不完好等);③计算综合完好率:A=B×100/C。其中,A为综合完好率,B为检查运输设备完好数量,C为检查运输设备数量；2.运输设备编号管理查现场和设备台账，并做对比检查', '各种运输设备管理台账', '1.①完好率每降低1个百分点,扣0.2分;②综合完好率低于70%,扣5分;③矿车、专用车完好率低于60%,扣5分;④运送人员设备不完好扣2.5分；2.1辆车没有编号扣0.5分', 5, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (281, 1, 232, '运输/运输设备/普通轨斜巷（井）人车/架空乘人装置/机车/调度绞车/无极绳连续牵引车、绳牵引卡轨车、绳牵引单轨吊车/单轨吊车/无轨胶轮车', '8.3.2(1)', '1.普通轨斜巷（井）人车\n(1).制动装置齐全、灵敏、可靠；(2).装备有跟车工在运行途中任何地点都能发送紧急停车信号的装置，并具有通话和信号发送、接收功能，灵敏可靠\n2.架空乘人装置:\n(1).架空乘人装置正常运行。每日至少对整个装置进行1次检查；(2).工作制动装置和安全制动装置齐全、可靠；(3).运行坡度、运行速度不得超过《煤矿安全规程》规定；(4).装设超速、打滑、全程急停、防脱绳、变坡点防掉绳、张紧力下降、越位等保护装置，并达到齐全、灵敏、可靠；(5).有断轴保护措施；(6).各上下人地点装备通信信号装置，具备通话和信号发送接收功能，灵敏、可靠；(7).沿线设有延时启动声光预警信号及便于人员操作的紧急停车装置；(8).减速器应设置油温检测装置，当油温异常时能发出报警信号；(9).钢丝绳安全系数、插接长度、断丝面积、直径减小量、锈蚀程度符合《煤矿安全规程》规定\n3.机车\n（1）.制动装置符合规定，齐全、可靠；（2）.列车或者单独机车前有照明、后有红灯；（3）.警铃(喇叭)、连接装置和撒砂装置完好；（4）.同一水平行驶5台及以上机车时，装备机车运输集中信号控制系统及机车通信设备；同一水平行驶7台及以上机车时，装备机车运输监控系统；（5）.新建投产的大型矿井的井底车场和运输大巷，装备机车运输监控系统或者运输集中信号控制系统；（6）.防爆蓄电池机车或者防爆柴油机动力机车装备甲烷断电仪或者便携式甲烷检测报警仪；（7）.防爆柴油机动力机车装备自动保护装置和防灭火装置\n4.调度绞车\n（1）.安装符合设计要求，固定可靠；（2）.制动装置符合规定，齐全、可靠；（3）.钢丝绳安全系数、断丝面积、直径减小量、锈蚀程度以及滑头、保险绳插接长度符合《煤矿安全规程》规定；（4）.声光信号齐全、完好\n5.无极绳连续牵引车、绳牵引卡轨车、绳牵引单轨吊车\n(1).闸灵敏可靠，使用正常；(2).装备越位、超速、张紧力下降等安全保护装置，并正常使用；(3).设置司机与相关岗位工之间的信号联络装置；设有跟车工时，应设置跟车工与牵引绞车司机联络用的信号和通信装置；(4).驱动部、各车场设置行车报警和信号装置；(5).钢丝绳安全系数、插接长度、断丝面积、直径减小量、锈蚀程度符合《煤矿安全规程》规定\n6.单轨吊车\n(1).具备2路以上相对独立回油的制动系统；(2).设置既可手动又能自动的安全闸，并正常使用；(3).超速保护、甲烷断电仪、防灭火设备等装置齐全、可靠；(4).机车设置车灯和喇叭，列车的尾部设置红灯；(5).柴油单轨吊车的发动机排气超温、冷却水超温、尾气水箱水位、润滑油压力等保护装置灵敏、可靠；(6).蓄电池单轨吊车装备蓄电池容量指示器及漏电监测保护装置，且齐全、可靠\n7.无轨胶轮车 \n(1).车辆转向系统、制动系统、照明系统、警示装置等完好可靠,车辆自带防止停车自溜的设施或工具；(2).装备自动保护装置、便携式甲烷检测报警仪、防灭火设备等安全保护装置；(3).行驶5台及以上无轨胶轮车时，装备车辆位置监测系统；(4).装备有通信设备；(5).运送人员应使用专用人车；(6).载人或载货数量在额定范围内；(7).运行速度，运人时不超过25km/h，运送物料时不超过40km/h,车辆不空挡滑行', '1.查现场。1处不符合要求“运输设备”大项不得分\n2-7.查现场和资料。未按规定装设有关装置扣2分，其他1处不符合要求扣1分，单项扣至10分为止', '1.普通轨斜巷（井）人车①查现场,根据《矿用斜井人车技术条件》(MT388-2007)规定,检查制动装置是否齐全、灵敏、可靠;②检查每节人车是否都有制动装置,检查工作制动装置是否灵敏可靠;③检查是否有紧急停车信号装置,功能是否齐全且灵敏可靠；2.架空乘人装置①检查乘人装置每日检查记录,是否做到每日至少对整个装置进行1次检查;②现场检查工作制动器、安全制动器是否齐全有效,安全制动装置是否设置在驱动轮上(《煤矿安全规程》第三百八十三条规定)③现场检查运行坡度、运行速度是否符合《煤矿安全规程》第三百八十三条规定；④检查是否装设超速、打滑、全程急停、防脱绳、变坡点防掉绳、张紧力下降、越位等保护装置,装置是否齐全、灵敏、可靠(《煤矿安全规程》第三百八十三条规定);⑤现场检查是否有断轴保护措施(《煤矿安全规程》第三百八十三条规定)；⑥现场检查各上下人地点是否装备通信信号装置且具备通话和信号发送接收功能，下地点前方是否设声光报警的警告信号(《煤矿安全规程》第三百八十三条规定);⑦现场检查沿线是否设置延时启动声光预瞥信号和便于人员操作的紧急停车装置;⑧现场检查减速器是否设置油温检测装置,并查看油温异常时能否报警(《煤矿安全规程》第三百八十三条规定；⑨根据《煤矿安全规程》第三百九十条、第三百九十一条规定,现场检查钢丝绳安全系数、插接长度、断丝面积、直径减小量、锈蚀程度是否符合规程；3.机车①现场(包括井上下)查电机车制动是否可靠;②查机车或列车是否前有照明后有红尾；③查机车车体状况,是否有完好的警铃,砂箱内是否有干砂,砂管是否畅通;机车列车连接装置是否完好；④对照设计说明书,查同一水平行驶5台及以上机车时,是否装备机车运输集中信号控制系统及机车通信设备;同一水平行驶7台及以上机车时,是否装备机车运输监控系统;⑤新建矿井指2017年以后投产的矿井，查看2017年1月以后投产且年产1.2Mt以上矿井的井底车场、运输大巷是否装备机车运输监控系统或者运输集中信号控制系统(《煤矿安全规程》第三百七十七条);⑥现场检查防爆电机车或者防爆柴油机动力车是否装备甲烷断电仪或便携式甲烷检测报警仪；⑦现场检查防爆柴油机动力机车是否装备自动保护装置和防灭火装置；4.调度绞车①对照设计,现场检查絞车安装是否符合设计,是否固定可靠;运输絞车安装在轨道线路一侧时,运输绞车外缘距运输车辆突出部分距离是否不小于0.3m;②现场检查绞车闸把及杠杆系统是否动作灵活可靠,旋闸后闸把位置是否不超过水平位置,拉杆螺栓叉头、阐把、销轴是否无损伤变形,拉杆螺栓是否有背帽紧固,闸带是否无断裂,磨损余厚是否不小于3mm,闸轮磨损深度是否不大于2mm,闸轮表面是否无油迹；③现场检查絞车钢丝绳是否符合《煤矿矿井机电设备完好标准》《煤矿安全规程》(第四百零八条-第四百一十三条)要求;④现场查看声光信号是否齐全、完好；5.无极绳连续牵引车、绳牵引卡轨车、绳牵引单轨吊车①结合资料,现场检查设备完好是否符合《煤矿安全规程》第三百九十条和第三百九十一条、《煤矿井下辅助运输设计规范》(GB50533-2009)第6.4条、第7条、第8.3条的要求；②依据《煤矿井下钢丝绳牵引卡轨车技术条件》(MT/T590)规定,查车房现场，检查无极绳连续牵引车、绳牵引卡轨车、绳牵引单轨吊车是否装备越位、超速、张紧力下降等安全保护装置,并正常使用；③现场检查是否设置司机与相关岗位工之间的信号联络装置和通信装置；④现场检查在驱动部位、各车场是否设置行车报警装置；⑤现场检查钢丝绳、插接长度、断丝面积、直径减小量、锈蚀程度等情况是否符合《煤矿安全规程》关于钢丝绳的各项规定；6.单轨吊车①现场检查是否有2路以上独立回油制动系统（《煤矿安全规程》第三百九十条规定）；②现场检查是否设置两类制动闸,是否\n可靠（《煤矿安全规程》第三百九十条规定）；③现场检查超速保护、甲烷断电仪、防灭火设备等装置是否齐全并结合相关标准，现场检查可靠性；④检查是否有车灯和喇叭,列车尾部是否设置红灯(《煤矿安全规程》第三百九十条规定);⑤检查保护是否可靠,是否具有发动机排气超温、冷却水超温、尾气水箱水位润滑油压力等保护装置;排气口的排气温度是否超过77℃,是否表面温度是否超过150℃,冷却水温度是否超过95℃（《煤矿安全规程》第三百七十八条规定）⑥检查蓄电池单轨\n吊车是否装备容量指示器及漏电监测保护装置,结合相关标准现场检查是否可靠；7.无轨胶轮车①结合相关报告、证书、规程、标准,检查车辆转向系统、制动系统、照明系统、警示装置等是否完好,车辆是否携带停车自溜装置;②现场检查甲烷检测报警仪、防灭火设备等安全保护装置是否配备;③检查行驶5台及以上无轨胶轮车时,是否装备车辆位置监测系统(《煤矿安全规程》第三百九十二条规定)；④查是否有通讯设备；⑤现场检查是否存在使用非运人车辆运送人员情况;⑥现场检查是否存在超员乘车现象,是否有安全带或其他牢固的依托物;检查载货车辆装载数量在额定范围内;⑦现场检查运送人员时运行速度是否不超过25km/h,运送物料时是否不超过40km/h,是否空挡滑行(《煤矿安全规程》第三百九十二条规定)\n\n\n', '1.架空乘人装置：①架空乘人装置日常检查记录；②钢丝绳换绳记录、每日验绳记录；钢丝绳性能检验报告；③架空乘人装置设计说明书、图纸等；2.机车：①机车运输集中信号控制系统或机车运输监控系统设计说明书；3.调度绞车：①绞车安装设计或作业规程；②煤矿矿井机电设备完好标准、煤矿安全规程；4.无极绳连续牵引车、绳牵引卡轨车、绳牵引单轨吊车：①无极绳连续牵引车、绳牵引卡轨车、绳牵引单轨吊车设计说明书；②《煤矿井下辅助运输设计规范》（GB50533—2009）；③煤矿井下钢丝绳牵引卡轨车技术条件；5.单轨吊车：单轨吊车设计说明书；6.无轨胶轮车：①无轨胶轮车标志证书、说明书；②无轨胶轮车年度检测检验报告；③无轨胶轮车司机操作规程', '1.普通轨斜巷（井）人车：1处不符合要求扣22分；2.架空乘人装置：①未开展检查扣1分;②每缺1项保护装置，扣2分；③缺少断轴保护措施，扣2分；④制动闸不符合要求，扣2分;⑤上下人地点未装备通信信号装置,扣2分；⑥通信信号装置不完好，扣1分；⑦沿线未设有延时启动声光预警信号、紧急停车装置,扣2分;⑧声光预警信号、紧急停车装置不完好不符合要求,扣1分;⑨其他不符合要求,1处扣1分,本项扣到10分为止；3.机车：①制动装置不符合规定,1台扣1分;②列车或者单独机车没有照明、红尾灯,一台扣2分;照明灯照度不符合要求或尾灯不亮一台扣1分;③机车缺少撒砂装置,扣2分;缺少干砂或砂管口未正对轨面的,一台扣1分；④同一水平行驶5台及以上机未按要求装设集中信号控制系统及机车通信设备、运输监控系统的,扣2分;⑤防爆蓄电池机车或者防爆柴油机动力机车未装备甲烷断电仪或者便携式\n甲烷检测报警仪,防爆柴油机动力机车未装备自动保护装置和防灭火装置,扣2分;⑥连接装置不符合要求,扣1分;⑦其他有不完好现象,1处不符合要求,扣1分；本项扣到10分为止；4.调度绞车：①绞车固定不稳固,钢丝绳安全系统数不符合要求,发现1处扣2分;②未设置声光信号,扣2分;③其他1处不符合要求,扣1分;本项扣到10分为止；5.无极绳连续牵引车、绳牵引卡轨车、绳牵引单轨吊车：①闸不灵敏、不可靠,钢丝绳安全系统数不符合要求,保护装置不符合要求,1处扣2分;②司机与跟车工之间未设置通信设施、驱动部和各车场未设置行车报警和信号装置,扣2分;③其他1处不符合要求，扣1分;本项扣到10分为止；6.单轨吊车：①单轨吊车制动不可靠、缺少少保护装置，1处扣2分；②其他1处不符合要求，扣1分；本项扣到10分为止；7.无轨胶轮车：①车辆未装设自动保护装置、车辆不完好,一台扣2分;②矿井未对车辆装设通讯、监测装置、灭火设施扣2分;③无通讯设备扣2分；④未使用专用人车运送人员,扣1分;⑤截人超过规定人数,扣1分;载物超过规定要求的,扣1分;⑥超速行驶或司机不规范操作车辆的,1人次扣1分；⑦其他1处不符合要求,扣1分;本项扣到10分为止', 17, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (282, 1, 234, '运输/运输安全设施/挡车装置和跑车防护装置', '8.4.1(1)', '挡车装置和跑车防护装置齐全、可靠，并正常使用', '查现场。1处不符合要求不得分', '1.查现场装置是否齐全、可靠、正常使用,是否符合《煤矿安全规程》第三百八七条关于挡车装置、防护装置5个方面的规定；2.结合说明书、日常记录等资料,现场\n检查防跑车和跑车防护装置是否能正常使用', '1.挡车装置和跑车防护装置安装设计说明书；2.日常检查记录', '未按要求装设防跑车和跑车防护的，扣5分', 5, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (283, 1, 235, '运输/运输安全设施/安全警示', '8.4.2(1)', '1.斜巷各车场及中间通道口装备有声光行车报警装置，并使用正常；2.斜巷双钩提升装备错码信号；3.弯道、井底车场、其他人员密集的地点、顶车作业区装备有声光预警信号装置，关键部位道岔装备有道岔位置指示器；4.各乘人地点悬挂有明显的停车位置指示牌； 5.斜巷车场悬挂最大提升车辆数及最大提升载荷数的明确标识；6.无轨胶轮车运输巷道各岔口、错车点、弯道、车场等处设有行车指示等安全标志和信号；7.有轨运输与无轨运输交叉处、有轨运输行人通行处等危险路段设置有限速和警示装置', '查现场。未按规定装设1处扣1分，装设但不符合要求1处扣0.5分', '对照”基本要求“逐项进行现场检查', NULL, '未按规定装设1处扣1分，装设但不符合要求1处扣0.5分', 10, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (284, 1, 236, '运输/运输安全设施/物料捆绑/连接装置', '8.4.3(1)', '1.捆绑固定牢固可靠，有防跑防滑措施\n2.保险链（绳）、连接环（链）、连接杆、插销、滑头及其连接方式符合规定', '查现场。1处不符合要求不得分', '1.现场检查：车辆运送松散物料时是否捆绑；2.查现场，看连接装置及连接方式是否符合《煤矿安全规程》第四百一十六条规定', '物料捆绑封车规定', '1.出不符合要求，扣5分', 5, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (285, 1, 238, '运输/运输管理/组织保障/制度保障', '8.5.1(1)', '1.有负责运输管理工作的机构\n2.包含以下内容：1.岗位安全生产责任制度2.运输设备运行、检修、检测等管理规定；3.运输安全设施检查、试验等管理规定；4.轨道线路检查、维修等管理规定；5.辅助运输安全事故汇报管理规定等', '1.查资料。不符合要求1处扣2分\n2.查资料。每缺1种或每1处不符合要求扣0.5分', '1.查询运输管理工作的机构成立的文件以及相关人员的任职文件；2.①检查是否有“基本要求”中5项制度和规定；②检查岗位安全生产制度是否覆盖各级运输管理人员、岗位工种；③检查运输设备运行、检修、检测等管理规定是否包括本矿各种运输设备和设施(电机车、柴油机窄轨机车、斜巷人车、平巷人车、架空乘人装置\n运输绞车、绳牵引连续运输车、卡轨车、无轨胶轮机车、单轨吊、齿轮车、齿卡轨车、连接装置、挡车装置、各种信号装置、钢丝绳、小型电器等)验收、检查维护、大修报废、运行和使用管理办法；④检查运输安全设施检查、试验等管理规定是否包括本矿有关运输设备和设施(电机车、柴油机窄轨机车、斜巷人车、架\n空乘人装置、无轨胶轮机车、单轨吊、连接装置)试验管理或年审办法；⑤检查线路检查、维修等管理规定是否包括本矿各种线路(提升系统、运输系统、通信系统、轨道线路、牵引网路、单轨吊线路、无轨胶轮车道路等)验收、检查、维护、运行和管理办法；⑥检查辅助运输安全事故汇报管理规定，是否包括事故责任追究办法、乘人管理办法、行车不行人管理办法、捆绑封车管理规定、特殊物料运送管理规定、综采设备支架运送管理办法等；⑦检查上述各项制度是否符合现场实际，是否以正式文件或加盖矿公章印发', '1.运输管理工作的机构成立的文件，人员任职文件；2.①运输管理人员、各岗位工种安全生产责任制；②运输各项管理制度、规定汇编', '1.无管理人员、无管理机构文件，扣2分；2.①5项制度和规定每缺1项扣1分；未正式印发或加盖公章，每项扣0.5分；②内容不符合要求每项扣0.5分；单项扣完为止', 3, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (286, 1, 239, '运输/运输管理/技术资料/检测检验', '8.5.2(1)', '1.技术资料\n(1).有运输设备、设施、线路的图纸、技术档案、维修记录；(2).施工作业规程、技术措施符合有关规定；(3).运输系统、设备选型和能力计算资料齐全\n2.检测检验\n(1).更新或大修及使用中的斜巷（井）人车，有完整的重载全速脱钩测试报告及连接装置的探伤报告\n(2).新投用机车应测定制动距离，之后每年测定1次，有完整的制动距离测试报告；(3).斜巷提升连接装置每年进行1次2倍于其最大静荷重的拉力试验，有完整的拉力试验报告；(4).架空乘人装置、单轨吊车、无轨胶轮车、齿轨车、卡轨车、无极绳连续牵引车等按《煤矿安全规程》或相关规范要求进行检测、检验、试验，有完整的检测、检验、试验报告', '1.查资料。每缺1种或每1处不符合要求扣0.5分\n2.(1).查资料。不符合要求扣7分 \n3.(2-4)查资料。不符合要求1处扣1分', '1.①检查运输设备设施、线路技术档案、图纸、维修记录是否齐全完整；②查工程施工是否有作业规程、安全技术措施,内容是否齐全完整,是否有针对性、符合有关规定;③检查运输系统、设备选型设计和能力计算资料是否齐全完整；2.根据现场设备、设施情况或运输管理台账,查验是否有斜井人车安全性能检验报告、连接装置的探伤报告及检测机构资质；3.①查是否有按照《窄轨列车制动距离试验细则》的试验方法、步骤和记录表进行制动试验的完整试验措施、记录和试验报告，测试周期是否符合要求；②查是否每年有由具有检验资质的单位按照《煤矿窄轨车辆连接件连接链》(MT244.1-2005)和《煤矿窄轨车辆连接件连接插销》(MT244.2—2005)进行定期试验的完整检测记录和拉力试验报告；③查是否有架空乘人装置的年度检测记录和检验报告；④查是否有单轨吊车的年度检测记录和检验报告；⑤查是否有无轨胶轮车年度检测报告；是否有无轨胶轮车司机进行年审的报告(包括培训、考试、考核和体检等工作);是否有按规定对无轨胶轮车进行大修后的检验报告;⑥查是否有齿轨机车按照《煤矿用防爆柴油机胶套轮/齿轨卡轨车技术条件》(MT/T 588-1996)和《煤矿用防爆柴油机钢轮/齿轨机车及齿轨装置》(MT/T 589-1996)要\n求进行的检测、检验;⑦查是否有无极绳连续牵引车按照《无极绳连续牵引车》(MT/T988-2006)要求进行的检测、检验;⑧查测试报告测试项目是否完整、准确可靠,有结论意见', '1①运输系统图；②运输设备、设施管理台账、技术档案、图纸资料；③运输设备设施大中修日常维修计划及相关记录;④各种运输设备选型和能力计算资料；⑤运输施工作业规程和技术措施资料；2.斜井人员年度安全性能检验报告及连接装置的探伤报告；3.①电机车制动距离试验报告及年审报告；②连接链和连接插销的拉力试验报告；③架空乘人装置的年度检验报告;④单轨吊车的年度检验报告；⑤无轨胶轮车年度检测报告,无轨胶轮车司机进行年审报告,无轨胶轮车大修后的检验报告；⑥齿轨机车、无极绳连续牵引车的检测检修记录；⑦其他机车日常检修测试记录', '1.每缺1种或每1处不符合要求扣0.5分；2.缺少斜巷人车的检测检验报告、连接装置探伤报告，或检测机构缺少资质或资质不符合要求，扣7分；3.不符合要求1处扣1分', 7, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (287, 1, 241, '运输/岗位规范/专业技能/规范作业', '8.6.1(1)', '1.专业技能\n(1).管理和技术人员掌握相关的规程、规范等有关内容；(2).作业人员掌握本岗位操作规程、安全措施\n2.规范作业\n（1）.严格执行岗位安全生产责任制；（2）.无“三违”行为；（3）.作业前进行安全确认', '1.查资料和现场。不符合要求1处扣0.5分\n2.查现场。发现1人“三违”扣5分；其他不符合要求1处扣1分。', '1.①查资料，是否建立岗位操作规程等；②现场提问，随机抽考1名管理或技术人员回答3个问题，如果都回答不正确，视为不符合要求；随机抽取1名关键岗位人员回答1个最关键应知应会问题，如果回答不正确，视为不符合要求；2.①查现场实操，是否执行岗位责任制，是否进行安全确认；②现场检查有无“三违”行为', '岗位操作规程', '1.不符合要求1处扣0.5分；2.①发现1人次“三违”，扣5分；②1人次未进行安全确认或行为不规范扣1分', 5, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (288, 1, 243, '运输/文明生产/作业场所', '8.7.1(1)', '1.运输线路、设备硐室、车间等卫生整洁，设备清洁，材料分类、集中码放整齐\n2.主要运输线路水沟畅通，巷道无淤泥、积水。水沟侧作为人行道时，盖板齐全、稳固', '查现场。不符合要求1处扣0.2分', '现场检查运输线路、设备硐室卫生及设备材料码放情况；现场检查水沟是否畅通，巷道是否有淤泥、积水；水沟侧作为人行道时，盖板是否齐全、稳固', NULL, '不符合要求1处扣0.2分', 3, NULL, 1, 23, 0, 1);
INSERT INTO `stdchk_item` VALUES (289, 1, 246, '职业卫生/职业卫生管理/组织保障', '9.1.1(1)', '建有职业病危害防治领导机构；有负责职业病危害防治管理的机构，配备专职职业卫生管理人员', '查资料。无领导机构、管理机构不得分；无专职人员扣1分', '1.查领导机构、管理机构、专职管理人员设置文件,是否有2机构设置的正式文件；2.查是否配有专职的职业卫生管理人员，不对人数做具体要求', '1.两机构成立的正式文件;2.“两机构一人员”的分工及职责；3.人事变动后人员变更的调整通知文件4.专职职业卫生管理人员的资格证书及相关聘任文件或证书复印件', '1.两机构设置没有正式文件,扣2分；2.没有专职人员扣1分；3.其他问题一般扣0.5分', 2, NULL, 1, 1903, 0, 1);
INSERT INTO `stdchk_item` VALUES (290, 1, 247, '职业卫生/职业卫生管理/责任落实', '9.1.2(1)', '明确煤矿主要负责人为煤矿职业危害防治工作的第一责任人；明确职业病危害防治领导机构、负责职业病危害防治管理的机构和人员的职责', '查资料。未明确第一责任人或未明确领导机构、管理机构及人员职责不得分；机构没有正常开展工作扣1分', '1.查文件,明确矿主要负责人是否为职业危害防治工作第一责任人；2.查文件,领导机构、管理机构职责是否明确,两机构的各级人员职责是否明确;3.查机构职责及人员职责是否合理、具有可操作性', '1.主要负责人的责任文件和职责；2.领导机构的职业病危害防治职责；3.各部门的职业病危害防治职责；4.各级人员的职业病危害防治职责；5.其他相关的职业病危害防治职责', '1.未明确主要负责人为职业危害防治工作第一责任人,扣2分；2.没有领导机构职责、管理机构、机构人员职责，扣2分；3.机构及人员职责不合理,扣0.5分；4.机构未开展相应工作扣1分,开展工作但不完善1处扣0.1分,扣到0.5分为止', 2, NULL, 1, 1903, 0, 1);
INSERT INTO `stdchk_item` VALUES (291, 1, 248, '职业卫生/职业卫生管理/制度完善', '9.1.3(1)', '按规定建立完善职业病危害防治相关制度，主要包括：职业病危害防治责任制度、职业病危害警示与告知制度、职业病危害项目申报制度、职业病防护设施管理制度、职业病个体防护用品管理制度、职业病危害日常监测及检测、评价管理制度、建设项目职业卫生“三同时”制度、劳动者职业健康监护及其档案管理制度、职业病诊断、鉴定及报告制度、职业病危害防治经费保障及使用管理制度、职业卫生档案管理制度、职业病危害事故应急管理制度及法律、法规、规章规定的其他职业病危害防治制度', '查资料。未建立制度不得分；制度不全，每缺1项扣1分；制度内容不符合要求或未能及时修订 1项扣0.5分', '1.查煤矿是否有职业病危害防治相关制度,是否以正式文件下发；2.查制度内容是否符合国家相关法律(《煤矿作业场所职业病危害防治规定》《职业病防治法》),是否\n与国家最新的法律法规要求相符合', '1.职业病危害防治相关制度下发的文件；2.职业病危害防治相关制度会审记录', '1.未建立制度,扣2分；2.每缺1项制度,扣1分；3.制度内容不符合要求或未能及时修订,1项扣0.5分', 2, NULL, 1, 1903, 0, 1);
INSERT INTO `stdchk_item` VALUES (292, 1, 249, '职业卫生/职业卫生管理/经费保障', '9.1.4(1)', '职业病危害防治专项经费满足工作需要', '查资料。未提取经费或经费不能满足需要不得分', '1.年初查年度生产经营经费计划里安全措施经费提取是否满足职业病防治专项经费需要,经费是否批准;年底查是否有因为经费问题导致影响职业病防治的情况;2.检查专项经费列支是否满足工作需要,专项费用是否按以下费用列支:①职业病危害因素的预防和治理;②职业病危害防护设施配置和维护；③个人防护用品配备与维护;④职业病危害因素日常监测、检测与评价;⑤职业健康监护;⑥职业病诊断鉴定及治疗康复；⑦职业卫生宣传教育与培训;⑧职业危害警示标示配置;⑨工伤保险支出;⑩其他与煤矿安全生产职业病危害直接相关的支出', '1.年度职业病防治专项经费提取计划文件；2.职业病防治专项经费提取计划审批表；3.职业病防治专项经费提取明细；4.专项经费对应费用发票复印件及相关凭证', '未提取经费或经费不能满足要求不得分', 1, NULL, 1, 1903, 0, 1);
INSERT INTO `stdchk_item` VALUES (293, 1, 250, '职业卫生/职业卫生管理/工作计划', '9.1.5(1)', '有职业病危害防治规划、年度计划和实施方案；年度计划应包括目标、指标、进度安排、保障措施、考核评价方法等内容。实施方案应包括时间、进度、实施步骤、技术要求、考核内容、验收方法等内容', '查资料。无规划不得分，无年度计划、实施方案扣1分；相关要素不全的，每缺1项扣0.5分', '1.查职业病危害防治规划、年度计划和实施方案的正式文件，并查文件审批程序是否符合要求；2.查年度计划内容，目标、指标、进度安排、保障措施、考核评价方法等是否有缺项；3.查实施方案内容，时间、进度、实施步骤、技术要求、考核内容、验收方法等是否有缺项', '1.制定职业病危害防治规划正式文件；2.制定职业病危害防治年度计划和实施方案正式文件', '1.无规划或规划未经审批并以正式文件下发不得分,无年度计划、实施方案或未经审批并以正式文件下发,扣1分;2.年度计划、实施方案内容缺1项扣0.5分;3.年度计划、实施方案与相关法规及规划违背的1处扣0.5分,扣到1分为止', 2, NULL, 1, 1903, 0, 1);
INSERT INTO `stdchk_item` VALUES (294, 1, 251, '职业卫生/职业卫生管理/档案管理', '9.1.6(1)', '分年度建立职业卫生档案，内容包括作业场所职业病危害因素种类清单、岗位分布以及作业人员接触情况等资料，职业病防护设施基本信息及其配置、使用、维护、检修与更换等记录，作业场所职业病危害因素检测、评价报告与记录，职业病个体防护用品配备、发放、维护与更换等记录，煤矿主要负责人、职业卫生管理人员和劳动者的职业卫生培训资料，职业病危害事故报告与应急处置记录，劳动者职业健康检查结果汇总资料，存在职业禁忌证、职业健康损害或者职业病的劳动者处理和安置情况记录，职业病危害项目申报情况记录，其他有关职业卫生管理的资料或者文件', '查资料。未建立档案不得分；档案缺项，每缺1项扣1分', '1.查是否分年度建立职业卫生档案,至少分年度保存近3年职业卫生档案;2.查每年度档案内容是否齐全；3.查档案中各项资料是否符合相关法律法规要求', '年度职业卫生档案', '1.未建立档案不得分；2.建立档案但未分年度扣0.5分;近3年的档案，缺1年扣0.5分；3.档案缺项,每缺1项扣1分；4.档案内容不真实、不完善,1处扣到0.2分,扣到0.4分为止', 3, NULL, 1, 1903, 0, 1);
INSERT INTO `stdchk_item` VALUES (295, 1, 252, '职业卫生/职业卫生管理/危害告知', '9.1.7(1)', '与劳动者订立或者变更劳动合同时，应将作业过程中可能产生的职业病危害及其后果、防护措施和相关待遇等如实告知劳动者，并在劳动合同中载明', '查资料，抽查10份劳动合同。未全部进行告知不得分', '1.抽查10份劳动合同,是否所有员工都有职业病危害告知书并签字;2.查看告知书是否规范,内容是否有岗位针对性；3.检查职业危害告知书内容是否完整，应包括作业过程中可能产生的职业病危害及其后果、防护措施和相关待遇等', '1.全体员工名单；2.抽查的10位员工的劳动合同', '1.抽查10份劳动合同，未全部进行告知,扣1分；2.告知书没有岗位针对性,不齐全,缺1个工种扣0.1分,扣到0.5分为止', 1, NULL, 1, 1903, 0, 1);
INSERT INTO `stdchk_item` VALUES (296, 1, 253, '职业卫生/职业卫生管理/工伤保险', '9.1.8(1)', '为存在劳动关系的劳动者（含劳务派遣工）足额缴纳工伤保险', '查资料。未全部参加工伤保险不得分', '查工伤缴费凭证或复印件,是否足额缴纳', '1.职工工伤保险缴纳凭证(或台账)；2.职工名单或工资单', '未全部参加工伤保险，扣1分', 1, NULL, 1, 1903, 0, 1);
INSERT INTO `stdchk_item` VALUES (297, 1, 254, '职业卫生/职业卫生管理/检测评价', '9.1.9(1)', '每年进行一次作业场所职业病危害因素检测，每3年进行一次职业病危害现状评价；根据检测、评价结果，制定整改措施；检测、评价结果向煤矿安全监察机构报告', '查资料。未按周期检测评价不得分；其他1处不符合要求扣1分', '1.检查是否进行定期职业病危害因素检测,评价单位资质是否符合要求；2.检查是否进行职业病危害现状评价，评价单位资质是否符合要求；3.检查检测评价报告中超标项是否有进行整改并保存整改资料;4.检查检测、评价结果是否向煤矿安全监察机构报告', '1.定期职业病危害因素检测报告(每年扣1次)；2.职业病危害现状评价报告(每3年1次);3.检测、评价结果向煤矿安全监察机构报告的回执(或其他有效记录)；4.整改措施台账', '1.未按周期检测评价，扣3分;评价单位资质不符合要求1次扣1分；2.检测、评价结果中超标项未制定整改措施,扣1分；3.检测、评价结果3个月内没有向煤矿安全监察机构报告的,扣1分；4.其他1处不符合要求扣1分', 3, NULL, 1, 1903, 0, 1);
INSERT INTO `stdchk_item` VALUES (298, 1, 255, '职业卫生/职业卫生管理/个体防护', '9.1.10(1)', '按照《煤矿职业卫生个体防护用品配备标准》（AQ1051）为劳动者（含劳务派遣工）发放符合要求的个体防护用品，做好记录，并指导和督促劳动者正确使用,严格执行劳动防护用品过期销毁制度', '查现场和资料。现场抽查4个岗位，每个岗位抽查1人，未按照AQ1051发放个体防护用品的，每缺1项扣1分， 1人1项用品不符合要求扣0.5分，每发现1人未使用个体防护用品扣0.5分；无个体防护用品发放登记记录扣2分，记录不完整、不清楚的，扣0.5分', '1.检查劳保用品发放是否按岗位划分符合AQ1051标准要求；2.查发放台账,是否根据岗位细化劳保用品发放；3.查是否制定劳动账防护用品过期销毁制度(主要针对应急救援的滤毒灌等防毒用品）；4.查是否建立劳动防护用品过期销毁记录与台账；5.查劳动防护用品性能是否达到标准，是否有产品说明书、检验报告和合格证;6.通过现场描述抽查是否会用防护用品', '1.劳动用品发放台账、发放记录；2.劳动防护用品说明书、检验报告；3.指导督促劳动者使用防护用品的相关材料(培训学习档案等)；4.劳动防护用品过期销毁记录与台账', '1.现场抽查4个岗位，每个岗位抽查1人,未按照AQ1051发放个体防护用品,每缺1项扣1分；1人1项用品不符合要求扣0.5分；2.作业现场每发现1人未使用个体防护用品,扣0.5分;每发现1人不会用防护用品,扣0.5分；3.无个体防护用品发放登记记录扣2分;记录不完整、不清楚的,扣0.5分', 4, NULL, 1, 1903, 0, 1);
INSERT INTO `stdchk_item` VALUES (299, 1, 256, '职业卫生/职业卫生管理/公告警示', '9.1.11(1)', '在醒目位置设置公告栏，公布工作场所职业病危害因素检测结果。对产生严重职业病危害的作业岗位，应在其醒目位置设置警示标识和警示说明，载明产生职业病危害的种类、后果、预防以及应急救援措施等内容', '查现场。未按规定公布检测结果不得分，公告栏公布不全，每缺1项扣1分，警示标识和警示说明缺失、内容不全1处扣0.5分', '1.检查定期职业病危害因素检测结果是否在醒目位置设置公告栏公示,根据《用人单位职业病危害告知与警示标识管理规范》(安监总厅安健[2014]111号)检查公\n告栏内容是否全面;2.查在粉尘、噪声、毒害、热害4个方面产生危害较大的位置(如采掘工作面、地面煤仓、储煤场,主要通风机房,老空水巷充电硎室等位置),检查作业地点职业危害警示标识和警示告知卡牌板是否规范制作、悬挂;警示标识和警示说明是否醒目、悬挂是否合适;3.查警示告知卡牌板内容是否缺项,应包括:职业病危害的种类、后果、预防以及应急救援措施', NULL, '1.未按规定公布检测结果,扣3分；2.公告栏公布内容不全，每缺1项扣1分；3.警示标识和警示说明缺失、内容不全,1处扣0.5分', 3, NULL, 1, 1903, 0, 1);
INSERT INTO `stdchk_item` VALUES (300, 1, 258, '职业卫生/职业病危害/监测人员', '9.2.1(1)', '配备职业病危害因素监测人员；监测人员经培训合格后上岗作业', '查资料和现场。未配备人员或未经培训合格不得分', '1.检查监测人员培训证书是否配备监测人员,证书是否在有效期内;2.职业病危害因素监测人员也可以由瓦斯检查工或矿井测风工、测尘工兼任,检查是否有合格证;3.下井检查监测人员设备操作是否正确', '1.职业病危害因素监测人员资格证书；2.监测人员工作内容的相关文件等', '1.未配备人员或无培训合格证,扣2分；2.现场监测人员操作不规范,扣1分', 2, NULL, 1, 1903, 0, 1);
INSERT INTO `stdchk_item` VALUES (301, 1, 259, '职业卫生/职业病危害/粉尘', '9.2.2(1)', '1.按规定配备2台（含）以上粉尘采样器或直读式粉尘浓度测定仪等粉尘浓度测定设备', '查资料和现场。无设备不得分；配备监测仪器不足扣1分', '1.检查粉尘采样器或粉尘浓度测定仪是否配备,数量是否达到要求;2.检查粉尘采样器或粉尘浓度测定仪是否定期年检', '粉尘采样器或粉尘浓度测定仪产品购买发票或入库单', '1.无设备扣2分；2.配备监测仪器不足，扣1分;仪器损坏不能正常使用的,1台扣1分；3.仪器未按规定检定,1次扣0.5分', 2, NULL, 1, 1903, 0, 1);
INSERT INTO `stdchk_item` VALUES (302, 2, 259, '职业卫生/职业病危害/粉尘', '9.2.2(2)', '2.采煤工作面回风巷、掘进工作面回风流设置有粉尘浓度传感器，并接入安全监控系统', '查资料和现场。粉尘浓度传感器设置不符合要求1处扣1分，未接入安全监控系统扣1分', '1.检查各采掘工作面是否设置粉尘传感器,悬挂位置是否符合规定；2.检查监控终端,粉尘传感器数据是否实现在线监测', '1.粉尘浓度传感器安设布置图;2.粉尘传感器台合账、数据记录和监控数据', '1.采掘工作面未设置粉尘浓度传感器或设置不符合要求,1处扣1分;2.未接入安全监控系统，扣1分', 2, NULL, 1, 1903, 0, 1);
INSERT INTO `stdchk_item` VALUES (303, 3, 259, '职业卫生/职业病危害/粉尘', '9.2.2(3)', '3.粉尘监测地点布置符合规定', '查资料。监测地点不符合要求1处扣1分', '对照现场检查粉尘监测地点布置图或者计划表等文件,是否符合《煤矿安全规程》第六百四十三条规定', '1.粉尘监测地点布置文件；2.粉尘监测地点布置图', '监测地点不符合要求或者缺少监测点,1处扣1分', 5, NULL, 1, 1903, 0, 1);
INSERT INTO `stdchk_item` VALUES (304, 4, 259, '职业卫生/职业病危害/粉尘', '9.2.2(4)', '4.粉尘监测周期符合规定，总粉尘浓度井工煤矿每月测定2次、露天煤矿每月测定1次或采用实时在线监测；粉尘分散度每6个月测定1次或采用实时在线监测；呼吸性粉尘浓度每月测定1次；粉尘中游离二氧化硅含量每6个月测定1次,在变更工作面时也须测定1次；开采深度大于200m 的露天煤矿,在气压较低的季节应当适当增加测定次数', '查资料。总粉尘、呼吸性粉尘浓度监测周期不符合要求且未采用实时在线监测不得分；其他监测周期不符合要求1处扣1分', '1.检查近1年粉尘测定报表,粉尘监测周期是否符合规定；2.检查粉尘分散度、粉尘中游离的二氧化硅含量检测报告,看周期是否符合要求;3.查在变更工作面时粉尘中游离二氧化硅含量是否进行测定', '1.粉尘监测报表；2.粉尘在线监测数据；3.粉尘分散度、粉尘中游离的二氧化硅含量检测报告', '1.总粉尘、呼吸性粉尘浓度监测周期不符合要求且未采用实时在线监测，扣3分；2.其他监测周期不符合要求,1处扣1分', 3, NULL, 1, 1903, 0, 1);
INSERT INTO `stdchk_item` VALUES (305, 5, 259, '职业卫生/职业病危害/粉尘', '9.2.2(5)', '5.采用定点监测、个体监测方法对粉尘进行监测', '查资料和现场。不符合要求不得分', '查台账,查现场，是否采用定点、个体监测方法对粉尘进行监测', '1.定点监测原始记录；2.个体监测原始记录', '不符合要求,扣1分', 1, NULL, 1, 1903, 0, 1);
INSERT INTO `stdchk_item` VALUES (306, 6, 259, '职业卫生/职业病危害/粉尘', '9.2.2(6)', '6.粉尘浓度不超过规定：粉尘短时间定点监测结果不超过时间加权平均容许浓度的2倍；粉尘定点长时间监测、个体工班监测结果不超过时间加权平均容许浓度', '查资料和现场。无监测数据,不得分；浓度每超标1项扣1分', '1.检查粉尘测定报表、粉尘浓度传感器监测记录、当年的年度检测(评价)报告中是否有不合格点；2.现场测试15min检查监测器数据,并与历史台账对照,查浓度是否超过规定', '1.粉尘测定报表；2.粉尘传感器监测记录；3.当年的年度检测(评价)报告', '1.无监测数据,扣10分；2.浓度每超标1项,扣1分', 10, NULL, 1, 1903, 0, 1);
INSERT INTO `stdchk_item` VALUES (307, 1, 260, '职业卫生/职业病危害/噪声', '9.2.3(1)', '1.按规定配备有2台（含）以上噪声测定仪器，作业场所噪声至少每6个月监测1次', '查资料和现场。测定仪器每缺1台扣1分；监测周期不符合要求扣1分', '1.查是否配备噪声测定仪器,数量是否符合要求；2.查噪声监测周期是否符合要求；3.查噪声测定仪器是否超检验周期,是否有校验报告', '1.噪声测定仪器的购买发票或者入库单等；2.噪声测定仪器校验报告；3.噪声监测记录', '1.测定仪器每缺1台，扣1分;仪器没有检验，1台扣0.5分；2.监测周期不符合要求，扣1分；3.机器损坏不能正常使用,1台扣1分', 2, NULL, 1, 1903, 0, 1);
INSERT INTO `stdchk_item` VALUES (308, 2, 260, '职业卫生/职业病危害/噪声', '9.2.3(2)', '2.噪声监测地点布置符合规定', '查资料。不符合要求1处扣0.5分', '现场检查噪声监测地点是否符合《煤矿安全规程》第六百五十八条规定', '1.噪声监测地点布置图(或计划、文件)；2.噪声监测记录符', '1.没有噪声监测地点布置,扣0.5分；2.噪声监测地点布置不符合要求,1处扣0.5分', 4, NULL, 1, 1903, 0, 1);
INSERT INTO `stdchk_item` VALUES (309, 3, 260, '职业卫生/职业病危害/噪声', '9.2.3(3)', '3.劳动者接触噪声8h或40h等效声级不超过85dB(A)', '查资料和现场。无监测数据不得分，声级超过规定发现1处扣1分', '1.查作业地点噪声监测数据台账,噪声强度应按照劳动者实际接触时间和定点监测强度进行计算,工班个体监测数据可直接应用；2.现场测定3次,取平均值,检查监测地点噪声声级是否超标', '1.各作业地点噪声监测数据台账；2.劳动者接触噪声时间统计表和等效声级计算表', '1.无监测数据,扣4分；2.声级超过规定,发现1处扣1分', 4, NULL, 1, 1903, 0, 1);
INSERT INTO `stdchk_item` VALUES (310, 1, 261, '职业卫生/职业病危害/高温', '9.2.4(1)', '采掘工作面回风流和机电设备硐室设置温度传感器；采掘工作面空气温度超过26℃、机电设备硐室超过30℃时,缩短超温地点工作人员的工作时间,并给予高温保健待遇；采掘工作面的空气温度超过30℃、机电设备硐室超过34℃时停止作业；有热害的井工煤矿应当采取通风等非机械制冷降温措施，无法达到环境温度要求时,采用机械制冷降温措施。', '查资料和现场。1处不符合要求不得分。', '1.检查各采掘工作面回风流和机电设备硐室是否设置温度传感器；2.查温度传感器是否按标准悬挂；3.查各地点温度是否超标；4.查温度超标地点是否停止作业或采取降温措施、给与高温作业地点人员相关待遇；5.检查有热害的井工煤矿是否采取相应降温措施', '1.温度监测记录；2.温度传感器布置图；3.监测数据和人工测定数据原始记录资料;4.温度传感器超温报警记录；5.高温作业地点的管理制度,工作人员的待遇执行记录', '1处不符合要求,扣3分', 3, NULL, 1, 1903, 0, 1);
INSERT INTO `stdchk_item` VALUES (311, 1, 262, '职业卫生/职业病危害/化学毒物', '9.2.5(1)', '对作业环境中氧化氮、一氧化碳、二氧化硫浓度每3个月至少监测1次，对硫化氢浓度每月至少监测1次；化学毒物等职业病危害因素浓度/强度符合规定', '查资料和现场。未进行监测或危害因素浓度/强度超过规定不得分；监测项目不全，每缺1项扣1分；监测周期不符合要求1项扣1分', '1.查台账与现场带仪器检查结合；2.检查监测数据是否建立台账；3.查化学毒物监测周期是否符合《煤矿安全规程》规定；4.检查作业场所空气中化学毒物浓度和强度是否符合要求', '1.作业环境中化学毒物的监测台账;2.现场监测数据资', '1.未进行监测,扣4分；2.危害因素浓度/强度超过规定,扣4分；3.监测项目不全,每缺1项扣1分；4.监测周期不符合要求，1项扣1分', 4, NULL, 1, 1903, 0, 1);
INSERT INTO `stdchk_item` VALUES (312, 1, 264, '职业卫生/职业健康防护/上岗前检查', '9.3.1(1)', '组织新录用人员和转岗人员进行上岗前职业健康检查，检查机构具备职业健康检查资质，形成职业健康检查评价报告；不安排未经上岗前职业健康检查和有职业禁忌证的劳动者从事接触职业病危害的作业', '查资料。未安排检查或者检查机构无资质、无职业健康检查评价报告、检查项目不符合规定不得分；其他1人不符合要求扣1分', '1.检查岗前是否有职业健康检查评价报告；2.检查体检机构是否具有职业健康检查资质；3.查体检报告结果是否有职业禁忌证人员；4.查劳动合同等,是否有职业禁忌证人员从事接触职业病危害的作业；5.检查体检项目是否符合规定《职业健康监护技术规范》(GBZ188-2014)', '1.新录用人员和转岗人员上岗前职业健康检查报告;2.检体机构的职业健康检查资质证明;3.人员体检报告；4.有职业禁忌证的劳动者的劳动合同', '1.未安排检查或者检查机构无资质、无职业健康检查评价报告、检查项目不符合规定的,扣3分；2.其他1人不符合要求，扣1分', 3, NULL, 1, 1903, 0, 1);
INSERT INTO `stdchk_item` VALUES (313, 1, 265, '职业卫生/职业健康防护/在岗期间检查', '9.3.2(1)', '按规定周期组织在岗人员进行职业健康检查，检查机构具备职业健康检查资质，形成职业健康检查评价报告；发现与所从事的职业相关的健康损害的劳动者，调离原工作岗位并妥善安置', '查资料。未安排检查、周期不符合规定或者检查机构无资质、无职业健康检查评价报告、检查项目不符合规定不得分；健康损害劳动者未调离、安置的，发现1人扣1分', '1.查看在岗人员体检评价报告,检查体检机构是否具有职业健康检查资质；2.查体检报告结果是否有从事职业相关的健康损害的劳动者；查劳动合同等,是否对其调离原岗位并妥善安置；3.查在职人员体检档案,在岗期间职业健康检查周期是否符合《煤矿作业规程》的要求,健康检查项目是否符合《职业健康监护技术规范》(GBZ188-2014)的要求', '1.在岗人员体检评价报告；2.检体机构的职业健康检查资质证明；3.在岗期间人员体检报告;4.职业健康检查规案；5.从事职业相关的健康损害劳动者的劳动合同', '1.未安排检查、周期不符合规定或者检查机构无资质、无职业健康检查评价报告、检查项目不符合规定的,扣3分；2.健康损害劳动者未调离、安置的,发现1人扣1分', 3, NULL, 1, 1903, 0, 1);
INSERT INTO `stdchk_item` VALUES (314, 1, 266, '职业卫生/职业健康防护/离岗检查', '9.3.3(1)', '准备调离或脱离作业及岗位人员组织进行离岗职业健康检查，检查机构具备职业健康检查资质，形成职业健康检查评价报告', '查资料。未安排检查或者检查机构无资质的、无报告的、检查项目不符合规定的，不得分；离岗检查有遗漏的，发现1人扣1分', '1.检查是否有离岗体检评价报告,体检机构是否具有职业健康检查资质；2.离岗前3个月内进行的职业健康检查均可视为离岗职业健康检查,健康检查项目是否符合《职业健康监护技术规范》(GBZ188-2014)的要求', '1.离岗体检报告；2.检体机构的职业健康检查资质;3.职业健康检查评价报告', '1.未安排检查或者检查机构无资质、无报告、检查项目不符合规定的,扣3分；2.离岗检查有遗漏的，发现1人扣1分', 3, NULL, 1, 1903, 0, 1);
INSERT INTO `stdchk_item` VALUES (315, 1, 267, '职业卫生/职业健康防护/应急检查', '9.3.4(1)', '对遭受或可能遭受急性职业病危害的劳动者进行健康检查和医学观察', '查资料。未对劳动者进行健康检查和医学观察的不得分', '查看检查、观察档案及健康检查报告是否对遭受或可能遭受急性职业病危害的劳动者进行健康检查和医学观察', '对遭受或可能遭受急性职业病危害的劳动者进行健康检查和医学观察的档案', '未对遭受或可能遭受急性职业病危害的劳动者进行健康检查和医学观察的不得分', 2, NULL, 1, 1903, 0, 1);
INSERT INTO `stdchk_item` VALUES (316, 1, 268, '职业卫生/职业健康防护/结果告知', '9.3.5(1)', '按规定将职业健康检查结果书面告知劳动者', '查资料。未将职业健康检查结果书面告知劳动者不得分；每遗漏1人扣0.5分', '检查矿方是否将体检结果书面告知劳动者', '体检结果告知书', '1.未将职业健康检查结果书面告知劳动者,扣3分；2.每遗漏1人扣0.5分', 3, NULL, 1, 1903, 0, 1);
INSERT INTO `stdchk_item` VALUES (317, 1, 269, '职业卫生/职业健康防护/监护档案', '9.3.6(1)', '建立劳动者个人职业健康监护档案，并按照有关规定的期限妥善保存；档案包括劳动者个人基本情况、劳动者职业史和职业病危害接触史、历次职业健康检查结果及处理情况、职业病诊疗等资料；劳动者离开时应如实、无偿为劳动者提供职业健康监护档案复印件并签章', '查资料。未建立档案或未按要求向劳动者提供复印件不得分；档案内容不全，每缺1项扣1分；未指定人员负责保管扣1分', '1.检查是否建立劳动者个人职业健康监护档案,并按照有关规定的期限妥善保存；2.查是否有指定人员负责保管的文件、合同等；3.查通过存档的向劳动者提供复印件并签章的登记表格,是否如实、无偿为劳动者提供职业健康监护档案复印件,劳动者是否签章；4.抽查不同工种的个人健康监护档案，内容是否齐全', '1.劳动者个人职业健康监护档案；2.档案保管制度；3.存档的向劳动者提供复印件并签章的表格；4.设专职人员负责保管档案的文件', '1.未建立档案或未按要求向劳动者提供复印件，扣4分；2.档案内容不全,每缺1项扣1分；3.未指定人员负责保管扣1分', 4, NULL, 1, 1903, 0, 1);
INSERT INTO `stdchk_item` VALUES (318, 1, 271, '职业卫生/职业病诊断鉴定/职业病诊断', '9.4.1(1)', '安排劳动者、疑似职业病病人进行职业病诊断', '查资料和现场。走访询问不少于10名职业病危害严重的重点岗位的劳动者，有1人提出职业病诊断申请而被无理由拒绝或未安排疑似职业病病人进行职业病诊断不得分', '1.查访或电话询问不少于10名职业危害严重的重点岗位的劳动者；2.检查是否有人员提出职业病诊断申请被拒绝情况', '1.劳动者、疑似职业病人进行职业病诊断的资料；2.劳动者个人职业健康监护档案;3.劳动者、疑似职业病病人提出职业病诊断的申请', '有1人提出职业病诊断申请而被无理由拒绝或未安排疑似职业病病人进行职业病诊断,不得分', 3, NULL, 1, 1903, 0, 1);
INSERT INTO `stdchk_item` VALUES (319, 1, 272, '职业卫生/职业病诊断鉴定/职业病病人待遇', '9.4.2(1)', '保障职业病病人依法享受国家规定的职业病待遇', '查资料和现场。走访询问不少于5名职业病患者，有1人未保证职业病待遇不得分', '1.查访或电话询问不少于5名患者；2.查确诊职业病病人是否执行工伤赔付，职业病病人是否依法享受国家规定的工伤补偿、免费医疗、住院伙食护理补贴等职业病待遇', '1.职业病病人确诊证明材料；2.职业病病人工伤赔付资料；3.职业病病人待遇发放台账', '有1人未保证职业病待遇,不得分', 2, NULL, 1, 1903, 0, 1);
INSERT INTO `stdchk_item` VALUES (320, 1, 273, '职业卫生/职业病诊断鉴定/治疗、定期检查和康复', '9.4.3(1)', '安排职业病病人进行治疗、定期检查、康复', '查资料。对照职业病病人名单和诊断病例档案，检查职业病病人治疗、定期检查和康复记录，1项1人次未安排扣1分', '查相关记录、花名册,是否安排职业病病人进行治疗、定期检查、康复', '1.职业病病人进行治疗、定期检查、康复记录；2.职业病病人花名册', '检查职业病病人治疗、定期检查和康复记录,1项1人次未安排,扣1分', 2, NULL, 1, 1903, 0, 1);
INSERT INTO `stdchk_item` VALUES (321, 1, 274, '职业卫生/职业病诊断鉴定/职业病病人安置', '9.4.4(1)', '将职业病病人调离接触职业病危害岗位并妥善安置', '查资料和现场。1人未按规定安置不得分', '1.查调整文件、证明等,是否对确诊职业病病人进行岗位调整,新岗位是否涉职业危害；2.查职业病病人调岗日期是否滞后,安置是否及时', '1.确诊职业病病人岗位调整文件、证明；2.劳动者个人职业健康监护档案', '1人未按规定安置不得分', 2, NULL, 1, 1903, 0, 1);
INSERT INTO `stdchk_item` VALUES (322, 1, 275, '职业卫生/职业病诊断鉴定/诊断、鉴定资料', '9.4.5(1)', '提供职业病诊断、伤残等级鉴定所需要的职业史和职业病危害接触史、作业场所职业病危害因素检测结果等资料', '查资料和现场。走访询问不少于5名当事人，煤矿不提供或不如实提供有关资料不得分', '1.查确诊职业病病人岗位接触史作业地点是否与涉职业危害作业场所相对应；2.查访或电话询问不少于5名当事人,看是否提供', '1.劳动者个人职业健康监护档案；2.职业病诊断、伤残等级鉴定所需要的资料；3.确诊职业病病人诊断、伤残等级鉴定所需要的职业史和职业病危害接触史、作业场所职业病危害因素检测结果等资料', '煤矿不提供或不如实提供有关资料不得分', 3, NULL, 1, 1903, 0, 1);
INSERT INTO `stdchk_item` VALUES (323, 1, 277, '职业卫生/工会监督/工会监督与维权', '9.5.1(1)', '设立劳动保护监督检查委员会', '查资料。不符合要求不得分', '1.检查是否有设立劳动保护监督检查委员会的正式文件；2.查看文件是否明确监督委员会机构及各成员的各项职责', '成立劳动保护监督检查委员会正式文件', '1.没有设立劳动保护监督委员会,扣2分；2.委员会机构、成员职责缺少1项,扣0.5分，扣到1分为止', 2, NULL, 1, 1903, 0, 1);
INSERT INTO `stdchk_item` VALUES (324, 2, 277, '职业卫生/工会监督/工会监督与维权', '9.5.1(2)', '对职业病防治工作进行监督，维护劳动者的合法权益', '查资料和现场。工会组织未依法对职业病防治工作进行监督不得分，开展监督活动没有记录扣1分', '1.查看是否有监督制度；2.查监督资料,工会是否对职业病防治工作进行监督；3.查工会监督检查工作是否有记录,是否活动是对照制度执行的；4.查检查记录是否齐全,是否有检查人员、整改落实及验收人员签字；5.检查下井人员是否有井下人员定位记录', '1.监督制度；2.工会组织对职业病监督检查资料；3.工会组织职责及维权记录；4.工会监督检查工作记录及人员签字；5.井下定位记录', '1.工会组织未依法对职业病防治工作进行监督，扣2分；2.没有制度扣1分;制度内容不具工作指导性，扣0.5分；3.开展监督活动没有记录,扣1分；4.下井检查记录不真实，扣1分', 2, NULL, 1, 1903, 0, 1);
INSERT INTO `stdchk_item` VALUES (325, 1, 280, '安全培训和应急管理/安全培训/基础保障', '10.1.1(1)', '1.有负责安全生产培训工作的机构，配备满足工作需求的人员', '查资料。不符合要求1处扣1分', '1.检查是否有负责安全培训工作的机构，培训机构设置是否以正式文件下发,机构职责是否明确；2.查是否有培训机构人员,人员业务分工是否明确；3.查所配备人员是否与《煤矿安全培训规定》和矿培训计划匹配;4.查是否有矿安全培训工作领导机构，是否有分管安全培训的领导', '1.职工人数(管理人员和操作人员数量)花名册；2.劳务派遣合同和安全管理协议,劳务派遣人员花名册；3.煤矿安全培训工作机构设置文件(专职机构、兼职机构)；4.煤矿安全培训人员配置文件(专职人员、兼职人员)；5.煤矿年度安全培训正式文件。培训机构的职责、人员职责', '1.无安全培训工作领导机构,扣1分；2.矿主要负责人不是全培训第一责任人扣1分；3.人员配备不满足工作需求,扣1分；4.培训工作机构人员业务分工不明确,扣1分；5.其他不符合要求,1处扣1分', 3, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (326, 2, 280, '安全培训和应急管理/安全培训/基础保障', '10.1.1(2)', '2.建立并执行安全培训管理制度', '查资料。未建立制度不得分；制度不完善1处扣0.5分。', '1.检查是否有安全培训制度,是否以正式文件下发；2.查制度内容是否符合国家相关法律、法规规定、是否完整(内容涵盖《煤矿安全培训规定》要求,至少应包括:①煤矿主要负责人A、B类安全生产管理人员培训；②并下10大特种作业人员;③3班组长任前培训;④“四新”培训;⑤应急培训；⑥职业危害培训；⑦其他专项培训,法律法规、重新上岗前、重大灾害、事故警示、干部知识更新、工人学历提升等;⑧每类制度中资源保障、计划编制、培训对象、形式方式、培训方法、培训内容、培训学时、培训管理、考核发证、效果检验、监督检查等管理要素）；3.检查该制度是否符合法律法规要求，是否可操作', '1.制度的正式文件(或其他具有权威载体形式)2.煤矿安全培训管理制度汇编；3.制度的执行记录和考核记录等', '1.未建立安全培训制度，扣2分；2.制度不完善,制度中内容、管理要素缺项,或与现行法律、法规要求不符,1处扣0.5分；3.制度执行有重大缺陷，1处扣0.5分,扣到1分为止', 2, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (327, 3, 280, '安全培训和应急管理/安全培训/基础保障', '10.1.1(3)', '3.具备安全培训条件的煤矿，按规定配备师资和装备、设施；不具备培训条件的煤矿，可委托其他机构进行安全培训', '查现场和资料。场所、师资、设施等不符合要求或欠缺1处扣0.2分；不具备条件且未委托培训的不得分', '1.自行对“三项人员”(主要负责人、安全管理人员、特种作业人员)开展安全培训的煤矿,实地查看培训机构场所、设施、台账情况,查是否符合《安全培训机构基本条件》(AQT8011—2016)第3.1条规定要求；2.查教师档案、外培协议书等资料,全矿专职教师加上兼职教师是否覆盖全专业；3.检查矿自主培训能力及外委培训情况，如无能力自主培训，需外委培训', '1.教师(包括专兼职）档案、花名册；2.教学场所、教学设备、设施等台账、实物；3.外委培训协议书或培训记录等资料', '1.开展自主培训的煤矿，场所、师资、设施等不符合要求或欠缺1处扣0.2分；2.不具备条件且未委托培训的不得分', 2, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (328, 4, 280, '安全培训和应急管理/安全培训/基础保障', '10.1.1(4)', '4.按照规定比例提取安全培训经费，做到专款专用', '查资料。未提取培训经费不得分，经费不足扣1分，未做到专款专用扣1分', '1.查全矿上年度工资总额、本年度和上年度全矿培训经费提取台账、支出凭证、本年度经费计划,是否提取培训经费；2.按照公式:上年度工资总额x1.5%X40%,计算并检查是否按比例足额提取了培训经费；3.查是否做到专款专用', '1.煤矿费用提取文件(或制度的相关规定）；2.煤矿财务部门的费用提取证据性资料；3.全矿上年度工资总额;本年度和上年度全矿培训经费提取台账；4.煤矿培训费用支出凭证', '1.未提取安全培训经费不得分；2.安全培训经费不足扣1分；3.安全培训经费未做到专款专用扣1分', 2, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (329, 1, 281, '安全培训和应急管理/安全培训/组织实施', '10.1.2(1)', '1.有年度培训计划并组织实施', '查资料。无计划不得分', '1.查是否有煤矿年度培训计划,是否以正式文件下发；2.查计划内容是否完整(培训需求、类别、数量、师资保障、资源保障、培训管理、效果考核等内容);3.查是否按计划实施的进度按时完成培训工作', '1.年度安全培训计划正式文件;2.培训计划下发到位发放表、签收单；3.计划实施进度材料；4.未完成计划的说明材料', '无年度安全培训计划不得分,年度内未完成培训计划的扣1分', 2, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (330, 2, 281, '安全培训和应急管理/安全培训/组织实施', '10.1.2(2)', '2.培训对象覆盖所有从业人员（包括劳务派遣者）', '查资料和现场。培训对象欠缺1种扣0.2分', '1.查是否存在劳务派遣工,数量及管理模式是否成建制管理；2.查是否存在外委施工单位；3.查培训是否完全覆盖《煤矿安全培训规定》规定的煤矿企业主要负责人、安全生产管理人员、特种作业人员、其他从业人员这4类人员', '1.全矿人员花名册（台账）；2.劳务派遣工名单；3.外委施工人员名单；4.年度培训计划、培训资料', '1.培训对象欠缺1种扣0.2分,不同培训对象累加,扣到2分为止;2.对外委托施工单位施工从事采掘生产工作的，本矿必须与外委单位所有施工人员签订劳动协议并纳入本矿安全培训和劳资管理,否则属重大事故隐患', 2, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (331, 3, 281, '安全培训和应急管理/安全培训/组织实施', '10.1.2(3)', '3.安全培训学时符合规定', '查资料。不符合要求1处扣0.5分', '1.查安全培训学时是否满足《煤矿安全培训规定》要求；2.从本年度已经完成的自行培训项目中，抽取2-3类检查,查阅学时实施是否符合规定', '1.培训计划、教学计划；2.教学课程表、教师日志；3.教案', '不符合要求1处扣0.5分', 2, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (332, 4, 281, '安全培训和应急管理/安全培训/组织实施', '10.1.2(4)', '4.针对不同专业的培训对象和培训类别，开展有针对性的培训；使用新工艺、新技术、新设备、新材料时，对有关从业人员实施针对性安全再培训', '查资料和现场。培训无针对性扣1分，其他1处不符合要求扣0.5分', '1.查会议纪要,本矿有无”四新”项目；查矿培训资料,是否开展相应“四新”培训；2.查培训大纲,培训有无针对性', '1.全矿安全生产会议纪要；2.“四新”培训资料:培训计划、培训通知、培训对象、培训课程表、培训内容、培训教案(课件)、考勤表、培训考试、培训小结等;3.培训大纲', '1.培训无针对性,扣1分；2.有“四新”项目未开展培训,扣0.5分;有培训但资料不齐全,建议完善；3.其他1处不符合要求扣0.5分', 2, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (333, 5, 281, '安全培训和应急管理/安全培训/组织实施', '10.1.2(5)', '5.特种作业人员经专门的安全技术培训（含复训）并考核合格', '查资料和现场。1人不符合要求不得分', '1.查台账,全矿特种作业人数是多少；2.查培训合格记录台账,是否做到井下特种作业(井下电气、爆破、监测、瓦检、安检、提升机作业、采掘机操作、抽采、防突、探放水)人员、地面相关特殊工种培训全覆盖；3.查特种作业人员是否培训合格取得操作资格证上岗作业。结合比对管理台账信息,在井下、地面实地抽查几位特种作业人员持证情况:至少3类特种作业人员,每一类至少抽查3人，是否存在证件失效或未取证上岗作业', '1.特种作业人员管理台账；2.特种作业人员有效证件；3.特种作业人员培训合格记录台账;4.特种作业人员证件管理台账;5.复训记录', '1.发现1名特种作业人员未经培训上岗,扣6分；2.发现1名特种作业人员证件失效后仍未经培训，扣6分；3.发现1名特种作业人员未复训合格上岗,扣6分', 6, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (334, 6, 281, '安全培训和应急管理/安全培训/组织实施', '10.1.2(6)', '6.主要负责人和职业卫生管理人员接受职业卫生培训；接触职业病危害因素的从业人员上岗前接受职业卫生培训和在岗期间的定期职业卫生培训', '查资料和现场。主要负责人未经过职业卫生培训扣1分；其他1人不符合要求扣0.2分', '1.查资料,矿主要负责人是否接受了职业卫生培训；2.查相关资料,看职业卫生管理人员、接触职业病危害因素的从业人员上岗前和在岗期间是否接受培训;3.查是否专门建立了职业危害培训计划和教学方案', '1.矿长、五职矿长接受职业卫生培训的证件、材料；2.矿职业病防治领导机构文件、职业卫生管理人员接受职业卫生培训的记录；3.新工人名单;新工人入矿的职业卫生培训证据资料；4.全矿人员名册、接尘接毒在役职工职业卫生培训记录', '1.主要负责人未经过职业卫生培训,扣1分；2.其他1人不符合要求扣0.2分', 3, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (335, 7, 281, '安全培训和应急管理/安全培训/组织实施', '10.1.2(7)', '7.井工煤矿从事采煤、掘进、机电、运输、通风、地测等工作的班组长任职前接受专门的安全培训并经考核合格', '查资料和现场。1人不符合要求扣0.2分', '1.查煤矿自主培训班组长,是否建立班组长培训专项计划和教学方案；2.现场分别抽查采煤、开拓、掘进、机电、运输、通风、爆破、地测基层施工区队班组长各1人,是否有培训相关记录；3.2018年3月1日后,采煤、掘进、机电、运输、通风、防治水等工作的班组长是否由上一级煤矿企业组织实施', '1.班组长任命文件或聘任文件、花名册、培训合格证；2.在职班组长培训安排；3.班组长培训计划、培训通知、培训对象、培训课程表、培训内容、培训教案（课件）、考勤表、培训考试、培训小结等', '每个类别的班组长有1人不符合要求(任职前未培训),扣0.2分', 3, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (336, 8, 281, '安全培训和应急管理/安全培训/组织实施', '10.1.2(8)', '8. 组织有关人员开展应急预案培训，熟练掌握应急预案相关内容', '查资料和现场。不符合要求1处扣0.2分', '1.对照本矿《应急预案》内容,查全矿应急培训方案记录是否有对应安排;是否组织有关人员开展应急预案培训；2.抽查应急指挥人员、应急机构工作人员、应急救援专业组、应急救援队伍、现场操作人员各1人,是否掌握应急预案相关内容:指挥机构人员的职责、任务及应急处置指挥程序;应急救援专业组的任务、应急处置程序、应急处置措施;应急救援队伍的应急处置任务、应急施救措施;现场作业人员的应急避险技能;3.现场抽查不同岗位的操作人员,对岗位职责、存在风险及处理程序是否熟悉', '1.培训计划、培训通知、培训对象、培训课程表、培训内容、培训教案(课件)、考勤表、培训考试、培训小结等记录;2.最新版本《应急预案》', '1.应急指挥人员、应急机构工作人员、应急教援专业组、应急救援队伍、现场操作人员,1人不熟悉应急预案相关内容,扣0.2分；2.全矿应急培训安排记录与应急预案1处不对应扣0.2分', 3, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (337, 9, 281, '安全培训和应急管理/安全培训/组织实施', '10.1.2(9)', '9.煤矿主要负责人和安全生产管理人员自任职之日起6个月内，通过安全培训主管部门组织的安全生产知识和管理能力考核', '查资料。1人不符合要求不得分', '1.检查煤矿矿长、安全生产管理人员是否在履职6个月内考核合格；2.抽查矿长、副矿长,并分别抽查安监部门、技术部门、调度部门、通风部门、开掘部门、机电部门等各1人,检查相关台账资料,是否于任职6个月内通过安全生产知识和管理能力考核;3.现场抽查矿领导及生产部门负责人，是否有过期未重新考核的情况', '1.任命或聘任文件；2.人员台账或花名册；3.有效培训证件;4.能力考核结果或面试成绩；5.初训和再培训计划和安排；6.煤矿主要负责人和安全生产管理人员考核合格证；7.安全管理人员培训及持证情况台账', '查任职文件,1人不符合要求不得分', 3, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (338, 1, 282, '安全培训和应急管理/安全培训/持证上岗', '10.1.3(1)', '1.特种作业人员持《特种作业人员操作资格证》上岗', '查资料和现场。不符合要求1人扣1分', '现场抽查特种作业人员是否现场持证；证件是否有效', '1.特种作业人员台账；2.有效证件；3.特种作业人员资格证', '1人未持《特种作业人员操作资格证》上岗扣1分', 2, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (339, 2, 282, '安全培训和应急管理/安全培训/持证上岗', '10.1.3(2)', '2.煤矿主要负责人和安全生产管理人员通过考核取得合格证上岗', '查资料和现场。不符合要求1人扣1分', '1.查培训和持证台帐；2.现场抽查矿领导及生产部门负责人，是否有过期未换证或复训记录不全的情况(证件看原件);3.查阅并验证矿长、安全生产管理人员培训合格资质证件。查验证件的有效期限', '1.培训和持证台账；2.有效证件', '1人未取得考核合格证上岗扣1分', 2, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (340, 3, 282, '安全培训和应急管理/安全培训/持证上岗', '10.1.3(3)', '3.煤矿其他人员经培训合格取得培训合格证上岗', '查资料和现场。不符合要求1人扣0.2分', '1.对井下(降压站、变电所、主井绞车房、避难硐室、有人值守的水泵房、压风机房、胶带岗位工、把钩工等)岗位人员进行证件抽查；2.查是否存在失效、过期、持证工种与本人从事岗位不符等情况；3.抽查地面作业岗位,抽查变电所、主副井口、地面输煤系统的配电工、信号工、把钩工、胶带输送机司机等不少于5人', '1.培训和持证台账；2.有效证件', '1人未取得培训合格证上岗扣0.2分', 2, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (341, 1, 283, '安全培训和应急管理/安全培训/培训档案', '10.1.4(1)', '1.建立安全生产教育和培训档案，内容包含各类别、各专业安全培训的时间、内容、参加人员、考核结果等', '查资料和现场。未建立档案不得分；档案内容不完整每缺1项扣0.2分', '1.查阅资料和档案管理现场，是否有档案；2.查档案管理要素是否齐全；3.核查档案是否造假', '1.档案管理制度；2.纸质版档案；3.电子版档案', '1.未建立安全生产教育和培训档案，扣3分；2.档案内容不完整，每缺1项内容，扣0.2分；3.档案造假，造假1项，扣0.2分', 3, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (342, 2, 283, '安全培训和应急管理/安全培训/培训档案', '10.1.4(2)', '2.建立健全特种作业人员培训、复训档案', '查资料。未建立档案不得分；档案内容不完整每缺1项扣0.2分', '1.查是否按照一人档管理;2.根据《煤矿安全培训规定》第八条，查档案内容是否完整：①学员登记表,包括学员的文化程度、职务、职称、工作经历、技能等级晋升等情况；②身份证复印件、学历证书复印件;③历次接受安全培训、考核的情况;④其他有关情况\n', '培训、复训档案', '1.未建立特种作业人员培训、复训档案，扣3分；2.档案内容不完整，每缺1项扣0.2分', 3, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (343, 3, 283, '安全培训和应急管理/安全培训/培训档案', '10.1.4(3)', '3.档案管理规范', '查资料和现场。不符合要求1处扣0.2分', '1.查是否有矿档案管理制度;2.查电子档案、纸张档案是否分类清晰、内容完整,按档案管理标准存放检索有序；3.查资料归档程序（送交、编号、登记、立卷、分类、入库）是否完善、正确；4.查资料是否安全存放管理(遗失、破损、污染、潮湿、被盗等);5.查档案日常管理（检索、使用、查阅、借阅、复印）是否规范;6.查档案保存期限销毁管理和实施是否符合规定', '1.档案管理制度；2.档案目录或索引；3.归档记录；4.安全存放管理台账；5.日常管理记录', ' 1.档案目录或索引、编号、归档、借阅、保存期限、销毁,不符合要求,1处扣0.2分;2.资料安全存放管理遗失、破损、污染、潮湿、被盗等),不符合要求,1处扣0.2分;\n3.其他不符合要求,1处扣0.2分', 3, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (344, 1, 285, '安全培训和应急管理/班组安全建设/制度建设', '10.2.1(1)', '建立并严格执行下列制度：1.班组长安全生产责任制；2.班前、班后会和交接班制度；3.班组安全生产标准化和文明生产管理制度；4.学习制度；5.安全承诺制度；6.民主管理班务公开制度；7.安全绩效考核制度', '查资料。每缺1项制度扣0.2分，1项制度不严格执行不得分', '1.查7项制度是否建立,是否都有矿正式文件;2.查每项制度4大内容(制度目标、职制分工、职责具体内容、考核)是否完整;3.通过查看制度执行的证据和结果资料,检查制度是否被严格执行', '1.各项制度发布的正式文件(或汇编);2.制度制定完善修订会议记录或纪要;3.安全生产责任制汇编;4.班会记录;5.交接班记录;6.学习记录;7.安全承诺书;8.民主活动记录;9.安全绩效考核记', '1.每缺1项制度扣0.1分；2.1项制度不严格执行扣0.2分；3.1项制度4大内容，有1个内容不完善扣0.1分，扣到0.2分为止', 1, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (345, 1, 286, '安全培训和应急管理/班组安全建设/组织建设', '10.2.2(1)', '1.安全生产目标明确，有群众安全监督员（不得由班组长兼任）', '查资料和现场。不符合要求1项扣0.2分', '1.查班组是否有安全目标管理的责任部门,有无全矿安全目标在班组的分解情况;2.查班组目标内容是否合理(人身事故、非伤亡事故、隐患排查、反“三违”、参与安全管理、“三无”班组创建等),班组安全目标是否实现;3.查煤矿群监员有关管理规定,群监员管理台账,群监员是否发挥作用;4.抽查(抽样)监督员设置,是否覆盖所有班组,有无正式的聘任程序,职责和权利是否明确,如何实施管理,是否存在班组长兼任', '1.矿级年度安全生产目标;2.区(队)和班组年度安全生产目标；3.目标和指标考核资料及实现情况;4.群众监督员聘用文件;5.群众监督员管理台账；6.群众监督员工作记录', '1.班组无安全目标或者安全目标不明确,扣0.2分；2.无群监员或者群监员未覆盖所有班组,扣0.2分；3.班组长兼任群监员,扣0.2分;4.群监员工作无记录,扣0.2分;5.其他不符合要求,1项扣0.2分', 1, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (346, 2, 286, '安全培训和应急管理/班组安全建设/组织建设', '10.2.2(2)', '2.班组建有民主管理机构，并组织开展班组民主活动', '查资料和现场。未建立机构不得分；民主活动开展不符合要求扣0.5分', '1.查是否建立班组民主活动管理机构;2.查班组的民主活动记录痕迹,是否有民主管理台账', '1.煤矿对区队、班组民主管理机构设置、活动开展的制度规定2.班组民主活动记录、民主管理台账', '1.未建立民主管理机构，不得分；2.民主活动开展不符合要求，扣0.5分', 1, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (347, 3, 286, '安全培训和应急管理/班组安全建设/组织建设', '10.2.2(3)', '3.开展班组建设创先争优活动、组织优秀班组和优秀班组长评选活动，建立表彰奖励机制', '查资料和现场。未建立机制或未开展活动不得分', '1.查是否有创先争优的表彰制度;2.查是否有创先争优的机制手册或结果(竞赛安排、项目成果报告、优秀班组长推荐表等),创先争优活动是否组织实施', '1.矿对班组创优争先活动、评优评选活动的制度性、机制性的文件;2.优秀班组和优秀班组长评选原始记录、表格等材料;3.优秀班组和优秀班组长表彰奖励文件和奖励兑现资料', '1.未建立机制，扣1分；2.未开展活动，扣1分', 1, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (348, 4, 286, '安全培训和应急管理/班组安全建设/组织建设', '10.2.2(4)', '4.建立班组长选聘、使用、培养机制', '查资料。未建立机制不得分', '1.查是否对班组长培训培养机制以正式文件建立制度;2.查机制文件并与矿上实际情况对比,检查是否落实:班组长任职条件是否明晰;煤矿、区队如何实施并执行(提拔、奖励情况等)', '1.煤矿对班组长选聘、使用、培养、管理的制度性、机制性的文件;2.班组长台账;3.班组长选聘资料4.班组长培养记录；5.班组长个人业绩档案;6.班组长考评资料等', '1.未建立机制，扣1分；2.建立机制未严格执行，扣0.5分', 1, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (349, 5, 286, '安全培训和应急管理/班组安全建设/组织建设', '10.2.2(5)', '5.赋予班组长及职工在安全生产管理、规章制度制定、安全奖罚、民主评议等方面的知情权、参与权、表达权、监督权', '查资料和现场。不符合要求1处扣0.2分', '1.查有无班组民主活动和“四项权利”执行的记录痕迹;2.查煤矿有无将“四项权利”授权到位的形式、载体、机制;3.现场抽查班组长不少于3人、职工不该少于3人,提问是否知晓“四项权利”', '1.煤矿对“四项权利”的制度性规定和明确授权文件;2.“四项权利”实施的证据性资料；3.班组长组织的会议纪要、记录台账、机制载体', '1.煤矿无“四项权利”的制度性规定和明确授权文件,扣0.2分;2.“四项权利”提问,人都回答不正确,扣0.2分；3.无班组民主活动和“四项权利”执行的记录痕迹,1处扣0.2分', 1, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (350, 1, 287, '安全培训和应急管理/班组安全建设/现场管理', '10.2.3(1)', '1.班前有安全工作安排，班组长督促落实作业前进行安全确认', '查资料和现场。不符合要求1处扣0.2分', '1.现场查班组的安全确认是否有痕迹,交接班记录是否有安全工作安排;2.查现场,通过班中检查员工现场演示,检查是否班前进行了安全确认安排', '交接班记录', '1.班前无安全工作安排或者安全工作安排不具体,扣0.2分;2.查现场,班中工作现场的安全确认不符合要求,1处扣0.2分;3.其他不符合要求,1处扣0.2分', 1, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (351, 2, 287, '安全培训和应急管理/班组安全建设/现场管理', '10.2.3(2)', '2.严格执行交接班制度，交接重点内容包括隐患及整改、安全状况、安全条件及安全注意事项', '查资料。不符合要求1处扣0.2分', '1.查交接班制度,内容是否完整,交接程序是否明晰、具有可操作性；2.查交接班制度执行的相关各类记录台\n账;对照制度规定,查交接内容是否完整;交接人员签名是否完整,是否符合制度规定', '1.交接班制度;2.现场交接班记录台账;3.矿领导带班现场交接班地点与记录;4.生产单位班组现场交接班记录;5.辅助单位班组现场交接班记录；6.调度班组现场交扣0\n接班记录;7.地面重要部门班组现场交接班记录', '1.交接班记录内容不完整,缺1处扣0.2分;2.交接人员签名不完整、不符合制度要求,1处扣0.2分;3.内容不完整,缺1项扣0.2分', 1, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (352, 3, 287, '安全培训和应急管理/班组安全建设/现场管理', '10.2.3(3)', '3.组织班组正规循环作业和规范操作', '查资料和现场。不符合要求1处扣0.2分', '1.抽查采掘工作面班组长,现场讲述或演示正规循环作业,询问组织班组规范操作的措施;2.随机抽查采煤机司机、掘进机司机、采掘支护工、运输机司机、絞车司机、泵站司机、电钳工、瓦检工、爆破工之中几个,现场实操是否能做到规范操作\n', '1.采掘作业规程；2.采掘正规循环作业资料；3.岗位操作规程', '不符合要求1处扣0.2分', 1, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (353, 4, 287, '安全培训和应急管理/班组安全建设/现场管理', '10.2.3(4)', '4.井工煤矿实施班组工程（工作）质量巡回检查，严格工程（工作）质量验收', '查资料和现场。不符合要求1处扣0.2分', '1.查采掘班组施工作业工程质量管理程序和内容(作业规程);2.抽查是否有采掘工作面月度、旬度、工作日的班组工程质量检查、验收相关记录(报表);3.抽查是否有班组采掘作业现场安全和工程质量巡回检查相关记录', '1.采掘作业规程；2.班组工程质量检查、验收相关记录（报表）；3.制度执行记录及考核记录', '1.未开展班组工程（工作）质量巡回检查，1处扣0.2分；2.未开展工程（工作）质量验收，1处扣0.2分', 1, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (354, 1, 289, '安全培训和应急管理/应急管理/机构和职责', '10.3.1(1)', '1.建立应急救援工作日常管理领导机构、工作机构和应急救援指挥机构，人员配备满足工作需要，职责明确；2.有固定的应急救援指挥场所', '查资料和现场。不符合要求1处扣0.2分', '1.根据《生产安全事故应急预案管理办法》(国家安全生产监督管理总局令第88号)规定,查现场指挥部日常应急管理机构设置的文件,是否设有3个机构;2.查文件中是否明确日常应急管理第一责任人、分管领导,是否明确人员配置情况;是否明晰应急岗位职责;3.查矿上文件中对固定应急救援场所有无规定', '1.应急管理体系及3个机构的文件；2.指挥部相关文件', '1.未明确矿应急管理第一责任人,扣0.2分;2.未明确应急管理分管领导,扣0.2分;3.日常管理领导机构、工作机构、应急救援指挥机构人员职责不明确,不符合要求,1处扣0.2分;4.应急管理工作机构人员配备不满足工作需要,扣0.2分;5.无固定的应急救援指挥场所,扣0.2分;6.其他不符合要求,1处扣0.2分', 2, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (355, 1, 290, '安全培训和应急管理/应急管理/制度建设', '10.3.2(1)', '建立健全以下制度：1.事故监测与预警制度；2.应急值守制度；3.应急信息报告和传递制度；4.应急投入及资源保障制度；5.应急预案管理制度；6.应急演练制度；7.应急救援队伍管理制度；8.应急物资装备管理制度；9.安全避险设施管理和使用制度；10.应急资料档案管理制度', '查资料。每缺1项制度扣1分，制度内容不完善1处扣0.2分', '1.查文件、制度汇编。看10项制度是否都以正式文件下发；2.随机取样，查阅制度内容是否与国家安全生产和应急管理法律法规规章等基本要求符合；查阅制度要素是否完整；查阅制度是否合理、可操作；3.查阅制度内容设置是否存在内容重叠或遗漏、职责不明确、程序不清晰等情况；4.抽样查阅制度管理的主责部门是否清晰', '1.发布制度的正式文件；2.制度汇编；3.10项制度的执行记录和考核记录', '1.每缺1项制度，扣1分；2.制度内容不完善，1处扣0.2分', 2, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (356, 1, 291, '安全培训和应急管理/应急管理/应急保障', '10.3.3(1)', '1.配备应急救援物资、装备或设施，建立台账，按规定储存、维护、保养、更新、定期检查等；2.有可靠的信息通讯和传递系统，保持最新的内部和外部应急响应通讯录；3.配置必需的急救器材和药品；与就近的医疗机构签订急救协议；4.建立覆盖本煤矿所有专项应急预案相关专业的技术专家库', '查资料和现场。不符合要求1处扣0.5分', '1.查应急物资：(1)查验物资管理台账、清单情况:管理台账或清单;物资储存齐全、完好记录物资维护、保养、定期检查、更新记录;管理职责及其履行；(2)台账与实物对照,现场核查地面和井下应急物资库；2.查信息通讯和传不符递系统：(1)查阅该煤矿指挥部是否有通讯系统、信息资料的传输系统(2)通讯系统日常维护和保养职责任务是否明确(3)结合任务职责并现场拨打电话,核查通讯系统的功能是否满足职责要求;(4)指挥部是否有最新的煤矿内部和外部通讯录；3.查医疗配备：(1)查看煤矿有无签订急救协议书,并通过查急救医务人员档案、急救记录等，检查急救协议书的签暑(日期、有效期、有效性等)是否符合要求;(2)查应急预案中有无医疗急救保障中关于急救器材的相关内容；(3)对照台账验证：急救器材配置或设置急救药品(止血品、纱带、担架、救心丸等)的设置或配置。急救医务人员的配置和设置是否足够且不过期；(4)现场查看井口和井下急救站；4.查专家库:(1)查是否在应急预案中有专家库的相关内容,是否有专家管理办法;(2)查是否建立了专家库,通过查专家档案、聘任文件看专业类别是否齐全,是否体现本矿专家最高(3)查有无专家通讯录,有无专家服务', '1.煤矿应急物资台帐；2.煤矿应急物资维护、保养、更新、定期检查记录;3.通讯系统日常维护和保养制度4.通讯系统的职责任务;5.指挥部最新的煤矿内部通讯录;6指挥部最新的外部通讯录；7.急救协议书；8.应急预案；9.急救器材和药品台账；10.急救医务人员档案；11.急救工作记录和值班记录、交接班记录；12.应急技术专家管理办法；13.应急技术专家名单和档案；14.应急技术专家聘任文件；15.所有专项预案；16.专家通讯录；17.应急技术专家服务记录等', '1.无应急物资台账,扣0.5分；2.现场查应急物资仓库，应急救援物资装备或者设施不足,扣0.5分；3.应急物资维护、保养、更新、定期检查无记录或者记录不规范,扣0.5分；4.通讯系统、信息资料的传输系统不可靠、功能不全,扣0.5分；5.指挥部最新的煤矿内部和外部通讯录,不符合要求,1处扣0.5分；6.现场查看井口和井下急救站,急救器材和药品配备不足或者过期,扣0.5分；7.未与就近的医疗机构签订急救协议,扣0.5分；8.与就近的医疗机构签订急救协议但签署不符合要求,1处扣0.5分；9.无应急技术专家管理办法,扣0.5分；10.技术专家专业未覆盖煤矿所有专项预案相关专业，扣0.5分；11.无技术专家通讯录，扣0.5分；12.技术专家无服务记录或未开展相关服务活动，扣0.5分', 4, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (357, 1, 292, '安全培训和应急管理/应急管理/安全避险系统', '10.3.4(1)', '按规定建立完善井下安全避险设施；每年由总工程师组织开展安全避险系统有效性评估', '查资料和现场。不符合要求1处扣0.2分', '对照《煤矿安全规程》第六百七十三条至第六百九十二条的相关规定：1.查是否有符合本安全避险的必备设施；2.检查该煤矿是否有对安全避险系统有效性实施评估的方案，是否有评估报告', '1.安全避险系统图；2.避险设备管理制度；3.安全避险设施台账；4.安全避险系统有效性评估工作方案、评估原始记录、评估报告等', '1.井下安全避险设施不符合要求，1处扣0.2分；2.安全避险系统的有效性评估工作方案、评估报告等不符合要求，1处扣0.3分', 2, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (358, 1, 293, '安全培训和应急管理/应急管理/应急广播系统', '10.3.5(1)', '井下设置应急广播系统，井下人员能够清晰听到应急指令', '查现场。未建立系统不得分；1处生产作业地点不能够听到应急指令扣0.5分', '1.查是否建有井下应急广播系统；2.抽查井下几个不同作业场所，终端音箱覆盖位置是否全覆盖', NULL, '1.未建立系统扣1分；2.1处生产作业地点不能够听到应急指令扣0.5分', 1, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (359, 1, 294, '安全培训和应急管理/应急管理/个体防护装备', '10.3.6(1)', '按规定配置足量的自救器，入井人员随身携带；矿井避灾路线上按需求设置自救器补给站', '查资料和现场。自救器的备用量不足10％扣0.5分；其他1人（处）不符合要求扣0.2分', '1.查煤矿自救器管理和维护台账,备用量是否符合要求;自救器配置数量是否符合管理制度规定;2.对照避灾路线图,护台井下现场抽查是否按要求在避灾路线上设置了自救器补给站;3.核对当天下井人员台账与自救器使用登记表,是否有人员未佩戴自救器', '1.自救器管理和维护台账；2.矿井避灾路线图；3.入井人员台账、自救器使用登记表', '1.自救器的备用量不足10%，扣0.5分；2.1人入井未携带自救器，扣0.2分；3.避灾路线上未设置自救器补给站，1处不符合要求扣0.2分', 1, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (360, 1, 295, '安全培训和应急管理/应急管理/紧急处置权限', '10.3.7(1)', '明确授予带班人员、班组长、瓦斯检查工、调度人员遇险处置权和紧急避险权', '查资料。权力未明确不得分', '1.查有无给四类人员的遇险处置权和职工的紧急避险权的授权书和授权文件；2.查有无应急培训和宣贯相关权力的记录；3.现场抽查四类人员是否知晓2项权力的内容', '1.四类人员的遇险处置权和职工的紧急避险权的授权书；权威性文件；2.四类人员相关权力实施宣贯、培训的记录', '权力未明确，扣1分', 1, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (361, 1, 296, '安全培训和应急管理/应急管理/技术资料', '10.3.8(1)', '1.井工煤矿应急指挥中心备有最新的采掘工程平面图、矿井通风系统图、井上下对照图、井下避灾路线图、灾害预防与处理计划、应急预案；2.露天煤矿应急指挥中心备有最新的采剥、排土工程平面图和运输系统图、防排水系统图及排水设备布置图、井工老空区与露天矿平面对照图、应急救援预案', '查现场和资料。每缺1项扣1分', '1.现场查井工煤矿应急指挥中心是否保存有“四图”“两书”；查看“四图”“两书”是否为最新；2.现场查露天煤矿是否备有“五图一案”', '1.井工煤矿“四图”“两书”；2.露天煤矿“五图一案”', '每缺1项，扣1分', 3, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (362, 1, 297, '安全培训和应急管理/应急管理/队伍建设', '10.3.9(1)', '1.煤矿有矿山救护队为其服务', '查资料。不符合要求不得分', '查矿山救护队的设立文件或救护协议书', '救护队设立文件或救护协议书', '煤矿无矿山救护队为其服务，扣4分', 4, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (363, 2, 297, '安全培训和应急管理/应急管理/队伍建设', '10.3.9(2)', '2.井工煤矿不具备设立矿山救护队条件的应组建兼职救护队，并与就近的救护队签订救护协议。兼职救护队按照《矿山救护规程》的相关规定配备器材和装备，实施军事化管理，器材和装备完好，定期接受专职矿山救护队的业务培训和技术指导，按照计划实施应急施救训练和演练', '查资料和现场。没有矿山救护队为本矿服务的或未签订救护协议不得分，其他1处不符合要求扣0.5分', '1.查本矿上级企业是否建立设立专职矿山救护队,并在30min内能到达本矿,若如此,本矿可不建立兼职救护队;2.不符合上述条件的煤矿,查是否有兼职救护队成立文件；3.查看是否同就近的救护队签订救护协议；4.现场检查兼职救护队装备、器材是否与台账一致,配备是否符合规定；5.通过查看军事化管理记录及现场查看训练等方式,检查是否进行军事化管理；6.查看兼职救护队员是否有培训证书、学习记录、训练演练记录;7.检查救护协议书签署及期限是否有效', '1.救护协议；2.兼职救护队成立文件；3.兼职救护队队员档案；4.兼职救护队队员培训证书；5.兼职救护队的器材和装备台账；6.训练装备、训练作息、值班、参演等军事化管理记录；7.兼职救护队的培训与技术学习记录；8.兼职救护队的应急施救训练和演练记录；9.矿山救护规程', '1.煤矿企业专职救护队不满足要求，且煤矿没有建立兼职救护队，扣3分；2.其他1处不符合要求，扣0.5分', 3, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (364, 1, 298, '安全培训和应急管理/应急管理/应急预案', '10.3.10(1)', '1.预案编制与修订（1）按照《生产安全事故应急预案管理办法》编制应急预案，并按规定及时修订；（2）按规定组织应急预案的评审，形成书面评审结果。评审通过的应急预案由煤矿主要负责人签署公布，及时发放（3）应急预案与煤矿所在地政府的生产安全事故应急预案相衔接', '查资料。未编制应急预案的“应急预案”项不得分；应急预案修订不及时不得分；应急预案有欠缺1处扣0.5分，应急预案未组织评审不得分，评审证据资料、签署和发放管理环节1处不符合要求扣0.5分，应急预案发放不及时扣1分，应急预案未与政府预案相衔接扣0.5分', '1.查看煤矿是否编制应急预案;2.查看应急预案是否在国家安全生产监督管理总局令第88号规定的7种情形：①依据的法律、法规、规章、标准及上位预案中的有关规定发生重大变化的;②应急指挥机构及其职责发生调整的;③面临的事故风险发生重大变化的;④重要应急资源发生重大变化的；⑤预案中的其他重要信息发生变化的;⑥在应急演练和事故应急救援中发现问题需要修订的;⑦编制单位认为应当修订的其他情况下及时修订；3.检查应急预案内容是否符合国家安全生产监督管理总局令第88号第八条、第十三条、第十四条的规定,是否有缺项;4.查看应急预案是否由煤矿主要负责人签署发布,是否有书面评审结果；5.依据《生产安全事故应急预案管理办法》(国家安全生产监督管理总局令第88号)查看矿应急预案是否3年评估一次', '1.成立应急预案编制工作小组文件；2.成立预案修订工作小组文件；3.预案有效版本；4.预案评审报告；5.预案签发发布文件；6.预案发放记录；7.应急预案与政府预案相衔接说明', '1.未编制应急预案,扣6分;2.应急预案修订不及时,扣3分;3.应急预案内容有欠缺,1处扣0.5分;4.应急预案未组织评审,扣3分;5.应急预案评审证据资料、签署和发放管理环节1处不符合要求,扣0.5分;6.应急预案发放不及时,扣1分;7.应急预案未与政府预案相衔接，扣0.5分', 3, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (365, 2, 298, '安全培训和应急管理/应急管理/应急预案', '10.3.10(2)', '2.按照应急预案和灾害预防与处理计划的相关内容，针对重点工作场所、重点岗位的风险特点制定应急处置卡', '查资料和现场。不符合要求1处扣0.2分', '1.现场查看井下重点场所和重要岗位是否悬挂有应急处置卡；2.查阅应急处置卡的内容是否合理、全面', '应急预案和灾害预防处理计划', '应急处置卡岗位、场所数量设置不足、内容不正确，1处扣0.2分', 1, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (366, 3, 298, '安全培训和应急管理/应急管理/应急预案', '10.3.10(3)', '3.按照分级属地管理的原则，按规定时限、程序完成应急预案上报并进行告知性备案', '查资料。未按照规定上报、备案不得分', '查资料，是否3个月内向上级主管部门、煤监分局进行备案', '1.备案登记表、备案申请表、备案评审表；2.备案回执单或其他能够证明以及上报备案的材料', '1.应急预案未备案，不得分；2.应急预案备案超过规定时限，不得分', 1, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (367, 4, 298, '安全培训和应急管理/应急管理/应急预案', '10.3.10(4)', '4.煤矿发生事故在第一时间启动应急预案，实施应急响应、组织应急救援；并按照规定的时限、程序上报事故信息', '查资料。不符合要求1处扣0.5分', '查应急指挥办公室有无相关图表，口述演练1次', '应急演练办公室图表', '不按规定程序启动应急预案，不上报事故信息，上报时限、程序不符合要求，1处扣0.5分', 1, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (368, 1, 299, '安全培训和应急管理/应急管理/应急演练', '10.3.11(1)', '1.有应急演练规划、年度计划和演练工作方案，内容符合相关规定', '查资料。不符合要求1处扣0.2分', '1，查有无应急演练规划、年度计划、演练工作方案；2.查演练工作方案是否符合《生产安全事故应急演练指南》（AQ/T 9007—2011）要求', '1.应急演练规划；2.应急演练年度计划；3.应急演练工作方案', '1.应急演练规划不符合要求，1处扣0.2分；2.应急演练年度计划不符合要求，1处扣0.2分；3.演练工作方案不符合要求，1处扣0.2分', 2, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (369, 2, 299, '安全培训和应急管理/应急管理/应急演练', '10.3.11(2)', '2.按规定3年内完成所有综合应急预案和专项应急预案演练', '查资料。不符合要求不得分', '查3年内的综合应急预案和全部专项演练计划是否完成', '1.3年演练计划表；2.按计划实施演练的资料', '3年内未完成所有综合应急预案和专项应急预案演练，不得分', 1, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (370, 3, 299, '安全培训和应急管理/应急管理/应急演练', '10.3.11(3)', '3.应急预案及演练、灾害预防和处理计划的实施由矿长组织；记录翔实完整，并进行评估、总结', '查资料。演练和计划的实施组织主体不符合要求不得分；其他1处不符合要求扣0.5分', '1.查煤矿预案演练的制度，演练的组织是否由矿长实施；2.查记录是否齐全；是否留有音像资料；3.检查演练评估和总结是否符合要求；4.查阅预案演练文本编制是否规范、完整', '1.预案演练制度；2.预案演练方案演练记录；3.预案演练评估报告；4.预案演练总结报告；5.演练实施记录', '1.演练和计划的实施组织主体不符合要求，扣4分；2.演练评估、总结、记录等不符合要求，1处扣0.5分；3.专项应急预案演练没记录，扣0.5分；4.评估、总结针对性不强，1处扣0.5分；5.其他1处不符合要求扣0.5分', 4, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (371, 1, 300, '安全培训和应急管理/应急管理/资料档案', '10.3.12(1)', '1.应急资料归档保存，连续完整，保存期限不少于2年', '查资料。不符合要求1处扣0.5分', '1.查应急资料是否都归档保存，保存期限是否少于2年；2.查目录、索引，是否有保存不连续的', '1.应急管理档案目录或索引；2.应急资料档案', '1.应急资料未归档保存，扣0.5分；2.应急资料不连续、不完整，扣0.5分；3.保存期限不足2年，扣0.5分', 2, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (372, 2, 300, '安全培训和应急管理/应急管理/资料档案', '10.3.12(2)', '2.应急管理档案内容完整真实（应包括组织机构、工作制度、应急预案、上报备案、应急演练、应急救援、协议文书等）管理规范', '查资料和现场。不符合要求1处扣0.2分', '1.查应急管理档案内容中的7个管理要素是否齐全、规范；2.现场查档案目录或索引是否规范，查归档记录、借阅记录、销毁记录看各工序是否符合要求', '1.档案目录或索引；2.归档记录；3.借阅记录；4.销毁记录', '应急管理档案中组织机构、工作制度、应急预案、上报备案、应急演练、应急救援、协议文书等，不符合要求，1处扣0.5分', 2, NULL, 1, 30, 0, 1);
INSERT INTO `stdchk_item` VALUES (373, 1, 303, '调度和地面设施/调度基础工作/组织机构', '11.1.1(1)', '1.有调度指挥部门，岗位职责明确', '查资料。无调度指挥部门不得分，岗位职责不明确1处扣0.5分', '1.查矿是否有调度部门机构设置正式文件；2.查机构和岗位人员职责是否明确', '1.机构文件或机构设置情况的说明材料；2.人员任命文件；3.各岗位责任制', '1.无调度指挥部门，不得分；2.岗位职责不明确，1处扣0.5分', 2, NULL, 1, 164, 0, 1);
INSERT INTO `stdchk_item` VALUES (374, 2, 303, '调度和地面设施/调度基础工作/组织机构', '11.1.1(2)', '2.每天24h专人值守，每班工作人员满足调度工作要求', '查现场。人员配备不足或无值守人员不得分', '1.查现场是否有人值守；2.查调度员值班安排表，人员能否满足24h值班需要', NULL, '人员配备不足或无值守人员，不得分', 2, NULL, 1, 164, 0, 1);
INSERT INTO `stdchk_item` VALUES (375, 1, 304, '调度和地面设施/调度基础工作/管理制度', '11.1.2(1)', '制定并严格执行岗位安全生产责任制、调度值班制度、交接班制度、汇报制度、信息汇总分析制度、调度人员入井（坑）制度、业务学习制度、事故和突发事件信息报告与处理制度、文档管理制度等', '查资料与现场。每缺1项制度扣1分；制度内容不全或未执行，每处扣0.5分', '1.查矿正式文件下发是否有这9项制度；2.查机构和人员配备，制度是否健全；3.对应制度里内容的记录台账，查制度是否执行', '1.9项制度的文件；2.安全生产责任记录、调度值班记录、交接班记录、汇报记录、信息汇总分析记录、调度人员入井记录、业务学习记录、事故和突发事件报告处理记录、文档管理记录等', '1.缺1项制度，扣1分；2.每项制度内容不全或未执行，扣0.5分', 3, NULL, 1, 164, 0, 1);
INSERT INTO `stdchk_item` VALUES (376, 1, 305, '调度和地面设施/调度基础工作/技术支撑', '11.1.3(1)', '备有《煤矿安全规程》规定的图纸、事故报告程序图（表）、矿领导值班、带班安排与统计表、生产计划表、重点工程进度图（表）、矿井灾害预防和处理计划、事故应急救援预案等，图（表）保持最新版本', '查资料。无矿井灾害预防和处理计划、事故应急救援预案不得分；每缺1种图（表）扣1分，图（表）未及时更新1处扣0.5分', '1.查调度室是否有《煤矿安全规程》第十四条规定的11类图纸、值班安排表、年度生产经营计划、月度生产计划、年度采掘衔接计划、事故报告程序图(表)、重点工程进度图(表)、矿井灾害预防和处理计划、事故应急救援预案等;2.查图、表与预案和现场是否相符合,是否及时更新', '图纸、计划、表格、预案等', '1.无矿井灾害预防和处理计划、事故应急救援预案，不得分；2.每缺1种图（表）纸，扣1分；3.图表未更新，扣0.5分', 5, NULL, 1, 164, 0, 1);
INSERT INTO `stdchk_item` VALUES (377, 1, 307, '调度和地面设施/调度管理/组织协调', '11.2.1(1)', '1.掌握生产动态，协调落实生产作业计划，按规定处置生产中出现的各种问题，并准确记录\n2.按规定及时上报安全生产信息，下达安全生产指令并跟踪落实、做好记录。', '查资料。不符合要求1处扣0.5分', '对照生产计划查各种处置台账记录，查看调度员是否按规定协调处置各种问题查调度值班记录、报表、处置跟踪记录，看是否做到上传下达，并跟踪记录', '报表和处置跟踪记录；调度值班记录；处置结果和记录', '缺1项处置或无记录，扣0.5分', 3, NULL, 1, 164, 0, 1);
INSERT INTO `stdchk_item` VALUES (378, 1, 308, '调度和地面设施/调度管理/应急处置', '11.2.2(1)', '出现险情或发生事故时，及时下达撤人指令、报告事故信息，按程序启动事故应急预案，跟踪现场处置情况并做好记录', '查资料。未授权调度员遇险情下达撤人调度指令、发现1次没有在出现险情下达撤人指令或未按程序启动事故应急预案或未及时跟踪现场处置情况不得分，记录不规范1处扣0.5分', '\n1.查是否有有关授权书；2.查安全监控系统、电话录音等有无险情（如瓦斯超限超过20min等），是否做到应急处置，是否有记录\n', '记录本、电话录音、授权书', '1.未授权调度员遇险情下达撤人调度指令、发现1次在出现险情时没有下达撤人指令或未按程序启动事故应急预案或未及时跟踪现场处置情况，不得分；2.记录不准确或无录音，扣0.5分', 2, NULL, 1, 164, 0, 1);
INSERT INTO `stdchk_item` VALUES (379, 1, 309, '调度和地面设施/调度管理/深入现场', '11.2.3(1)', '按规定深入现场，了解安全生产情况', '查资料。每缺1人次深入现场扣1分', '查调度制度规定和人员定位系统与人员下井记录对比，看是否一致', '调度制度、人员下井记录', '1.未按规定下井缺1人次，扣1分；2.记录不准确，扣0.5分', 2, NULL, 1, 164, 0, 1);
INSERT INTO `stdchk_item` VALUES (380, 1, 310, '调度和地面设施/调度管理/调度记录', '11.2.4(1)', '1.值班记录整洁、清晰，完整、无涂改', '查现场。不符合要求1处扣0.5分', '查原始纸质记录与调度员值班记录对比', '原始纸质记录、调度员值班记录、电话录音', '记录不全或缺内容扣1分；涂改扣0.5分', 4, NULL, 1, 164, 0, 1);
INSERT INTO `stdchk_item` VALUES (381, 2, 310, '调度和地面设施/调度管理/调度记录', '11.2.4(2)', '2.有调度值班、交接班及安全生产情况统计等台账（记录）', '查资料。无台账（记录）的不得分；台账（记录）内容不完整、数据不准确1处扣0.5分', '查调度综合原始台账和调度员记录对比', '综合原始台账、调度员记录、电话录音', '1.无台账（记录），扣2分；2.台账（记录）内容不完整、数据不准确，1处扣0.5分', 2, NULL, 1, 164, 0, 1);
INSERT INTO `stdchk_item` VALUES (382, 3, 310, '调度和地面设施/调度管理/调度记录', '11.2.4(3)', '3.有产、运、销、存的统计台账（运、销、存企业集中管理的除外），内容齐全，记录规范', '查资料。无台账（记录）的不得分；台账（记录）内容不完整、数据不准确1处扣0.5分', '1.查产、运、销、存台账和有关记录是否有涂改，记录是否规范；2.产量在线监控系统和产、运、销、存台账对照，检查记录是否齐全、准确', '产运销存台账', '1.无台账，扣2分；2.篡改产量监控数据，扣1分；3.内容不完整、数据不准确，1处扣0.5分', 2, NULL, 1, 164, 0, 1);
INSERT INTO `stdchk_item` VALUES (383, 1, 311, '调度和地面设施/调度管理/调度汇报', '11.2.5(1)', '1.每班调度汇总有关安全生产信息', '查资料。抽查1个月相关记录。缺少或内容不全，每1处扣0.5分', '调阅安全部门安全生产信息，抽查1个月记录', '安全部门安全生产信息记录', '缺少或内容不全，每1处扣0.5', 2, NULL, 1, 164, 0, 1);
INSERT INTO `stdchk_item` VALUES (384, 2, 311, '调度和地面设施/调度管理/调度汇报', '11.2.5(2)', '2.按规定上报调度安全生产信息日报表、旬（周）、月调度安全生产信息统计表、矿领导值班带班情况统计表', '查资料。不符合要求1处扣1分', '1.查调度日、旬、月报及矿领导带班统计表上报记录；2.对照人员定位系统查看矿领导带班等内容是否属实', '1.调度日、旬、月报；2.矿领导带班统计表上报记录', '1.缺1种报表或统计表，扣1分；2.领导带班制度不落实，1人扣1分', 6, NULL, 1, 164, 0, 1);
INSERT INTO `stdchk_item` VALUES (385, 1, 312, '调度和地面设施/调度管理/雨季“三防”', '11.2.6(1)', '组织落实雨季“三防”相关工作，并做好记录', '查资料和现场。1处不符合要求不得分', '1.查煤矿是否有雨季“三防”文件、调度室会议纪要、具体分工文件，调度室是否有雨季“三防”专有施工记录、排水系统的联合试运转报告；2.现场检查是否有相关措施痕迹', '雨季“三防”文件、调度室会议纪要、分工文件、专有施工记录、排水系统联合试运转报告', '无文件、纪要落实雨季“三防”工作安排，扣2分', 2, NULL, 1, 164, 0, 1);
INSERT INTO `stdchk_item` VALUES (386, 1, 314, '调度和地面设施/调度信息化/通信装备', '11.3.1(1)', '1.装备调度通信系统，与主要硐室、生产场所（露天矿为无线通信系统）、应急救援单位、医院(井口保健站、急救站)、应急物资仓库及上级部门实现有线直拨', '查现场和资料。不符合要求1处扣0.5分', '现场查是否有调度通信系统，是否能够正常使用', NULL, '1.电话直拨地点，缺1处扣0.5分；2.电话不能正常使用，扣0.5分', 4, NULL, 1, 164, 0, 1);
INSERT INTO `stdchk_item` VALUES (387, 2, 314, '调度和地面设施/调度信息化/通信装备', '11.3.1(2)', '2. 有线调度通信系统有选呼、急呼、全呼、强插、强拆、录音等功能。调度工作台电话录音保存时间不少于3个月', '查现场和资料。不符合要求1处扣0.5分', '1.到调度室试用电话各功能，查电话录音及保存时间；2.查电话录音记录', '通话录音记录', '1.无调度通信系统扣4分；2.功能不全，1处扣0.5分；3.保存时间不足3个月，扣0.5分', 4, NULL, 1, 164, 0, 1);
INSERT INTO `stdchk_item` VALUES (388, 3, 314, '调度和地面设施/调度信息化/通信装备', '11.3.1(3)', '3.按《煤矿安全规程》规定装备与重要工作场所直通的有线调度电话', '查现场和资料。不符合要求1处扣0.5分', '1.查各有关重点生产单位和重点机房有无直通电话（拿起来几秒就通，不需要拨号）；2.查电话本，看是否覆盖所有重要场所，重要场所指《煤矿安全规程》第五百零七条列出的地点', '电话本', '1.1处没有直通，扣0.5分；2.1处不能正常通话，扣0.5分', 4, NULL, 1, 164, 0, 1);
INSERT INTO `stdchk_item` VALUES (389, 1, 315, '调度和地面设施/调度信息化/监控系统', '11.3.2(1)', '1.跟踪安全监控系统有关参数变化情况，掌握矿井安全生产状态', '查现场和资料。不符合要求1处扣0.5分', '现场检查调度员的工作记录和安全监控系统参数变化记录，看是否进行跟踪；是否掌握矿井安全生产状态', '调度员工作记录', '不符合要求1处扣0.5分', 2, NULL, 1, 164, 0, 1);
INSERT INTO `stdchk_item` VALUES (390, 2, 315, '调度和地面设施/调度信息化/监控系统', '11.3.2(2)', '2.及时核实、处置系统预（报）警情况并做好记录', '查现场和资料。有1项预（报）警未处置扣0.5分', '1.查系统预警处理记录，是否及时核实、处置、记录；2.现场查是否有监视员，与调度员沟通是否畅通', '1.系统预警记录；2.调度员工作记录', '有1项预（报）警未处置扣0.5分', 4, NULL, 1, 164, 0, 1);
INSERT INTO `stdchk_item` VALUES (391, 1, 316, '调度和地面设施/调度信息化/人员位置监测', '11.3.3(1)', '装备井下人员位置监测系统，准确显示井下总人数、人员时空分布情况，具有数据存储查询功能。矿调度室值班员监视人员位置等信息，填写运行日志', '查现场和资料。无系统或运行不正常、无数据存储查询功能不得分，数据不准确1处扣0.5分，未正常填写运行日志1次扣0.5分', '1.现场查看有无人员位置监测系统,运行是否正常,能否调取历史数据;2.查井口人员下井公示人数,井下外来人员是否配备定位卡,核定下井准确人数,并与机器显示人数核对；3.查是否有调度员填写的运行日志;现场检查是否有人监视', '运行日志', '1.无系统或运行不正常、无数据存储功能,不得分;2.功能不全,扣1分;3.读卡器数量不足或人员时空分布不准确,扣0.5分；4.未正常填写运行日志,1次扣0.5分\n\n', 4, NULL, 1, 164, 0, 1);
INSERT INTO `stdchk_item` VALUES (392, 1, 317, '调度和地面设施/调度信息化/图像监视', '11.3.4(1)', '矿调度室设置图像监视系统的终端显示装置，并实现信息的存储和查询', '查现场和资料。调度室无显示装置扣1分，显示装置运行不正常、存储或查询功能不全1处扣0.5分', '1.查现场是否有调度视频终端；2.现场操作是否实现信息查询，能否查阅历史数据；查是否有历史视频记录', '历史信息记录', '1.未按规定装设显示装置，扣1分；2.显示装置运行不正常、存储或查询功能不全1处扣0.5分；3.图像不清晰，建议改善', 2, NULL, 1, 164, 0, 1);
INSERT INTO `stdchk_item` VALUES (393, 1, 318, '调度和地面设施/调度信息化/信息管理系统', '11.3.5(1)', '采用信息化手段对调度报表、生产安全事故统计表等数据进行处理，实现对煤矿安全生产信息跟踪、管理、预警、存储和传输功能', '查现场和资料。无管理信息系统或系统功能不全、运行不正常不得分；其他1处不符合要求扣0.5分', '1.查是否有调度信息管理系统；2.查系统运行是否正常，功能是否齐全', NULL, '1.无系统或系统不能正常运行，不得分；2.功能不全，1处扣0.5分', 3, NULL, 1, 164, 0, 1);
INSERT INTO `stdchk_item` VALUES (394, 1, 320, '调度和地面设施/岗位规范/专业技能', '11.4.1(1)', '1.具备煤矿安全生产相关专业知识、掌握岗位相关知识；2.人员经培训合格', '查资料和现场。不符合要求1处扣0.5分', '1.现场随机查管理技术人员和普通岗位人员,随机抽1个人回答3个问题,如果回答全部不正确,视为不符合要求;提问调度专业关键岗位人员1个最关键问题,如果回答不正确,视为不符合要求;2.查调度人员培训后是否有合格证', '资格合格证', '不符合要求1处扣0.5分', 2, NULL, 1, 164, 0, 1);
INSERT INTO `stdchk_item` VALUES (395, 1, 321, '调度和地面设施/岗位规范/规范作业', '11.4.2(1)', '1.严格执行岗位安全生产责任制；2.无“三违”行为', '查现场。发现“三违”不得分，不执行岗位责任制1人次扣0.5分', '1.按照正常程序现场操作，查看操作是否正确；2.检查过程中是否有“三违”行为', NULL, '1.发现“三违”行为不得分；2.未执行岗位责任制，1人次扣0.5分', 2, NULL, 1, 164, 0, 1);
INSERT INTO `stdchk_item` VALUES (396, 1, 323, '调度和地面设施/文明生产/文明办公', '11.5.1(1)', '1.设备、设施安装符合规定\n2.图纸、资料、文件、牌板及工作场所清洁整齐、置物有序', '查现场和资料。不符合要求1处扣0.5分', '查现场,查图纸,查电话、调度台、终端显示、人员定位、安全监测、通信、消防设施等调度设备设施安装是否符合规定；查现场是否清洁整齐、置物是否有序', '施工图纸、调度室设备台账资料', '不符合要求1处扣0.5分', 2, NULL, 1, 164, 0, 1);
INSERT INTO `stdchk_item` VALUES (397, 1, 325, '调度和地面设施/地面办公场所/办公室', '11.6.1(1)', '办公室配置满足工作需要，办公设施齐全、完好', '查现场。不符合要求1处扣0.5分', '查各有关办公室现场，配置是否满足要求，是否设施齐全、完好', NULL, '不符合要求1处扣0.5分', 1, NULL, 1, 164, 0, 1);
INSERT INTO `stdchk_item` VALUES (398, 1, 326, '调度和地面设施/地面办公场所/会议室', '11.6.2(1)', '配置有会议室，设施齐全、完好', '查现场。不符合要求1处扣0.5分', '查现场是否有会议室，设施是否齐全、完好及容纳人数是否满足要求', NULL, '不符合要求1处扣0.5分', 1, NULL, 1, 164, 0, 1);
INSERT INTO `stdchk_item` VALUES (399, 1, 328, '调度和地面设施/两堂一舍/职工食堂', '11.7.1(1)', '1.基础设施齐全、完好，满足高峰和特殊时段职工就餐需要；2.符合卫生标准要求，工作人员按要求持健康证上岗', '查资料和现场。基础设施不齐全扣1分，不符合卫生标准扣3分，未持证上岗的1人扣1分，不能满足就餐需求不得分', '1.检查用餐时段食堂基础设施是否明显不够；2.检查是否有卫生食品经营许可证;3.检查食堂人员是否有健康证\n', '食品经营许可证、工作人员健康证等', '1.基础设施不齐全,扣1分；2.不符合卫生标准(没有食品经营许可证),扣3分；3.未持证上岗的,1人扣1分;\n否有健康证；4.明显不能满足就餐需求,不得分', 5, NULL, 1, 164, 0, 1);
INSERT INTO `stdchk_item` VALUES (400, 1, 329, '调度和地面设施/两堂一舍/职工澡堂', '11.7.2(1)', '1.职工澡堂设计合理，满足职工洗浴要求；2.设有更衣室、浴室、厕所和值班室，设施齐全完好，有防滑、防寒、防烫等安全防护设施', '查记录和现场。不能满足职工洗浴要求或脏乱的不得分，基础设施不全每缺1处扣1分，安全防护设施每缺1处扣1分', '1.现场查设计是否合理,能否满足职工不能满足职工洗浴要需求,房间设置及设施是否齐全,设施是否完好，是否有安全防护设施；2.查现场环境是否脏乱', NULL, '1.不能满足职工洗浴要求或脏乱的,不得分；2.基础设施不全,每缺1处扣1分;安全防护设施每缺1处扣1分', 8, NULL, 1, 164, 0, 1);
INSERT INTO `stdchk_item` VALUES (401, 1, 330, '调度和地面设施/两堂一舍/职工宿舍及洗衣房', '11.7.3(1)', '1.职工宿舍布局合理，人均面积不少于5㎡；2.室内整洁，设施齐全、完好,物品摆放有序；3.洗衣房设施齐全（洗、烘、熨），洗衣房、卫生间符合《工业企业设计卫生标准》的要求', '查记录和现场。职工宿舍不能满足人均面积5m2及以上、室内脏乱的不得分，其他不符合要求1处扣1分', '抽查3~5间职工宿舍，面积、卫生、设施情况是否符合要求', '1.管理制度；2.工业企业设计卫生标准', '职工宿舍不能满足人均面积5m²及以上、室内脏乱的不得分，其他不符合要求1处扣1分', 7, NULL, 1, 164, 0, 1);
INSERT INTO `stdchk_item` VALUES (402, 1, 332, '调度和地面设施/工业广场/工业广场', '11.8.1(1)', '1.工业广场设计符合规定要求，布局合理，工作区与生活区分区设置；2.物料分类码放整齐；3.煤仓及储煤场储煤能力满足煤矿生产能力要求；4.停车场规划合理、划线分区，车辆按规定停放整齐，照明符合要求', '查资料和现场。不符合要求1处扣0.5分', '1.现场查广场是否布局合理,工作区生活区是否分区;物料是否分类码放整齐储煤能力是否满足生产需要2.现场检查停车场是否划线分区,各种车辆是否按规定停整齐,照明是否符合要求', '广场设计', '不符合要求1处扣0.5分', 2, NULL, 1, 164, 0, 1);
INSERT INTO `stdchk_item` VALUES (403, 1, 333, '调度和地面设施/工业广场/工业道路', '11.8.2(1)', '工业道路应符合《厂矿道路设计规范》的要求,道路布局合理，实施硬化处理', '查现场。不符合要求1处扣0.5分', '现场检查道路是否符合规范要求,布局是否合理,是否硬化处理', NULL, '不符合要求1处扣0.5分', 2, NULL, 1, 164, 0, 1);
INSERT INTO `stdchk_item` VALUES (404, 1, 334, '调度和地面设施/工业广场/环境卫生', '11.8.3(1)', '1.依条件实施绿化；2.厕所规模和数量适当、位置合理，设施完好有效，符合相应的卫生标准；3.每天对储煤场、场内运煤道路进行整理、清洁，洒水降尘', '查现场。不符合要求1处扣0.5分', '现场查看广场是否绿化,厕所是否卫生，是否对储煤场、场内分运煤道路进行整理、清洁,洒水降尘\n', NULL, '不符合要求的1处扣0.5分', 2, NULL, 1, 164, 0, 1);
INSERT INTO `stdchk_item` VALUES (405, 1, 336, '调度和地面设施/地面设备材料库/设备库房', '11.9.1(1)', '1.仓储配套设备设施齐全、完好；2.不同性能的材料分区或专库存放并采取相应的防护措施；3.货架布局合理，实行定置管理', '查资料和现场。不符合要求1处扣0.5分', '1.对照台账，现场检查仓储配套设备设施是否齐全、完好；不同材料是否分区或是否专库存放并采取防护措施；2.查库房是否实行定置管理；查货架码放高度和齐整度是否符合制度要求', '仓储设备台账、材料台账', '1.无消防设施、无安全防护措施，1处扣1分；2.其他1处不符合要求扣0.5分', 2, NULL, 1, 164, 0, 1);

-- ----------------------------
-- Table structure for stdchk_item_defcooperator
-- ----------------------------
DROP TABLE IF EXISTS `stdchk_item_defcooperator`;
CREATE TABLE `stdchk_item_defcooperator`  (
  `item_id` int(11) NOT NULL COMMENT '标准化检查条目ID',
  `user_id` int(11) NOT NULL COMMENT '配合人员ID',
  PRIMARY KEY (`item_id`, `user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '记录标准化检查条目默认的配合人员' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of stdchk_item_defcooperator
-- ----------------------------
INSERT INTO `stdchk_item_defcooperator` VALUES (1, 75);
INSERT INTO `stdchk_item_defcooperator` VALUES (1, 81);
INSERT INTO `stdchk_item_defcooperator` VALUES (1, 87);
INSERT INTO `stdchk_item_defcooperator` VALUES (1, 97);
INSERT INTO `stdchk_item_defcooperator` VALUES (1, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (1, 1903);
INSERT INTO `stdchk_item_defcooperator` VALUES (1, 1904);
INSERT INTO `stdchk_item_defcooperator` VALUES (2, 23);
INSERT INTO `stdchk_item_defcooperator` VALUES (2, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (2, 75);
INSERT INTO `stdchk_item_defcooperator` VALUES (2, 81);
INSERT INTO `stdchk_item_defcooperator` VALUES (2, 87);
INSERT INTO `stdchk_item_defcooperator` VALUES (2, 97);
INSERT INTO `stdchk_item_defcooperator` VALUES (2, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (2, 1903);
INSERT INTO `stdchk_item_defcooperator` VALUES (2, 1904);
INSERT INTO `stdchk_item_defcooperator` VALUES (3, 23);
INSERT INTO `stdchk_item_defcooperator` VALUES (3, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (3, 75);
INSERT INTO `stdchk_item_defcooperator` VALUES (3, 81);
INSERT INTO `stdchk_item_defcooperator` VALUES (3, 87);
INSERT INTO `stdchk_item_defcooperator` VALUES (3, 97);
INSERT INTO `stdchk_item_defcooperator` VALUES (3, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (3, 1903);
INSERT INTO `stdchk_item_defcooperator` VALUES (3, 1904);
INSERT INTO `stdchk_item_defcooperator` VALUES (4, 23);
INSERT INTO `stdchk_item_defcooperator` VALUES (4, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (4, 75);
INSERT INTO `stdchk_item_defcooperator` VALUES (4, 81);
INSERT INTO `stdchk_item_defcooperator` VALUES (4, 87);
INSERT INTO `stdchk_item_defcooperator` VALUES (4, 97);
INSERT INTO `stdchk_item_defcooperator` VALUES (4, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (4, 1903);
INSERT INTO `stdchk_item_defcooperator` VALUES (4, 1904);
INSERT INTO `stdchk_item_defcooperator` VALUES (5, 23);
INSERT INTO `stdchk_item_defcooperator` VALUES (5, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (5, 75);
INSERT INTO `stdchk_item_defcooperator` VALUES (5, 81);
INSERT INTO `stdchk_item_defcooperator` VALUES (5, 87);
INSERT INTO `stdchk_item_defcooperator` VALUES (5, 97);
INSERT INTO `stdchk_item_defcooperator` VALUES (5, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (5, 1903);
INSERT INTO `stdchk_item_defcooperator` VALUES (5, 1904);
INSERT INTO `stdchk_item_defcooperator` VALUES (6, 23);
INSERT INTO `stdchk_item_defcooperator` VALUES (6, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (6, 75);
INSERT INTO `stdchk_item_defcooperator` VALUES (6, 81);
INSERT INTO `stdchk_item_defcooperator` VALUES (6, 87);
INSERT INTO `stdchk_item_defcooperator` VALUES (6, 97);
INSERT INTO `stdchk_item_defcooperator` VALUES (6, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (6, 1903);
INSERT INTO `stdchk_item_defcooperator` VALUES (6, 1904);
INSERT INTO `stdchk_item_defcooperator` VALUES (7, 23);
INSERT INTO `stdchk_item_defcooperator` VALUES (7, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (7, 75);
INSERT INTO `stdchk_item_defcooperator` VALUES (7, 81);
INSERT INTO `stdchk_item_defcooperator` VALUES (7, 87);
INSERT INTO `stdchk_item_defcooperator` VALUES (7, 97);
INSERT INTO `stdchk_item_defcooperator` VALUES (7, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (7, 1903);
INSERT INTO `stdchk_item_defcooperator` VALUES (7, 1904);
INSERT INTO `stdchk_item_defcooperator` VALUES (8, 23);
INSERT INTO `stdchk_item_defcooperator` VALUES (8, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (8, 75);
INSERT INTO `stdchk_item_defcooperator` VALUES (8, 81);
INSERT INTO `stdchk_item_defcooperator` VALUES (8, 87);
INSERT INTO `stdchk_item_defcooperator` VALUES (8, 97);
INSERT INTO `stdchk_item_defcooperator` VALUES (8, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (8, 1903);
INSERT INTO `stdchk_item_defcooperator` VALUES (8, 1904);
INSERT INTO `stdchk_item_defcooperator` VALUES (9, 23);
INSERT INTO `stdchk_item_defcooperator` VALUES (9, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (9, 75);
INSERT INTO `stdchk_item_defcooperator` VALUES (9, 81);
INSERT INTO `stdchk_item_defcooperator` VALUES (9, 87);
INSERT INTO `stdchk_item_defcooperator` VALUES (9, 97);
INSERT INTO `stdchk_item_defcooperator` VALUES (9, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (9, 1903);
INSERT INTO `stdchk_item_defcooperator` VALUES (9, 1904);
INSERT INTO `stdchk_item_defcooperator` VALUES (10, 23);
INSERT INTO `stdchk_item_defcooperator` VALUES (10, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (10, 75);
INSERT INTO `stdchk_item_defcooperator` VALUES (10, 81);
INSERT INTO `stdchk_item_defcooperator` VALUES (10, 87);
INSERT INTO `stdchk_item_defcooperator` VALUES (10, 97);
INSERT INTO `stdchk_item_defcooperator` VALUES (10, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (10, 1903);
INSERT INTO `stdchk_item_defcooperator` VALUES (10, 1904);
INSERT INTO `stdchk_item_defcooperator` VALUES (325, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (325, 95);
INSERT INTO `stdchk_item_defcooperator` VALUES (325, 1904);
INSERT INTO `stdchk_item_defcooperator` VALUES (326, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (327, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (328, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (329, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (330, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (331, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (332, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (333, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (334, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (335, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (336, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (337, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (338, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (339, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (340, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (341, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (342, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (343, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (344, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (345, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (346, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (347, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (348, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (349, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (350, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (351, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (352, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (353, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (354, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (355, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (356, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (357, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (358, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (359, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (360, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (361, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (362, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (363, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (364, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (365, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (366, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (367, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (368, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (369, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (370, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (371, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (372, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (373, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (373, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (374, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (374, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (375, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (375, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (376, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (376, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (377, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (377, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (378, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (378, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (379, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (379, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (380, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (380, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (381, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (381, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (382, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (382, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (383, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (383, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (384, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (384, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (385, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (385, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (386, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (386, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (387, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (387, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (388, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (388, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (389, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (389, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (390, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (390, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (391, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (391, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (392, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (392, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (393, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (393, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (394, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (394, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (395, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (395, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (396, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (396, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (397, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (397, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (398, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (398, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (399, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (399, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (400, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (400, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (401, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (401, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (402, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (402, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (403, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (403, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (404, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (404, 164);
INSERT INTO `stdchk_item_defcooperator` VALUES (405, 30);
INSERT INTO `stdchk_item_defcooperator` VALUES (405, 164);

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父菜单ID，一级菜单为0',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单URL',
  `perms` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
  `type` int(11) DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单图标',
  `order_num` int(11) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 52 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, 0, '系统管理', '/index', 'index', 0, 'layui-icon-set', 0);
INSERT INTO `sys_menu` VALUES (31, 1, '用户管理', '/sysuser/index', 'sysuser/index', 1, 'layui-icon-username', 0);
INSERT INTO `sys_menu` VALUES (32, 31, '添加用户', '/sysuser/add', 'sysuser/add', 0, '', 0);
INSERT INTO `sys_menu` VALUES (33, 31, '修改用户', '/sysuser/edit', 'sysuser/edit', 0, '', 0);
INSERT INTO `sys_menu` VALUES (34, 31, '删除用户', '/sysuser/delete', 'sysuser/delete', 0, '', 0);
INSERT INTO `sys_menu` VALUES (35, 31, '重置密码', '/sysuser/reset', 'sysuser/reset', 0, '', 0);
INSERT INTO `sys_menu` VALUES (36, 48, '标准化检查条目', '/stdchkitem/index', 'stdchkitem/index', 0, '', 0);
INSERT INTO `sys_menu` VALUES (37, 48, '查看在线人员', '/mgr/unfreeze', '/mgr/unfreeze', 0, 'layui-icon-carousel', 0);
INSERT INTO `sys_menu` VALUES (38, 36, '标准化检查条目添加', '/stdchkitem/add', 'stdchkitem/indexadd', 0, '', 0);
INSERT INTO `sys_menu` VALUES (39, 1, '角色管理', '/sysrole/index', 'sysrole/index', 1, 'layui-icon-group', 0);
INSERT INTO `sys_menu` VALUES (40, 39, '添加角色', '/sysrole/add', 'sysrole/add', 0, '', 0);
INSERT INTO `sys_menu` VALUES (41, 39, '修改角色', '/sysrole/edit', 'sysrole/edit', 0, '', 0);
INSERT INTO `sys_menu` VALUES (42, 39, '删除角色', '/sysrole/delete', 'sysrole/delete', 0, '', 0);
INSERT INTO `sys_menu` VALUES (43, 36, '标准化检查条目编辑', '/stdchkitem/edit', 'stdchkitem/edit', 0, '', 0);
INSERT INTO `sys_menu` VALUES (44, 1, '菜单管理', '/sysmenu/index', 'sysmenu/index', 1, 'layui-icon-set-fill', 0);
INSERT INTO `sys_menu` VALUES (45, 44, '添加菜单', '/sysmenu/add', 'sysmenu/add', 0, '', 0);
INSERT INTO `sys_menu` VALUES (46, 44, '修改菜单', '/sysmenu/edit', 'sysmenu/edit', 0, '', 0);
INSERT INTO `sys_menu` VALUES (47, 44, '删除菜单', '/sysmenu/delete', 'sysmenu/delete', 0, '', 0);
INSERT INTO `sys_menu` VALUES (48, 0, '业务日志', '/log', '/log', 1, 'layui-icon-engine', 0);
INSERT INTO `sys_menu` VALUES (51, 37, 'ASF', '阿斯蒂芬', 'ASF', 1, 'ASF', 0);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者ID',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, 'admin', 'admin', 1, NULL);
INSERT INTO `sys_role` VALUES (2, '是大法官', '是大法官', 0, NULL);
INSERT INTO `sys_role` VALUES (3, '为', '娃儿', 1, NULL);
INSERT INTO `sys_role` VALUES (6, '12', '12', 1, NULL);
INSERT INTO `sys_role` VALUES (7, '收费', 'ASF', 1, NULL);
INSERT INTO `sys_role` VALUES (9, '收费', 'ASF', 1, NULL);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  `menu_id` bigint(20) DEFAULT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 204 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色与菜单对应关系' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (64, 2, 1);
INSERT INTO `sys_role_menu` VALUES (65, 2, 31);
INSERT INTO `sys_role_menu` VALUES (66, 2, 32);
INSERT INTO `sys_role_menu` VALUES (67, 2, 33);
INSERT INTO `sys_role_menu` VALUES (68, 2, 34);
INSERT INTO `sys_role_menu` VALUES (69, 2, 35);
INSERT INTO `sys_role_menu` VALUES (70, 2, 36);
INSERT INTO `sys_role_menu` VALUES (71, 2, 39);
INSERT INTO `sys_role_menu` VALUES (72, 2, 38);
INSERT INTO `sys_role_menu` VALUES (73, 2, 40);
INSERT INTO `sys_role_menu` VALUES (74, 2, 41);
INSERT INTO `sys_role_menu` VALUES (75, 2, 42);
INSERT INTO `sys_role_menu` VALUES (76, 2, 43);
INSERT INTO `sys_role_menu` VALUES (77, 2, 44);
INSERT INTO `sys_role_menu` VALUES (78, 2, 45);
INSERT INTO `sys_role_menu` VALUES (79, 2, 46);
INSERT INTO `sys_role_menu` VALUES (80, 2, 47);
INSERT INTO `sys_role_menu` VALUES (86, 6, 1);
INSERT INTO `sys_role_menu` VALUES (87, 6, 31);
INSERT INTO `sys_role_menu` VALUES (88, 6, 32);
INSERT INTO `sys_role_menu` VALUES (89, 6, 33);
INSERT INTO `sys_role_menu` VALUES (90, 6, 34);
INSERT INTO `sys_role_menu` VALUES (91, 6, 35);
INSERT INTO `sys_role_menu` VALUES (92, 6, 36);
INSERT INTO `sys_role_menu` VALUES (93, 6, 39);
INSERT INTO `sys_role_menu` VALUES (94, 6, 38);
INSERT INTO `sys_role_menu` VALUES (95, 6, 40);
INSERT INTO `sys_role_menu` VALUES (96, 6, 41);
INSERT INTO `sys_role_menu` VALUES (97, 6, 42);
INSERT INTO `sys_role_menu` VALUES (98, 6, 43);
INSERT INTO `sys_role_menu` VALUES (99, 6, 44);
INSERT INTO `sys_role_menu` VALUES (100, 6, 45);
INSERT INTO `sys_role_menu` VALUES (101, 6, 46);
INSERT INTO `sys_role_menu` VALUES (102, 6, 47);
INSERT INTO `sys_role_menu` VALUES (103, 6, 48);
INSERT INTO `sys_role_menu` VALUES (104, 6, 37);
INSERT INTO `sys_role_menu` VALUES (105, 6, 51);
INSERT INTO `sys_role_menu` VALUES (140, 1, 1);
INSERT INTO `sys_role_menu` VALUES (141, 1, 31);
INSERT INTO `sys_role_menu` VALUES (142, 1, 32);
INSERT INTO `sys_role_menu` VALUES (143, 1, 33);
INSERT INTO `sys_role_menu` VALUES (144, 1, 34);
INSERT INTO `sys_role_menu` VALUES (145, 1, 35);
INSERT INTO `sys_role_menu` VALUES (146, 1, 39);
INSERT INTO `sys_role_menu` VALUES (147, 1, 41);
INSERT INTO `sys_role_menu` VALUES (148, 1, 44);
INSERT INTO `sys_role_menu` VALUES (149, 1, 45);
INSERT INTO `sys_role_menu` VALUES (150, 1, 46);
INSERT INTO `sys_role_menu` VALUES (151, 1, 47);
INSERT INTO `sys_role_menu` VALUES (152, 1, 36);
INSERT INTO `sys_role_menu` VALUES (153, 1, 37);
INSERT INTO `sys_role_menu` VALUES (154, 1, 51);
INSERT INTO `sys_role_menu` VALUES (155, 1, 38);
INSERT INTO `sys_role_menu` VALUES (156, 1, 43);
INSERT INTO `sys_role_menu` VALUES (157, 7, 1);
INSERT INTO `sys_role_menu` VALUES (158, 7, 31);
INSERT INTO `sys_role_menu` VALUES (159, 7, 32);
INSERT INTO `sys_role_menu` VALUES (160, 7, 33);
INSERT INTO `sys_role_menu` VALUES (161, 7, 34);
INSERT INTO `sys_role_menu` VALUES (162, 7, 35);
INSERT INTO `sys_role_menu` VALUES (163, 7, 39);
INSERT INTO `sys_role_menu` VALUES (164, 7, 40);
INSERT INTO `sys_role_menu` VALUES (165, 7, 41);
INSERT INTO `sys_role_menu` VALUES (166, 7, 42);
INSERT INTO `sys_role_menu` VALUES (167, 7, 44);
INSERT INTO `sys_role_menu` VALUES (168, 7, 45);
INSERT INTO `sys_role_menu` VALUES (169, 7, 46);
INSERT INTO `sys_role_menu` VALUES (170, 7, 47);
INSERT INTO `sys_role_menu` VALUES (171, 7, 48);
INSERT INTO `sys_role_menu` VALUES (172, 7, 36);
INSERT INTO `sys_role_menu` VALUES (173, 7, 38);
INSERT INTO `sys_role_menu` VALUES (174, 7, 43);
INSERT INTO `sys_role_menu` VALUES (175, 7, 37);
INSERT INTO `sys_role_menu` VALUES (176, 7, 51);
INSERT INTO `sys_role_menu` VALUES (177, 9, 1);
INSERT INTO `sys_role_menu` VALUES (178, 9, 31);
INSERT INTO `sys_role_menu` VALUES (179, 9, 32);
INSERT INTO `sys_role_menu` VALUES (180, 9, 33);
INSERT INTO `sys_role_menu` VALUES (181, 9, 34);
INSERT INTO `sys_role_menu` VALUES (182, 9, 35);
INSERT INTO `sys_role_menu` VALUES (183, 9, 39);
INSERT INTO `sys_role_menu` VALUES (184, 9, 40);
INSERT INTO `sys_role_menu` VALUES (185, 9, 41);
INSERT INTO `sys_role_menu` VALUES (186, 9, 42);
INSERT INTO `sys_role_menu` VALUES (187, 9, 44);
INSERT INTO `sys_role_menu` VALUES (188, 9, 45);
INSERT INTO `sys_role_menu` VALUES (189, 9, 46);
INSERT INTO `sys_role_menu` VALUES (190, 9, 47);
INSERT INTO `sys_role_menu` VALUES (191, 9, 36);
INSERT INTO `sys_role_menu` VALUES (192, 9, 38);
INSERT INTO `sys_role_menu` VALUES (193, 9, 43);
INSERT INTO `sys_role_menu` VALUES (194, 9, 37);
INSERT INTO `sys_role_menu` VALUES (195, 9, 51);
INSERT INTO `sys_role_menu` VALUES (196, 3, 39);
INSERT INTO `sys_role_menu` VALUES (197, 3, 40);
INSERT INTO `sys_role_menu` VALUES (198, 3, 41);
INSERT INTO `sys_role_menu` VALUES (199, 3, 42);
INSERT INTO `sys_role_menu` VALUES (200, 3, 44);
INSERT INTO `sys_role_menu` VALUES (201, 3, 45);
INSERT INTO `sys_role_menu` VALUES (202, 3, 46);
INSERT INTO `sys_role_menu` VALUES (203, 3, 47);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '密码',
  `salt` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '盐',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '手机号',
  `sex` tinyint(2) DEFAULT NULL COMMENT '性别',
  `pic` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '图像',
  `status` tinyint(4) DEFAULT NULL COMMENT '状态  0：禁用   1：正常',
  `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者ID',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `remark` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统用户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'admin', '3743a4c09a17e6f2829febd09ca54e627810001cf255ddcae9dabd288a949c4a', '123', '儿童医院', '18729036400', 1, 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1563792443051&di=33020e781cc439b4367caa8c10c66128&imgtype=0&src=http%3A%2F%2Fb-ssl.duitang.com%2Fuploads%2Fitem%2F201506%2F09%2F20150609224048_RCQPf.jpeg', 1, 1, '2019-01-18 11:11:11', '儿童医院');
INSERT INTO `sys_user` VALUES (5, '是大法官', NULL, '123', '是大法官', '是大法官', 1, NULL, 1, 1, '2019-01-18 11:11:11', '是大法官');
INSERT INTO `sys_user` VALUES (6, 'ASF', 'e8d5b653e99321f1a53637aaab69d9ed08e3c28ce7eb6b2206d443bf5110ed8e', '123', '啊', '啊', 0, NULL, 1, 1, '2019-07-28 08:06:12', '阿斯蒂芬');
INSERT INTO `sys_user` VALUES (7, '123', NULL, '123', '阿萨德', '阿斯蒂芬', 1, NULL, 1, 1, '2019-08-01 01:35:01', '阿斯蒂芬');
INSERT INTO `sys_user` VALUES (11, '1234', 'e8d5b653e99321f1a53637aaab69d9ed08e3c28ce7eb6b2206d443bf5110ed8e', '123', '阿萨德', '阿斯蒂芬', 1, NULL, 1, 1, '2019-08-01 01:39:37', '阿斯蒂芬');
INSERT INTO `sys_user` VALUES (15, '1', 'e8d5b653e99321f1a53637aaab69d9ed08e3c28ce7eb6b2206d443bf5110ed8e', '123', '阿斯蒂芬', 'ASF', 1, NULL, 1, 1, '2019-08-01 01:43:37', '阿斯蒂芬');
INSERT INTO `sys_user` VALUES (16, '2', 'e8d5b653e99321f1a53637aaab69d9ed08e3c28ce7eb6b2206d443bf5110ed8e', '123', '阿斯蒂芬', 'ASF', 1, NULL, 1, 1, '2019-08-01 01:43:39', '阿斯蒂芬');
INSERT INTO `sys_user` VALUES (17, '3', 'e8d5b653e99321f1a53637aaab69d9ed08e3c28ce7eb6b2206d443bf5110ed8e', '123', '阿斯蒂芬', 'ASF', 1, NULL, 1, 1, '2019-08-01 01:43:41', '阿斯蒂芬');
INSERT INTO `sys_user` VALUES (18, '4', 'e8d5b653e99321f1a53637aaab69d9ed08e3c28ce7eb6b2206d443bf5110ed8e', '123', '阿斯蒂芬', 'ASF', 1, NULL, 1, 1, '2019-08-01 01:43:42', '阿斯蒂芬');
INSERT INTO `sys_user` VALUES (19, '5', 'e8d5b653e99321f1a53637aaab69d9ed08e3c28ce7eb6b2206d443bf5110ed8e', '123', '阿斯蒂芬', 'ASF', 1, NULL, 1, 1, '2019-08-01 01:43:44', '阿斯蒂芬');
INSERT INTO `sys_user` VALUES (20, '6', 'e8d5b653e99321f1a53637aaab69d9ed08e3c28ce7eb6b2206d443bf5110ed8e', '123', '阿斯蒂芬', 'ASF', 1, NULL, 1, 1, '2019-08-01 01:43:45', '阿斯蒂芬');
INSERT INTO `sys_user` VALUES (21, '7', 'e8d5b653e99321f1a53637aaab69d9ed08e3c28ce7eb6b2206d443bf5110ed8e', '123', '阿斯蒂芬', 'ASF', 1, NULL, 1, 1, '2019-08-01 01:43:48', '阿斯蒂芬');
INSERT INTO `sys_user` VALUES (22, '8', 'e8d5b653e99321f1a53637aaab69d9ed08e3c28ce7eb6b2206d443bf5110ed8e', '123', '阿斯蒂芬', 'ASF', 1, NULL, 1, 1, '2019-08-01 01:43:51', '阿斯蒂芬');
INSERT INTO `sys_user` VALUES (24, '9', 'e8d5b653e99321f1a53637aaab69d9ed08e3c28ce7eb6b2206d443bf5110ed8e', '123', '阿斯蒂芬', 'ASF', 1, NULL, 1, 1, '2019-08-01 01:43:55', '阿斯蒂芬');
INSERT INTO `sys_user` VALUES (25, '10', 'e8d5b653e99321f1a53637aaab69d9ed08e3c28ce7eb6b2206d443bf5110ed8e', '123', '阿斯蒂芬', 'ASF', 1, NULL, 1, 1, '2019-08-01 01:43:57', '阿斯蒂芬');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 67 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户与角色对应关系' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (8, 0, 1);
INSERT INTO `sys_user_role` VALUES (9, 0, 2);
INSERT INTO `sys_user_role` VALUES (50, 5, 1);
INSERT INTO `sys_user_role` VALUES (51, 5, 2);
INSERT INTO `sys_user_role` VALUES (53, 6, 1);
INSERT INTO `sys_user_role` VALUES (54, 1, 1);
INSERT INTO `sys_user_role` VALUES (55, 7, 1);
INSERT INTO `sys_user_role` VALUES (56, 11, 1);
INSERT INTO `sys_user_role` VALUES (57, 15, 1);
INSERT INTO `sys_user_role` VALUES (58, 16, 1);
INSERT INTO `sys_user_role` VALUES (59, 17, 1);
INSERT INTO `sys_user_role` VALUES (60, 18, 1);
INSERT INTO `sys_user_role` VALUES (61, 19, 1);
INSERT INTO `sys_user_role` VALUES (62, 20, 1);
INSERT INTO `sys_user_role` VALUES (63, 21, 1);
INSERT INTO `sys_user_role` VALUES (64, 22, 1);
INSERT INTO `sys_user_role` VALUES (65, 24, 1);
INSERT INTO `sys_user_role` VALUES (66, 25, 1);

SET FOREIGN_KEY_CHECKS = 1;
