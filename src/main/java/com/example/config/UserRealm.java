package com.example.config;

import com.example.entity.SysMenu;
import com.example.entity.SysRole;
import com.example.entity.SysUser;
import com.example.mapper.SysMenuMapper;
import com.example.mapper.SysRoleMapper;
import com.example.mapper.SysUserMapper;
import com.example.utils.ShiroUtils;
import com.example.utils.TreeBuider;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.*;

@Component
public class UserRealm extends AuthorizingRealm {
    @Resource
    private SysUserMapper sysUserMapper;
    @Resource
    private SysMenuMapper sysMenuMapper;
    @Resource
    private SysRoleMapper sysRoleMapper;

    /**
     * 授权(验证权限时调用)
     * 获取用户权限集合
     */
    @Override
    public AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        SysUser user = (SysUser) principals.getPrimaryPrincipal();
        if (user == null) {
            throw new UnknownAccountException("账号不存在");
        }
        List<String> permsList;
        //默认用户拥有最高权限
        List<SysMenu> menuList = new ArrayList<>();
        List<SysRole> roleList = new ArrayList<>();
        if ("admin".equals(user.getUsername())) {
            menuList = sysMenuMapper.selectList(null);
            roleList = sysRoleMapper.selectList(null);
        } else {
            menuList = sysMenuMapper.selectListByUserId(user.getId());
            roleList = sysRoleMapper.selectListByUserId(user.getId());
        }
        permsList = new ArrayList<>(menuList.size());
        for (SysMenu menu : menuList) {
            permsList.add(menu.getPerms());
        }
        //用户权限列表
        Set<String> permsSet = new HashSet<>();
        for (String perms : permsList) {
            if (StringUtils.isEmpty(perms)) {
                continue;
            }
            permsSet.addAll(Arrays.asList(perms.trim().split(",")));
        }
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        info.setStringPermissions(permsSet);

        menuList = TreeBuider.buildByRecursive(menuList);
        SecurityUtils.getSubject().getSession().setAttribute("menus", menuList);
        SecurityUtils.getSubject().getSession().setAttribute("roles", roleList);
        return info;
    }

    /**
     * 认证(登录时调用)
     * 验证用户登录
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authToken) throws AuthenticationException {
        UsernamePasswordToken token = (UsernamePasswordToken) authToken;
        //查询用户信息
        SysUser user = sysUserMapper.selectOne(token.getUsername());
        //账号不存在
        if (user == null) {
            throw new UnknownAccountException("账号不存在！");
        }
        String password=user.getPassword();
        Object passwordToken=token.getCredentials();
        String passwordShiro=ShiroUtils.sha256(new String((char[]) passwordToken),user.getSalt());
        if (!password.equals(passwordShiro)) {
            throw new AccountException("密码不正确");
        }
        //账号锁定
        if (user.getStatus() == 0) {
            throw new LockedAccountException("账号已被锁定,请联系管理员");
        }
        SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(user, user.getPassword(), ByteSource.Util.bytes(user.getSalt()), getName());
        List<SysMenu> menuList = new ArrayList<>();
        List<SysRole> roleList = new ArrayList<>();
        if ("admin".equals(user.getUsername())) {
            menuList = sysMenuMapper.selectList(null);
            roleList = sysRoleMapper.selectList(null);
        } else {
            menuList = sysMenuMapper.selectListByUserId(user.getId());
            roleList = sysRoleMapper.selectListByUserId(user.getId());
        }
        menuList = TreeBuider.buildByRecursive(menuList);
        SecurityUtils.getSubject().getSession().setAttribute("menus", menuList);
        SecurityUtils.getSubject().getSession().setAttribute("roles", roleList);
        return info;
    }

    @Override
    public void setCredentialsMatcher(CredentialsMatcher credentialsMatcher) {
        HashedCredentialsMatcher shaCredentialsMatcher = new HashedCredentialsMatcher();
        shaCredentialsMatcher.setHashAlgorithmName(ShiroUtils.hashAlgorithmName);
        shaCredentialsMatcher.setHashIterations(ShiroUtils.hashIterations);
        super.setCredentialsMatcher(shaCredentialsMatcher);
    }
}