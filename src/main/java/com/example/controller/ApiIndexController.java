package com.example.controller;

import cn.hutool.json.JSONObject;
import com.example.mapper.SysMenuMapper;
import com.example.mapper.SysUserMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
@RequestMapping("/apiindex")
public class ApiIndexController {

    @Resource
    private SysUserMapper sysUserMapper;
    @Resource
    private SysMenuMapper sysMenuMapper;

    @RequestMapping("/index1")
    @ResponseBody
    public JSONObject index(HttpServletRequest request, Map<String, Object> map) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code", 200);
        jsonObject.put("msg", "微信");
        return jsonObject;
    }

    @RequestMapping("/index2")
    @ResponseBody
    public JSONObject index2(HttpServletRequest request, Map<String, Object> map) {
        JSONObject jsonObject = new JSONObject();
        Map<String, String[]> parameterMap = request.getParameterMap();

        jsonObject.put("code", 200);
        jsonObject.put("data", parameterMap);
        jsonObject.put("msg", "微信");
        return jsonObject;
    }
}
