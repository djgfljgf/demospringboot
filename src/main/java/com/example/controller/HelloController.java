package com.example.controller;

import cn.hutool.db.Db;
import cn.hutool.db.Entity;
import cn.hutool.extra.emoji.EmojiUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.example.entity.SysUser;
import com.example.mapper.SysMenuMapper;
import com.example.mapper.SysUserMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/index")
public class HelloController {

    @Resource
    private SysUserMapper sysUserMapper;

    @Resource
    private SysMenuMapper sysMenuMapper;

    @RequestMapping("/test")
    public String test() {
        return "阿拉山口京东方哈路上看见对方哈里斯京东方";
    }

    @ResponseBody
    @RequestMapping("/view")
    public JSONObject view() {
        JSONObject jsonObject = new JSONObject();
        //String a = "我是一个小小的可爱的字符串";
        //
        ////结果为："\\u6211\\u662f\\u4e00\\u4e2a\\u5c0f\\u5c0f\\u7684\\u53ef\\u7231\\u7684\\u5b57\\u7b26\\u4e32"
        //String unicode = Convert.strToUnicode(a);
        //
        ////结果为："我是一个小小的可爱的字符串"
        //String raw = Convert.unicodeToStr(unicode);
        //
        //
        ////当前时间
        //Date date = DateUtil.date();
        ////当前时间
        //Date date2 = DateUtil.date(Calendar.getInstance());
        ////当前时间
        //Date date3 = DateUtil.date(System.currentTimeMillis());
        ////当前时间字符串，格式：yyyy-MM-dd HH:mm:ss
        //String now = DateUtil.now();
        ////当前日期字符串，格式：yyyy-MM-dd
        //String today= DateUtil.today();
        //String dateStr = "2017-03-01 22:33:23";
        //Date date = DateUtil.parse(dateStr);
        //
        ////一天的开始，结果：2017-03-01 00:00:00
        //Date beginOfDay = DateUtil.beginOfDay(date);
        //
        ////一天的结束，结果：2017-03-01 23:59:59
        //Date endOfDay = DateUtil.endOfDay(date);
        //生成的UUID是带-的字符串，类似于：a5c8a5e8-df2b-4706-bea4-08d0939410e3
        //String uuid = IdUtil.randomUUID();
        //
        ////生成的是不带-的字符串，类似于：b17f24ff026d40949c85a24f4f375d42
        //String simpleUUID = IdUtil.simpleUUID();
        //String[] a = {"abc", "bcd", "def"};
        //Console.log(a);//控制台输出：[abc, bcd, def]
        //boolean isEmail = Validator.isEmail("loolly@gmail.com");

        //Img.from(FileUtil.file("d:/3.png"))
        //        .setQuality(0.1)//压缩比率
        //        .write(FileUtil.file("d:/3target.jpg"));

        //String jsonStr = "{\"b\":\"value2\",\"c\":\"value3\",\"a\":\"value1\"}";
        ////方法一：使用工具类转换
        //JSONObject jsonObject = JSONUtil.parseObj(jsonStr);
        ////方法二：new的方式转换
        //JSONObject jsonObject2 = new JSONObject(jsonStr);
        //
        ////JSON对象转字符串
        //jsonObject.toString();

        //String jsonStr = "[\"value1\", \"value2\", \"value3\"]";
        //JSONArray array = JSONUtil.parseArray(jsonStr);

        //user为表名  数据库操作
        //try {
        //    final List<Entity> sysuser = Db.use().findAll("sysuser");
        //    Console.log(sysuser);
        //} catch (SQLException e) {
        //    e.printStackTrace();
        //}

        //http操作
        //GET请求
        //String content = HttpUtil.get("http://localhost:8080/index/json");
        //System.out.println(content);

        //POST请求
        //HashMap<String, Object> paramMap = new HashMap<>();
        //paramMap.put("city", "北京");
        //
        //String result1 = HttpUtil.post("http://localhost:8080/index/json", paramMap);
        //System.out.println(result1);

        Map<String, Object> map = new HashMap<>();
        map.put("msg", "成功过");

        try {
            final List<Entity> userList = Db.use().findAll("sysuser");
            map.put("userList", userList);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        SysUser sysUser = new SysUser();
        sysUser.setId(0L);
        sysUser.setUsername("");
        sysUser.setPassword("");
        sysUser.setSalt("");
        sysUser.setEmail("");
        sysUser.setMobile("");
        sysUser.setStatus(0L);
        sysUser.setCreateUserId(0L);
        sysUser.setCreateTime(new Timestamp(new java.util.Date().getTime()));

        map.put("user", sysUser);

        final String s = JSONUtil.parseObj(map).toString();
        String result1 = HttpUtil.post("http://localhost:8080/index/json", s);
        System.out.println(result1);
        jsonObject.put("code", 200);
        jsonObject.put("msg", result1);
        jsonObject.put("data", map);
        return jsonObject;

    }

    //这种参数默认map 必须是map对象
    @RequestMapping("/json")
    public ModelAndView json(HttpServletRequest request, @RequestBody Map<String, Object> jsonmap) {
        //这种参数默认map 必须是json、格式的字符串
        //String s = (String) map.get("sysuser");

        //String userList = request.getParameter("userList");
        //JSONArray userList = JSONUtil.parseArray(map.get("userList"));
        //List<User> users = userList.toList(User.class);
        ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());

        mv.addObject("loginUrl", "www.baidu.com");
        mv.addObject("1", "阿卡就是的发挥");
        mv.addObject("name", jsonmap.toString());
        return mv;
    }

    //这种参数默认map 必须是json、格式的字符串
    /*@RequestMapping("/json")
    public ModelAndView json(HttpServletRequest request, @RequestBody Map<String, Object> map) {
        //这种参数默认map 必须是json、格式的字符串
        //String s = (String) map.get("sysuser");
        JSONObject sysuser = JSONUtil.parseObj(map.get("sysuser"));
        User user2 = sysuser.toBean(User.class);

        //String userList = request.getParameter("userList");
        JSONArray userList = JSONUtil.parseArray(map.get("userList"));
        List<User> users = userList.toList(User.class);
        ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());

        mv.addObject("loginUrl", "www.baidu.com");
        mv.addObject("1", "阿卡就是的发挥");
        mv.addObject("name", users.toString());
        return mv;
    }*/

    @RequestMapping("/user")
    public ModelAndView openUserList(ModelAndView modelAndView) {
        modelAndView.setViewName("sysuser");

        //List<SysUser> userList = SysUserMapper.selectList(null);
        //modelAndView.addObject("userList", userList);
        return modelAndView;
    }

    @RequestMapping("/mail")
    public String mail() {
        //MailUtil.send("393978119@qq.com", "测试", "邮件来自Hutool测试", false);
        //QrCodeUtil.generate("https://hutool.cn/", 300, 300, FileUtil.file("d:/qrcode.jpg"));
        String alias = EmojiUtil.toAlias("😄");//:smile:

        return "123" + alias;
    }

}