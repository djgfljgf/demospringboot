package com.example.controller;

import com.alibaba.fastjson.JSON;
import com.example.entity.SysMenu;
import com.example.entity.SysUser;
import com.example.mapper.SysUserMapper;
import com.example.utils.ShiroUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.List;

@Controller
public class IndexController extends BaseController {

    @Resource
    private SysUserMapper sysUserMapper;

    @RequestMapping("/")
    //@RequiresPermissions("index")
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView("index");
        Subject subject = ShiroUtils.getSubject();
        SysUser sysUser = (SysUser) subject.getPrincipal();

        List<SysMenu> menus = (List<SysMenu>) SecurityUtils.getSubject().getSession().getAttribute("menus");
        List<SysMenu> roles = (List<SysMenu>) SecurityUtils.getSubject().getSession().getAttribute("roles");

        modelAndView.addObject("menus", menus);
        modelAndView.addObject("roles", roles);
        modelAndView.addObject("sysUser", (sysUser));
        modelAndView.addObject("sysuser", JSON.toJSONString(sysUser));
        return modelAndView;
    }

    @RequestMapping("/welcome")
    //@RequiresPermissions("index/welcome")
    public ModelAndView welcome() {
        ModelAndView modelAndView = new ModelAndView("page/welcome");
        Subject subject = ShiroUtils.getSubject();
        SysUser sysUser = (SysUser) subject.getPrincipal();
        List<SysMenu> roles = (List<SysMenu>) SecurityUtils.getSubject().getSession().getAttribute("roles");
        modelAndView.addObject("roles", roles);
        modelAndView.addObject("sysuser", sysUser);
        return modelAndView;
    }

}
