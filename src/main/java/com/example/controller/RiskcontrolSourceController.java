package com.example.controller;

import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.entity.RiskcontrolCategory;
import com.example.entity.RiskcontrolSource;
import com.example.entity.SysUser;
import com.example.mapper.RiskcontrolCategoryMapper;
import com.example.mapper.RiskcontrolSourceMapper;
import com.example.utils.ShiroUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/risksource")
public class RiskcontrolSourceController {

    @Resource
    private RiskcontrolSourceMapper riskcontrolSourceMapper;
    @Resource
    private RiskcontrolCategoryMapper riskcontrolCategoryMapper;

    @RequestMapping("/index")
    public ModelAndView index(HttpServletRequest request) {

        Subject subject = ShiroUtils.getSubject();
        SysUser sysUser = (SysUser) subject.getPrincipal();
        String method = request.getMethod();
        if ("POST".equals(method)) {
            ModelAndView modelAndView = new ModelAndView(new MappingJackson2JsonView());

            Integer pageNo = Integer.valueOf(request.getParameter("page"));
            Integer pageSize = Integer.valueOf(request.getParameter("pageSize"));
            Page<Map> page = new Page<Map>(pageNo, pageSize);
            QueryWrapper<Map<String, Object>> wrapper = new QueryWrapper<>();

            IPage<Map<String, Object>> stdchkItems = riskcontrolSourceMapper.selectListAndCatgoryPage(page);

            //List<Map<String,Object>> stdchkItems = stdchkItemMapper.selectListAndCatgory();
            modelAndView.addObject("data", stdchkItems);
            modelAndView.addObject("sysuser", sysUser);
            return modelAndView;
        } else {
            ModelAndView modelAndView = new ModelAndView("page/risksource/risksource");
            modelAndView.addObject("sysuser", sysUser);
            return modelAndView;
        }
    }

    @RequestMapping("/importFile")
    //@RequiresPermissions("sysmenu/edit")
    public ModelAndView importFile(HttpServletRequest request, @RequestParam(value = "file") MultipartFile file) throws InterruptedException {
        Subject subject = ShiroUtils.getSubject();
        SysUser sysUser = (SysUser) subject.getPrincipal();
        String method = request.getMethod();
        if ("POST".equals(method)) {
            ModelAndView modelAndView = new ModelAndView(new MappingJackson2JsonView());

            //判断文件是否为空
            if (file == null) {
                return null;
            }
            //获取文件名
            String name = file.getOriginalFilename();
            //进一步判断文件是否为空（即判断其大小是否为0或其名称是否为null）
            long size = file.getSize();
            if (name == null || ("").equals(name) && size == 0) return null;

            //批量导入。参数：文件名，文件。
            try {
                ExcelReader reader = ExcelUtil.getReader((file.getInputStream()));
                List<Map<String, Object>> readAll = reader.readAll();

                long typeId = 0;
                for (Map<String, Object> stringObjectMap : readAll) {
                    String type = stringObjectMap.get("类型")!=null?stringObjectMap.get("类型").toString():"";
                    String content =  stringObjectMap.get("风险源内容")!=null?stringObjectMap.get("风险源内容").toString():"";
                    String measure =  stringObjectMap.get("管控措施")!=null?stringObjectMap.get("管控措施").toString():"";
                    String possible =  stringObjectMap.get("可能性")!=null?stringObjectMap.get("可能性").toString():"";
                    String rate =  stringObjectMap.get("暴露率")!=null?stringObjectMap.get("暴露率").toString():"";
                    String consequence =  stringObjectMap.get("后果")!=null?stringObjectMap.get("后果").toString():"";
                    String riskvalue = stringObjectMap.get("风险值")!=null?stringObjectMap.get("风险值").toString():"";
                    String level = stringObjectMap.get("程度编号")!=null?stringObjectMap.get("程度编号").toString():"";
                    //System.out.println(name1);

                    if (type != null&&type!="") {
                        RiskcontrolCategory riskcontrolCategory = new RiskcontrolCategory();
                        riskcontrolCategory.setName(type);
                        riskcontrolCategory.setDeleted(0);
                        riskcontrolCategoryMapper.insert(riskcontrolCategory);
                        typeId = riskcontrolCategory.getId();
                    } else {

                        RiskcontrolSource riskcontrolSource = new RiskcontrolSource();
                        riskcontrolSource.setCategoryId(typeId);
                        riskcontrolSource.setDescription(content);
                        //riskcontrolSource.setNumber(0L);
                        riskcontrolSource.setLevelId(0);
                        riskcontrolSource.setMeasures(measure);
                        riskcontrolSource.setEnabled(1);
                        riskcontrolSource.setDeleted(0);
                        riskcontrolSource.setFlags(0);
                        riskcontrolSource.setPossibility(Long.parseLong(possible));
                        riskcontrolSource.setExposureRate(Long.parseLong(rate));
                        riskcontrolSource.setConsequences(Long.parseLong(consequence));
                        riskcontrolSource.setRiskValue(Long.parseLong(riskvalue));
                        //riskcontrolSource.setRiskFlag(0L);

                        riskcontrolSourceMapper.insert(riskcontrolSource);
                    }

                }
                modelAndView.addObject("code", 200);
                modelAndView.addObject("msg", "成功！");
            } catch (IOException e) {
                e.printStackTrace();
                modelAndView.addObject("code", 0);
                modelAndView.addObject("msg", e.getMessage());
            }

            return modelAndView;
        } else {
            ModelAndView modelAndView = new ModelAndView("page/risksource/risksource_import");
            modelAndView.addObject("sysUser", sysUser);
            return modelAndView;
        }
    }
}
