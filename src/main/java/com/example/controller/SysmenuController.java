package com.example.controller;

import com.alibaba.fastjson.JSON;
import com.example.entity.SysMenu;
import com.example.entity.SysUser;
import com.example.mapper.SysMenuMapper;
import com.example.mapper.SysUserMapper;
import com.example.utils.ShiroUtils;
import com.example.utils.TreeBuider;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/sysmenu")
public class SysmenuController extends BaseController {

    @Resource
    private SysUserMapper sysUserMapper;

    @Resource
    private SysMenuMapper sysMenuMapper;

    @RequestMapping("/index")
    //@RequiresPermissions("sysmenu/index")
    public ModelAndView index(HttpServletRequest request) {
        Subject subject = ShiroUtils.getSubject();
        SysUser sysUser = (SysUser) subject.getPrincipal();
        String method = request.getMethod();
        if ("POST".equals(method)) {
            ModelAndView modelAndView = new ModelAndView(new MappingJackson2JsonView());
            List<SysMenu> menus = sysMenuMapper.selectList(null);
            modelAndView.addObject("data", menus);
            modelAndView.addObject("menuscount", menus.size());
            modelAndView.addObject("sysuser", sysUser);
            return modelAndView;
        } else {
            ModelAndView modelAndView = new ModelAndView("page/sysmenu/menu");
            modelAndView.addObject("sysuser", sysUser);
            return modelAndView;
        }

    }

    @RequestMapping("/add")
    //@RequiresPermissions("sysmenu/edit")
    public ModelAndView add(HttpServletRequest request, SysMenu sysMenu) throws InterruptedException {
        Subject subject = ShiroUtils.getSubject();
        SysUser sysUser = (SysUser) subject.getPrincipal();
        String method = request.getMethod();
        if ("POST".equals(method)) {
            ModelAndView modelAndView = new ModelAndView(new MappingJackson2JsonView());
            sysMenuMapper.insert(sysMenu);
            modelAndView.addObject("sysUser", sysUser);
            modelAndView.addObject("code", 200);
            modelAndView.addObject("msg", "成功！");
            return modelAndView;
        } else {
            String id = request.getParameter("id");
            ModelAndView modelAndView = new ModelAndView("page/sysmenu/menu_add");
            SysMenu sysMenuTemp = sysMenuMapper.selectById(id);
            modelAndView.addObject("sysMenu", sysMenuTemp);
            List<SysMenu> menuList = sysMenuMapper.selectList(null);
            menuList = TreeBuider.buildByRecursive(menuList);
            modelAndView.addObject("menuList", JSON.toJSONString(menuList));
            modelAndView.addObject("sysUser", sysUser);
            return modelAndView;
        }
    }

    @RequestMapping("/edit")
    //@RequiresPermissions("sysmenu/edit")
    public ModelAndView edit(HttpServletRequest request, SysMenu sysMenu) throws InterruptedException {
        Subject subject = ShiroUtils.getSubject();
        SysUser sysUser = (SysUser) subject.getPrincipal();
        String method = request.getMethod();
        if ("POST".equals(method)) {
            ModelAndView modelAndView = new ModelAndView(new MappingJackson2JsonView());
            sysMenuMapper.updateById(sysMenu);
            modelAndView.addObject("sysUser", sysUser);
            modelAndView.addObject("code", 200);
            modelAndView.addObject("msg", "成功！");
            return modelAndView;
        } else {
            String id = request.getParameter("id");
            ModelAndView modelAndView = new ModelAndView("page/sysmenu/menu_edit");
            SysMenu sysMenuTemp = sysMenuMapper.selectById(id);
            modelAndView.addObject("sysMenu", sysMenuTemp);
            List<SysMenu> menuList = sysMenuMapper.selectList(null);
            menuList = TreeBuider.buildByRecursive(menuList);
            modelAndView.addObject("menuList", JSON.toJSONString(menuList));
            modelAndView.addObject("sysUser", sysUser);
            return modelAndView;
        }
    }

    @RequestMapping("/delete")
    //@RequiresPermissions("sysmenu/edit")
    public ModelAndView delete(HttpServletRequest request, SysMenu sysMenu) throws InterruptedException {
        Subject subject = ShiroUtils.getSubject();
        SysUser sysUser = (SysUser) subject.getPrincipal();
        String method = request.getMethod();
        String id = request.getParameter("id");
        ModelAndView modelAndView = new ModelAndView(new MappingJackson2JsonView());
        sysMenuMapper.deleteById(id);
        modelAndView.addObject("sysUser", sysUser);
        modelAndView.addObject("code", 200);
        modelAndView.addObject("msg", "成功！");
        return modelAndView;
    }

    @RequestMapping("/widgetMenu")
    @ResponseBody
    //@RequiresPermissions("sysmenu/edit")
    public ModelAndView widgetMenu(HttpServletRequest request) throws InterruptedException {
        Subject subject = ShiroUtils.getSubject();
        SysUser sysUser = (SysUser) subject.getPrincipal();
        ModelAndView modelAndView = new ModelAndView(new MappingJackson2JsonView());
        List<SysMenu> menuList = sysMenuMapper.selectList(null);
        for (SysMenu sysMenu : menuList) {
            sysMenu.setCheckArr("0");
        }
        menuList = TreeBuider.buildByRecursive(menuList);
        modelAndView.addObject("data", menuList);
        //modelAndView.addObject("code", 0);
        //modelAndView.addObject("msg", 200);
        Map<String, Object> map = new HashMap<>();
        map.put("code", 200);
        map.put("message", "操作成功");
        modelAndView.addObject("status", map);
        //modelAndView.addObject("sysUser", sysUser);
        return modelAndView;
    }

}
