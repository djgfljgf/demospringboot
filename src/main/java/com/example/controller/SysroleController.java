package com.example.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.entity.SysMenu;
import com.example.entity.SysRole;
import com.example.entity.SysRoleMenu;
import com.example.entity.SysUser;
import com.example.mapper.SysMenuMapper;
import com.example.mapper.SysRoleMapper;
import com.example.mapper.SysRoleMenuMapper;
import com.example.utils.ShiroUtils;
import com.example.utils.TreeBuider;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/sysrole")
public class SysroleController extends BaseController {

    @Resource
    private SysRoleMapper sysRoleMapper;
    @Resource
    private SysMenuMapper sysMenuMapper;
    @Resource
    private SysRoleMenuMapper sysRoleMenuMapper;

    @RequestMapping("/index")
    //@RequiresPermissions("/sysrole/index")
    public ModelAndView index(HttpServletRequest request) {
        Subject subject = ShiroUtils.getSubject();
        SysUser sysUser = (SysUser) subject.getPrincipal();
        String method = request.getMethod();
        if ("POST".equals(method)) {
            Integer pageNo = Integer.valueOf(request.getParameter("page"));
            Integer pageSize = Integer.valueOf(request.getParameter("pageSize"));
            Page<Map> page = new Page<>(pageNo, pageSize);
            ModelAndView modelAndView = new ModelAndView(new MappingJackson2JsonView());
            IPage<Map> sysRoles = sysRoleMapper.selectMapsByPage(page,null);
            modelAndView.addObject("data", sysRoles);
            modelAndView.addObject("sysuser", sysUser);
            return modelAndView;
        } else {
            ModelAndView modelAndView = new ModelAndView("page/sysrole/role");
            modelAndView.addObject("sysuser", sysUser);
            return modelAndView;
        }
    }

    @RequestMapping("/add")
    //@RequiresPermissions("sysmenu/add")
    public ModelAndView add(HttpServletRequest request, SysRole sysRole) throws InterruptedException {
        Subject subject = ShiroUtils.getSubject();
        SysUser sysUserLogin = (SysUser) subject.getPrincipal();
        String method = request.getMethod();
        if ("POST".equals(method)) {
            ModelAndView modelAndView = new ModelAndView(new MappingJackson2JsonView());
            sysRole.setCreateUserId(sysUserLogin.getId());
            final int insert = sysRoleMapper.insert(sysRole);
            //System.out.printf("insert后返回的用户id:"+sysRole.getId());
            String[] menuIds = request.getParameterValues("menuIds[]");
            if (menuIds != null) {
                for (String menuId : menuIds) {
                    SysRoleMenu sysRoleMenu = new SysRoleMenu();
                    sysRoleMenu.setRoleId(sysRole.getId());
                    sysRoleMenu.setMenuId(Long.parseLong(menuId));
                    sysRoleMenuMapper.insert(sysRoleMenu);
                }
            }

            modelAndView.addObject("sysUser", sysUserLogin);
            modelAndView.addObject("code", 200);
            modelAndView.addObject("msg", "成功！");
            return modelAndView;
        } else {
            String id = request.getParameter("id");
            ModelAndView modelAndView = new ModelAndView("page/sysrole/role_add");
            SysMenu sysMenuTemp = sysMenuMapper.selectById(id);
            modelAndView.addObject("sysMenu", sysMenuTemp);
            List<SysMenu> menuList = sysMenuMapper.selectList(null);
            menuList = TreeBuider.buildByRecursive(menuList);
            modelAndView.addObject("menuList", JSON.toJSONString(menuList));
            modelAndView.addObject("sysUser", sysUserLogin);
            return modelAndView;
        }
    }

    @RequestMapping("/edit")
    //@RequiresPermissions("sysmenu/edit")
    public ModelAndView edit(HttpServletRequest request, SysRole sysRole) throws InterruptedException {
        Subject subject = ShiroUtils.getSubject();
        SysUser sysUserLogin = (SysUser) subject.getPrincipal();
        String method = request.getMethod();
        if ("POST".equals(method)) {
            String[] menuIds = request.getParameterValues("menuIds[]");
            ModelAndView modelAndView = new ModelAndView(new MappingJackson2JsonView());
            sysRole.setCreateUserId(sysUserLogin.getId());
            sysRoleMapper.updateById(sysRole);
            Map<String, Object> map = new HashMap<>();
            map.put("role_id", sysRole.getId());
            sysRoleMenuMapper.deleteByMap(map);
            if (menuIds != null) {
                for (String menuId : menuIds) {
                    SysRoleMenu sysRoleMenu = new SysRoleMenu();
                    sysRoleMenu.setRoleId(sysRole.getId());
                    sysRoleMenu.setMenuId(Long.parseLong(menuId));
                    sysRoleMenuMapper.insert(sysRoleMenu);
                }
            }
            modelAndView.addObject("sysUser", sysUserLogin);
            modelAndView.addObject("code", 200);
            modelAndView.addObject("msg", "成功！");
            return modelAndView;
        } else {
            String id = request.getParameter("id");
            ModelAndView modelAndView = new ModelAndView("page/sysrole/role_edit");
            SysRole sysRoleTemp = sysRoleMapper.selectById(id);
            modelAndView.addObject("sysRole", sysRoleTemp);
            List<SysMenu> menuList = sysMenuMapper.selectList(null);
            menuList = TreeBuider.buildByRecursive(menuList);
            modelAndView.addObject("menuList", JSON.toJSONString(menuList));
            List<SysMenu> userMenuList = sysMenuMapper.selectListByRoleId(id);
            modelAndView.addObject("userMenuList", JSON.toJSONString(userMenuList, SerializerFeature.WriteMapNullValue));
            return modelAndView;
        }
    }

    @RequestMapping("/delete")
    //@RequiresPermissions("sysmenu/delete")
    public ModelAndView delete(HttpServletRequest request, SysMenu sysMenu) throws InterruptedException {
        Subject subject = ShiroUtils.getSubject();
        SysUser sysUser = (SysUser) subject.getPrincipal();
        String method = request.getMethod();
        String id = request.getParameter("id");
        ModelAndView modelAndView = new ModelAndView(new MappingJackson2JsonView());
        sysRoleMapper.deleteById(id);
        Map<String, Object> map = new HashMap<>();
        map.put("role_id", id);
        sysRoleMenuMapper.deleteByMap(map);

        modelAndView.addObject("sysUser", sysUser);
        modelAndView.addObject("code", 200);
        modelAndView.addObject("msg", "成功！");
        return modelAndView;
    }

}
