package com.example.controller;

import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import com.example.entity.SysUser;
import com.example.mapper.SysMenuMapper;
import com.example.mapper.SysUserMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/weixin")
public class WeixinController {

    @Resource
    private SysUserMapper sysUserMapper;
    @Resource
    private SysMenuMapper sysMenuMapper;

    @RequestMapping("/index")
    @ResponseBody
    public JSONObject index(HttpServletRequest request, Map<String, Object> map) {
        JSONObject jsonObject = new JSONObject();
        HashMap<String, Object> paramMap = new HashMap<>();
        Map<String, String[]> parameterMap = request.getParameterMap();
        for(String key : parameterMap.keySet()){
            String[] value = parameterMap.get(key);
            paramMap.put(key, value[0]);
        }

        String result = HttpUtil.post("http://localhost:8081/springbootdemo/weixin/index2", paramMap);
        List<SysUser> sysTasks = sysUserMapper.selectList(null);
        jsonObject.put("code", 200);
        jsonObject.put("data", sysTasks);
        jsonObject.put("msg", "微信");
        return jsonObject;
    }

    @RequestMapping("/index2")
    @ResponseBody
    public JSONObject index2(HttpServletRequest request, Map<String, Object> map) {
        JSONObject jsonObject = new JSONObject();
        Map<String, String[]> parameterMap = request.getParameterMap();

        jsonObject.put("code", 200);
        jsonObject.put("data", parameterMap);
        jsonObject.put("msg", "微信");
        return jsonObject;
    }
}
