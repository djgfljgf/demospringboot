package com.example.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class RiskcontrolCategory {

  @TableId(value = "id",type = IdType.AUTO)
  private long id;
  private long parentId;
  private String name;
  private long number;
  private long deleted;

}
