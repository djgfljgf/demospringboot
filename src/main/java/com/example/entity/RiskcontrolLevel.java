package com.example.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class RiskcontrolLevel {

  @TableId(value = "id",type = IdType.AUTO)
  private long id;
  private String name;
  private long number;
  private long slight;
  private long fatal;
  private long enabled;
  private long deleted;
  private String remark;

}
