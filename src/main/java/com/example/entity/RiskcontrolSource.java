package com.example.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class RiskcontrolSource {

  @TableId(value = "id",type = IdType.AUTO)
  private long id;
  private long categoryId;
  private String description;
  private long number;
  private long levelId;
  private String measures;
  private long enabled;
  private long deleted;
  private long flags;
  private long possibility;
  private long exposureRate;
  private long consequences;
  private long riskValue;
  private long riskFlag;

}
