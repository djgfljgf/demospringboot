package com.example.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class SysUserRole {
    @TableId
    private long id;
    private long userId;
    private long roleId;

}
