package com.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.entity.RiskcontrolCategory;

public interface RiskcontrolCategoryMapper extends BaseMapper<RiskcontrolCategory> {
}
