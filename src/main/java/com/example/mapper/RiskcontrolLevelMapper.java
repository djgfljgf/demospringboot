package com.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.entity.RiskcontrolLevel;

public interface RiskcontrolLevelMapper extends BaseMapper<RiskcontrolLevel> {
}
