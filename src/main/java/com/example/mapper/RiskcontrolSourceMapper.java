package com.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.entity.RiskcontrolSource;

import java.util.Map;

public interface RiskcontrolSourceMapper extends BaseMapper<RiskcontrolSource> {
    IPage<Map<String, Object>> selectListAndCatgoryPage(Page<Map> page);
}
