package com.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.entity.SysMenu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    List<SysMenu> selectListByUserId(@Param("userid") long userid);

    List<SysMenu> selectListByRoleId(String id);
    //
    //@Select("SELECT * FROM sysuser")
    //    //@Results({@Result(property = "account", column = "account"), @Result(property = "nickname", column = "nickname"), @Result(property = "nickname", column = "nickname")})
    //List<User> getAll();
    //
    //@Select("SELECT * FROM sysuser WHERE id = #{id}")
    //@Results({@Result(property = "userSex", column = "user_sex"), @Result(property = "nickName", column = "nick_name")})
    //User getOne(Long id);
    //
    //@Insert("INSERT INTO users(userName,passWord,user_sex) VALUES(#{userName}, #{passWord}, #{userSex})")
    //int insert(User sysuser);
    //
    //@Update("UPDATE users SET userName=#{userName},nick_name=#{nickName} WHERE id =#{id}")
    //void update(User sysuser);
    //
    //@Delete("DELETE FROM users WHERE id =#{id}")
    //void delete(Long id);


    //List<Map<String,Object>> list=selectByMeasure();

}