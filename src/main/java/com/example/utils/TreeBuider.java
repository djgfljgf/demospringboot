package com.example.utils;

import cn.hutool.db.Db;
import cn.hutool.db.Entity;
import com.example.entity.SysMenu;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TreeBuider {

    /**
     * 两层循环实现建树
     *
     * @param SysMenus 传入的树节点列表
     * @return
     */
    public static List<SysMenu> bulid(List<SysMenu> SysMenus) {

        List<SysMenu> trees = new ArrayList<SysMenu>();

        for (SysMenu SysMenu : SysMenus) {

            if ("0".equals(SysMenu.getParentId())) {
                trees.add(SysMenu);
            }

            for (SysMenu it : SysMenus) {
                if (it.getParentId() == (SysMenu.getId())) {
                    if (SysMenu.getChildren() == null) {
                        SysMenu.setChildren(new ArrayList<SysMenu>());
                    }
                    SysMenu.getChildren().add(it);
                }
            }
        }
        return trees;
    }

    /**
     * 使用递归方法建树
     *
     * @param SysMenus
     * @return
     */
    public static List<SysMenu> buildByRecursive(List<SysMenu> SysMenus) {
        List<SysMenu> trees = new ArrayList<SysMenu>();
        for (SysMenu SysMenu : SysMenus) {
            if (0 == (SysMenu.getParentId())) {
                trees.add(findChildren(SysMenu, SysMenus));
            }
        }
        return trees;
    }

    /**
     * 递归查找子节点
     *
     * @param SysMenus
     * @return
     */
    public static SysMenu findChildren(SysMenu SysMenu, List<SysMenu> SysMenus) {
        for (SysMenu it : SysMenus) {
            if (SysMenu.getId() == (it.getParentId())) {
                if (SysMenu.getChildren() == null) {
                    SysMenu.setChildren(new ArrayList<SysMenu>());
                }
                SysMenu.getChildren().add(findChildren(it, SysMenus));
            }
        }
        return SysMenu;
    }

    /**
     * 组装下拉树的列表数据
     * @param SysMenus
     * @param pid
     * @param itemprefix
     * @return
     */
    public static List<SysMenu> treeSelect(List<SysMenu> SysMenus, String pid, String itemprefix) {
        List<SysMenu> trees = new ArrayList<>();
        String[] icon = {"│", "├", "└"};
        int number = 1;
        for (SysMenu sysMenu : SysMenus) {
            if (pid.equals(sysMenu.getParentId())) {
                int brotherCount = 0;
                //判断当前有多少个兄弟分类
                for (SysMenu menu : SysMenus) {
                    if (pid.equals(menu.getParentId())) {
                        brotherCount++;
                    }
                }
                if (brotherCount > 0) {
                    String j = "", k = "";
                    if (number == brotherCount) {
                        j += icon[2];
                        k = "";
                    } else {
                        j += icon[1];
                        k = icon[0];
                    }
                    String spacer = itemprefix != "" ? itemprefix + j : "";
                    sysMenu.setName(spacer + sysMenu.getName());
                    trees.add(sysMenu);
                    number++;
                    treeSelect(SysMenus, String.valueOf(sysMenu.getId()), itemprefix + k + " ");
                }
            }
        }
        return trees;
    }

    public static void main(String[] args) {

        List<Entity> trees = null;
        try {
            trees = Db.use().findAll("sys_menu");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println(trees);
        List<SysMenu> trees_ = buildByRecursive(null);
        System.out.println(trees_);

    }
}

