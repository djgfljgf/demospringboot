var layui;
layui.use(['form', 'laydate', 'element', 'laypage', 'table', 'layer', 'tree', 'jquery'], function () {
    layui = layui;
    $ = layui.jquery;
    form = layui.form;
    element = layui.element;
    laydate = layui.laydate; //日期插件
    lement = layui.element; //面包导航
    laypage = layui.laypage; //分页
    layer = layui.layer; //弹出层
    table = layui.table;
    tree = layui.tree;
    //
    //  //导航的hover效果、二级菜单等功能，需要依赖element模块
    //   larry-side-menu向左折叠
    //if ($(window).width() < 750) {
    //    trun = 0;
    //} else {
    //    trun = 1;
    //}
    //$('.larry-side-menu').click(function () {
    //    if (trun) {
    //        $('.layui-side-menu').animate({left: '0px'}, 200).siblings('.layadmin-pagetabs').animate({left: '230px'}, 200);
    //        trun = 0;
    //    } else {
    //        $('.layui-side-menu').animate({left: '-250px'}, 200).siblings('.layadmin-pagetabs').animate({left: '0px'}, 200);
    //        trun = 1;
    //    }
    //});
    //$('.layui-side-menu').animate({left: '-200px'}, 150).siblings('.layadmin-pagetabs').animate({left: '0px'}, 200);
    //  //监听导航点击
   /* element.on('nav(layadmin-layout-left)', function (elem) {
        console.log(elem);
        title = elem.find('cite').text();
        url = elem.find('a').attr('_href');
        for (var i = 0; i < $('.x-iframe').length; i++) {
            if ($('.x-iframe').eq(i).attr('src') == url) {
                element.tabChange('x-tab', i);
                return;
            }
        }
        ;
        var id = new Date().getTime();
        res = element.tabAdd('x-tab', {
            id: id,
            title: title, //用于演示
            content: '<iframe frameborder="0" src="' + url + '" class="x-iframe"></iframe>'
            //content:  url
        });
        element.tabChange('x-tab', id);
        $('.layui-tab-title li').eq(0).find('i').remove();
    });*/
});

function addTab(id, title, url) {
    var li = $(".layui-tab-title li[lay-id=" + id + "]").length;
    //console.log(li);
    if (li > 0) {
        //tab已经存在直接切换tab
        layui.element.tabChange('layadmin-layout-tabs', id);
    } else {
        //var indexLoading = layer.load(0, { shade: false }); //0代表加载的风格，支持0-2
        var indexLoading = layer.msg('加载中', {
            icon: 16,
            shade: 0.01
        });
        layui.element.tabAdd('layadmin-layout-tabs', {
            id: id,
            title: title, //用于演示
            autoRefresh: true,
            contextMenu: true,
            content: '<iframe frameborder="0" src="' + url + '" class="layadmin-iframe"></iframe>'
            //content: "<span id='externalHtml" + id + "'></span>"
        });
        //$("#externalHtml" + id).html(str);
        layui.element.tabChange('layadmin-layout-tabs', id);
        layer.close(indexLoading);
        //$.get(url, {}, function (str) {
        //  //创建tab
        //  // console.log(str);
        //  layui.element.tabAdd('layadmin-layout-tabs', {
        //    id: id,
        //    title: title, //用于演示
        //    autoRefresh: true,
        //    contextMenu: true,
        //    //content: '<iframe frameborder="0" src="' + url + '" class="layadmin-iframe"></iframe>'
        //    content: "<span id='externalHtml" + id + "'></span>"
        //  });
        //  $("#externalHtml" + id).html(str);
        //  layui.element.tabChange('layadmin-layout-tabs', id);
        //  layer.close(indexLoading);
        //}).error(function () {
        //  layer.close(indexLoading);
        //  layer.msg('加载出错，请确认服务器返回！');
        //  console.log("error");
        //});
    }
}

$('body').on('dblclick', '.layui-tab-title li', function (elem) {
    var id = $(this).attr("lay-id");
    if (id == "0") {
        layer.msg('首页不能关闭！');
    } else {
        layui.element.tabDelete("layadmin-layout-tabs", $(this).attr("lay-id"));//删除
    }
});
/*弹出层*/

/*
	参数解释：
	title	标题
	url		请求的url
	id		需要操作的数据id
	w		弹出层宽度（缺省调默认值）
	h		弹出层高度（缺省调默认值）
*/
function x_admin_show(title, url, w, h) {
    if (title == null || title == '') {
        title = false;
    }
    if (url == null || url == '') {
        url = "404.html";
    }
    if (w == null || w == '') {
        w = 800;
    }
    if (h == null || h == '') {
        h = ($(window).height() - 50);
    }
    layer.open({
        type: 2,
        offset: 't',
        area: [w + 'px', h + 'px'],
        fix: false, //不固定
        maxmin: true,
        anim: 6,
        shadeClose: true,
        shade: 0.4,
        scrollbar: false,
        zIndex: 999,
        title: title,
        content: url,
        //btn: ['按钮一', '按钮二', '按钮三'],
        yes: function (index, layero) {
            //按钮【按钮一】的回调
            console.log(index);
            console.log(layero);
        },
        btn2: function (index, layero) {
            //按钮【按钮二】的回调
            //return false 开启该代码可禁止点击该按钮关闭
        },
        btn3: function (index, layero) {
            //按钮【按钮三】的回调
            //return false 开启该代码可禁止点击该按钮关闭
        },
        cancel: function () {
            //右上角关闭回调
            //return false 开启该代码可禁止点击该按钮关闭
        }
    });
}
/*关闭弹出框口*/
function x_admin_close() {
    var index = parent.layer.getFrameIndex(window.name);
    parent.layer.close(index);
}

function fillForm(obj, o) {
    var enhance = new enhanceForm({
        elem: obj
    });
    enhance.filling(o);
}

function fullscreen() {
    var ac = 'layui-icon-screen-full', ic = 'layui-icon-screen-restore';
    var ti = $(this).find('i');
    var isFullscreen = document.fullscreenElement || document.msFullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement || false;
    if (isFullscreen) {
        var efs = document.exitFullscreen || document.webkitExitFullscreen || document.mozCancelFullScreen || document.msExitFullscreen;
        if (efs) {
            efs.call(document);
        } else if (window.ActiveXObject) {
            var ws = new ActiveXObject('WScript.Shell');
            ws && ws.SendKeys('{F11}');
        }
        ti.addClass(ac).removeClass(ic);
    } else {
        var el = document.documentElement;
        var rfs = el.requestFullscreen || el.webkitRequestFullscreen || el.mozRequestFullScreen || el.msRequestFullscreen;
        if (rfs) {
            rfs.call(el);
        } else if (window.ActiveXObject) {
            var ws = new ActiveXObject('WScript.Shell');
            ws && ws.SendKeys('{F11}');
        }
        ti.addClass(ic).removeClass(ac);
    }
}

function popupRight(path,title) {
    var index = layer.open({
        area: '336px',
        shadeClose: true,
        title:title,
        //path: path,
        type: 2,
        offset: 'r',
        //shade: 2,
        skin: 'layui-layer-adminRight',
        anim: 5,//0平滑放大默认,1从上掉落,2从最底部往上滑入,3从左滑入,4从左翻滚,5渐显,6抖动
        isOutAnim: false,
        closeBtn: false,
        resize: false,
        //btn: ['提交保存', '取消添加'],
        content: path,
        yes: function (index, layero) {
            var iframeWin = window[layero.find('iframe')[0]['name']];
            if (iframeWin.checkform()) {
            }
        },
        success: function (layero, index) {
            //$(layero).children('.layui-layer-content').load(path);
        },
        end: function () {
            layer.closeAll('tips');
        },
        cancel: function () {
            //右上角关闭回调
            //return false 开启该代码可禁止点击该按钮关闭
        }
    })
}